webpackJsonp([24],{

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventoryFilterPageModule", function() { return InventoryFilterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inventory_filter__ = __webpack_require__(742);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InventoryFilterPageModule = /** @class */ (function () {
    function InventoryFilterPageModule() {
    }
    InventoryFilterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__inventory_filter__["a" /* InventoryFilterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__inventory_filter__["a" /* InventoryFilterPage */]),
            ],
        })
    ], InventoryFilterPageModule);
    return InventoryFilterPageModule;
}());

//# sourceMappingURL=inventory-filter.module.js.map

/***/ }),

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InventoryFilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the InventoryFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InventoryFilterPage = /** @class */ (function () {
    function InventoryFilterPage(navCtrl, navParams, modalController, viewCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalController = modalController;
        this.viewCtrl = viewCtrl;
        this.events = events;
        this.effortValue = 0;
        this.goalProgress = {
            lower: '10',
            upper: '50'
        };
        events.subscribe('closeInventoryModal', function () {
            _this.myDismiss();
        });
    }
    InventoryFilterPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad InventoryFilterPage');
        //this.passedId = this.navParams.get('userId');
        this.typologyList = [
            { "type": "2 BHK", selected: false },
            { "type": "3 BHK", selected: false },
            { "type": "4 BHK", selected: false },
            { "type": "5 BHK", selected: false },
            { "type": "6 BHK", selected: false },
            { "type": "7 BHK", selected: false },
            { "type": "8 BHK", selected: false },
        ];
        this.towerList = [
            { "type": "C1", selected: false },
            { "type": "C2", selected: false },
            { "type": "C3", selected: false },
            { "type": "C4", selected: false },
            { "type": "C5", selected: false },
            { "type": "C6", selected: false },
            { "type": "C7", selected: false },
            { "type": "C8", selected: false },
        ];
        this.facing = [
            { "type": "North", selected: false },
            { "type": "East", selected: false },
            { "type": "West", selected: false },
            { "type": "South", selected: false },
        ];
        this.carpetList = [
            { "type": "900-1100 BHK", selected: false },
            { "type": "1000-1200 BHK", selected: false },
            { "type": "1100-1300 BHK", selected: false },
            { "type": "1200-1400 BHK", selected: false },
        ];
        this.availableList = [
            { "type": "Available", selected: false },
            { "type": "Blocked", selected: false },
            { "type": "Sold Out", selected: false }
        ];
        this.floorList = [
            { "type": "2nd Floor", selected: false },
            { "type": "3rd Floor", selected: false },
            { "type": "4th Floor", selected: false },
            { "type": "5th Floor", selected: false },
            { "type": "6th Floor", selected: false },
            { "type": "7th Floor", selected: false },
            { "type": "8th Floor", selected: false },
            { "type": "9th Floor", selected: false },
            { "type": "10th Floor", selected: false }
        ];
    };
    InventoryFilterPage.prototype.myDismiss = function () {
        this.viewCtrl.dismiss();
    };
    InventoryFilterPage.prototype.change = function () {
        console.log(this.goalProgress);
    };
    InventoryFilterPage.prototype.selectDiv = function (index, type) {
        if (type == "Typology") {
            this.typologyList[index].selected = (this.typologyList[index].selected) ? false : true;
        }
        else if (type == "Tower") {
            this.towerList[index].selected = (this.towerList[index].selected) ? false : true;
        }
        else if (type == "Facing") {
            this.facing[index].selected = (this.facing[index].selected) ? false : true;
        }
        else if (type == "Carpet") {
            this.carpetList[index].selected = (this.carpetList[index].selected) ? false : true;
        }
        else if (type == "Available") {
            this.availableList[index].selected = (this.availableList[index].selected) ? false : true;
        }
        else if (type == "Floor") {
            /* let findIndex=this.floorList.findIndex(x => x.selected == true);
            (findIndex > -1 ) ? this.floorList[findIndex].selected = false : ''; */
            this.floorList[index].selected = (this.floorList[index].selected) ? false : true;
        }
    };
    InventoryFilterPage.prototype.getFilter = function () {
        var typologyArr = this.typologyList.filter(function (x) { return x.selected == true; });
        var towerArr = this.towerList.filter(function (x) { return x.selected == true; });
        var facingArr = this.facing.filter(function (x) { return x.selected == true; });
        var carpetArr = this.carpetList.filter(function (x) { return x.selected == true; });
        var availableArr = this.availableList.filter(function (x) { return x.selected == true; });
        var floor = this.floorList.filter(function (x) { return x.selected == true; });
        var price = this.goalProgress;
        var payloadData = {
            type: typologyArr,
            tower: towerArr,
            facing: facingArr,
            carpet: carpetArr,
            available: availableArr,
            floors: floor,
            price: price
        };
        console.log(payloadData);
        this.myDismiss();
    };
    InventoryFilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-inventory-filter',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/inventory-filter/inventory-filter.html"*/'<!--\n  Generated template for the InventoryFilterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>\n      <button (click)="myDismiss()" class="close_btn">\n        X\n      </button> Property Filter</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<!-- <ion-content padding> -->\n  <ion-content>\n    <ion-list>\n\n      <ion-item>\n          <!-- <div class="chkBox" (click)="selectDiv(i,\'Typology\')" *ngFor="let typo of typologyList; let i=index" >\n            <div class="iconChk" [ngClass]="{\'borderNone\':typo.selected === true }">\n              <ion-icon name="checkbox" class="iconChekBox" [ngClass]="{\'selectedCheckBox\':typo.selected === true }"></ion-icon>\n            </div>\n            <div class="filter_name">{{typo.type}}</div>\n          </div> -->\n          <!-- <div class="checkBoxDiv" (click)="selectDiv(i,\'Typology\')" *ngFor="let typo of typologyList; let i=index" [ngClass]="{\'selected_color\':typo.selected === true }">\n            <div class="filter_name">\n              {{typo.type}}\n              <span class="cross">X</span>\n            </div>\n          </div> -->\n          <div class="filter_type">\n            <img src="assets/imgs/typology.png">\n            Typology\n          </div>\n          <div class="upperDiv">\n            <div class="scrollCustomBtns" (click)="selectDiv(i,\'Typology\')" *ngFor="let typo of typologyList; let i=index" [ngClass]="{\'selected_color\':typo.selected === true }">\n                <div class="filter_name">\n                  {{typo.type}}\n                  <span class="cross">X</span>\n                </div>\n            </div>\n          </div>\n      </ion-item>\n    </ion-list>\n\n    <ion-list>\n\n        <ion-item>\n          <!-- <div class="chkBox" (click)="selectDiv(i,\'Tower\')" *ngFor="let tower of towerList; let i=index" >\n            <div class="iconChk" [ngClass]="{\'borderNone\':tower.selected === true }">\n              <ion-icon name="checkbox" class="iconChekBox" [ngClass]="{\'selectedCheckBox\':tower.selected === true }"></ion-icon>\n            </div>\n            <div class="filter_name">{{tower.type}}</div>\n          </div> -->\n          <!-- <div class="checkBoxDiv" (click)="selectDiv(i,\'Tower\')" *ngFor="let tower of towerList; let i=index" [ngClass]="{\'selected_color\':tower.selected === true }">\n              <div class="filter_name">\n                {{tower.type}}\n                <span class="cross">X</span>\n              </div>\n          </div> -->\n          <div class="filter_type">\n            <img src="assets/imgs/tower-icon.png">\n            Tower\n          </div>\n          <div class="upperDiv">\n            <div class="scrollCustomBtns" (click)="selectDiv(i,\'Tower\')" *ngFor="let tower of towerList; let i=index" [ngClass]="{\'selected_color\':tower.selected === true }">\n                <div class="filter_name">\n                  {{tower.type}}\n                  <span class="cross">X</span>\n                </div>\n            </div>\n          </div>\n        </ion-item>\n    </ion-list>\n\n    <ion-list>\n\n        <ion-item>\n        <!--\n          <div class="chkBox" (click)="selectDiv(i,\'Floor\')" *ngFor="let floor of floorList; let i=index" >\n            <div class="iconChk" [ngClass]="{\'borderNone\':floor.selected === true }">\n              <ion-icon name="checkbox" class="iconChekBox" [ngClass]="{\'selectedCheckBox\':floor.selected === true }"></ion-icon>\n            </div>\n            <div class="filter_name">{{floor.type}}</div>\n          </div>\n         -->\n         <!-- <div class="upperDiv">\n            <div class="floor" [ngClass]="{\'dot\':floor.selected === true }" (click)="selectDiv(i,\'Floor\')" *ngFor="let floor of floorList; let i=index" [ngClass]="{\'selected_floor\':floor.selected === true }">\n                {{floor.type}}\n            </div>\n        </div> -->\n        <div class="filter_type">\n          <img src="assets/imgs/floor-icon.png">\n          Floor\n        </div>\n        <div class="upperDiv">\n          <div class="scrollCustomBtns" (click)="selectDiv(i,\'Floor\')" *ngFor="let floor of floorList; let i=index" [ngClass]="{\'selected_color\':floor.selected === true }">\n            <div class="filter_name">\n              {{floor.type}}\n              <span class="cross">X</span>\n            </div>\n          </div>\n        </div>\n        </ion-item>\n\n    </ion-list>\n\n    <ion-list>\n\n        <ion-item>\n          <!-- <div class="chkBox" (click)="selectDiv(i,\'Facing\')" *ngFor="let face of facing; let i=index" >\n            <div class="iconChk" [ngClass]="{\'borderNone\':face.selected === true }">\n              <ion-icon name="checkbox" class="iconChekBox" [ngClass]="{\'selectedCheckBox\':face.selected === true }"></ion-icon>\n            </div>\n            <div class="filter_name">{{face.type}}</div>\n          </div> -->\n            <!-- <div class="checkBoxDiv" (click)="selectDiv(i,\'Facing\')" *ngFor="let face of facing; let i=index" [ngClass]="{\'selected_color\':face.selected === true }">\n                <div class="filter_name">{{face.type}}</div>\n            </div> -->\n            <div class="filter_type">\n              <img src="assets/imgs/facing-icon.png">\n              Facing\n            </div>\n            <div class="upperDiv">\n              <div class="scrollCustomBtns" (click)="selectDiv(i,\'Facing\')" *ngFor="let face of facing; let i=index" [ngClass]="{\'selected_color\':face.selected === true }">\n                <div class="filter_name">\n                  {{face.type}}\n                  <span class="cross">X</span>\n                </div>\n              </div>\n            </div>\n\n        </ion-item>\n    </ion-list>\n\n    <ion-list>\n\n      <ion-item>\n        <!-- <div class="chkBox carpetWitdh" (click)="selectDiv(i,\'Carpet\')" *ngFor="let carpet of carpetList; let i=index" >\n          <div class="iconChk" [ngClass]="{\'borderNone\':carpet.selected === true }">\n            <ion-icon name="checkbox" class="iconChekBox" [ngClass]="{\'selectedCheckBox\':carpet.selected === true }"></ion-icon>\n          </div>\n          <div class="filter_name">{{carpet.type}}</div>\n        </div> -->\n        <div class="filter_type">\n          <img src="assets/imgs/carpet-area-icon.png">\n          Carpet Area\n        </div>\n          <div class="checkBoxDiv" (click)="selectDiv(i,\'Carpet\')" *ngFor="let carpet of carpetList; let i=index" [ngClass]="{\'selected_color\':carpet.selected === true }">\n              <div class="filter_name">\n                {{carpet.type}}\n                <span class="cross">X</span>\n              </div>\n          </div>\n      </ion-item>\n    </ion-list>\n\n  <ion-list class="filterbox">\n    <div class="filter_type">\n      <img src="assets/imgs/budget-icon.png">\n      Budget (Approx.)\n    </div>\n    <ion-item>\n      <!-- <ion-label></ion-label> -->\n      <ion-range dualKnobs="true" [(ngModel)]="goalProgress" min="0" max="100" step="1" pin="true" snaps="false" (ionChange)="change()">\n      </ion-range>\n    </ion-item>\n    <div class="clearfix filterrangetext">\n      <div class="filterfrom">\n        <div class="txtgrey">From</div>\n        <div class="filterprice">\n          <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;"> 25 Lakh\n        </div>\n      </div>\n      \n      <div class="filterfrom float-right">\n        <div class="txtgrey float-right">From</div>\n        <div class="clearfix"></div>\n        <div class="filterprice">\n          <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;"> 25 Lakh\n        </div>\n      </div>\n\n    </div>\n  </ion-list>\n  \n\n  <!-- <ion-list>\n    <div class="filter_type">\n      Available Status\n    </div>\n    <ion-item>\n        <div class="checkBoxDiv" (click)="selectDiv(i,\'Available\')" *ngFor="let available of availableList; let i=index" [ngClass]="{\'selected_color\':available.selected === true }">\n            <div class="filter_name">{{available.type}}</div>\n        </div>\n    </ion-item>\n  </ion-list> -->\n\n  <ion-list>\n    <!-- <button class="submit"></button> -->\n    <div class="filterBtn">\n      <button ion-button round (click)="getFilter()" class="fltrSubmitBtn">Submit</button>\n    </div>\n\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/inventory-filter/inventory-filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], InventoryFilterPage);
    return InventoryFilterPage;
}());

//# sourceMappingURL=inventory-filter.js.map

/***/ })

});
//# sourceMappingURL=24.js.map