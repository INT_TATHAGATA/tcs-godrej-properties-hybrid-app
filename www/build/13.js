webpackJsonp([13],{

/***/ 700:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationFormPageModule", function() { return ApplicationFormPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__application_form__ = __webpack_require__(733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(728);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ApplicationFormPageModule = /** @class */ (function () {
    function ApplicationFormPageModule() {
    }
    ApplicationFormPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__application_form__["a" /* ApplicationFormPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__application_form__["a" /* ApplicationFormPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], ApplicationFormPageModule);
    return ApplicationFormPageModule;
}());

//# sourceMappingURL=application-form.module.js.map

/***/ }),

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__test_test__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__ = __webpack_require__(731);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__menu_menu__ = __webpack_require__(732);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import { Component } from './component/component';






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["e" /* IonicModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the TestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        console.log('Hello TestComponent Component');
        this.text = 'Hello World';
    }
    TestComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'test',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/test/test.html"*/'<!-- Generated template for the TestComponent component -->\n<div>\n  {{text}}\n</div>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/test/test.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HeadBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HeadBarComponent = /** @class */ (function () {
    function HeadBarComponent(events, navCtrl, navParams) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notificationDot = false;
        events.subscribe('showNotiDot', function () {
            _this.notificationDot = true;
        });
        //this.text = 'Hello World';
    }
    HeadBarComponent.prototype.toggleMenu = function () {
    };
    HeadBarComponent.prototype.openwishlistpage = function () {
        this.navCtrl.push('WishlistPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("text"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("backTrue"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "backTrue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("menuIcon"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "menuIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("image"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "image", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("noti"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "noti", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("heart"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "heart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("pin"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "pin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("hTitle"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "hTitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeading"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "subHeading", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeadingText"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "subHeadingText", void 0);
    HeadBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'head-bar',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/head-bar/head-bar.html"*/'<!-- Generated template for the HeadBarComponent component -->\n<div>\n  <button ion-button block menuToggle *ngIf="menuIcon" style="float: left;">\n    <!-- <ion-icon name="ios-menu-outline"></ion-icon> -->\n    <img src="assets/imgs/menu-toggle.png" alt="">\n  </button>\n  <!-- <ion-icon name="ios-menu-outline" *ngIf="menuIcon" (click)="toggleMenu()"></ion-icon> -->\n  <div class="text" *ngIf="text"> {{hTitle}}</div>\n  <div class="subheading" *ngIf="subHeading">\n    {{subHeadingText}}\n  </div>\n  <div class="image" *ngIf="image">\n    <img src="assets/imgs/godrej_properties.jpeg" alt="">\n  </div>\n  <div class="iconLists">\n    <ion-icon name="ios-notifications-outline" *ngIf="noti" style="font-size: 1.5em; float: right; margin-right: 15px;">\n      <!-- <ion-badge id="notifications-badge" color="danger"></ion-badge> -->\n      <span [ngClass]="{\'dot\': notificationDot == true}"></span>\n      <!-- <ion-badge item-end>260k</ion-badge> -->\n    </ion-icon>\n    <ion-icon name="ios-heart-outline" *ngIf="heart" style="font-size: 1.5em; float: right; margin-right: 15px;"\n      (click)="openwishlistpage()">\n      <span class="dot"></span>\n    </ion-icon>\n    <ion-icon name="ios-pin-outline" *ngIf="pin" style="font-size: 1.5em; float: right; margin-right: 15px;">\n\n    </ion-icon>\n  </div>\n</div>'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/head-bar/head-bar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], HeadBarComponent);
    return HeadBarComponent;
}());

//# sourceMappingURL=head-bar.js.map

/***/ }),

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FootBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__ = __webpack_require__(727);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_constants__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the FootBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FootBarComponent = /** @class */ (function () {
    function FootBarComponent(events, modalCtrl, storageProvider, searchApi, loader, toast) {
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.storageProvider = storageProvider;
        this.searchApi = searchApi;
        this.loader = loader;
        this.toast = toast;
        this.searchListArr = [];
        console.log('Hello FootBarComponent Component');
    }
    FootBarComponent.prototype.openSearch = function () {
        var _this = this;
        this.loader.show('Loading');
        this.searchApi.propertySearchInfo().subscribe(function (response) {
            _this.loader.hide();
            if (response.status == __WEBPACK_IMPORTED_MODULE_4__app_app_constants__["a" /* AppConst */].HTTP_SUCESS_STATUS.OK) {
                _this.addModal = _this.modalCtrl.create("SearchPage", { responseData: response });
                _this.addModal.onDidDismiss(function (item) {
                    if (item) {
                        //this.searchListArr=item;
                    }
                });
                _this.addModal.present();
            }
            else {
                _this.toast.show(response.msg);
            }
        }, function (error) {
            _this.loader.hide();
            _this.toast.show(error.statusText);
        });
    };
    FootBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'foot-bar',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/foot-bar/foot-bar.html"*/'<!-- Generated template for the FootBarComponent component -->\n<!-- <div class="footer_div">\n    <div>\n        <ion-icon name="ios-home-outline"></ion-icon>\n      Home\n    </div>\n    <div>Book a Visit</div>\n    <div>\n      <ion-icon name="ios-cart-outline"></ion-icon>\n      Buy Now\n    </div>\n    <div>\n      <ion-icon name="ios-call-outline"></ion-icon>\n      Support\n    </div>\n</div>\n<ion-grid>\n  <ion-row>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col></ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n  </ion-row>\n</ion-grid>-->\n\n<ion-footer class="foot">\n\n\n    <ion-grid no-padding>\n        <ion-row no-padding class="menrow">\n            <ion-col no-padding >\n                <div class="iconclass">\n                    <!-- <ion-icon name="ios-home-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/soa-icons.png" />\n                    </div>\n                    <div class="iconlabel">SOA</div>\n                    <!-- <div class="iconlabel">{{logintext}}</div> -->\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 2}">\n                    <!-- <ion-icon name="ios-call"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/service-req-icons.png" />\n                    </div>\n                    <div class="iconlabel">Service Req</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding class="searchcol" (click)="openSearch()">\n                <ion-icon name="ios-menu-outline"></ion-icon>\n                <!-- <img src="assets/imgs/MenuIcon.png" class="footer_main_menu" /> -->\n                <div class="searchicon-wrap">\n                    <div class="searchicon">\n                        <img src="assets/imgs/search-icon.png" />\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 4}">\n                    <!-- <ion-icon name="ios-cart-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/chat-icons.png" />\n                    </div>\n\n                    <div class="iconlabel">Chat Bot</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 3}">\n                    <!-- <ion-icon name="ios-call-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/support-icons.png" />\n                    </div>\n\n\n                    <div class="iconlabel">Support</div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-footer>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/foot-bar/foot-bar.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */]]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]) === "function" && _f || Object])
    ], FootBarComponent);
    return FootBarComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=foot-bar.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        console.log('Hello MenuComponent Component');
        this.text = 'Hello World';
    }
    MenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/menu/menu.html"*/'<!-- Generated template for the MenuComponent component -->\n<!-- <div class="sidemenu">\n  TEST\n\n</div> -->\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/menu/menu.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationFormPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ApplicationFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ApplicationFormPage = /** @class */ (function () {
    function ApplicationFormPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.isOpen = true;
        this.noti = true;
        this.heart = true;
        this.pin = false;
    }
    ApplicationFormPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApplicationFormPage');
    };
    ApplicationFormPage.prototype.getCollapse = function (value) {
        this.isOpen = !this.isOpen;
    };
    ApplicationFormPage.prototype.goToPay = function () {
        this.navCtrl.push("PaymentPage");
    };
    ApplicationFormPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-application-form',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/application-form/application-form.html"*/'<!--\n  Generated template for the ApplicationFormPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n\n    <ion-navbar>\n      <!-- <ion-title>Inventory Booking</ion-title> -->\n    <div class="p_title">Booking Summary <div>Godjet Aqau</div></div>\n      <head-bar hTitle="Application Form" [noti]="noti" [heart]="heart" [pin]="pin" ></head-bar>\n    </ion-navbar>\n\n  </ion-header>\n\n\n<ion-content padding>\n  <div class="infobok">\n      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n  </div>\n  <div class="details">\n    <div class="headingText">\n      Personal Information\n    </div>\n    <div class="inforeadonly">\n      <div class="infoTitle">Full Name</div>\n      <div class="infopdtl">Uttam Chowdhury</div>\n    </div>\n    \n    <div class="inforeadonly">\n        <div class="infoTitle">Mobile Number</div>\n        <div class="infopdtl">+91 8100236961</div>\n    </div>\n    \n    <div class="inforeadonly">\n        <div class="infoTitle">Email</div>\n        <div class="infopdtl">uttamk.chowdhury@indusnet.co.in</div>\n    </div>\n    \n    <div class="inforeadonly">\n        <div class="infoTitle">Resident Address</div>\n        <div class="infopdtl">SDF Building, SDF More, Kolkata - 700091</div>\n    </div>\n    \n    <div class="inforeadonly">\n        <div class="infoTitle">Purpose of Purchase</div>\n        <div class="infopdtl">Investment</div>\n    </div>\n    \n    <div class="inforeadonly">\n        <div class="infoTitle">Second Applicant</div>\n        <div class="infopdtl">Aditi</div>\n    </div>\n    \n    <div class="inforeadonly">\n        <div class="infoTitle">Relationship</div>\n        <div class="infopdtl">Wife</div>\n    </div>\n      \n  </div>\n\n  <div class="flatTitle">Flat Details</div>\n  <div class="flatDetails">\n    <div class="flatContent">\n        <div class="fnamenTitle">\n          <div class="fcate">Project Name</div>\n          <div class="fanswer">Godrej Azure</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Property</div>\n            <div class="fanswer">GPTT011402</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Unit</div>\n            <div class="fanswer">1404</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Flat Type</div>\n            <div class="fanswer">2BHK</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Flat No.</div>\n            <div class="fanswer">14</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Tower Code</div>\n            <div class="fanswer">GN10</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Tower Name</div>\n            <div class="fanswer">Godrej Prime - T1</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Carpet Area (Sq. mt.)</div>\n            <div class="fanswer">54.1</div>\n        </div>\n      \n        <div class="fnamenTitle">\n            <div class="fcate">Total Consideration Cost</div>\n            <div class="fanswer">30 Lakh</div>\n        </div>\n        <div class="Flatcost">\n          Flat Details\n          <div><img src="assets/imgs/rupee-indian-blue.png">50,000/-</div>\n          <div class="clearfix"></div>\n        </div>\n            \n    </div>\n  </div>\n    <!-- <ion-card> -->\n        <!-- <img src="img/nin-live.png"/> -->\n        <!-- <div>\n          <ion-icon name="grid" class="float-left"></ion-icon>\n          <div class="title">Project Details</div>\n          <div class="thin-border"></div>\n          <ion-icon (click)="getCollapse(\'pd\')" *ngIf="isOpen" name="ios-arrow-up-outline" class="arrow-caraousel"></ion-icon>\n          <ion-icon (click)="getCollapse(\'pd\')" *ngIf="!isOpen" name="ios-arrow-down-outline" class="arrow-caraousel"></ion-icon>\n        </div> -->\n        <!-- <ion-card-content *ngIf="isOpen"> -->\n          <!-- <ion-card-title>\n            Nine Inch Nails Live\n          </ion-card-title>\n          <p>\n            The most popular industrial group ever, and largely\n            responsible for bringing the music to a mass audience.\n          </p>-->\n\n          <!-- <div class="projectDetails">\n            <div class="left">\n              Project Code\n            </div>\n            <div class="right">\n              The Trees\n            </div>\n\n            <div class="left">\n              Unit Details\n            </div>\n            <div class="right">\n              TC 66598\n            </div>\n\n            <div class="left">\n              Typology\n            </div>\n            <div class="right">\n              3 BHK\n            </div>\n\n            <div class="left">\n              Carpet Area (RERA)\n            </div>\n            <div class="right">\n              1100 sq. ft.\n            </div>\n\n            <div class="left">\n              Exclusive Area\n            </div>\n            <div class="right">\n              1000 sq. ft.\n            </div>\n\n            <div class="left">\n              Total Area\n            </div>\n            <div class="right">\n              1100 sq. ft.\n            </div>\n\n            <div class="left">\n              View/Facing\n            </div>\n            <div class="right">\n              North\n            </div>\n\n            <div class="left">\n              Palette options\n            </div>\n            <div class="right">\n              XXX-XXX\n            </div>\n\n            <div class="left">\n              Car Parking Type\n            </div>\n            <div class="right">\n              Dependent\n            </div>\n\n            <div class="left">\n              No. of car parking\n            </div>\n                <div class="right">\n              2\n            </div>\n          </div> -->\n        <!-- </ion-card-content> -->\n    <!-- </ion-card> -->\n    <!-- <ion-card class="download">\n        <ion-icon name="ios-download-outline" class="download_icon"></ion-icon>\n        <div class="text">Download Application Form</div>\n    </ion-card> -->\n    <button ion-button round class="button_pay btn-grey" style="width: 100%">Edit</button>\n    <button ion-button round class="button_pay" style="width: 100%" (click)="goToPay()">Proceed To Payment</button>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/application-form/application-form.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ApplicationFormPage);
    return ApplicationFormPage;
}());

//# sourceMappingURL=application-form.js.map

/***/ })

});
//# sourceMappingURL=13.js.map