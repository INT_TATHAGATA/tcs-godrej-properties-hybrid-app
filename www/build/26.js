webpackJsonp([26],{

/***/ 703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function() { return ForgotPasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgot_password__ = __webpack_require__(736);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ForgotPasswordPageModule = /** @class */ (function () {
    function ForgotPasswordPageModule() {
    }
    ForgotPasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__forgot_password__["a" /* ForgotPasswordPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__forgot_password__["a" /* ForgotPasswordPage */]),
            ],
        })
    ], ForgotPasswordPageModule);
    return ForgotPasswordPageModule;
}());

//# sourceMappingURL=forgot-password.module.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_db__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(navCtrl, navParams, fb, storageProvider, Authapi, loader, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.storageProvider = storageProvider;
        this.Authapi = Authapi;
        this.loader = loader;
        this.toast = toast;
        this.disableButton = true;
        this.panNumber = false;
        this.onlyNumberAllowed = false;
        this.validEmail = false;
        this.invalid = true;
    }
    ForgotPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotPasswordPage');
    };
    ForgotPasswordPage.prototype.ngOnInit = function () {
        var _this = this;
        this.forgotPasswordForm = this.fb.group({
            textvalue: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
            ]),
            pan: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]()
        });
        this.forgotPasswordForm.valueChanges.subscribe(function (v) {
            _this.onlyNumberAllowed = false;
            _this.validEmail = false;
            _this.invalid = true;
            if (v.textvalue != "") {
                var checkEmail = _this.forgotPasswordForm.value.textvalue.indexOf("@");
                if (!isNaN(v.textvalue) && checkEmail == -1) {
                    var regEx = _this.storageProvider.onlyNumberPattern();
                    var checkPlus = v.textvalue.indexOf("+");
                    console.log(checkPlus);
                    var result = v.textvalue.search(regEx);
                    _this.onlyNumberAllowed = false;
                    if (result == -1) {
                        _this.invalid = true;
                        _this.onlyNumberAllowed = true;
                    }
                    else {
                        _this.invalid = false;
                    }
                }
                else if (isNaN(v.textvalue)) {
                    var regEx = _this.storageProvider.emailPattern();
                    var result = v.textvalue.search(regEx);
                    _this.validEmail = false;
                    if (result == -1) {
                        _this.invalid = true;
                        _this.validEmail = true;
                    }
                    else {
                        _this.invalid = false;
                    }
                }
            }
            if (v.password == "") {
                _this.invalid = true;
            }
        });
    };
    Object.defineProperty(ForgotPasswordPage.prototype, "f", {
        get: function () {
            return this.forgotPasswordForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    ForgotPasswordPage.prototype.forgotPassword = function () {
        var _this = this;
        //
        var type, postData;
        if (this.panNumber) {
            type = "pan";
            postData = {
                type: type,
                username: this.forgotPasswordForm.value.pan
            };
        }
        else {
            var checkEmail = this.forgotPasswordForm.value.textvalue.indexOf("@");
            if (checkEmail > -1) {
                type = "email";
            }
            else {
                type = "mob";
            }
            postData = {
                type: type,
                username: this.forgotPasswordForm.value.textvalue
            };
        }
        this.loader.show('Please wait...');
        this.Authapi.postForgotPasswordApi(postData).subscribe(function (response) {
            _this.loader.hide();
            if (response.status == "200") {
                var data = {
                    response_otp: response.otp,
                    uid: response.uid,
                    postd: postData
                };
                _this.navCtrl.push("VarifyOtpPage", { formData: data });
            }
            else {
                if (response.status == "501") {
                    _this.panNumber = true;
                    _this.forgotPasswordForm = _this.fb.group({
                        textvalue: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](_this.forgotPasswordForm.value.textvalue, [
                            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
                        ]),
                        pan: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(_this.storageProvider.panPattern()),
                        ])
                    });
                }
                _this.toast.show(response.msg);
            }
        }, function (error) {
            console.log(error);
        });
        /*
      } else {
    
      } */
    };
    ForgotPasswordPage.prototype.onkeyDown = function () {
        var newPan = this.forgotPasswordForm.value.pan.toUpperCase();
        this.forgotPasswordForm.setValue({ textvalue: this.forgotPasswordForm.value.textvalue, pan: newPan });
    };
    ForgotPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forgot-password',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/forgot-password/forgot-password.html"*/'<!--\n  Generated template for the ForgotPasswordPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Forgot Password</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <img src="assets/imgs/godrej_properties.jpeg" class="propertiesImg" alt="">\n\n  <div class="formForgotPassword">\n    <form class="loginForm" [formGroup]="forgotPasswordForm">\n      <ion-list>\n        <ion-item>\n          <ion-label floating>\n            <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Email Id / Mobile Number\n          </ion-label>\n        <ion-input type="text" name="textvalue" formControlName="textvalue"></ion-input>\n      </ion-item>\n      <div class="error">\n        <div *ngIf="f?.textvalue.errors?.required && f?.textvalue.touched">Email id / Mobile Number is required</div>\n        <div *ngIf="onlyNumberAllowed">\n          Not a valid number.\n        </div>\n        <div *ngIf="validEmail">\n          Not a valid email id.\n        </div>\n      </div>\n\n      <ion-item *ngIf="panNumber == true">\n        <ion-label floating>\n          <ion-icon name="ios-card-outline"></ion-icon>\n          &nbsp; Pan\n        </ion-label>\n        <ion-input type="text" name="pan" formControlName="pan" (keyup)="onkeyDown()"></ion-input>\n      </ion-item>\n      <div class="error" *ngIf="panNumber == true">\n        <div *ngIf="f?.pan.errors?.required && f?.pan.touched">Pan is required</div>\n        <div *ngIf="f?.pan.errors?.pattern && f?.pan.touched">Not a valid pan numbers</div>\n      </div>\n\n\n        <button ion-button round class="width-100" [disabled]="invalid" (click)="forgotPassword()">\n          Send\n        </button>\n      </ion-list>\n\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/forgot-password/forgot-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());

//# sourceMappingURL=forgot-password.js.map

/***/ })

});
//# sourceMappingURL=26.js.map