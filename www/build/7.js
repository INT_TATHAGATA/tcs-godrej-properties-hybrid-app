webpackJsonp([7],{

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationFormPageModule", function() { return ApplicationFormPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__application_form__ = __webpack_require__(924);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(762);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_apis_projectdetails__ = __webpack_require__(767);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ApplicationFormPageModule = /** @class */ (function () {
    function ApplicationFormPageModule() {
    }
    ApplicationFormPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__application_form__["a" /* ApplicationFormPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__application_form__["a" /* ApplicationFormPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_4__providers_apis_projectdetails__["a" /* ProjectDetailsApiProvider */]]
        })
    ], ApplicationFormPageModule);
    return ApplicationFormPageModule;
}());

//# sourceMappingURL=application-form.module.js.map

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__test_test__ = __webpack_require__(763);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__ = __webpack_require__(764);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__ = __webpack_require__(765);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__menu_menu__ = __webpack_require__(766);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import { Component } from './component/component';






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 763:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the TestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        console.log('Hello TestComponent Component');
        this.text = 'Hello World';
    }
    TestComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'test',template:/*ion-inline-start:"D:\projects\gpl\src\components\test\test.html"*/'<!-- Generated template for the TestComponent component -->\n<div>\n  {{text}}\n</div>\n'/*ion-inline-end:"D:\projects\gpl\src\components\test\test.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the HeadBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HeadBarComponent = /** @class */ (function () {
    function HeadBarComponent(events, navCtrl, navParams, storageProvider) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storageProvider = storageProvider;
        this.notificationDot = false;
        this.isGuest = this.storageProvider.isGuest();
        events.subscribe('showNotiDot', function () {
            _this.notificationDot = true;
        });
        //this.text = 'Hello World';
    }
    HeadBarComponent.prototype.toggleMenu = function () {
    };
    HeadBarComponent.prototype.openwishlistpage = function () {
        this.navCtrl.push('WishlistPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("text"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("backTrue"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "backTrue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("menuIcon"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "menuIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("image"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "image", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("noti"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "noti", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("heart"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "heart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("pin"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "pin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("hTitle"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "hTitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeading"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "subHeading", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeadingText"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "subHeadingText", void 0);
    HeadBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'head-bar',template:/*ion-inline-start:"D:\projects\gpl\src\components\head-bar\head-bar.html"*/'<!-- Generated template for the HeadBarComponent component -->\n\n<div>\n\n  <button ion-button block menuToggle *ngIf="menuIcon" style="float: left;">\n\n    <!-- <ion-icon name="ios-menu-outline"></ion-icon> -->\n\n    <img src="assets/imgs/menu-toggle.png" alt="">\n\n  </button>\n\n  <!-- <ion-icon name="ios-menu-outline" *ngIf="menuIcon" (click)="toggleMenu()"></ion-icon> -->\n\n  <div class="text" *ngIf="text"> {{hTitle}}</div>\n\n  <div class="subheading" *ngIf="subHeading">\n\n    {{subHeadingText}}\n\n  </div>\n\n  <div class="image" *ngIf="image">\n\n    <img src="assets/imgs/godrej_properties.jpeg" alt="">\n\n  </div>\n\n  <div class="iconLists" *ngIf="isGuest != \'1\'">\n\n    <ion-icon name="ios-notifications-outline" *ngIf="noti" style="font-size: 1.5em; float: right; margin-right: 15px;">\n\n      <!-- <ion-badge id="notifications-badge" color="danger"></ion-badge> -->\n\n      <span [ngClass]="{\'dot\': notificationDot == true}"></span>\n\n      <!-- <ion-badge item-end>260k</ion-badge> -->\n\n    </ion-icon>\n\n    <ion-icon name="ios-heart-outline" *ngIf="heart" style="font-size: 1.5em; float: right; margin-right: 15px;"\n\n      (click)="openwishlistpage()">\n\n      <span class="dot"></span>\n\n    </ion-icon>\n\n    <ion-icon name="ios-pin-outline" *ngIf="pin" style="font-size: 1.5em; float: right; margin-right: 15px;">\n\n\n\n    </ion-icon>\n\n  </div>\n\n</div>'/*ion-inline-end:"D:\projects\gpl\src\components\head-bar\head-bar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]])
    ], HeadBarComponent);
    return HeadBarComponent;
}());

//# sourceMappingURL=head-bar.js.map

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FootBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_constants__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the FootBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FootBarComponent = /** @class */ (function () {
    function FootBarComponent(events, modalCtrl, storageProvider, searchApi, loader, toast) {
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.storageProvider = storageProvider;
        this.searchApi = searchApi;
        this.loader = loader;
        this.toast = toast;
        this.searchListArr = [];
        console.log('Hello FootBarComponent Component');
    }
    FootBarComponent.prototype.openSearch = function () {
        var _this = this;
        this.loader.show('Loading');
        this.searchApi.propertySearchInfo().subscribe(function (response) {
            _this.loader.hide();
            if (response.status == __WEBPACK_IMPORTED_MODULE_4__app_app_constants__["a" /* AppConst */].HTTP_SUCESS_STATUS.OK) {
                _this.addModal = _this.modalCtrl.create("SearchPage", { responseData: response });
                _this.addModal.onDidDismiss(function (item) {
                    if (item) {
                        //this.searchListArr=item;
                    }
                });
                _this.addModal.present();
            }
            else {
                _this.toast.show(response.msg);
            }
        }, function (error) {
            _this.loader.hide();
            _this.toast.show(error.statusText);
        });
    };
    FootBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'foot-bar',template:/*ion-inline-start:"D:\projects\gpl\src\components\foot-bar\foot-bar.html"*/'<!-- Generated template for the FootBarComponent component -->\n<!-- <div class="footer_div">\n    <div>\n        <ion-icon name="ios-home-outline"></ion-icon>\n      Home\n    </div>\n    <div>Book a Visit</div>\n    <div>\n      <ion-icon name="ios-cart-outline"></ion-icon>\n      Buy Now\n    </div>\n    <div>\n      <ion-icon name="ios-call-outline"></ion-icon>\n      Support\n    </div>\n</div>\n<ion-grid>\n  <ion-row>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col></ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n  </ion-row>\n</ion-grid>-->\n\n<ion-footer class="foot">\n\n\n    <ion-grid no-padding>\n        <ion-row no-padding class="menrow">\n            <ion-col no-padding >\n                <div class="iconclass">\n                    <!-- <ion-icon name="ios-home-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/soa-icons.png" />\n                    </div>\n                    <div class="iconlabel">SOA</div>\n                    <!-- <div class="iconlabel">{{logintext}}</div> -->\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 2}">\n                    <!-- <ion-icon name="ios-call"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/service-req-icons.png" />\n                    </div>\n                    <div class="iconlabel">Service Req</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding class="searchcol" (click)="openSearch()">\n                <ion-icon name="ios-menu-outline"></ion-icon>\n                <!-- <img src="assets/imgs/MenuIcon.png" class="footer_main_menu" /> -->\n                <div class="searchicon-wrap">\n                    <div class="searchicon">\n                        <img src="assets/imgs/search-icon.png" />\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 4}">\n                    <!-- <ion-icon name="ios-cart-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/chat-icons.png" />\n                    </div>\n\n                    <div class="iconlabel">Chat Bot</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 3}">\n                    <!-- <ion-icon name="ios-call-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/support-icons.png" />\n                    </div>\n\n\n                    <div class="iconlabel">Support</div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-footer>\n'/*ion-inline-end:"D:\projects\gpl\src\components\foot-bar\foot-bar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]])
    ], FootBarComponent);
    return FootBarComponent;
}());

//# sourceMappingURL=foot-bar.js.map

/***/ }),

/***/ 766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        console.log('Hello MenuComponent Component');
        this.text = 'Hello World';
    }
    MenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu',template:/*ion-inline-start:"D:\projects\gpl\src\components\menu\menu.html"*/'<!-- Generated template for the MenuComponent component -->\n<!-- <div class="sidemenu">\n  TEST\n\n</div> -->\n'/*ion-inline-end:"D:\projects\gpl\src\components\menu\menu.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 767:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectDetailsApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This provider is responsible for calling Project Details Api Project Details page
 *
 * @export
 * @class ProjectDetailsApiProvider
 * @author tathagata sur
 */
var ProjectDetailsApiProvider = /** @class */ (function () {
    function ProjectDetailsApiProvider(http) {
        this.http = http;
        this.LOCATION_URL = 'get_property_location_rest.json';
        this.UPDATEPROJECT_URL = 'update_history.json';
        this.INVENTORY_LIST = 'res_inventory_list.json';
        this.VISIT_BOOK = 'book_visit.json';
        this.REQUEST_CALL = 'request_callback.json';
        this.INVENTORY_FILTER_DATA = 'res_inventory_filter.json';
        this.INVENTORY_SEARCH = 'res_inventory_search.json';
        this.COSTSHEETBRK_UP = 'res_booking_cross_breakup.json';
        this.BOOKING_ADD = 'res_booking_chk_add.json';
        this.BOOKING_CONFRIM = 'booking_confirmation_data.json';
    }
    ProjectDetailsApiProvider.prototype.goprojectdetails = function (PROJECTID) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + 'property_details_rest/' + PROJECTID + '/?_format=json';
        return this.http.get(url, PROJECTID);
    };
    ProjectDetailsApiProvider.prototype.getprojectbanar = function (PROJECTID) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + 'property_banner/' + PROJECTID + '/?_format=json';
        return this.http.get(url);
    };
    ProjectDetailsApiProvider.prototype.getprojectaminities = function (PROJECTID) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + 'property_aminities_rest/' + PROJECTID + '/?_format=json';
        return this.http.get(url);
    };
    ProjectDetailsApiProvider.prototype.getprojectgalleries = function (PROJECTID) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + 'property_galleries_rest/' + PROJECTID + '/?_format=json';
        return this.http.get(url);
    };
    ProjectDetailsApiProvider.prototype.getprojectlocation = function (PROJECTID) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.LOCATION_URL;
        return this.http.post(url, PROJECTID);
    };
    ProjectDetailsApiProvider.prototype.updateProjectDetails = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.UPDATEPROJECT_URL;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.getinventorylist = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.INVENTORY_LIST;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.postVisitBook = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.VISIT_BOOK;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.postRequestCall = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.REQUEST_CALL;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.getinventoryfiterdata = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.INVENTORY_FILTER_DATA;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.getinventorysearch = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.INVENTORY_SEARCH;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.getcostsheetbrkup = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.COSTSHEETBRK_UP;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.postBookingDetails = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.BOOKING_ADD;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider.prototype.postBookingConfrimDetails = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.BOOKING_CONFRIM;
        return this.http.post(url, data);
    };
    ProjectDetailsApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], ProjectDetailsApiProvider);
    return ProjectDetailsApiProvider;
}());

//# sourceMappingURL=projectdetails.js.map

/***/ }),

/***/ 924:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationFormPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_projectdetails__ = __webpack_require__(767);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_toast__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the ApplicationFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ApplicationFormPage = /** @class */ (function () {
    function ApplicationFormPage(navCtrl, navParams, storageProvider, projectdetailsApi, loader, toast, events, modalCtrl, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storageProvider = storageProvider;
        this.projectdetailsApi = projectdetailsApi;
        this.loader = loader;
        this.toast = toast;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.iab = iab;
        this.isOpen = true;
        this.noti = true;
        this.heart = true;
        this.pin = false;
    }
    ApplicationFormPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApplicationFormPage');
        this.projectDetails = this.navParams.get('projectDetails');
        this.parameter = this.navParams.get('parameter');
        this.enquiryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));
    };
    ApplicationFormPage.prototype.getCollapse = function (value) {
        this.isOpen = !this.isOpen;
    };
    ApplicationFormPage.prototype.goToPay = function () {
        var _this = this;
        if (localStorage.getItem('isGuest') == '1') {
            this.toast.show('Please Login to view this page');
            this.addModal = this.modalCtrl.create("HomePage", {});
            this.addModal.onDidDismiss(function (item) {
                if (item == 'loggedIn') {
                    _this.events.publish('guestToUser');
                    _this.postBookingDetails();
                }
            });
            localStorage.setItem('fromPage', 'BookingSummary');
            this.addModal.present();
        }
        else {
            this.postBookingDetails();
        }
    };
    ApplicationFormPage.prototype.postBookingDetails = function () {
        var _this = this;
        var enqueryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));
        var jsonData = {
            designation: enqueryFormData.designation,
            device_id: this.storageProvider.getDeviceInfo().uuid,
            dob: enqueryFormData.dob,
            email_id: enqueryFormData.email,
            first_name: enqueryFormData.fname,
            inventory_id: this.projectDetails.inv.nid,
            last_name: enqueryFormData.lname,
            office_city: enqueryFormData.ofcCity,
            office_locality: enqueryFormData.ofcLocation,
            payment_amount: this.parameter.curl_response.project_stock.booking_amount,
            phone_number: enqueryFormData.mobile,
            profession: enqueryFormData.occupation,
            project_id: localStorage.getItem('projectId'),
            reffered_by: enqueryFormData.referral_partners,
            residance_ownership: enqueryFormData.ownership,
            residance_type: enqueryFormData.typologyTypes,
            salutation: enqueryFormData.salutaion,
            user_id: localStorage.getItem('userId')
        };
        this.loader.show('Please wait...');
        this.projectdetailsApi.postBookingDetails(jsonData).subscribe(function (response) {
            _this.loader.hide();
            if (response.status == '200') {
                _this.OpenPaymentPage(response.booking_id, response.booking_no, response.amount);
            }
            _this.toast.show(response.msg);
        }, function (error) {
            _this.loader.hide();
        });
    };
    ApplicationFormPage.prototype.editEnquiry = function () {
        this.navCtrl.push("EnquiryFormPage", { details: this.projectDetails });
    };
    ApplicationFormPage.prototype.OpenPaymentPage = function (booking_id, booking_no, amount) {
        var sessonid = localStorage.getItem('Cookie').split('=')[1];
        var param = '?auth=' + sessonid + '&user_id=' + localStorage.getItem('userId') + '&booking_id=' + booking_id + '&booking_no=' + booking_no + '&amount=' + amount;
        var self = this;
        var url = __WEBPACK_IMPORTED_MODULE_7__app_app_constants__["a" /* AppConst */].baseUrl + 'api_payment_redirect' + param;
        console.log('url', url);
        var option = {
            location: 'no',
            hidden: 'no',
            toolbar: 'no',
        };
        var browser = this.iab.create(url, '_blank', option);
        browser.on('loadstop').subscribe(function (event) {
            console.log(event.url);
            // if (event.url === '/success') {
            //   browser.close();
            //   self.toast.show('Payment Successfully');
            //   this.boookingconfrim();
            // } else if (event.url === '/error') {
            //   browser.close();
            //   self.toast.show('Some thing went wrong in payment');
            // }
        });
    };
    ApplicationFormPage.prototype.boookingconfrim = function () {
        var _this = this;
        var data = {
            user_id: localStorage.getItem('userId'),
            booking_id: "3508"
        };
        this.loader.show('Please wait...');
        this.projectdetailsApi.postBookingConfrimDetails(data).subscribe(function (response) {
            _this.loader.hide();
            if (response.status == '200') {
                // this.navCtrl.push("PaymentPage");
            }
        }, function (error) {
            _this.loader.hide();
        });
    };
    ApplicationFormPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-application-form',template:/*ion-inline-start:"D:\projects\gpl\src\pages\application-form\application-form.html"*/'<!--\n\n  Generated template for the ApplicationFormPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <!-- <ion-title>Inventory Booking</ion-title> -->\n\n    <div class="p_title">Booking Summary <div>Godjet Aqau</div>\n\n    </div>\n\n    <head-bar hTitle="Application Form" [noti]="noti" [heart]="heart" [pin]="pin"></head-bar>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="infobok">\n\n    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum\n\n    sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\n  </div>\n\n  <div class="details">\n\n    <div class="headingText">\n\n      Personal Information\n\n    </div>\n\n    <div class="inforeadonly">\n\n      <div class="infoTitle">Full Name</div>\n\n      <div class="infopdtl">{{enquiryFormData?.fname}} {{enquiryFormData?.lname}}</div>\n\n    </div>\n\n\n\n    <div class="inforeadonly">\n\n      <div class="infoTitle">Mobile Number</div>\n\n      <div class="infopdtl">{{enquiryFormData?.mobile}}</div>\n\n    </div>\n\n\n\n    <div class="inforeadonly">\n\n      <div class="infoTitle">Email</div>\n\n      <div class="infopdtl">{{enquiryFormData?.email}}</div>\n\n    </div>\n\n\n\n    <div class="inforeadonly">\n\n      <div class="infoTitle">Resident Address</div>\n\n      <div class="infopdtl">{{enquiryFormData?.contact_adds}}</div>\n\n    </div>\n\n\n\n    <div class="inforeadonly">\n\n      <div class="infoTitle">Purpose of Purchase</div>\n\n      <div class="infopdtl">{{enquiryFormData?.contact_adds}}</div>\n\n    </div>\n\n    <!-- <div class="applicants" *ngIf="enquiryFormData.applicantlist.length > 0">\n\n      <div class="applicantList" *ngFor="allicant of enquiryFormData.applicantlist">\n\n        <div class="inforeadonly">\n\n          <div class="infoTitle">Second Applicant</div>\n\n          <div class="infopdtl">{{allicant.name}}</div>\n\n        </div>\n\n        <div class="inforeadonly">\n\n          <div class="infoTitle">Relationship</div>\n\n          <div class="infopdtl">{{allicant.relation}}</div>\n\n        </div>\n\n      </div>\n\n    </div> -->\n\n\n\n\n\n  </div>\n\n\n\n  <div class="flatTitle">Flat Details</div>\n\n  <div class="flatDetails">\n\n    <div class="flatContent">\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Project Name</div>\n\n        <div class="fanswer">{{projectDetails?.pName}}</div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Property</div>\n\n        <div class="fanswer"></div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Unit</div>\n\n        <div class="fanswer">{{projectDetails?.inv.unit}}</div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Flat Type</div>\n\n        <div class="fanswer">{{projectDetails?.inv.typology_tags}}</div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Flat No.</div>\n\n        <div class="fanswer">{{projectDetails?.inv.flat_code}}</div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Tower Code</div>\n\n        <div class="fanswer"></div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Tower Name</div>\n\n        <div class="fanswer">{{projectDetails?.inv.tower}}</div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Carpet Area (Sq. mt.)</div>\n\n        <div class="fanswer">{{projectDetails?.inv.carpet_area}}</div>\n\n      </div>\n\n\n\n      <div class="fnamenTitle">\n\n        <div class="fcate">Total Consideration Cost</div>\n\n        <div class="fanswer">{{parameter?.curl_response.project_stock.total_price}}</div>\n\n      </div>\n\n      <div class="Flatcost">\n\n        Flat Details\n\n        <div><img src="assets/imgs/rupee-indian-blue.png">{{parameter?.curl_response.project_stock.total_price}}/-</div>\n\n        <div class="clearfix"></div>\n\n      </div>\n\n      <div class="Flatcost">\n\n          Booking Amount\n\n          <div><img src="assets/imgs/rupee-indian-blue.png">{{parameter?.curl_response.project_stock.booking_amount}}/-</div>\n\n          <div class="clearfix"></div>\n\n        </div>\n\n\n\n    </div>\n\n  </div>\n\n  <button ion-button round class="button_pay btn-grey commonButnClass" style="width: 100%" (click)="editEnquiry()">Edit</button>\n\n  <button ion-button round class="button_pay commonButnClass" style="width: 100%" (click)="goToPay()">Proceed To\n\n    Payment</button>\n\n</ion-content>'/*ion-inline-end:"D:\projects\gpl\src\pages\application-form\application-form.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_apis_projectdetails__["a" /* ProjectDetailsApiProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_apis_projectdetails__["a" /* ProjectDetailsApiProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__["a" /* LoadingProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__["a" /* LoadingProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__providers_utils_toast__["a" /* ToasterProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_utils_toast__["a" /* ToasterProvider */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */]) === "function" && _j || Object])
    ], ApplicationFormPage);
    return ApplicationFormPage;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
}());

//# sourceMappingURL=application-form.js.map

/***/ })

});
//# sourceMappingURL=7.js.map