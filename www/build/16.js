webpackJsonp([16],{

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialPageModule", function() { return TutorialPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tutorial__ = __webpack_require__(755);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TutorialPageModule = /** @class */ (function () {
    function TutorialPageModule() {
    }
    TutorialPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */]),
            ],
        })
    ], TutorialPageModule);
    return TutorialPageModule;
}());

//# sourceMappingURL=tutorial.module.js.map

/***/ }),

/***/ 755:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';


/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TutorialPage = /** @class */ (function () {
    function TutorialPage(navCtrl, navParams, menu, storagePrivider, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.storagePrivider = storagePrivider;
        this.toast = toast;
        this.showSkip = true;
        this.ciltyList = [];
        this.typologyList = [];
        this.ready = {
            wait: true,
            move: false
        };
    }
    TutorialPage.prototype.ionViewDidLoad = function () {
        this._ionViewDidEnter_is_complete = true;
        this.menu.enable(false);
        this.ready = {
            wait: true,
            move: false
        };
    };
    TutorialPage.prototype.ngOnInit = function () {
        this.ciltyList = [
            { id: 1, name: "Pune", selected: false },
            { id: 2, name: "Mumbai", selected: false },
            { id: 3, name: "Delhi", selected: false },
            { id: 4, name: "Kolkata", selected: false },
            { id: 5, name: "Begaluru", selected: false },
        ];
        this.typologyList = [
            { id: 1, name: "2 Bhk", selected: false },
            { id: 2, name: "3 Bhk", selected: true },
            { id: 3, name: "3+ Bhk", selected: false },
            { id: 4, name: "4 Bhk", selected: false },
            { id: 5, name: "5 Bhk", selected: false },
        ];
        //this.slides.ionSlideProgress.subscribe(progress => this.onSliderProgress(progress));
        //this.slides.ionSlideDidChange.subscribe(data => this.onSliderProgress(data));
    };
    TutorialPage.prototype.onSliderProgress = function (progress) {
        /* if (this._ionViewDidEnter_is_complete && this.slides.getActiveIndex() == 0) {
            console.log('% progress: ' + progress);
            // do work here...
        } */
    };
    TutorialPage.prototype.nextSlide = function () {
        this.slides.slideNext();
    };
    TutorialPage.prototype.startApp = function () {
        //localStorage.setItem("isTutorial","1");
        this.storagePrivider.set("isTutorial", "1");
        this.navCtrl.setRoot('HomePage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    TutorialPage.prototype.onSlideChangeStart = function (slider) {
        /* debugger;
        let currentIndex = this.slides.getActiveIndex();
        console.log('Current index is', currentIndex);
        this.showSkip = !slider.isEnd(); */
    };
    TutorialPage.prototype.ionViewWillLeave = function () {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true);
    };
    TutorialPage.prototype.select = function (index, type, extra) {
        if (type == "city") {
            var findSelected = this.ciltyList.findIndex(function (X) { return X.selected == true; });
            if (findSelected > -1) {
                this.ciltyList[findSelected].selected = false;
            }
            this.ciltyList[index].selected = true;
        }
        else if (type == "typology") {
            var findSelected = this.typologyList.findIndex(function (X) { return X.selected == true; });
            if (findSelected > -1) {
                this.typologyList[findSelected].selected = false;
            }
            this.typologyList[index].selected = true;
        }
        else if (type == "ready") {
            (extra == "wait") ? this.ready = {
                wait: true,
                move: false
            } : this.ready = {
                wait: false,
                move: true
            };
        }
        this.nextSlide();
    };
    TutorialPage.prototype.goToPage = function (page) {
        this.navCtrl.push(page);
    };
    TutorialPage.prototype.getLocation = function () {
        var _this = this;
        this.storagePrivider.getLocation().then(function (coOrd) {
            var lat = coOrd.coords.latitude;
            var long = coOrd.coords.longitude;
            _this.storagePrivider.getReverseGeoCode(lat, long).then(function (result) {
                console.log(result);
                var findCityNAme = _this.ciltyList.findIndex(function (x) { return x.name == result.locality; });
                if (findCityNAme > -1) {
                    var findSelected = _this.ciltyList.findIndex(function (x) { return x.selected == true; });
                    if (findSelected > -1) {
                        _this.ciltyList[findSelected].selected = false;
                    }
                    _this.ciltyList[findCityNAme].selected = true;
                }
                else {
                    _this.toast.show('Please select your prefered city.');
                }
                _this.nextSlide();
                //result.locality
            }, function (err) {
                console.log(err);
            }).catch(function (error) {
                console.log(error);
            });
        }, function (err) {
            console.log(err);
        }).catch(function (error) {
            console.log(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('slides'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
    ], TutorialPage.prototype, "slides", void 0);
    TutorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tutorial',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/tutorial/tutorial.html"*/'<!--\n  Generated template for the TutorialPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>tutorial</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n<!-- <ion-header no-shadow>\n  <ion-navbar>\n\n  </ion-navbar>\n</ion-header> -->\n\n<ion-content no-bounce>\n  <ion-slides pager="true" #slides>\n\n    <ion-slide>\n      <div class="headerImg">\n        <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n      </div>\n      <img src="assets/imgs/logo.png" class="slide-image" />\n      <h2 class="slide-title">Find Your Dream Home</h2>\n      <p>Help us to know you better</p>\n\n      <button ion-button outline (click)="nextSlide()" class="nextSlides">Let\'s Go</button>\n      <ion-buttons end *ngIf="showSkip" class="skip">\n        <button ion-button (click)="startApp()" color="primary">Skip</button>\n      </ion-buttons>\n    </ion-slide>\n\n    <ion-slide (ionSlideDidChange)="onSlideChangeStart($event)">\n        <div class="headerImg">\n          <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n        </div>\n      <img src="assets/imgs/logo.png" class="slide-image" />\n      <h2 class="slide-title">The City Of My Choice</h2>\n      <p>Slide 2 Description</p>\n      <div class="choiceButtons">\n        <div class="buttonLeft">\n          <button ion-button outline (click)="nextSlide()" class="citySelect">Select City</button>\n        </div>\n        <div class="buttonLeft">\n          <button ion-button outline (click)="getLocation()" class="nearMe">Near Me</button>\n        </div>\n      </div>\n    </ion-slide>\n\n    <ion-slide (ionSlideWillChange)="onSlideChangeStart($event)">\n        <div class="headerImg">\n          <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n        </div>\n        <ion-item>\n          <ion-list *ngFor="let city of ciltyList; let i=index" (click)="select(i,\'city\')">\n            <div class="cityName">\n                <div class="customCheckBox">\n                    <div class="bigDot">\n                        <div [ngClass]="{\'smallDot\': city.selected == true}" class="smallDot"></div>\n                      </div>\n                </div>\n                <div class="text">\n                    {{city.name}}\n                </div>\n            </div>\n          </ion-list>\n        </ion-item>\n    </ion-slide>\n\n    <ion-slide>\n      <div class="headerImg">\n        <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n      </div>\n      <img src="assets/imgs/logo.png" class="slide-image" />\n      <h2 class="slide-title">There\'s Room for Everyone</h2>\n      <p>Slide description goes here</p>\n\n      <button ion-button outline (click)="nextSlide()" class="nextSlides typology">Select Typology</button>\n\n    </ion-slide>\n\n    <ion-slide (ionSlideWillChange)="onSlideChangeStart($event)">\n        <div class="headerImg">\n          <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n        </div>\n        <ion-item>\n          <ion-list *ngFor="let typology of typologyList; let i=index" (click)="select(i,\'typology\')">\n            <div class="cityName">\n                <div class="customCheckBox">\n                    <div class="bigDot">\n                        <div [ngClass]="{\'smallDot\': typology.selected == true}" class="smallDot"></div>\n                      </div>\n                </div>\n                <div class="text">\n                    {{typology.name}}\n                </div>\n            </div>\n          </ion-list>\n        </ion-item>\n    </ion-slide>\n\n    <ion-slide>\n      <div class="headerImg">\n        <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n      </div>\n      <img src="assets/imgs/logo.png" class="slide-image" />\n      <h2 class="slide-title">It\'s Now or Later</h2>\n      <p>Slide description goes here</p>\n\n      <div class="readyOrWait">\n        <div class="wait" (click)="select(\'\',\'ready\',\'wait\')">\n            <div class="customCheckBox moveIn">\n                <div class="bigDot inDot">\n                    <div [ngClass]="{\'smallDot\': ready.wait == true}" class="smallDot"></div>\n                  </div>\n            </div>\n            <div class="text rowText">\n                Ready To Wait\n            </div>\n        </div>\n        <div class="ready" (click)="select(\'\',\'ready\',\'move\')">\n            <div class="customCheckBox moveIn">\n                <div class="bigDot inDot">\n                    <div [ngClass]="{\'smallDot\': ready.move == true}" class="smallDot"></div>\n                  </div>\n            </div>\n            <div class="text rowText">\n                Ready To Move-In\n            </div>\n        </div>\n      </div>\n\n    </ion-slide>\n\n    <ion-slide>\n      <div class="headerImg">\n        <img src="../../assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n      </div>\n      <img src="assets/imgs/logo.png" class="slide-image" />\n      <h2 class="slide-title">Know My Family</h2>\n      <p>Slide description goes here</p>\n\n      <button ion-button outline (click)="goToPage(\'KnowMePage\')" class="nextSlides typology">Know Me Better</button>\n\n    </ion-slide>\n\n<!--     <ion-slide (ionSlideWillChange)="onSlideChangeStart($event)" class="cusSli">\n\n    </ion-slide> -->\n\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/tutorial/tutorial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__["a" /* ToasterProvider */]])
    ], TutorialPage);
    return TutorialPage;
}());

//# sourceMappingURL=tutorial.js.map

/***/ })

});
//# sourceMappingURL=16.js.map