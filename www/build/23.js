webpackJsonp([23],{

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KnowMePageModule", function() { return KnowMePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__know_me__ = __webpack_require__(743);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var KnowMePageModule = /** @class */ (function () {
    function KnowMePageModule() {
    }
    KnowMePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__know_me__["a" /* KnowMePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__know_me__["a" /* KnowMePage */]),
            ],
        })
    ], KnowMePageModule);
    return KnowMePageModule;
}());

//# sourceMappingURL=know-me.module.js.map

/***/ }),

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KnowMePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the KnowMePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var KnowMePage = /** @class */ (function () {
    function KnowMePage(navCtrl, navParams, fb, menu) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.menu = menu;
        this.epattern = new RegExp("[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}");
        this.onlyAlphabet = new RegExp("^[a-zA-Z ]+$");
        this.onlyNumber = new RegExp("^[0-9]+$");
        this.gender = {
            male: true,
            female: false
        };
        this.status = {
            single: true,
            married: false
        };
    }
    KnowMePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad KnowMePage');
    };
    KnowMePage.prototype.ngOnInit = function () {
        this.menu.swipeEnable(false);
        this.knowMeForm = this.fb.group({
            fullName: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.onlyAlphabet)
            ]),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.epattern)
            ]),
            mobile: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.onlyNumber),
            ]),
            hobby: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ]),
            leisureActivity: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ]),
            nOfKids: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', []),
            school: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', []),
            familyIncome: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', []),
            favHangout: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [])
        });
    };
    Object.defineProperty(KnowMePage.prototype, "f", {
        get: function () {
            return this.knowMeForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    KnowMePage.prototype.checkBox = function (type, value) {
        if (type == "Gender") {
            (value == "M") ? this.gender = {
                male: true,
                female: false
            } : this.gender = {
                male: false,
                female: true
            };
        }
        else if (type == "mStatus") {
            (value == "S") ? this.status = {
                single: true,
                married: false
            } : this.status = {
                single: false,
                married: true
            };
        }
    };
    KnowMePage.prototype.submit = function () {
        var formValue = this.knowMeForm.value;
        (this.gender.male) ? formValue["gender"] = "M" : formValue["gender"] = "F";
        (this.status.single) ? formValue["status"] = "S" : formValue["status"] = "M";
        console.log(formValue);
    };
    KnowMePage.prototype.goToLogin = function () {
        this.navCtrl.setRoot('HomePage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    KnowMePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-know-me',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/know-me/know-me.html"*/'<!--\n  Generated template for the KnowMePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>know-me</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content padding>\n  <div class="formsKnow">\n    <div class="headerImg">\n      <img src="assets/imgs/godrej_properties.jpeg" class="himage" alt="">\n    </div>\n    <form class="knowMeForm" [formGroup]="knowMeForm">\n      <ion-list>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Name *\n          </ion-label>\n          <ion-input type="text" name="fullName" formControlName="fullName" ></ion-input>\n        </ion-item>\n        <div class="error">\n            <div *ngIf="f.fullName.errors?.required && f.fullName.touched">FullName is required</div>\n            <div *ngIf="f.fullName.errors?.pattern && f.fullName.touched">Only aplhabets</div>\n        </div>\n\n        <ion-item>\n          <ion-label floating>\n            <ion-icon name="ios-mail-outline"></ion-icon>\n            &nbsp; Email *\n          </ion-label>\n          <ion-input type="email" name="email" formControlName="email"></ion-input>\n        </ion-item>\n        <div class="error">\n            <div *ngIf="f.email.errors?.required && f.email.touched">Email is required</div>\n            <div *ngIf="f.email.errors?.pattern && f.email.touched">Not a valid email</div>\n        </div>\n\n        <ion-item>\n          <ion-label floating >\n            <ion-icon name="ios-phone-portrait-outline"></ion-icon>\n              &nbsp; Mobile Number *\n          </ion-label>\n          <ion-input type="tel" name="mobile" formControlName="mobile"></ion-input>\n        </ion-item>\n        <div class="error">\n            <div *ngIf="f.mobile.errors?.required && f.mobile.touched">Mobile is required</div>\n            <div *ngIf="f.mobile.errors?.pattern && f.mobile.touched">Only numbers</div>\n            <!-- <div *ngIf="(f.mobile.errors?.minlength || f.mobile.errors?.maxlength) && f.mobile.touched">Mobile Number should be 10 digits</div> -->\n        </div>\n\n        <ion-item>\n          <div class="genderMaritial">\n            <!-- <div class="gMSta left"> -->\n                <div class="icon">\n\n                    &nbsp; Gender\n                </div>\n                <div class="customCheckBox" (click)="checkBox(\'Gender\',\'M\')">\n                    <div class="bigDot sliderCheckbox">\n                        <div [ngClass]="{\'smallDot\': gender.male == true}" class="smallDot"></div>\n                      </div>\n                    <div class="text sliderText">\n                        &nbsp; Male\n                    </div>\n                </div>\n                <div class="customCheckBox left-50" (click)="checkBox(\'Gender\',\'F\')">\n                    <div class="bigDot sliderCheckbox">\n                      <div [ngClass]="{\'smallDot\': gender.female == true}"></div>\n                    </div>\n                    <div class="text sliderText">\n                        &nbsp; Female\n                    </div>\n                </div>\n            <!--</div>\n\n             <div class="gMSta right"> -->\n               <div class="d-block"></div>\n                <div class="icon">\n\n                    &nbsp; Maritial Status\n                </div>\n                <div class="customCheckBox" (click)="checkBox(\'mStatus\',\'S\')">\n                    <div class="bigDot sliderCheckbox">\n                        <div [ngClass]="{\'smallDot\': status.single == true}" class="smallDot"></div>\n                      </div>\n                    <div class="text sliderText">\n                        &nbsp; Single\n                    </div>\n                </div>\n                <div class="customCheckBox left-50" (click)="checkBox(\'mStatus\',\'M\')">\n                    <div class="bigDot sliderCheckbox">\n                      <div [ngClass]="{\'smallDot\': status.married == true}"></div>\n                    </div>\n                    <div class="text sliderText">\n                        &nbsp; Married\n                    </div>\n                </div>\n            <!-- </div> -->\n          </div>\n\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Hobby *\n          </ion-label>\n          <ion-select formControlName="hobby">\n            <ion-option value="xbox">Xbox</ion-option>\n            <ion-option value="ps4s">PlayStation 4 Slim</ion-option>\n            <ion-option value="xboxones">Xbox One S</ion-option>\n          </ion-select>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Leisure Activity *\n          </ion-label>\n          <ion-select formControlName="leisureActivity">\n              <ion-option value="xbox">Xbox</ion-option>\n              <ion-option value="ps4s">PlayStation 4 Slim</ion-option>\n              <ion-option value="xboxones">Xbox One S</ion-option>\n          </ion-select>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Number Of Kids\n          </ion-label>\n          <ion-input type="text" name="fname" formControlName="nOfKids" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; School\n          </ion-label>\n          <ion-input type="text" name="fname" formControlName="school" ></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Family Income\n          </ion-label>\n          <ion-select formControlName="familyIncome">\n            <ion-option value="xbox">Xbox</ion-option>\n            <ion-option value="ps4s">PlayStation 4 Slim</ion-option>\n            <ion-option value="xboxones">Xbox One S</ion-option>\n          </ion-select>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; Favorite Hangout Place\n          </ion-label>\n          <ion-input type="text" name="favHangout" formControlName="favHangout" ></ion-input>\n        </ion-item>\n\n        <button ion-button icon-end large clear (click)="submit()" class="knowBtn">\n          Submit\n        </button>\n        <button ion-button outline (click)="goToLogin()"> Go To Login</button>\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/know-me/know-me.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */]])
    ], KnowMePage);
    return KnowMePage;
}());

//# sourceMappingURL=know-me.js.map

/***/ })

});
//# sourceMappingURL=23.js.map