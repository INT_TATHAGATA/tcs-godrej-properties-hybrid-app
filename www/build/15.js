webpackJsonp([15],{

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VarifyOtpPageModule", function() { return VarifyOtpPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__varify_otp__ = __webpack_require__(757);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_auth__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VarifyOtpPageModule = /** @class */ (function () {
    function VarifyOtpPageModule() {
    }
    VarifyOtpPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__varify_otp__["a" /* VarifyOtpPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__varify_otp__["a" /* VarifyOtpPage */]),
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_apis_auth__["a" /* AuthProvider */]]
        })
    ], VarifyOtpPageModule);
    return VarifyOtpPageModule;
}());

//# sourceMappingURL=varify-otp.module.js.map

/***/ }),

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VarifyOtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_db__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the VarifyOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VarifyOtpPage = /** @class */ (function () {
    function VarifyOtpPage(navCtrl, navParams, fb, storageProvider, events, Authapi, loader, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.storageProvider = storageProvider;
        this.events = events;
        this.Authapi = Authapi;
        this.loader = loader;
        this.toast = toast;
        this.previousPage = "";
    }
    VarifyOtpPage.prototype.ionViewDidLoad = function () {
        // debugger
        console.log('ionViewDidLoad VarifyOtpPage');
        this.registerData = this.navParams.get("formData");
    };
    VarifyOtpPage.prototype.ngOnInit = function () {
        //debugger
        this.previousPage = this.navCtrl.getPrevious().id;
        this.verifyForm = this.fb.group({
            first: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(1)
            ]),
            second: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(1)
            ]),
            third: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(1)
            ]),
            fourth: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(1)
            ]),
            fifth: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(1)
            ]),
            sixth: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(1),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(1)
            ])
        });
    };
    VarifyOtpPage.prototype.checkAndMove = function (event, element, input, nextEl) {
        if (this.verifyForm.controls[input].status == "VALID") {
            if (nextEl) {
                if (this.verifyForm.get(nextEl).value == "") {
                    element.focus();
                }
            }
        }
        else {
            this.verifyForm.get(input).setValue("");
        }
    };
    VarifyOtpPage.prototype.checkVerification = function () {
        var _this = this;
        var otp = this.verifyForm.value.first + this.verifyForm.value.second + this.verifyForm.value.third + this.verifyForm.value.fourth + this.verifyForm.value.fifth + this.verifyForm.value.sixth;
        if (Number(otp) == this.registerData.response_otp) {
            /* Submit Registration Data */
            var postData = {
                first_name: this.registerData.first_name,
                last_name: this.registerData.last_name,
                email: this.registerData.email,
                mob_no: this.registerData.mob_no,
                password: this.registerData.password,
                device_type: this.registerData.device_type,
                device_id: this.registerData.uuid
            };
            this.loader.show('Please wait...');
            this.Authapi.postRegistrationapi(postData).subscribe(function (response) {
                _this.loader.hide();
                if (response.status == "200") {
                    if (_this.previousPage == "ForgotPasswordPage") {
                        _this.navCtrl.push("HomePage");
                    }
                    else {
                        /* this.navCtrl.setRoot("DashboardPage", {}, {
                          animate: true,
                          direction: 'forward'
                        }); */
                        var data = { title: 'Godrej Home', component: 'GodrejHomePage' };
                        _this.events.publish('afterLoginRedirect', data);
                        _this.storageProvider.set("isLoggedIn", "1");
                        _this.storageProvider.set("userData", JSON.stringify(response));
                        //this.navCtrl.push("DashboardPage");
                    }
                }
                else {
                    _this.toast.show(response.msg);
                }
            }, function (error) {
                console.log(error);
            });
        }
        else {
            this.toast.show('Otp does not match');
        }
    };
    VarifyOtpPage.prototype.resendOtp = function () {
        var _this = this;
        if (this.previousPage == "ForgotPasswordPage") {
            this.loader.show('Please wait...');
            this.Authapi.postForgotPasswordApi(this.registerData.postd).subscribe(function (response) {
                if (response.status == "200") {
                    _this.toast.show(response.msg);
                    _this.registerData.response_otp = response.otp;
                }
                else {
                    _this.toast.show(response.msg);
                }
            }, function (error) {
                console.log(error);
            });
        }
        else {
            var data = {
                mobile_no: this.registerData.mob_no,
                email: this.registerData.email
            };
            this.loader.show('Please wait...');
            this.Authapi.postRegisterOtpApi(data).subscribe(function (response) {
                _this.loader.hide();
                if (response.status == "200") {
                    var postData = {
                        first_name: _this.registerData.first_name,
                        last_name: _this.registerData.last_name,
                        email: _this.registerData.email,
                        mob_no: _this.registerData.mob_no,
                        password: _this.registerData.password,
                        device_type: _this.registerData.device_type,
                        device_id: _this.registerData.device_id,
                        response_otp: response.otp
                    };
                    _this.registerData = postData;
                }
                else {
                    _this.toast.show(response.msg);
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    VarifyOtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-varify-otp',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/varify-otp/varify-otp.html"*/'<!--\n  Generated template for the VarifyOtpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Verify Account</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <div class="text">\n        Please enter the correct OTP\n      <!-- An OTP has been sent to your Mobile Number and Email Id -->\n    </div>\n\n    <div class="otp">\n      <form [formGroup]="verifyForm" class="verifyInput">\n        <div class="optinput">\n          <input type="text" (keyup)="checkAndMove($event,secondField,\'first\',\'second\')" formControlName="first">\n          <input type="text" #secondField class="second" (keyup)="checkAndMove($event,thirdField,\'second\',\'third\')" formControlName="second">\n          <input type="text" #thirdField class="second" (keyup)="checkAndMove($event,fourthField,\'third\',\'fourth\')" formControlName="third">\n          <input type="text" #fourthField class="second" (keyup)="checkAndMove($event,fifthField,\'fourth\',\'fifth\')" formControlName="fourth">\n          <input type="text" #fifthField class="second" (keyup)="checkAndMove($event,sixthField,\'fifth\',\'sixth\')" formControlName="fifth">\n          <input type="text" #sixthField class="second" (keyup)="checkAndMove($event,sixthField,\'sixth\',\'\')" formControlName="sixth">\n        </div>\n       <div class="d-block"></div>\n          <button ion-button round [disabled]="verifyForm.invalid" class="vBtn" (click)="checkVerification()">\n            Verify <ion-icon name="ios-arrow-forward-outline" class="vIcon"></ion-icon>\n          </button>\n       </form>\n       <div class="text resendText">\n         If you don\'t receive an OTP! <span class="resend" (click)="resendOtp()">Resend</span>\n       </div>\n       <img src="assets/imgs/godrej_properties.jpeg" class="propertiesImg" alt="">\n\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/varify-otp/varify-otp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]])
    ], VarifyOtpPage);
    return VarifyOtpPage;
}());

//# sourceMappingURL=varify-otp.js.map

/***/ })

});
//# sourceMappingURL=15.js.map