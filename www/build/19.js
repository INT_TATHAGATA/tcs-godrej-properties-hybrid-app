webpackJsonp([19],{

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(752);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_auth__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_apis_auth__["a" /* AuthProvider */]]
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_db__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, fb, storageProvider, modalCtrl, Authapi, loader, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.storageProvider = storageProvider;
        this.modalCtrl = modalCtrl;
        this.Authapi = Authapi;
        this.loader = loader;
        this.toast = toast;
        this.gender = {
            male: true,
            female: false
        };
        this.temsCheck = true;
        this.paswordReg = "^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{6,20})";
        this.disableButton = true;
        this.fName = false;
        this.notMatched = false;
        this.openClose = false;
        this.openCloseConf = true;
        this.typeText = "password";
        this.confType = "text";
        this.countryCodes = [];
        this.gender = {
            male: true,
            female: false
        };
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        this.gender = {
            male: true,
            female: false
        };
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.ngOnInit = function () {
        var _this = this;
        /* Signup form initialization */
        this.deviceinfo = this.storageProvider.getDeviceInfo();
        this.countryCodes = this.storageProvider.getCountryCodes();
        this.findIndia = this.countryCodes.findIndex(function (x) { return x.callingCodes === "91"; });
        this.signUpForm = this.fb.group({
            fname: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.storageProvider.onlyAlphabetPattern())
            ]),
            lname: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.storageProvider.onlyAlphabetPattern())
            ]),
            countryCode: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.countryCodes[this.findIndia].callingCodes, [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ]),
            mobile: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.storageProvider.onlyNumberPattern()),
            ]),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.storageProvider.emailPattern())
            ]),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.paswordReg)
            ]),
            cpassword: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6)
            ])
        });
        this.signUpForm.valueChanges.subscribe(function (v) {
            if (_this.temsCheck && _this.signUpForm.valid) {
                _this.disableButton = false;
            }
            _this.notMatched = false;
            if (v.password != v.cpassword) {
                _this.notMatched = true;
                _this.disableButton = true;
            }
        });
        /* Select India By Default  */
        this.onChange(this.countryCodes[this.findIndia]);
    };
    SignupPage.prototype.checkVerification = function () {
        var _this = this;
        var data = {
            mobile_no: this.signUpForm.value.countryCode + this.signUpForm.value.mobile,
            email: this.signUpForm.value.email
        };
        this.loader.show('Please wait...');
        this.Authapi.postRegisterOtpApi(data).subscribe(function (response) {
            _this.loader.hide();
            if (response.status == "200") {
                var postData = {
                    first_name: _this.signUpForm.value.fname,
                    last_name: _this.signUpForm.value.lname,
                    email: _this.signUpForm.value.email,
                    mob_no: _this.signUpForm.value.countryCode + _this.signUpForm.value.mobile,
                    password: _this.signUpForm.value.password,
                    device_type: "Android",
                    device_id: _this.deviceinfo.uuid,
                    response_otp: response.otp
                };
                _this.navCtrl.push("VarifyOtpPage", { formData: postData });
            }
            else {
                _this.toast.show(response.msg);
            }
        }, function (error) {
            console.log(error);
        });
    };
    SignupPage.prototype.register = function () {
    };
    Object.defineProperty(SignupPage.prototype, "f", {
        get: function () {
            return this.signUpForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    SignupPage.prototype.checkBox = function (value) {
        (value == "M") ? this.gender = {
            male: true,
            female: false
        } : this.gender = {
            male: false,
            female: true
        };
    };
    SignupPage.prototype.checkboxTerms = function () {
        this.temsCheck = !this.temsCheck;
        this.disableButton = true;
        if (this.temsCheck && this.signUpForm.valid) {
            this.disableButton = false;
        }
    };
    SignupPage.prototype.loginPage = function () {
        this.navCtrl.popToRoot();
    };
    SignupPage.prototype.eyeOpenClose = function (value, type) {
        if (value == "O") {
            if (type == "pass") {
                this.openClose = false;
                this.typeText = "password";
            }
            else {
                this.openCloseConf = false;
                this.confType = "password";
            }
        }
        else {
            if (type == "pass") {
                this.openClose = true;
                this.typeText = "text";
            }
            else {
                this.openCloseConf = true;
                this.confType = "text";
            }
        }
    };
    SignupPage.prototype.onChange = function (event) {
        this.flag = event.flag;
    };
    SignupPage.prototype.openTerms = function () {
        this.addModal = this.modalCtrl.create("TermsConditionPage", {});
        this.addModal.onDidDismiss(function (item) {
        });
        this.addModal.present();
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/signup/signup.html"*/'<!--\n  Generated template for the SignupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Signup</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="headreImg">\n    <img src="assets/imgs/godrej_properties.jpeg" class="propertiesImg" alt="">\n  </div>\n\n  <div class="formSignUp">\n    <form class="loginForm" [formGroup]="signUpForm">\n      <ion-list>\n\n        <ion-item>\n          <ion-label floating>\n              <ion-icon name="ios-person-outline"></ion-icon>\n              &nbsp; First Name *\n          </ion-label>\n          <ion-input type="text" name="fname" formControlName="fname" ></ion-input>\n        </ion-item>\n        <div class="error">\n            <div *ngIf="f.fname.errors?.required && f.fname.touched">First Name is required</div>\n            <div *ngIf="f.fname.errors?.pattern && f.fname.touched">Only aplhabets</div>\n        </div>\n\n\n        <ion-item>\n          <ion-label floating>\n            <ion-icon name="ios-person-outline"></ion-icon>\n            &nbsp; Last Name *\n          </ion-label>\n          <ion-input type="text" name="lname" formControlName="lname"></ion-input>\n        </ion-item>\n        <div class="error">\n            <div *ngIf="f.lname.errors?.required && f.lname.touched">Last Name is required</div>\n            <div *ngIf="f.lname.errors?.pattern && f.lname.touched">Only aplhabets</div>\n        </div>\n\n        <!-- <ion-item>\n          <div class="gender">\n              <div class="icon">\n                <ion-icon name="ios-transgender-outline"></ion-icon>\n                  &nbsp; Gender\n              </div>\n              <div class="customCheckBox" (click)="checkBox(\'M\')">\n                  <div class="bigDot">\n                      <div [ngClass]="{\'smallDot\': gender.male == true}" class="smallDot"></div>\n                    </div>\n                  <div class="text">\n                      &nbsp; Male\n                  </div>\n              </div>\n              <div class="customCheckBox left-50" (click)="checkBox(\'F\')">\n                  <div class="bigDot">\n                    <div [ngClass]="{\'smallDot\': gender.female == true}"></div>\n                  </div>\n                  <div class="text">\n                      &nbsp; Female\n                  </div>\n              </div>\n          </div>\n        </ion-item> -->\n        <div class="twonfild">\n          <div class="mobile-countrycode">\n            <div class="imageFlag">\n              <img src="{{flag}}" *ngIf="!(f.countryCode.errors?.required && f.countryCode.touched)" alt="">\n            </div>\n            <div class="cntNumber">\n                <ion-item>\n                  <!-- <ion-label>Country Code</ion-label> -->\n                  <ion-select formControlName="countryCode">\n                    <ion-option value="{{country.callingCodes}}" (ionSelect)="onChange(country)" *ngFor="let country of countryCodes; let i=index">{{country.callingCodes}}</ion-option>\n                    </ion-select>\n                </ion-item>\n            </div>\n          </div>\n\n          <div class="phone-input">\n            <ion-item>\n              <ion-label floating >\n                <ion-icon name="ios-phone-portrait-outline"></ion-icon>\n                  &nbsp; Mobile Number *\n              </ion-label>\n              <ion-input type="tel" name="mobile" formControlName="mobile"></ion-input>\n            </ion-item>\n          </div>\n        </div>\n\n        <div class="error">\n            <div *ngIf="f.countryCode.errors?.required && f.countryCode.touched">Countrycode is required</div>\n            <div *ngIf="f.mobile.errors?.required && f.mobile.touched">Mobile is required</div>\n            <div *ngIf="f.mobile.errors?.pattern && f.mobile.touched">Only numbers</div>\n            <!-- <div *ngIf="(f.mobile.errors?.minlength || f.mobile.errors?.maxlength) && f.mobile.touched">Mobile Number should be 10 digits</div> -->\n        </div>\n\n        <ion-item>\n          <ion-label floating>\n            <ion-icon name="ios-mail-outline"></ion-icon>\n            &nbsp; Email *\n          </ion-label>\n          <ion-input type="email" name="email" formControlName="email"></ion-input>\n        </ion-item>\n        <div class="error">\n            <div *ngIf="f.email.errors?.required && f.email.touched">Email is required</div>\n            <div *ngIf="f.email.errors?.pattern && f.email.touched">Not a valid email</div>\n        </div>\n        <ion-item class="paswrd">\n          <ion-label floating>\n            <ion-icon name="ios-mail-outline"></ion-icon>\n            &nbsp; Password *\n          </ion-label>\n          <ion-input type="{{typeText}}" name="password" formControlName="password"></ion-input>\n        </ion-item>\n        <div class="eyeIcons">\n          <div class="off" (click)="eyeOpenClose(\'O\',\'pass\')" *ngIf="openClose">\n            <ion-icon name="eye-off" ></ion-icon>\n          </div>\n          <div class="eyeOpen" (click)="eyeOpenClose(\'C\',\'pass\')" *ngIf="!openClose">\n            <ion-icon name="eye"></ion-icon>\n          </div>\n        </div>\n        <div class="error">\n          <div *ngIf="f.password.errors?.required && f.password.touched">Password is required</div>\n          <div *ngIf="f.password.errors?.pattern && f.password.touched">\n            1 Uppercase, 1 Lowercase, 1 special charecter with length min 6 to max 20\n          </div>\n        </div>\n        <ion-item class="paswrd">\n          <ion-label floating>\n            <ion-icon name="ios-mail-outline"></ion-icon>\n            &nbsp; Confirm Password *\n          </ion-label>\n          <ion-input type="{{confType}}" name="cpassword" formControlName="cpassword"></ion-input>\n        </ion-item>\n        <div class="eyeIcons">\n          <div class="off" (click)="eyeOpenClose(\'O\',\'conf\')" *ngIf="openCloseConf">\n            <ion-icon name="eye-off" ></ion-icon>\n          </div>\n          <div class="eyeOpen" (click)="eyeOpenClose(\'C\',\'conf\')" *ngIf="!openCloseConf">\n            <ion-icon name="eye"></ion-icon>\n          </div>\n        </div>\n\n        <div class="error">\n            <div *ngIf="f.cpassword.errors?.required && f.cpassword.touched">Confirm password is required</div>\n            <div *ngIf="f.cpassword.errors?.minlength && f.cpassword.touched">\n              6 characters long\n            </div>\n            <div *ngIf="notMatched">\n              Not Matched\n            </div>\n        </div>\n\n        <div class="terms_checkbox">\n          <div class="icon_checkbox">\n              <ion-icon name="md-checkmark-circle-outline" [ngClass]="{\'chechbox\': temsCheck, \'notchecked\': temsCheck == false }" (click)="checkboxTerms()"></ion-icon>\n          </div>\n          <div class="terms_condition">\n              <span class="text_terms">\n                  By register I accept <span class="termsOfUse" (click)="openTerms()"> Terms Of Use</span>\n              </span>\n          </div>\n        </div>\n\n        <button ion-button round class="width-100" [disabled]="disableButton" (click)="checkVerification()">Create Account</button>\n\n      </ion-list>\n    </form>\n    <div class="terms_checkbox login">\n      Already have an account ? <span class="have-account" (click)="loginPage()">Login</span>\n    </div>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

});
//# sourceMappingURL=19.js.map