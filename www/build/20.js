webpackJsonp([20],{

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search__ = __webpack_require__(751);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchPageModule = /** @class */ (function () {
    function SearchPageModule() {
    }
    SearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */]),
            ],
        })
    ], SearchPageModule);
    return SearchPageModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams, viewCtrl, storagePrivider, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.storagePrivider = storagePrivider;
        this.events = events;
        this.location = "";
        this.possetionList = [];
        this.cityList = [];
        this.typologyList = [];
        this.searchValues = [];
        events.subscribe('closeSearchModal', function () {
            _this.myDismiss('d');
        });
        this.commonClas = {
            residential: true,
            commertial: false
        };
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.resOrCom("");
        this.location = "Kolkata";
        //debugger
        if (this.navParams.data.params) {
            var editValue = this.storagePrivider.getValue();
            if (editValue.length > 0) {
                var getCityList = this.storagePrivider.getCityList();
                var findSelected = getCityList.findIndex(function (X) { return X.selected == true; });
                if (findSelected > -1) {
                    getCityList[findSelected].selected = false;
                }
                var newSelectedCity = getCityList.findIndex(function (X) { return X.name == _this.navParams.data.params[1]; });
                getCityList[newSelectedCity].selected = true;
                this.cityList = getCityList;
                var getpossessioList = this.storagePrivider.getPossessionList();
                var findSelectedPoss = getpossessioList.findIndex(function (X) { return X.selected == true; });
                if (findSelectedPoss > -1) {
                    getpossessioList[findSelectedPoss].selected = false;
                }
                var newSelectedPosse = getpossessioList.findIndex(function (X) { return X.name == _this.navParams.data.params[2]; });
                getpossessioList[newSelectedPosse].selected = true;
                this.possetionList = getpossessioList;
                var gettypoloList = this.storagePrivider.getTypologyList();
                var findSelectedtypo = gettypoloList.findIndex(function (X) { return X.selected == true; });
                if (findSelectedtypo > -1) {
                    gettypoloList[findSelectedtypo].selected = false;
                }
                var newSelectedTypo = gettypoloList.findIndex(function (X) { return X.name == _this.navParams.data.params[3]; });
                gettypoloList[newSelectedTypo].selected = true;
                this.typologyList = gettypoloList;
                /*         let price=this.navParams.data.params[4];
                        let lower=price.split('-')[0].split(' ')[0];
                        let upper=price.split('-')[1].split(' ')[0]; */
                this.goalProgress = this.storagePrivider.getPrice();
                /*
                this.goalProgress=this.navParams.data.params.price; */
            }
            else {
                this.cityList = this.navParams.data.params.cityLocality;
                this.possetionList = this.navParams.data.params.possession;
                this.typologyList = this.navParams.data.params.typology;
                this.goalProgress = this.navParams.data.params.price;
            }
        }
        else {
            this.cityList = this.storagePrivider.getCityList();
            this.possetionList = this.storagePrivider.getPossessionList();
            this.typologyList = this.storagePrivider.getTypologyList();
            this.goalProgress = this.storagePrivider.getPrice();
        }
        this.change();
    };
    SearchPage.prototype.myDismiss = function (value) {
        this.viewCtrl.dismiss(this.storagePrivider.getValue());
        if (value == "c") {
            this.events.publish('openSearchListPage', "SearchListPage");
        }
    };
    SearchPage.prototype.change = function () {
        this.lower = (this.goalProgress.lower >= 100) ? (this.goalProgress.lower / 100).toFixed(2) + " Cr" : (this.goalProgress.lower) + " Lkh";
        this.upper = (this.goalProgress.upper >= 100) ? (this.goalProgress.upper / 100).toFixed(2) + " Cr" : (this.goalProgress.upper) + " Lkh";
    };
    SearchPage.prototype.resOrCom = function (value) {
        (value == "R") ? this.commonClas = {
            residential: true,
            commertial: false
        } : this.commonClas = {
            residential: false,
            commertial: true
        };
    };
    SearchPage.prototype.click = function (pageName) {
        this.navCtrl.push(pageName);
    };
    SearchPage.prototype.getSelected = function (type, index) {
        //debugger
        if (type == "city") {
            this.findCitySelected = this.cityList.findIndex(function (x) { return x.selected === true; });
            (this.findCitySelected > -1) ? this.cityList[this.findCitySelected].selected = false : "";
            this.cityList[index].selected = true;
        }
        else if (type == "possession") {
            var findPossessionSelected = this.possetionList.findIndex(function (x) { return x.selected === true; });
            (findPossessionSelected > -1) ? this.possetionList[findPossessionSelected].selected = false : "";
            this.possetionList[index].selected = true;
        }
        else if (type == "typology") {
            var findTypologySelected = this.typologyList.findIndex(function (x) { return x.selected === true; });
            (findTypologySelected > -1) ? this.typologyList[findTypologySelected].selected = false : "";
            this.typologyList[index].selected = true;
        }
    };
    SearchPage.prototype.search = function () {
        var findCitySelected = this.cityList.findIndex(function (x) { return x.selected === true; });
        var findPossessionSelected = this.possetionList.findIndex(function (x) { return x.selected === true; });
        var findTypologySelected = this.typologyList.findIndex(function (x) { return x.selected === true; });
        var lPrice = (this.goalProgress.lower >= 100) ? (this.goalProgress.lower / 100).toFixed(2) + " Cr" : (this.goalProgress.lower) + " Lkh";
        var uPrice = (this.goalProgress.upper >= 100) ? (this.goalProgress.upper / 100).toFixed(2) + " Cr" : (this.goalProgress.upper) + " Lkh";
        this.searchValues = [
            this.location,
            this.cityList[findCitySelected].name,
            this.possetionList[findPossessionSelected].name,
            this.typologyList[findTypologySelected].name,
            lPrice + "-" + uPrice
        ];
        this.storagePrivider.setcityList(this.cityList);
        this.storagePrivider.setPossessionList(this.possetionList);
        this.storagePrivider.setTypologyList(this.typologyList);
        this.storagePrivider.setPrice(this.goalProgress);
        this.storagePrivider.setValue(this.searchValues);
        this.myDismiss('d');
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-header>\n\n    <ion-navbar class="searchclose">\n      <ion-title>\n        <button (click)="myDismiss(\'c\')" class="close_btn">\n            <!-- <ion-icon name="ios-arrow-round-back-outline"></ion-icon> -->\n            <ion-icon name="close"></ion-icon>\n        </button> Search </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n</ion-header>\n\n\n<ion-content class="noPadding searchbox">\n  <div class="resCom">\n    <div class="resLeft commonClass" [ngClass]="{\'selected\': commonClas.residential == true}" (click)="resOrCom(\'R\')">\n      <span>Residential</span>\n    </div>\n    <div class="comRight commonClass" [ngClass]="{\'selected\': commonClas.commertial == true}" (click)="resOrCom(\'C\')">\n      <span>Commercial</span>\n    </div>\n  </div>\n  <div class="textBox">\n    <div class="input">\n      <input type="text" name="" id="" class="inputBox" [(ngModel)]="location">\n      <div class="locationIcon">\n          <ion-icon name="ios-pin-outline"></ion-icon>\n      </div>\n    </div>\n    <div class="inputdrop">\n      <div>Lorem ipsum dolor sit amet consectetur adipisicing elit.</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n    </div>\n  </div>\n  <div class="locality">\n    <img src="assets/imgs/locality.png">\n    Locality <!-- <button ion-button outline (click)="click(\'InventoryBookingPage\')"></button> -->\n  </div>\n  <div class="localValues">\n      <div class="upperDiv">\n        <div class="margin-lR innerDiv" *ngFor="let city of cityList; let i=index" >\n            <button ion-button color="light" round class="roundValues" (click)="getSelected(\'city\',i)" [ngClass]="{\'selectedBackGround\': city.selected == true}"> {{city.name}} </button>\n\n            <span class="closeit">\n                <ion-icon name="close"></ion-icon>\n            </span>\n            <input *ngIf="city.selected == true" type="hidden" name="" #focus>\n        </div>\n      </div>\n  </div>\n  <div class="locality">\n    <img src="assets/imgs/possession.png">\n    Possession\n  </div>\n  <div class="localValues">\n    <div class="upperDiv">\n      <div class="margin-lR innerDiv" *ngFor="let possession of possetionList; let i=index">\n          <button ion-button color="light" round class="roundValues" (click)="getSelected(\'possession\',i)" [ngClass]="{\'selectedBackGround\': possession.selected == true}"> {{possession.name}} </button>\n          <span class="closeit">\n              <ion-icon name="close"></ion-icon>\n          </span>\n      </div>\n    </div>\n  </div>\n  <div class="locality">\n    <img src="assets/imgs/typology.png">\n    Typology\n  </div>\n  <div class="localValues">\n    <div class="upperDiv">\n      <div class="margin-lR innerDiv" *ngFor="let typology of typologyList; let i=index">\n          <button ion-button color="light" round class="roundValues" (click)="getSelected(\'typology\',i)" [ngClass]="{\'selectedBackGround\': typology.selected == true}"> {{typology.name}} </button>\n\n          <span class="closeit">\n              <ion-icon name="close"></ion-icon>\n          </span>\n      </div>\n    </div>\n  </div>\n  <div class="locality">\n      <img class="priceicon" src="assets/imgs/priceicon.png">\n    Price\n  </div>\n  <div class="localValues">\n      <ion-item>\n        <ion-label>\n          <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;">{{lower}} -\n          <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;">{{upper}}\n        </ion-label>\n        <ion-range class="rangeNoPadd" dualKnobs="true" [(ngModel)]="goalProgress" min="30" max="250" step="1" pin="true" snaps="false" (ionChange)="change()">\n        </ion-range>\n\n      </ion-item>\n      <div class="clearfix filterrangetext">\n        <div class="filterfrom">\n          <div class="txtgrey">From</div>\n          <div class="filterprice">\n            <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;"> 25 Lakh\n          </div>\n        </div>\n\n        <div class="filterfrom float-right">\n          <div class="txtgrey float-right">From</div>\n          <div class="clearfix"></div>\n          <div class="filterprice">\n            <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;"> 25 Lakh\n          </div>\n        </div>\n\n      </div>\n  </div>\n  <div class="localValues submitButton">\n    <button ion-button round class="searchBtm" (click)="search()">Submit</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ })

});
//# sourceMappingURL=20.js.map