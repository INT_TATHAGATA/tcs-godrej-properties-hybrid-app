webpackJsonp([12],{

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyDocumentsAddPageModule", function() { return MyDocumentsAddPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_documents_add__ = __webpack_require__(940);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(762);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MyDocumentsAddPageModule = /** @class */ (function () {
    function MyDocumentsAddPageModule() {
    }
    MyDocumentsAddPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_documents_add__["a" /* MyDocumentsAddPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_documents_add__["a" /* MyDocumentsAddPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], MyDocumentsAddPageModule);
    return MyDocumentsAddPageModule;
}());

//# sourceMappingURL=my-documents-add.module.js.map

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__test_test__ = __webpack_require__(763);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__ = __webpack_require__(764);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__ = __webpack_require__(765);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__menu_menu__ = __webpack_require__(766);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import { Component } from './component/component';






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 763:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the TestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        console.log('Hello TestComponent Component');
        this.text = 'Hello World';
    }
    TestComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'test',template:/*ion-inline-start:"D:\projects\gpl\src\components\test\test.html"*/'<!-- Generated template for the TestComponent component -->\n<div>\n  {{text}}\n</div>\n'/*ion-inline-end:"D:\projects\gpl\src\components\test\test.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the HeadBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HeadBarComponent = /** @class */ (function () {
    function HeadBarComponent(events, navCtrl, navParams, storageProvider) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storageProvider = storageProvider;
        this.notificationDot = false;
        this.isGuest = this.storageProvider.isGuest();
        events.subscribe('showNotiDot', function () {
            _this.notificationDot = true;
        });
        //this.text = 'Hello World';
    }
    HeadBarComponent.prototype.toggleMenu = function () {
    };
    HeadBarComponent.prototype.openwishlistpage = function () {
        this.navCtrl.push('WishlistPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("text"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("backTrue"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "backTrue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("menuIcon"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "menuIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("image"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "image", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("noti"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "noti", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("heart"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "heart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("pin"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "pin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("hTitle"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "hTitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeading"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "subHeading", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeadingText"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "subHeadingText", void 0);
    HeadBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'head-bar',template:/*ion-inline-start:"D:\projects\gpl\src\components\head-bar\head-bar.html"*/'<!-- Generated template for the HeadBarComponent component -->\n\n<div>\n\n  <button ion-button block menuToggle *ngIf="menuIcon" style="float: left;">\n\n    <!-- <ion-icon name="ios-menu-outline"></ion-icon> -->\n\n    <img src="assets/imgs/menu-toggle.png" alt="">\n\n  </button>\n\n  <!-- <ion-icon name="ios-menu-outline" *ngIf="menuIcon" (click)="toggleMenu()"></ion-icon> -->\n\n  <div class="text" *ngIf="text"> {{hTitle}}</div>\n\n  <div class="subheading" *ngIf="subHeading">\n\n    {{subHeadingText}}\n\n  </div>\n\n  <div class="image" *ngIf="image">\n\n    <img src="assets/imgs/godrej_properties.jpeg" alt="">\n\n  </div>\n\n  <div class="iconLists" *ngIf="isGuest != \'1\'">\n\n    <ion-icon name="ios-notifications-outline" *ngIf="noti" style="font-size: 1.5em; float: right; margin-right: 15px;">\n\n      <!-- <ion-badge id="notifications-badge" color="danger"></ion-badge> -->\n\n      <span [ngClass]="{\'dot\': notificationDot == true}"></span>\n\n      <!-- <ion-badge item-end>260k</ion-badge> -->\n\n    </ion-icon>\n\n    <ion-icon name="ios-heart-outline" *ngIf="heart" style="font-size: 1.5em; float: right; margin-right: 15px;"\n\n      (click)="openwishlistpage()">\n\n      <span class="dot"></span>\n\n    </ion-icon>\n\n    <ion-icon name="ios-pin-outline" *ngIf="pin" style="font-size: 1.5em; float: right; margin-right: 15px;">\n\n\n\n    </ion-icon>\n\n  </div>\n\n</div>'/*ion-inline-end:"D:\projects\gpl\src\components\head-bar\head-bar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]])
    ], HeadBarComponent);
    return HeadBarComponent;
}());

//# sourceMappingURL=head-bar.js.map

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FootBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_constants__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the FootBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FootBarComponent = /** @class */ (function () {
    function FootBarComponent(events, modalCtrl, storageProvider, searchApi, loader, toast) {
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.storageProvider = storageProvider;
        this.searchApi = searchApi;
        this.loader = loader;
        this.toast = toast;
        this.searchListArr = [];
        console.log('Hello FootBarComponent Component');
    }
    FootBarComponent.prototype.openSearch = function () {
        var _this = this;
        this.loader.show('Loading');
        this.searchApi.propertySearchInfo().subscribe(function (response) {
            _this.loader.hide();
            if (response.status == __WEBPACK_IMPORTED_MODULE_4__app_app_constants__["a" /* AppConst */].HTTP_SUCESS_STATUS.OK) {
                _this.addModal = _this.modalCtrl.create("SearchPage", { responseData: response });
                _this.addModal.onDidDismiss(function (item) {
                    if (item) {
                        //this.searchListArr=item;
                    }
                });
                _this.addModal.present();
            }
            else {
                _this.toast.show(response.msg);
            }
        }, function (error) {
            _this.loader.hide();
            _this.toast.show(error.statusText);
        });
    };
    FootBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'foot-bar',template:/*ion-inline-start:"D:\projects\gpl\src\components\foot-bar\foot-bar.html"*/'<!-- Generated template for the FootBarComponent component -->\n<!-- <div class="footer_div">\n    <div>\n        <ion-icon name="ios-home-outline"></ion-icon>\n      Home\n    </div>\n    <div>Book a Visit</div>\n    <div>\n      <ion-icon name="ios-cart-outline"></ion-icon>\n      Buy Now\n    </div>\n    <div>\n      <ion-icon name="ios-call-outline"></ion-icon>\n      Support\n    </div>\n</div>\n<ion-grid>\n  <ion-row>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col></ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n  </ion-row>\n</ion-grid>-->\n\n<ion-footer class="foot">\n\n\n    <ion-grid no-padding>\n        <ion-row no-padding class="menrow">\n            <ion-col no-padding >\n                <div class="iconclass">\n                    <!-- <ion-icon name="ios-home-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/soa-icons.png" />\n                    </div>\n                    <div class="iconlabel">SOA</div>\n                    <!-- <div class="iconlabel">{{logintext}}</div> -->\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 2}">\n                    <!-- <ion-icon name="ios-call"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/service-req-icons.png" />\n                    </div>\n                    <div class="iconlabel">Service Req</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding class="searchcol" (click)="openSearch()">\n                <ion-icon name="ios-menu-outline"></ion-icon>\n                <!-- <img src="assets/imgs/MenuIcon.png" class="footer_main_menu" /> -->\n                <div class="searchicon-wrap">\n                    <div class="searchicon">\n                        <img src="assets/imgs/search-icon.png" />\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 4}">\n                    <!-- <ion-icon name="ios-cart-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/chat-icons.png" />\n                    </div>\n\n                    <div class="iconlabel">Chat Bot</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 3}">\n                    <!-- <ion-icon name="ios-call-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/support-icons.png" />\n                    </div>\n\n\n                    <div class="iconlabel">Support</div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-footer>\n'/*ion-inline-end:"D:\projects\gpl\src\components\foot-bar\foot-bar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]])
    ], FootBarComponent);
    return FootBarComponent;
}());

//# sourceMappingURL=foot-bar.js.map

/***/ }),

/***/ 766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        console.log('Hello MenuComponent Component');
        this.text = 'Hello World';
    }
    MenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu',template:/*ion-inline-start:"D:\projects\gpl\src\components\menu\menu.html"*/'<!-- Generated template for the MenuComponent component -->\n<!-- <div class="sidemenu">\n  TEST\n\n</div> -->\n'/*ion-inline-end:"D:\projects\gpl\src\components\menu\menu.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 940:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyDocumentsAddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the MyDocumentsAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyDocumentsAddPage = /** @class */ (function () {
    function MyDocumentsAddPage(navCtrl, navParams, loader, toast, storageProvider, fb, transfer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loader = loader;
        this.toast = toast;
        this.storageProvider = storageProvider;
        this.fb = fb;
        this.transfer = transfer;
        this.noti = false;
        this.heart = false;
        this.pin = false;
        this.menuIcon = false;
        this.image = false;
        this.text = true;
        this.subHeading = false;
        this.subHeadingText = "";
        this.disabled = true;
    }
    MyDocumentsAddPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MyDocumentsAddPage');
    };
    MyDocumentsAddPage.prototype.ngOnInit = function () {
        this.uploadDocuments = this.fb.group({
            docType: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* Validators */].required]),
            docTitle: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* Validators */].required]),
            docNo: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* Validators */].required])
        });
    };
    Object.defineProperty(MyDocumentsAddPage.prototype, "f", {
        get: function () {
            return this.uploadDocuments.controls;
        },
        enumerable: true,
        configurable: true
    });
    MyDocumentsAddPage.prototype.openDocs = function () {
        var _this = this;
        this.storageProvider.chooseFile().then(function (uri) {
            _this.errorMsg = '';
            _this.selectedData = uri;
            var url = __WEBPACK_IMPORTED_MODULE_7__app_app_constants__["a" /* AppConst */].baseUrl + 'create_document_api.json';
            var dataObj = '';
            var fileTransfer = _this.transfer.create();
            var options = {
                fileKey: 'myDoc',
                fileName: 'myDocuments_' + new Date().getTime(),
                chunkedMode: false,
                mimeType: "multipart/form-data",
                headers: {
                    'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
                    'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
                },
                params: {
                    user_id: localStorage.getItem('userId')
                }
            };
            fileTransfer.upload(uri.localURL, url, options).then(function (data) {
                console.log(data);
            }).catch(function (err) {
                console.log(err);
            });
            fileTransfer.onProgress(function (prog) {
                console.log(prog);
            });
        }).catch(function (error) {
            _this.errorMsg = error;
        });
    };
    MyDocumentsAddPage.prototype.submit = function () {
    };
    MyDocumentsAddPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-my-documents-add',template:/*ion-inline-start:"D:\projects\gpl\src\pages\my-documents-add\my-documents-add.html"*/'<!--\n\n  Generated template for the MyDocumentsAddPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <head-bar hTitle="Add My Documents" [text]="text" [image]="image" [noti]="noti" [heart]="heart" [pin]="pin"\n\n      [menuIcon]="menuIcon"></head-bar>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <form class="loginForm" [formGroup]="uploadDocuments">\n\n    <ion-list>\n\n      <ion-item class="user-icon">\n\n        <ion-label floating>\n\n          &nbsp; Document Type *\n\n        </ion-label>\n\n        <ion-input type="text" name="docType" formControlName="docType"></ion-input>\n\n      </ion-item>\n\n    </ion-list>\n\n    <div class="error">\n\n      <div *ngIf="f?.docType.errors?.required && f?.docType.touched">Document type is Required</div>\n\n    </div>\n\n\n\n    <ion-list>\n\n      <ion-item class="user-icon">\n\n        <ion-label floating>\n\n          &nbsp; Document Title *\n\n        </ion-label>\n\n        <ion-input type="text" name="docTitle" formControlName="docTitle"></ion-input>\n\n      </ion-item>\n\n    </ion-list>\n\n    <div class="error">\n\n      <div *ngIf="f?.docTitle.errors?.required && f?.docTitle.touched">Document title is Required</div>\n\n    </div>\n\n\n\n\n\n    <ion-list>\n\n      <ion-item class="user-icon">\n\n        <ion-label floating>\n\n          &nbsp; Document No *\n\n        </ion-label>\n\n        <ion-input type="text" name="docNo" formControlName="docNo"></ion-input>\n\n      </ion-item>\n\n    </ion-list>\n\n    <div class="error">\n\n      <div *ngIf="f?.docNo.errors?.required && f?.docNo.touched">Document number is Required</div>\n\n    </div>\n\n\n\n    <ion-list>\n\n      <!-- <ion-item class="user-icon">\n\n          <ion-label floating>\n\n            &nbsp; Document *\n\n          </ion-label>\n\n          <ion-input type="text" name="docName" formControlName="docName" [disabled]="disabled" ></ion-input>\n\n        </ion-item> -->\n\n      <div class="uploadIcon" (click)="openDocs()">\n\n        <span style="float: left; width: 100%; font-size: 10px;">\n\n          Choose file (pdf,jpg,jpeg,png,doc,docx & max file size 10 MB)\n\n        </span>\n\n        <ion-icon name="ios-folder-outline"></ion-icon>\n\n      </div>\n\n      <div class="selectedFile" style="width:70px;">\n\n        <img [src]="selectedData?.uploadFilePath" *ngIf="selectedData?.mime == \'image\'">\n\n        <img src="assets/imgs/pdf_icon.jpg" *ngIf="selectedData?.mime == \'pdf\'">\n\n        <img src="assets/imgs/word_logo.png" *ngIf="selectedData?.mime == \'word\'">\n\n      </div>\n\n    </ion-list>\n\n    <!-- <div class="error">\n\n        <div *ngIf="f?.docName.errors?.required && f?.docName.touched">Document file is Required</div>\n\n      </div> -->\n\n    <div class="errorOutput" style="color: #ff0202;">\n\n      {{errorMsg}}\n\n    </div>\n\n    <div class="submit" (click)="submit()">\n\n      <button ion-button round>\n\n        Submit\n\n      </button>\n\n    </div>\n\n\n\n  </form>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\projects\gpl\src\pages\my-documents-add\my-documents-add.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__["a" /* LoadingProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__["a" /* LoadingProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__["a" /* ToasterProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__["a" /* ToasterProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */]) === "function" && _g || Object])
    ], MyDocumentsAddPage);
    return MyDocumentsAddPage;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=my-documents-add.js.map

/***/ })

});
//# sourceMappingURL=12.js.map