webpackJsonp([11],{

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(728);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DashboardPageModule = /** @class */ (function () {
    function DashboardPageModule() {
    }
    DashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], DashboardPageModule);
    return DashboardPageModule;
}());

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__test_test__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__ = __webpack_require__(731);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__menu_menu__ = __webpack_require__(732);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import { Component } from './component/component';






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["e" /* IonicModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__test_test__["a" /* TestComponent */],
                __WEBPACK_IMPORTED_MODULE_2__head_bar_head_bar__["a" /* HeadBarComponent */],
                __WEBPACK_IMPORTED_MODULE_3__foot_bar_foot_bar__["a" /* FootBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__menu_menu__["a" /* MenuComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the TestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var TestComponent = /** @class */ (function () {
    function TestComponent() {
        console.log('Hello TestComponent Component');
        this.text = 'Hello World';
    }
    TestComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'test',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/test/test.html"*/'<!-- Generated template for the TestComponent component -->\n<div>\n  {{text}}\n</div>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/test/test.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TestComponent);
    return TestComponent;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HeadBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HeadBarComponent = /** @class */ (function () {
    function HeadBarComponent(events, navCtrl, navParams) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notificationDot = false;
        events.subscribe('showNotiDot', function () {
            _this.notificationDot = true;
        });
        //this.text = 'Hello World';
    }
    HeadBarComponent.prototype.toggleMenu = function () {
    };
    HeadBarComponent.prototype.openwishlistpage = function () {
        this.navCtrl.push('WishlistPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("text"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("backTrue"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "backTrue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("menuIcon"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "menuIcon", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("image"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "image", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("noti"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "noti", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("heart"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "heart", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("pin"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "pin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("hTitle"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "hTitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeading"),
        __metadata("design:type", Boolean)
    ], HeadBarComponent.prototype, "subHeading", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])("subHeadingText"),
        __metadata("design:type", String)
    ], HeadBarComponent.prototype, "subHeadingText", void 0);
    HeadBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'head-bar',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/head-bar/head-bar.html"*/'<!-- Generated template for the HeadBarComponent component -->\n<div>\n  <button ion-button block menuToggle *ngIf="menuIcon" style="float: left;">\n    <!-- <ion-icon name="ios-menu-outline"></ion-icon> -->\n    <img src="assets/imgs/menu-toggle.png" alt="">\n  </button>\n  <!-- <ion-icon name="ios-menu-outline" *ngIf="menuIcon" (click)="toggleMenu()"></ion-icon> -->\n  <div class="text" *ngIf="text"> {{hTitle}}</div>\n  <div class="subheading" *ngIf="subHeading">\n    {{subHeadingText}}\n  </div>\n  <div class="image" *ngIf="image">\n    <img src="assets/imgs/godrej_properties.jpeg" alt="">\n  </div>\n  <div class="iconLists">\n    <ion-icon name="ios-notifications-outline" *ngIf="noti" style="font-size: 1.5em; float: right; margin-right: 15px;">\n      <!-- <ion-badge id="notifications-badge" color="danger"></ion-badge> -->\n      <span [ngClass]="{\'dot\': notificationDot == true}"></span>\n      <!-- <ion-badge item-end>260k</ion-badge> -->\n    </ion-icon>\n    <ion-icon name="ios-heart-outline" *ngIf="heart" style="font-size: 1.5em; float: right; margin-right: 15px;"\n      (click)="openwishlistpage()">\n      <span class="dot"></span>\n    </ion-icon>\n    <ion-icon name="ios-pin-outline" *ngIf="pin" style="font-size: 1.5em; float: right; margin-right: 15px;">\n\n    </ion-icon>\n  </div>\n</div>'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/head-bar/head-bar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], HeadBarComponent);
    return HeadBarComponent;
}());

//# sourceMappingURL=head-bar.js.map

/***/ }),

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FootBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__ = __webpack_require__(727);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_constants__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the FootBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FootBarComponent = /** @class */ (function () {
    function FootBarComponent(events, modalCtrl, storageProvider, searchApi, loader, toast) {
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.storageProvider = storageProvider;
        this.searchApi = searchApi;
        this.loader = loader;
        this.toast = toast;
        this.searchListArr = [];
        console.log('Hello FootBarComponent Component');
    }
    FootBarComponent.prototype.openSearch = function () {
        var _this = this;
        this.loader.show('Loading');
        this.searchApi.propertySearchInfo().subscribe(function (response) {
            _this.loader.hide();
            if (response.status == __WEBPACK_IMPORTED_MODULE_4__app_app_constants__["a" /* AppConst */].HTTP_SUCESS_STATUS.OK) {
                _this.addModal = _this.modalCtrl.create("SearchPage", { responseData: response });
                _this.addModal.onDidDismiss(function (item) {
                    if (item) {
                        //this.searchListArr=item;
                    }
                });
                _this.addModal.present();
            }
            else {
                _this.toast.show(response.msg);
            }
        }, function (error) {
            _this.loader.hide();
            _this.toast.show(error.statusText);
        });
    };
    FootBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'foot-bar',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/foot-bar/foot-bar.html"*/'<!-- Generated template for the FootBarComponent component -->\n<!-- <div class="footer_div">\n    <div>\n        <ion-icon name="ios-home-outline"></ion-icon>\n      Home\n    </div>\n    <div>Book a Visit</div>\n    <div>\n      <ion-icon name="ios-cart-outline"></ion-icon>\n      Buy Now\n    </div>\n    <div>\n      <ion-icon name="ios-call-outline"></ion-icon>\n      Support\n    </div>\n</div>\n<ion-grid>\n  <ion-row>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col></ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n    <ion-col><ion-icon name="ios-home-outline"></ion-icon>Home</ion-col>\n  </ion-row>\n</ion-grid>-->\n\n<ion-footer class="foot">\n\n\n    <ion-grid no-padding>\n        <ion-row no-padding class="menrow">\n            <ion-col no-padding >\n                <div class="iconclass">\n                    <!-- <ion-icon name="ios-home-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/soa-icons.png" />\n                    </div>\n                    <div class="iconlabel">SOA</div>\n                    <!-- <div class="iconlabel">{{logintext}}</div> -->\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 2}">\n                    <!-- <ion-icon name="ios-call"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/service-req-icons.png" />\n                    </div>\n                    <div class="iconlabel">Service Req</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding class="searchcol" (click)="openSearch()">\n                <ion-icon name="ios-menu-outline"></ion-icon>\n                <!-- <img src="assets/imgs/MenuIcon.png" class="footer_main_menu" /> -->\n                <div class="searchicon-wrap">\n                    <div class="searchicon">\n                        <img src="assets/imgs/search-icon.png" />\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 4}">\n                    <!-- <ion-icon name="ios-cart-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/chat-icons.png" />\n                    </div>\n\n                    <div class="iconlabel">Chat Bot</div>\n                </div>\n            </ion-col>\n            <ion-col no-padding>\n                <div class="iconclass" [ngClass]="{\'active\': selectedIndex == 3}">\n                    <!-- <ion-icon name="ios-call-outline"></ion-icon> -->\n                    <div class="fhicon">\n                        <img src="assets/imgs/support-icons.png" />\n                    </div>\n\n\n                    <div class="iconlabel">Support</div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-footer>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/foot-bar/foot-bar.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */]]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]) === "function" && _f || Object])
    ], FootBarComponent);
    return FootBarComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=foot-bar.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        console.log('Hello MenuComponent Component');
        this.text = 'Hello World';
    }
    MenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/menu/menu.html"*/'<!-- Generated template for the MenuComponent component -->\n<!-- <div class="sidemenu">\n  TEST\n\n</div> -->\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/components/menu/menu.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_db__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class f  or the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = /** @class */ (function () {
    function DashboardPage(navCtrl, navParams, menu, socialSharing, modalCtrl, events, 
        // public DashbordApi: DashordApiProvider,
        loadingCtrl, storageProvider, popoverCtrl, loader) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.socialSharing = socialSharing;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.storageProvider = storageProvider;
        this.popoverCtrl = popoverCtrl;
        this.loader = loader;
        // noti:boolean=true;
        // heart:boolean=true;
        // pin:boolean=true
        // menuIcon:boolean=true;
        // image:boolean=false;
        // text:boolean=false;
        this.noti = true;
        this.heart = true;
        this.pin = true;
        this.menuIcon = true;
        this.image = true;
        this.text = true;
        this.subHeading = false;
        this.subHeadingText = "";
        this.makeWishList = false;
        this.searchListArr = [];
        this.popOverDiv = false;
        this.initHide = false;
        this.otherProjects = false;
    }
    DashboardPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storageProvider.getUserDetails().then(function (userdata) {
            _this.username = userdata.userDetails;
        }).catch(function (error) {
            console.log(error);
        });
        this.searchListArr = [];
        //this.getCountries();
        this.menu.swipeEnable(true);
        //console.log('ionViewDidLoad DashboardPage');
        var cityList = [
            { name: "New Town", selected: false },
            { name: "Salt Lake", selected: true },
            { name: "Tollygaunge", selected: false },
            { name: "Ballygaunge", selected: false },
        ];
        var possesionList = [
            { name: "New Launch", selected: false },
            { name: "Ready To Move", selected: true },
            { name: "Under Construction", selected: false },
            { name: "Within 6 Months", selected: false }
        ];
        var typologyList = [
            { name: "2 Bhk", selected: false },
            { name: "3 Bhk", selected: true },
            { name: "3 Bhk+", selected: false },
            { name: "4 Bhk", selected: false },
        ];
        var price = {
            lower: "35",
            upper: "45"
        };
        this.storageProvider.setcityList(cityList);
        this.storageProvider.setPossessionList(possesionList);
        this.storageProvider.setTypologyList(typologyList);
        this.storageProvider.setPrice(price);
        // this.loader.show('Please wait...');
        // this.DashbordApi.onlyGet("project_banner/75").subscribe(
        //   (sucess)=>{
        //     this.loader.hide();
        //   },(err)=>{
        //     this.loader.hide();
        //   }
        // );
    };
    DashboardPage.prototype.getCountries = function () {
        var payload = [
            { "key": "email", "value": "subhendu.pal@indusnet.co.in", "description": "" },
            { "key": "password", "value": "1234", "description": "" },
            { "key": "devicetoken", "value": "test", "description": "" },
            { "key": "deviceid", "value": "1234", "description": "" },
            { "key": "devicetype", "value": "1", "description": "" }
        ];
        // this.DashbordApi.getCountries(payload)
        //   .subscribe(
        //     (countries) => {
        //       this.countries = countries;
        //     },
        //     error => this.errorMessage = <any>error);
    };
    DashboardPage.prototype.goToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    DashboardPage.prototype.share = function () {
        var message = "message";
        var subject = "subject";
        var file = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png";
        var url = "https://ionicframework.com/docs/native/social-sharing/";
        this.socialSharing.share(message, subject, file, url).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    DashboardPage.prototype.makeWish = function () {
        this.makeWishList = !this.makeWishList;
    };
    /* openSearch(value:any){
      debugger
      if(value!="ND"){
        let params={};
        if(this.searchListArr.length == 0){
          localStorage.setItem("fromDashboard","N")
          this.storageProvider.setValue(this.searchListArr);
          params={
            type:"C",// R => Residential C => Commertial
            location: "Kolkata",
            cityLocality:this.storageProvider.getCityList(),
            possession:this.storageProvider.getPossessionList(),
            typology:this.storageProvider.getTypologyList(),
            price:this.storageProvider.getPrice()
          };
        } else {
          params=this.searchListArr;
        }
        this.addModal = this.modalCtrl.create("SearchPage",{ params: params });
      } else {
        localStorage.setItem("fromDashboard","Y")
        this.addModal = this.modalCtrl.create("SearchPage",{ });
      }
  
      this.addModal.onDidDismiss((item:any) => {
  
        if (item) {
          let pageName=this.navCtrl.getActive();
          if(pageName.name=="ProjectDetailsPage"){
            this.navCtrl.pop();
          }
  
          this.searchListArr=item;
        }
      });
      this.addModal.present();
    } */
    DashboardPage.prototype.displayCounter = function (value) {
        console.log(value);
    };
    //
    DashboardPage.prototype.openPopOver = function () {
        this.popOverDiv = !this.popOverDiv;
        /* const popover = this.popoverCtrl.create("PopoverPage");
        popover.present(); */
    };
    DashboardPage.prototype.openClose = function () {
        this.initHide = !this.initHide;
    };
    DashboardPage.prototype.openOtherProjects = function () {
        this.otherProjects = !this.otherProjects;
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/dashboard/dashboard.html"*/'\n<ion-header>\n\n  <ion-navbar>\n      <head-bar hTitle="" [text]="text" [image]="image" [noti]="noti" [heart]="heart" [pin]="pin" [menuIcon]="menuIcon"></head-bar>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <!-- <button (click)="goToPage(\'InventoryBookingPage\')" style="margin-left: 20px;">\n    Go To Inventory Booking\n  </button> -->\n  <!-- -->\n    <div class="welcomeHeading">\n      <div class="welcome">\n        Welcome\n      </div>\n      <div class="username"> &nbsp; {{username?.fname}}</div>\n      <div class="dashboardHeading">\n        My Dashboard\n      </div>\n    </div>\n    <div class="dashboardSlider">\n      <ion-slides slidesPerView="1.1"  speed="1000">\n        <ion-slide >\n          <ion-card>\n              <div class="lastImage headerSlider">\n                  <img src="assets/imgs/img.png" width="100%" />\n                    <div class="iconList">\n                      <ion-icon name="md-more" (click)="openPopOver()"></ion-icon>\n                      <!--<ion-icon name="ios-heart-outline" *ngIf="!makeWishList" class="margin-right boldIcons" (click)="makeWish()"></ion-icon>\n                      <ion-icon name="ios-heart" *ngIf="makeWishList" class="margin-right boldIcons color" (click)="makeWish()"></ion-icon>\n                      <ion-icon ios="ios-share" md="md-share" class="boldIcons" (click)="share()"></ion-icon>-->\n                  </div>\n                  <div class="customPopOver" *ngIf="popOverDiv">\n                    <ion-list class="popList">\n                        <div class="typo">\n                            Typology :\n                        </div>\n                        <div class="valu">\n                            3BHK\n                        </div>\n                    </ion-list>\n                    <ion-list class="popList">\n                      <div class="typo">\n                          Carpet Area :\n                      </div>\n                      <div class="valu">\n                        1100sq.ft.\n                      </div>\n                    </ion-list>\n                    <ion-list class="popList">\n                      <div class="typo">\n                        Total Value :\n                      </div>\n                        <div class="valu">\n                          <img class="rsInr" src="assets/imgs/rupee-indian.png" alt=""> 85,00,000\n                        </div>\n                    </ion-list>\n                    <ion-list class="popList">\n                      <a href="" class="more">More</a>\n\n                    </ion-list>\n                  </div>\n                  <!-- <div class="iconsInImage reco">\n                      <div class="float-bottom">\n                        <div class="image">\n                          <img src="assets/imgs/rupee-indian.png" alt="">\n                        </div>\n                        <div class="price">\n                        1.1 Cr<sup>*</sup>\n                        </div>\n                      </div>\n                  </div> -->\n              </div>\n\n            <div class="product_text">\n                <div class="bottomText projectName">\n                  Project Name\n                  <!--<div class="floatLeft">\n\n                      <div class="prjectName">\n                      Project Name\n                    </div>\n                    <div class="location recLocation">\n                        <ion-icon name="ios-pin-outline"></ion-icon>\n                      Location\n                    </div>\n                  </div>-->\n                  <!-- <div class="floatRight">\n                    3 BHK\n                  </div> -->\n                  <div class="location recLocation topLoaction">\n                      <ion-icon name="ios-pin-outline"></ion-icon>\n                    &nbsp;  Location\n                  </div>\n                </div>\n            </div>\n          </ion-card>\n        </ion-slide>\n        <ion-slide >\n            <ion-card>\n                <div class="lastImage headerSlider">\n                    <img src="assets/imgs/img.png" width="100%" />\n                    <!-- <div class="iconList">\n                        <ion-icon name="ios-heart-outline" *ngIf="!makeWishList" class="margin-right boldIcons" (click)="makeWish()"></ion-icon>\n                        <ion-icon name="ios-heart" *ngIf="makeWishList" class="margin-right boldIcons color" (click)="makeWish()"></ion-icon>\n                        <ion-icon ios="ios-share" md="md-share" class="boldIcons" (click)="share()"></ion-icon>\n                    </div> -->\n                    <div class="iconList">\n                      <ion-icon name="md-more" (click)="openPopOver()"></ion-icon>\n                    </div>\n                    <div class="customPopOver" *ngIf="popOverDiv">\n                        <ion-list class="popList">\n                            <div class="typo">\n                                Typology :\n                            </div>\n                            <div class="valu">\n                                3BHK\n                            </div>\n                        </ion-list>\n                        <ion-list class="popList">\n                          <div class="typo">\n                              Carpet Area :\n                          </div>\n                          <div class="valu">\n                            1100sq.ft.\n                          </div>\n                        </ion-list>\n                        <ion-list class="popList">\n                          <div class="typo">\n                            Total Value :\n                          </div>\n                            <div class="valu">\n                              <img class="rsInr" src="assets/imgs/rupee-indian.png" alt=""> 85,00,000\n                            </div>\n                        </ion-list>\n                        <ion-list class="popList">\n                            <a href="" class="more">More</a>\n                        </ion-list>\n                      </div>\n                    <!-- <div class="iconsInImage reco">\n                        <div class="float-bottom">\n                          <div class="image">\n                            <img src="assets/imgs/rupee-indian.png" alt="">\n                          </div>\n                          <div class="price">\n                          1.1 Cr<sup>*</sup>\n                          </div>\n                        </div>\n                    </div> -->\n                </div>\n\n              <div class="product_text">\n                  <div class="bottomText projectName">\n                    Project Name\n                    <!--<div class="floatLeft">\n\n                        <div class="prjectName">\n                        Project Name\n                      </div>\n                      <div class="location recLocation">\n                          <ion-icon name="ios-pin-outline"></ion-icon>\n                        Location\n                      </div>\n                    </div>-->\n                    <!-- <div class="floatRight">\n                      3 BHK\n                    </div> -->\n\n                  </div>\n              </div>\n            </ion-card>\n        </ion-slide>\n      </ion-slides>\n    </div>\n\n    <div class="myAccountSummary">\n      <ion-card class="accCard">\n        <ion-card-header class="cardHeader">\n          <div class="left">My Account Summary</div>\n          <div class="right">\n            <div class="indiProperty">Individual Property</div>\n            <div class="downIcon" (click)="openOtherProjects()">\n                <ion-icon name="md-arrow-dropdown" class="iconDown" ></ion-icon>\n            </div>\n            <div class="customPopOver accntPopOver" *ngIf="otherProjects">\n              <ion-list class="popList">\n                  <div class="typo">\n                      Typology :\n                  </div>\n                  <div class="valu">\n                      3BHK\n                  </div>\n              </ion-list>\n              <ion-list class="popList">\n                <div class="typo">\n                    Carpet Area :\n                </div>\n                <div class="valu">\n                  1100sq.ft.\n                </div>\n              </ion-list>\n              <ion-list class="popList">\n                <div class="typo">\n                  Total Value :\n                </div>\n                  <div class="valu">\n                    <img class="rsInr" src="assets/imgs/rupee-indian.png" alt=""> 85,00,000\n                  </div>\n              </ion-list>\n              <ion-list class="popList">\n                <a href="" class="more">More</a>\n\n              </ion-list>\n            </div>\n          </div>\n        </ion-card-header>\n        <ion-card-content>\n          <div class="amountValues clearfix">\n            <div class="amountLeft">\n              <img class="inr" src="assets/imgs/rupee-indian.png" alt="">\n              <div class="amoutInr">4,51,73,890.00</div>\n              <div class="clearfix"></div>\n              <div class="totalvalutext">Total Value Billed</div>\n            </div>\n            <div class="amountRight">\n              <img class="inr" src="assets/imgs/rupee-indian.png" alt="">\n              <div class="amoutInr">6,00,000.00</div>\n              <div class="clearfix"></div>\n              <div class="totalvalutext">Last Billed Amount</div>\n              <div class="datesec"><img src="assets/imgs/dateicon.png" />02/03/2018</div>\n            </div>\n          </div>\n          <div class="initHide" *ngIf="initHide">\n            <div class="amountValues clearfix">\n              <div class="amountLeft">\n                <img class="inr" src="assets/imgs/rupee-indian.png" alt="">\n                <div class="amoutInr">34,38,721.00</div>\n                <div class="clearfix"></div>\n                <div class="totalvalutext">Total Amount Overdue</div>\n              </div>\n              <div class="amountRight">\n                <img class="inr" src="assets/imgs/rupee-indian.png" alt="">\n                <div class="amoutInr">24,000.00</div>\n                <div class="clearfix"></div>\n                <div class="totalvalutext">Total Interest Payable</div>\n                <div class="asdate">As on Date </div>\n              </div>\n            </div>\n            <div class="amountValues clearfix">\n              <div class="amountLeft">\n                <img class="inr" src="assets/imgs/rupee-indian.png" alt="">\n                <div class="amoutInr">4,22,53,000.00</div>\n                <div class="clearfix"></div>\n                <div class="totalvalutext">Total Amount Received</div>\n              </div>\n              <div class="amountRight">\n                <img class="inr" src="assets/imgs/rupee-indian.png" alt="">\n                <div class="amoutInr">2,00,000.00</div>\n                <div class="clearfix"></div>\n                <div class="totalvalutext">Last Payment Received</div>\n                <div class="datesec"><img src="assets/imgs/dateicon.png" />02/03/2018</div>\n              </div>\n            </div>\n          </div>\n          <div class="showHideDropdown" (click)="openClose()">\n            <ion-icon name="ios-arrow-dropdown-outline" class="blueDropdown"></ion-icon>\n          </div>\n        </ion-card-content>\n        <ion-card-header class="footer">\n          <div class="footerAmount">\n            <img class="inr payInr" src="assets/imgs/rupee-indian.png" alt="">\n            <ion-icon name="ios-information-circle-outline" class="information"></ion-icon>\n            <div class="amoutInr payAmntInr">33,33,3555</div>\n            <div class="payText">\n              Total Payable\n            </div>\n          </div>\n          <div class="buttonPayable" (click)="goToPage(\'PropertyPaymentPage\')">\n            <button ion-button round class="roundPayButton">Pay Now</button>\n          </div>\n        </ion-card-header>\n      </ion-card>\n    </div>\n    <!-- <div class="buttonPayable" (click)="goToPage(\'InventoryFilterPage\')">\n      <button ion-button round class="roundPayButton">Filter</button>\n    </div> -->\n    <div class="extrafecilities">\n      <div class="status" (click)="goToPage(\'ConstructionStatusPage\')">\n        <div class="roundIcon">\n          <!-- <ion-icon name="construct" class="extraFeciIcon"></ion-icon> -->\n          <img src="assets/imgs/construction-status.png" />\n        </div>\n        <div class="textfecility">Construction Status</div>\n      </div>\n\n      <div class="status" (click)="goToPage(\'ScheduleVisitPage\')">\n        <div class="roundIcon">\n          <!-- <ion-icon name="md-calendar" class="extraFeciIcon"></ion-icon> -->\n          <img src="assets/imgs/schedule-visit.png" />\n        </div>\n        <div class="textfecility">Schedule Visit</div>\n      </div>\n\n      <div class="status">\n        <div class="roundIcon">\n          <!-- <ion-icon name="md-home" class="extraFeciIcon"></ion-icon> -->\n          <img src="assets/imgs/loan-management.png" />\n        </div>\n        <div class="textfecility">Loan Management</div>\n      </div>\n\n      <div class="status">\n        <div class="roundIcon">\n          <!-- <ion-icon name="md-document" class="extraFeciIcon"></ion-icon> -->\n          <img src="assets/imgs/my-documents.png" />\n        </div>\n        <div class="textfecility">My Documents</div>\n      </div>\n    </div>\n\n    <ion-card>\n      <!-- <ion-card-header>\n\n      </ion-card-header>\n      <ion-card-content>\n        cdfgdf dgsr serg erger geg erg erg\n      </ion-card-content> -->\n    </ion-card>\n\n</ion-content>\n<foot-bar></foot-bar>\n\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/dashboard/dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_loader__["a" /* LoadingProvider */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ })

});
//# sourceMappingURL=11.js.map