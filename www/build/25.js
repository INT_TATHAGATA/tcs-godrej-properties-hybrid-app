webpackJsonp([25],{

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_auth__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__providers_apis_auth__["a" /* AuthProvider */]]
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_db__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { DashboardPage } from '../dashboard/dashboard';




//import { SignupPage } from '../signup/signup';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, fb, menu, storageProvider, events, Authapi, loader, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.menu = menu;
        this.storageProvider = storageProvider;
        this.events = events;
        this.Authapi = Authapi;
        this.loader = loader;
        this.toast = toast;
        this.isReadyToLogin = true;
        this.type = {
            email: true,
            mobile: false,
            pan: false
        };
        this.openClose = false;
        this.typeText = "password";
        this.onlyNumberAllowed = false;
        this.validEmail = false;
        this.panNumber = false;
        this.invalid = true;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.deviceinfo = this.storageProvider.getDeviceInfo();
        this.storageProvider.set("isTutorial", "1");
        this.menu.swipeEnable(false);
        this.loginForm = this.fb.group({
            textvalue: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('918961186006', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ]),
            pan: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('Asdqwe12!', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required]),
        });
        this.loginForm.valueChanges.subscribe(function (v) {
            _this.onlyNumberAllowed = false;
            _this.validEmail = false;
            _this.invalid = true;
            if (v.textvalue != "") {
                var checkEmail = _this.loginForm.value.textvalue.indexOf("@");
                if (!isNaN(v.textvalue) && checkEmail == -1) {
                    var regEx = _this.storageProvider.onlyNumberPattern();
                    var checkPlus = v.textvalue.indexOf("+");
                    console.log(checkPlus);
                    var result = v.textvalue.search(regEx);
                    _this.onlyNumberAllowed = false;
                    if (result == -1) {
                        _this.invalid = true;
                        _this.onlyNumberAllowed = true;
                    }
                    else {
                        _this.invalid = false;
                    }
                }
                else if (isNaN(v.textvalue)) {
                    var regEx = _this.storageProvider.emailPattern();
                    var result = v.textvalue.search(regEx);
                    _this.validEmail = false;
                    if (result == -1) {
                        _this.invalid = true;
                        _this.validEmail = true;
                    }
                    else {
                        _this.invalid = false;
                    }
                }
            }
            if (v.password == "") {
                _this.invalid = true;
            }
        });
    };
    Object.defineProperty(HomePage.prototype, "f", {
        get: function () {
            return this.loginForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    HomePage.prototype.goToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    HomePage.prototype.doLogin = function () {
        var _this = this;
        /*  */
        var type, postData;
        if (this.panNumber) {
            type = "pan";
            postData = {
                username: this.loginForm.value.pan,
                type: type,
                password: this.loginForm.value.password,
                device_type: "Android",
                device_id: this.deviceinfo.uuid
            };
        }
        else {
            var checkEmail = this.loginForm.value.textvalue.indexOf("@");
            if (checkEmail > -1) {
                type = "email";
            }
            else {
                type = "mob";
            }
            postData = {
                username: this.loginForm.value.textvalue,
                type: type,
                password: this.loginForm.value.password,
                device_type: "Android",
                device_id: this.deviceinfo.uuid
            };
        }
        this.loader.show('Please wait...');
        this.Authapi.postLoginapi(postData).subscribe(function (response) {
            _this.loader.hide();
            if (response.status == "200") {
                var data = { title: 'Godrej Home', component: 'GodrejHomePage' };
                _this.events.publish('afterLoginRedirect', data);
                _this.storageProvider.set("isLoggedIn", "1");
                _this.storageProvider.set("userData", JSON.stringify(response));
                localStorage.setItem('token', response.token);
                localStorage.setItem('Cookie', response.sessionName + '=' + response.sessionId);
            }
            else {
                if (response.status == "501") {
                    _this.panNumber = true;
                    _this.loginForm = _this.fb.group({
                        textvalue: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](_this.loginForm.value.textvalue, [
                            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
                        ]),
                        pan: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [
                            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(_this.storageProvider.panPattern()),
                        ]),
                        password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required]),
                    });
                }
                _this.toast.show(response.msg);
            }
        }, function (error) {
            console.log(error);
        });
        //localStorage.setItem("isLoggedIn","1");
        /* this.navCtrl.setRoot('GodrejHomePage', {}, {
          animate: true,
          direction: 'forward'
        }); */
    };
    HomePage.prototype.checkBox = function (type) {
        /* if(type == "E"){
          this.type={
            email:true,
            mobile:false,
            pan:false
          };
          this.loginForm = this.fb.group({
            emails: new FormControl('', [
              Validators.required,
              Validators.pattern(this.storageProvider.emailPattern())
            ]),
            password: new FormControl('', [Validators.required]),
          });
        } else if(type == "M"){
          this.type={
            email:false,
            mobile:true,
            pan:false
          };
          this.loginForm = this.fb.group({
            mobile: new FormControl('', [
              Validators.required,
              Validators.pattern(this.storageProvider.onlyNumberPattern()),
            ]),
            password: new FormControl('', [Validators.required]),
          });
        } else {
          this.type={
            email:false,
            mobile:false,
            pan:true
          };
          this.loginForm = this.fb.group({
            pan: new FormControl('', [
              Validators.required,
              Validators.pattern(this.storageProvider.panPattern()),
            ]),
            password: new FormControl('', [Validators.required]),
          });
        } */
    };
    HomePage.prototype.onkeyDown = function () {
        var newPan = this.loginForm.value.pan.toUpperCase();
        this.loginForm.setValue({ textvalue: this.loginForm.value.textvalue, pan: newPan, password: this.loginForm.value.password });
    };
    HomePage.prototype.eyeOpenClose = function (value) {
        if (value == "O") {
            this.openClose = false;
            this.typeText = "password";
        }
        else {
            this.openClose = true;
            this.typeText = "text";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('inputRef'),
        __metadata("design:type", Object)
    ], HomePage.prototype, "inputRef", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\projects\gpl\src\pages\home\home.html"*/'<!--\n  Generated template for the HomePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>home</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content padding>\n  <img src="assets/imgs/godrej_properties.jpeg" class="propertiesImg" alt="">\n  <div class="text">\n    Welcome\n  </div>\n  <!-- <div class="loginType">\n        <div class="loginwith">\n            Login With\n        </div>\n      <div class="emailLogin divType">\n        <div class="customCheckBox" (click)="checkBox(\'E\')">\n          <div class="bigDot">\n              <div [ngClass]="{\'smallDot\': type.email == true}" class="smallDot"></div>\n            </div>\n          <div class="text logTy">\n              &nbsp; Email\n          </div>\n        </div>\n      </div>\n      <div class="phoneLogin divType" >\n        <div class="customCheckBox" (click)="checkBox(\'M\')">\n          <div class="bigDot">\n              <div [ngClass]="{\'smallDot\': type.mobile == true}" class="smallDot"></div>\n            </div>\n          <div class="text logTy">\n              &nbsp; Mobile\n          </div>\n        </div>\n      </div>\n      <div class="panLogin divType">\n        <div class="customCheckBox" (click)="checkBox(\'P\')">\n          <div class="bigDot">\n              <div [ngClass]="{\'smallDot\': type.pan == true}" class="smallDot"></div>\n            </div>\n          <div class="text logTy">\n              &nbsp; Pan\n          </div>\n        </div>\n\n      </div>\n    </div> -->\n\n\n  <form class="loginForm" [formGroup]="loginForm">\n    <ion-list>\n\n      <ion-item *ngIf="type.email == true">\n        <ion-label floating>\n          <ion-icon name="ios-person-outline"></ion-icon>\n          &nbsp; Email Id / Mobile Number\n        </ion-label>\n        <ion-input type="text" name="textvalue" formControlName="textvalue"></ion-input>\n      </ion-item>\n      <div class="error">\n        <div *ngIf="f?.textvalue.errors?.required && f?.textvalue.touched">Email id / Mobile Number is required</div>\n        <div *ngIf="onlyNumberAllowed">\n          Not a valid number.\n        </div>\n        <div *ngIf="validEmail">\n          Not a valid email id.\n        </div>\n      </div>\n\n\n      <ion-item *ngIf="panNumber == true">\n        <ion-label floating>\n          <ion-icon name="ios-card-outline"></ion-icon>\n          &nbsp; Pan\n        </ion-label>\n        <ion-input type="text" name="pan" formControlName="pan" (keyup)="onkeyDown()"></ion-input>\n      </ion-item>\n      <div class="error" *ngIf="panNumber == true">\n        <div *ngIf="f?.pan.errors?.required && f?.pan.touched">Pan is required</div>\n        <div *ngIf="f?.pan.errors?.pattern && f?.pan.touched">Not a valid pan numbers</div>\n      </div>\n\n      <!-- <ion-item *ngIf="type.mobile == true">\n              <ion-label floating>\n                <ion-icon name="ios-call-outline"></ion-icon>\n                &nbsp; Mobile\n              </ion-label>\n            <ion-input type="tel" name="mobile" formControlName="mobile"></ion-input>\n          </ion-item>\n          <div class="error" *ngIf="type.mobile == true">\n            <div *ngIf="f?.mobile.errors?.required && f?.mobile.touched">Mobile is required</div>\n            <div *ngIf="f?.mobile.errors?.pattern && f?.mobile.touched">Only numbers</div>\n          </div>\n\n           -->\n\n      <ion-item class="passwordItem">\n        <ion-label floating>\n          <ion-icon name="ios-lock-outline"></ion-icon>\n          &nbsp; Password\n        </ion-label>\n        <ion-input type="{{typeText}}" name="password" formControlName="password"></ion-input>\n      </ion-item>\n      <div class="eyeIcons">\n        <div class="off" (click)="eyeOpenClose(\'O\')" *ngIf="openClose">\n          <ion-icon name="eye-off"></ion-icon>\n        </div>\n        <div class="eyeOpen" (click)="eyeOpenClose(\'C\')" *ngIf="!openClose">\n          <ion-icon name="eye"></ion-icon>\n        </div>\n      </div>\n\n      <div class="error">\n        <div *ngIf="f.password.errors?.required && f.password.touched">Password is required</div>\n      </div>\n      <div class="forgot" (click)="goToPage(\'ForgotPasswordPage\')">\n        Forgot Password?\n      </div>\n\n      <div padding>\n        <button ion-button round class="login-button" type="submit" [disabled]="invalid" (click)="doLogin()">\n          Login\n          <ion-icon name="ios-arrow-back-outline" class="rotate-to-right"></ion-icon>\n        </button>\n        <!-- <button ion-button color="primary" block>Login</button> -->\n      </div>\n\n    </ion-list>\n  </form>\n  <div class="guestLogin">\n    Continue as Guest\n  </div>\n  <div class="noaccount">\n    Don\'t have an account ? <span class="signUp" (click)="goToPage(\'SignupPage\')">Sign Up</span>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\projects\gpl\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__providers_db_db__["a" /* DbProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_db_db__["a" /* DbProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__["a" /* AuthProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_apis_auth__["a" /* AuthProvider */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_utils_loader__["a" /* LoadingProvider */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_utils_toast__["a" /* ToasterProvider */]) === "function" && _j || Object])
    ], HomePage);
    return HomePage;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=25.js.map