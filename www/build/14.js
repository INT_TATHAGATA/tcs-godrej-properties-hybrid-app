webpackJsonp([14],{

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search__ = __webpack_require__(752);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchPageModule = /** @class */ (function () {
    function SearchPageModule() {
    }
    SearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */]),
            ],
        })
    ], SearchPageModule);
    return SearchPageModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_db_db__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__ = __webpack_require__(727);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams, viewCtrl, storagePrivider, events, searchApi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.storagePrivider = storagePrivider;
        this.events = events;
        this.searchApi = searchApi;
        this.location = "";
        this.possetionList = [];
        this.cityList = [];
        this.typologyList = [];
        this.searchValues = [];
        this.listCity = [];
        this.responseData = this.navParams.get('responseData');
        this.responseData.budget_data = {
            lower: this.responseData.budget_data.min,
            upper: this.responseData.budget_data.max
        };
        this.responseData.project_status.map(function (item) {
            item['selected'] = false;
        });
        this.possetionList = this.responseData.project_status;
        this.responseData.typology_term_menu.map(function (item) {
            item['selected'] = false;
        });
        this.typologyList = this.responseData.typology_term_menu;
        this.listCity = this.responseData.city_term_menu;
        this.responseData.property_type_term_menu.map(function (item, index) {
            item['selected'] = false;
            if (index == 0) {
                item['selected'] = true;
            }
        });
        events.subscribe('closeSearchModal', function () {
            _this.myDismiss('d');
        });
        this.commonClas = this.responseData.property_type_term_menu;
        this.location = this.listCity[0].id;
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        this.goalProgress = this.responseData.budget_data;
        this.change();
    };
    SearchPage.prototype.myDismiss = function (value) {
        this.viewCtrl.dismiss(this.storagePrivider.getValue());
        if (value == "c") {
            this.events.publish('openSearchListPage', "SearchListPage");
        }
    };
    SearchPage.prototype.change = function () {
        this.lower = this.goalProgress.lower;
        this.upper = this.goalProgress.upper;
    };
    SearchPage.prototype.resOrCom = function (index) {
        var findSelected = this.commonClas.findIndex(function (x) { return x.selected == true; });
        this.commonClas[index].selected = true;
        if (findSelected > -1) {
            this.commonClas[findSelected].selected = false;
        }
    };
    SearchPage.prototype.click = function (pageName) {
        this.navCtrl.push(pageName);
    };
    SearchPage.prototype.getSelected = function (type, index) {
        //debugger
        if (type == "city") {
            this.findCitySelected = this.cityList.findIndex(function (x) { return x.selected === true; });
            (this.findCitySelected > -1) ? this.cityList[this.findCitySelected].selected = false : "";
            this.cityList[index].selected = true;
        }
        else if (type == "possession") {
            (this.possetionList[index].selected == false) ? this.possetionList[index].selected = true : this.possetionList[index].selected = false;
        }
        else if (type == "typology") {
            (this.typologyList[index].selected == false) ? this.typologyList[index].selected = true : this.typologyList[index].selected = false;
        }
    };
    SearchPage.prototype.search = function () {
        var findCitySelected = this.cityList.findIndex(function (x) { return x.selected === true; });
        var findPossessionSelected = this.possetionList.findIndex(function (x) { return x.selected === true; });
        var findTypologySelected = this.typologyList.findIndex(function (x) { return x.selected === true; });
        var lPrice = (this.goalProgress.lower >= 100) ? (this.goalProgress.lower / 100).toFixed(2) + " Cr" : (this.goalProgress.lower) + " Lkh";
        var uPrice = (this.goalProgress.upper >= 100) ? (this.goalProgress.upper / 100).toFixed(2) + " Cr" : (this.goalProgress.upper) + " Lkh";
        this.searchValues = [
            this.location,
            this.cityList[findCitySelected].name,
            this.possetionList[findPossessionSelected].name,
            this.typologyList[findTypologySelected].name,
            lPrice + "-" + uPrice
        ];
        this.storagePrivider.setcityList(this.cityList);
        this.storagePrivider.setPossessionList(this.possetionList);
        this.storagePrivider.setTypologyList(this.typologyList);
        this.storagePrivider.setPrice(this.goalProgress);
        this.storagePrivider.setValue(this.searchValues);
        this.myDismiss('d');
    };
    SearchPage.prototype.onChange = function (value) {
        console.log(value);
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-header>\n\n    <ion-navbar class="searchclose">\n      <ion-title>\n        <button (click)="myDismiss(\'d\')" class="close_btn">\n            <!-- <ion-icon name="ios-arrow-round-back-outline"></ion-icon> -->\n            <ion-icon name="close"></ion-icon>\n        </button> Search </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n</ion-header>\n\n\n<ion-content class="noPadding searchbox">\n  <div class="resCom">\n    <div class="resLeft commonClass" *ngFor="let item of commonClas; let i=index;" [ngClass]="{\'selected\': item.selected == true}" (click)="resOrCom(i)">\n      <span>{{item.name}}</span>\n    </div>\n    <!-- <div class="comRight commonClass" [ngClass]="{\'selected\': commonClas.commertial == true}" (click)="resOrCom(\'C\')">\n      <span>Commercial</span>\n    </div> -->\n  </div>\n  <div class="textBox">\n    <div class="input">\n      <!-- <input type="text" name="" id="" class="inputBox" [(ngModel)]="location"> -->\n      <ion-select class="inputBox dropDownCity" [(ngModel)]="location">\n          <ion-option value="{{city.id}}" (ionSelect)="onChange(city)" *ngFor="let city of listCity; let i=index">{{city.name}}</ion-option>\n      </ion-select>\n      <div class="locationIcon">\n          <ion-icon name="ios-pin-outline"></ion-icon>\n      </div>\n    </div>\n    <!-- <div class="inputdrop">\n      <div>Lorem ipsum dolor sit amet consectetur adipisicing elit.</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n      <div>Explicabo earum necessitatibus exercitationem fugit</div>\n    </div> -->\n  </div>\n  <div class="locality">\n    <img src="assets/imgs/locality.png">\n    Locality <!-- <button ion-button outline (click)="click(\'InventoryBookingPage\')"></button> -->\n  </div>\n  <div class="localValues">\n      <div class="upperDiv">\n        <div class="margin-lR innerDiv" *ngFor="let city of cityList; let i=index" >\n            <button ion-button color="light" round class="roundValues" (click)="getSelected(\'city\',i)" [ngClass]="{\'selectedBackGround\': city.selected == true}"> {{city.name}} </button>\n\n            <span class="closeit">\n                <ion-icon name="close"></ion-icon>\n            </span>\n            <input *ngIf="city.selected == true" type="hidden" name="" #focus>\n        </div>\n      </div>\n  </div>\n  <div class="locality">\n    <img src="assets/imgs/possession.png">\n    Possession\n  </div>\n  <div class="localValues">\n    <div class="upperDiv">\n      <div class="margin-lR innerDiv" *ngFor="let possession of possetionList; let i=index">\n          <button ion-button color="light" round class="roundValues" (click)="getSelected(\'possession\',i)" [ngClass]="{\'selectedBackGround\': possession.selected == true}"> {{possession.name}} </button>\n          <span class="closeit">\n              <ion-icon name="close"></ion-icon>\n          </span>\n      </div>\n    </div>\n  </div>\n  <div class="locality">\n    <img src="assets/imgs/typology.png">\n    Typology\n  </div>\n  <div class="localValues">\n    <div class="upperDiv">\n      <div class="margin-lR innerDiv" *ngFor="let typology of typologyList; let i=index">\n          <button ion-button color="light" round class="roundValues" (click)="getSelected(\'typology\',i)" [ngClass]="{\'selectedBackGround\': typology.selected == true}"> {{typology.name}} </button>\n\n          <span class="closeit">\n              <ion-icon name="close"></ion-icon>\n          </span>\n      </div>\n    </div>\n  </div>\n  <div class="locality">\n      <img class="priceicon" src="assets/imgs/priceicon.png">\n    Price\n  </div>\n  <div class="localValues">\n      <ion-item>\n        <ion-label>\n          <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;">{{lower}} -\n          <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;">{{upper}}\n        </ion-label>\n        <ion-range class="rangeNoPadd" dualKnobs="true" [(ngModel)]="goalProgress" [min]="responseData.budget_data.lower" [max]="responseData.budget_data.upper" step="1" pin="true" snaps="false" (ionChange)="change()">\n        </ion-range>\n\n      </ion-item>\n      <div class="clearfix filterrangetext">\n        <div class="filterfrom">\n          <div class="txtgrey">From</div>\n          <div class="filterprice">\n            <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;"> 25 Lakh\n          </div>\n        </div>\n\n        <div class="filterfrom float-right">\n          <div class="txtgrey float-right">From</div>\n          <div class="clearfix"></div>\n          <div class="filterprice">\n            <img src="assets/imgs/rupee-indian.png" alt="" style="width: 12px;"> 25 Lakh\n          </div>\n        </div>\n\n      </div>\n  </div>\n  <div class="localValues submitButton">\n    <button ion-button round class="searchBtm" (click)="search()">Submit</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/diptarghya/Documents/Diptarghya/Test/Git_GPL/tcs-godrej-properties-hybrid-app/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_db_db__["a" /* DbProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_apis_search__["a" /* SearchApiProvider */]) === "function" && _f || Object])
    ], SearchPage);
    return SearchPage;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=search.js.map

/***/ })

});
//# sourceMappingURL=14.js.map