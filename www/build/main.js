webpackJsonp([33],{

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DbProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_diagnostic__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_social_sharing__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_calendar__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__utils_toast__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_file_picker__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_file_chooser__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_file__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_platform_browser__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





















/*
  Generated class for the DbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DbProvider = /** @class */ (function () {
    function DbProvider(
        //private storage: Storage,
        events, nativeStorage, toastCtrl, loadingCtrl, camera, localNotifications, geolocation, diagnostic, nativeGeocoder, socialSharing, iab, calendar, device, toast, loader, fileChooser, iosFilePicker, file, transfer, sanitizer) {
        this.events = events;
        this.nativeStorage = nativeStorage;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.localNotifications = localNotifications;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.nativeGeocoder = nativeGeocoder;
        this.socialSharing = socialSharing;
        this.iab = iab;
        this.calendar = calendar;
        this.device = device;
        this.toast = toast;
        this.loader = loader;
        this.fileChooser = fileChooser;
        this.iosFilePicker = iosFilePicker;
        this.file = file;
        this.transfer = transfer;
        this.sanitizer = sanitizer;
        this._setValue = [];
        this._cityList = [];
        this._possessionList = [];
        this._typologyList = [];
        this._price = {};
        this.options = {
            location: 'yes',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Close',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
        console.log('Hello DbProvider Provider');
        this.loading = null;
    }
    DbProvider.prototype.emailPattern = function () {
        return "[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}";
    };
    DbProvider.prototype.onlyNumberPattern = function () {
        //return "^\+(?:[0-9] ?){6,14}[0-9]$";
        return "^(?=.*[0-9])[- +()0-9]+$";
        // return "^(?=.*[0-9])[0-9]+$";
    };
    DbProvider.prototype.onlyNumberPatternsignup = function () {
        //return "^\+(?:[0-9] ?){6,14}[0-9]$";
        return "^(?=.*[0-9])[0-9]+$";
        // return "^(?=.*[0-9])[0-9]+$";
    };
    DbProvider.prototype.numberOnly = function () {
        // return '^[0-9]*$';
        return '^([0-9])|([0])*$';
        // return "^(?=.*[0-9])[- +()0-9]+$";
    };
    DbProvider.prototype.maxlength = function (control) {
        console.log(control.value.toString().length);
        if (control.value.toString().length == 0) {
            return control.value;
        }
        else {
            // return (null)
        }
    };
    DbProvider.prototype.panPattern = function () {
        return "^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$";
    };
    DbProvider.prototype.onlyAlphabetPattern = function () {
        // return "^[a-zA-Z]+$";
        return /^[ A-Za-z .']*$/;
    };
    DbProvider.prototype.set = function (key, value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.nativeStorage.setItem(key, value)
                .then(function (suce) {
                console.log('Stored item!' + suce);
                resolve();
            }, function (error) {
                console.log('Error storing item' + error);
                reject();
            });
        });
    };
    DbProvider.prototype.getVal = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.nativeStorage.getItem(key)
                .then(function (data) {
                resolve(data);
                //console.log(data)
            }, function (error) {
                reject(error);
                //console.error(error)
            });
        });
    };
    DbProvider.prototype.clear = function () {
        this.nativeStorage.clear();
    };
    DbProvider.prototype.getValue = function () {
        return this._setValue;
    };
    DbProvider.prototype.setValue = function (value) {
        this._setValue = value;
    };
    DbProvider.prototype.setcityList = function (list) {
        this._cityList = list;
    };
    DbProvider.prototype.getCityList = function () {
        var sucess = localStorage.getItem("fromDashboard");
        localStorage.setItem("city", JSON.stringify(this._cityList));
        if (sucess == "Y") {
            var newCityList = JSON.parse(localStorage.getItem("city"));
            var selectedCity = newCityList.findIndex(function (X) { return X.selected == true; });
            if (selectedCity > -1) {
                newCityList[selectedCity].selected = false;
            }
            return newCityList;
        }
        else {
            return this._cityList;
        }
    };
    DbProvider.prototype.setPossessionList = function (value) {
        this._possessionList = value;
    };
    DbProvider.prototype.getPossessionList = function () {
        var sucess = localStorage.getItem("fromDashboard");
        localStorage.setItem("possession", JSON.stringify(this._possessionList));
        if (sucess == "Y") {
            var newPosseList = JSON.parse(localStorage.getItem("possession"));
            var selectedPosse = newPosseList.findIndex(function (X) { return X.selected == true; });
            if (selectedPosse > -1) {
                newPosseList[selectedPosse].selected = false;
            }
            return newPosseList;
        }
        else {
            return this._possessionList;
        }
    };
    DbProvider.prototype.setTypologyList = function (value) {
        this._typologyList = value;
    };
    DbProvider.prototype.getTypologyList = function () {
        localStorage.setItem("typo", JSON.stringify(this._typologyList));
        var sucess = localStorage.getItem("fromDashboard");
        if (sucess == "Y") {
            var newTypoList = JSON.parse(localStorage.getItem("typo"));
            var selectedTypo = newTypoList.findIndex(function (X) { return X.selected == true; });
            if (selectedTypo > -1) {
                newTypoList[selectedTypo].selected = false;
            }
            return newTypoList;
        }
        else {
            return this._typologyList;
        }
    };
    DbProvider.prototype.setPrice = function (value) {
        this._price = value;
    };
    DbProvider.prototype.getPrice = function () {
        return this._price;
    };
    DbProvider.prototype.getFromCamera = function (source) {
        var options = {
            quality: 90,
            /*
              DATA_URL (0) : Return image as base64-encoded string
              FILE_URI (1) : Return image file URI
              NATIVE_URI(2):Return image native URI
            */
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: source,
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 450,
            targetHeight: 450,
            mediaType: 0,
            /*
              Only works when PictureSourceType is PHOTOLIBRARY or SAVEDPHOTOALBUM.
              Defined in Camera.MediaType PICTURE: 0 allow selection of still pictures only.
              DEFAULT. Will return format specified via DestinationType VIDEO: 1 allow selection of video only,
              WILL ALWAYS RETURN FILE_URI ALLMEDIA : 2 allow selection from all media types
            */
            correctOrientation: true,
            saveToPhotoAlbum: false,
            cameraDirection: 0,
        };
        return options;
    };
    DbProvider.prototype.localNotificationNoNetwork = function (value) {
        var _this = this;
        if (value == "offline") {
            this.localNotifications.schedule({
                id: 0,
                title: 'No Network',
                text: 'Searching For Network',
                sticky: true,
                vibrate: false,
                progressBar: { value: 100 }
            });
        }
        else {
            this.localNotifications.clear(0).then(function (data) {
                _this.localNotifications.getIds().then(function (notiArr) {
                    console.log(notiArr);
                }, function (err) {
                    console.log(err);
                }).catch(function (error) {
                    console.log(error);
                });
            }, function (err) {
                console.log(err);
            }).catch(function (error) {
                console.log(error);
            });
        }
    };
    DbProvider.prototype.updateLocalNotification = function (value) {
        var _this = this;
        this.localNotifications.get(value).then(function (data) {
            var options = {
                id: 0,
                vibrate: false,
                progressBar: { value: (data.progressBar.value < 100) ? data.progressBar.value + 1 : 1 }
            };
            _this.localNotifications.setDefaults({ vibrate: false });
            _this.localNotifications.update(options);
            console.log(data);
        }, function (err) {
            console.log(err);
        }).catch(function (error) {
            console.log(error);
        });
    };
    DbProvider.prototype.localNotificationWhenAppIsInForeGround = function (data) {
        this.localNotifications.schedule({
            id: 1,
            text: data.title,
            //sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
            data: { secret: data },
        });
    };
    DbProvider.prototype.getLocation = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.diagnostic.isLocationEnabled().then(function (callback) {
                if (callback == false) {
                    _this.toast.show('Please enable your lcation');
                    reject("error");
                }
                else {
                    _this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(function (resp) {
                        resolve(resp);
                    }).catch(function (error) {
                        reject("error");
                    });
                }
            }, function (err) {
                reject("error");
            }).catch(function (error) {
                reject("error");
            });
        });
    };
    DbProvider.prototype.getReverseGeoCode = function (lat, long) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var options = {
                useLocale: true,
                maxResults: 10
            };
            _this.nativeGeocoder.reverseGeocode(lat, long, options)
                .then(function (result) {
                resolve(result[0]);
            }, function (err) {
                reject("error");
            })
                .catch(function (error) {
                reject("error");
            });
        });
    };
    DbProvider.prototype.setEvent = function (eventName, date) {
        var dOb = date.split('-');
        this.localNotifications.schedule({
            id: 110,
            title: eventName + '!!!',
            trigger: { every: { month: Number(dOb[1]), day: Number(dOb[2]), hour: 10, minute: 50 } },
            foreground: false
        });
    };
    DbProvider.prototype.getInitials = function (fName, lName) {
        return new Promise(function (resolve, reject) {
            if (fName != "" && lName != "") {
                var fInitials = fName.substring(0, 1).toUpperCase();
                var lInitials = lName.substring(0, 1).toUpperCase();
                var fullInitial = fInitials + lInitials;
                resolve(fullInitial);
            }
            else {
                resolve("GPL");
            }
        });
    };
    DbProvider.prototype.getUserDetails = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.getVal("userDetails").then(function (data) {
                var userDetails = JSON.parse(data);
                var fInitials = userDetails.first_name.substring(0, 1).toUpperCase();
                var lInitials = userDetails.last_name.substring(0, 1).toUpperCase();
                var fullInitial = fInitials + lInitials;
                resolve({ userDetails: userDetails, image: "", initials: fullInitial });
            }, function (err) {
                resolve("error");
            }).catch(function (error) {
                resolve("error");
            });
        });
    };
    DbProvider.prototype.setMinMaxDate = function () {
        return new Promise(function (resolve, reject) {
            var day = new Date();
            var nextDay = new Date(day);
            nextDay.setDate(day.getDate() + 1);
            var minDate = nextDay.toISOString();
            var nextYear = nextDay.setFullYear(nextDay.getFullYear() + 1);
            var maxDate = new Date(nextYear).toISOString();
            resolve(minDate + "~" + maxDate);
        });
    };
    DbProvider.prototype.socialShare = function (message, subject, file, url) {
        var _this = this;
        this.loader.show('Please wait...');
        this.socialSharing.share(message, subject, file, url).then(function () {
            _this.loader.hide();
        }).catch(function () {
            _this.loader.hide();
            // Error!
        });
    };
    DbProvider.prototype.openInAppBrowser = function (url) {
        // const browser = this.iab.create('https://ionicframework.com/');
        this.iab.create("https://ionicframework.com/", "_self", this.options);
    };
    DbProvider.prototype.calendarEvent = function (title, location, notes, startDate, endDate) {
        this.calendar.createEventInteractively(title, location, notes, startDate, endDate).then(function (data) {
            console.log(data);
        }).catch(function (err) {
            console.log(err);
        });
    };
    DbProvider.prototype.getCountryCodes = function () {
        var countryCodes = [
            {
                "name": "India",
                "callingCodes": "+91",
                "alpha3Code": "IND",
                "flag": "https://restcountries.eu/data/ind.svg"
            },
            {
                "name": "Afghanistan",
                "callingCodes": "+93",
                "alpha3Code": "AFG",
                "flag": "https://restcountries.eu/data/afg.svg"
            },
            {
                "name": "Åland Islands",
                "callingCodes": "+358",
                "alpha3Code": "ALA",
                "flag": "https://restcountries.eu/data/ala.svg"
            },
            {
                "name": "Albania",
                "callingCodes": "+355",
                "alpha3Code": "ALB",
                "flag": "https://restcountries.eu/data/alb.svg"
            },
            {
                "name": "Algeria",
                "callingCodes": "+213",
                "alpha3Code": "DZA",
                "flag": "https://restcountries.eu/data/dza.svg"
            },
            {
                "name": "American Samoa",
                "callingCodes": "+1684",
                "alpha3Code": "ASM",
                "flag": "https://restcountries.eu/data/asm.svg"
            },
            {
                "name": "Andorra",
                "callingCodes": "+376",
                "alpha3Code": "AND",
                "flag": "https://restcountries.eu/data/and.svg"
            },
            {
                "name": "Angola",
                "callingCodes": "+244",
                "alpha3Code": "AGO",
                "flag": "https://restcountries.eu/data/ago.svg"
            },
            {
                "name": "Anguilla",
                "callingCodes": "+1264",
                "alpha3Code": "AIA",
                "flag": "https://restcountries.eu/data/aia.svg"
            },
            {
                "name": "Antarctica",
                "callingCodes": "+672",
                "alpha3Code": "ATA",
                "flag": "https://restcountries.eu/data/ata.svg"
            },
            {
                "name": "Antigua and Barbuda",
                "callingCodes": "+1268",
                "alpha3Code": "ATG",
                "flag": "https://restcountries.eu/data/atg.svg"
            },
            {
                "name": "Argentina",
                "callingCodes": "+54",
                "alpha3Code": "ARG",
                "flag": "https://restcountries.eu/data/arg.svg"
            },
            {
                "name": "Armenia",
                "callingCodes": "+374",
                "alpha3Code": "ARM",
                "flag": "https://restcountries.eu/data/arm.svg"
            },
            {
                "name": "Aruba",
                "callingCodes": "+297",
                "alpha3Code": "ABW",
                "flag": "https://restcountries.eu/data/abw.svg"
            },
            {
                "name": "Australia",
                "callingCodes": "+61",
                "alpha3Code": "AUS",
                "flag": "https://restcountries.eu/data/aus.svg"
            },
            {
                "name": "Austria",
                "callingCodes": "+43",
                "alpha3Code": "AUT",
                "flag": "https://restcountries.eu/data/aut.svg"
            },
            {
                "name": "Azerbaijan",
                "callingCodes": "+994",
                "alpha3Code": "AZE",
                "flag": "https://restcountries.eu/data/aze.svg"
            },
            {
                "name": "Bahamas",
                "callingCodes": "+1242",
                "alpha3Code": "BHS",
                "flag": "https://restcountries.eu/data/bhs.svg"
            },
            {
                "name": "Bahrain",
                "callingCodes": "+973",
                "alpha3Code": "BHR",
                "flag": "https://restcountries.eu/data/bhr.svg"
            },
            {
                "name": "Bangladesh",
                "callingCodes": "+880",
                "alpha3Code": "BGD",
                "flag": "https://restcountries.eu/data/bgd.svg"
            },
            {
                "name": "Barbados",
                "callingCodes": "+1246",
                "alpha3Code": "BRB",
                "flag": "https://restcountries.eu/data/brb.svg"
            },
            {
                "name": "Belarus",
                "callingCodes": "+375",
                "alpha3Code": "BLR",
                "flag": "https://restcountries.eu/data/blr.svg"
            },
            {
                "name": "Belgium",
                "callingCodes": "+32",
                "alpha3Code": "BEL",
                "flag": "https://restcountries.eu/data/bel.svg"
            },
            {
                "name": "Belize",
                "callingCodes": "+501",
                "alpha3Code": "BLZ",
                "flag": "https://restcountries.eu/data/blz.svg"
            },
            {
                "name": "Benin",
                "callingCodes": "+229",
                "alpha3Code": "BEN",
                "flag": "https://restcountries.eu/data/ben.svg"
            },
            {
                "name": "Bermuda",
                "callingCodes": "+1441",
                "alpha3Code": "BMU",
                "flag": "https://restcountries.eu/data/bmu.svg"
            },
            {
                "name": "Bhutan",
                "callingCodes": "+975",
                "alpha3Code": "BTN",
                "flag": "https://restcountries.eu/data/btn.svg"
            },
            {
                "name": "Bolivia (Plurinational State of)",
                "callingCodes": "+591",
                "alpha3Code": "BOL",
                "flag": "https://restcountries.eu/data/bol.svg"
            },
            {
                "name": "Bonaire, Sint Eustatius and Saba",
                "callingCodes": "+5997",
                "alpha3Code": "BES",
                "flag": "https://restcountries.eu/data/bes.svg"
            },
            {
                "name": "Bosnia and Herzegovina",
                "callingCodes": "+387",
                "alpha3Code": "BIH",
                "flag": "https://restcountries.eu/data/bih.svg"
            },
            {
                "name": "Botswana",
                "callingCodes": "+267",
                "alpha3Code": "BWA",
                "flag": "https://restcountries.eu/data/bwa.svg"
            },
            {
                "name": "Brazil",
                "callingCodes": "+55",
                "alpha3Code": "BRA",
                "flag": "https://restcountries.eu/data/bra.svg"
            },
            {
                "name": "British Indian Ocean Territory",
                "callingCodes": "+246",
                "alpha3Code": "IOT",
                "flag": "https://restcountries.eu/data/iot.svg"
            },
            {
                "name": "Virgin Islands (British)",
                "callingCodes": "+1284",
                "alpha3Code": "VGB",
                "flag": "https://restcountries.eu/data/vgb.svg"
            },
            {
                "name": "Virgin Islands (U.S.)",
                "callingCodes": "+1 340",
                "alpha3Code": "VIR",
                "flag": "https://restcountries.eu/data/vir.svg"
            },
            {
                "name": "Brunei Darussalam",
                "callingCodes": "+673",
                "alpha3Code": "BRN",
                "flag": "https://restcountries.eu/data/brn.svg"
            },
            {
                "name": "Bulgaria",
                "callingCodes": "+359",
                "alpha3Code": "BGR",
                "flag": "https://restcountries.eu/data/bgr.svg"
            },
            {
                "name": "Burkina Faso",
                "callingCodes": "+226",
                "alpha3Code": "BFA",
                "flag": "https://restcountries.eu/data/bfa.svg"
            },
            {
                "name": "Burundi",
                "callingCodes": "+257",
                "alpha3Code": "BDI",
                "flag": "https://restcountries.eu/data/bdi.svg"
            },
            {
                "name": "Cambodia",
                "callingCodes": "+855",
                "alpha3Code": "KHM",
                "flag": "https://restcountries.eu/data/khm.svg"
            },
            {
                "name": "Cameroon",
                "callingCodes": "+237",
                "alpha3Code": "CMR",
                "flag": "https://restcountries.eu/data/cmr.svg"
            },
            {
                "name": "Canada",
                "callingCodes": "+1",
                "alpha3Code": "CAN",
                "flag": "https://restcountries.eu/data/can.svg"
            },
            {
                "name": "Cabo Verde",
                "callingCodes": "238",
                "alpha3Code": "CPV",
                "flag": "https://restcountries.eu/data/cpv.svg"
            },
            {
                "name": "Cayman Islands",
                "callingCodes": "+1345",
                "alpha3Code": "CYM",
                "flag": "https://restcountries.eu/data/cym.svg"
            },
            {
                "name": "Central African Republic",
                "callingCodes": "+236",
                "alpha3Code": "CAF",
                "flag": "https://restcountries.eu/data/caf.svg"
            },
            {
                "name": "Chad",
                "callingCodes": "+235",
                "alpha3Code": "TCD",
                "flag": "https://restcountries.eu/data/tcd.svg"
            },
            {
                "name": "Chile",
                "callingCodes": "+56",
                "alpha3Code": "CHL",
                "flag": "https://restcountries.eu/data/chl.svg"
            },
            {
                "name": "China",
                "callingCodes": "+86",
                "alpha3Code": "CHN",
                "flag": "https://restcountries.eu/data/chn.svg"
            },
            {
                "name": "Christmas Island",
                "callingCodes": "+61",
                "alpha3Code": "CXR",
                "flag": "https://restcountries.eu/data/cxr.svg"
            },
            {
                "name": "Colombia",
                "callingCodes": "+57",
                "alpha3Code": "COL",
                "flag": "https://restcountries.eu/data/col.svg"
            },
            {
                "name": "Comoros",
                "callingCodes": "+269",
                "alpha3Code": "COM",
                "flag": "https://restcountries.eu/data/com.svg"
            },
            {
                "name": "Congo",
                "callingCodes": "+242",
                "alpha3Code": "COG",
                "flag": "https://restcountries.eu/data/cog.svg"
            },
            {
                "name": "Congo (Democratic Republic of the)",
                "callingCodes": "+243",
                "alpha3Code": "COD",
                "flag": "https://restcountries.eu/data/cod.svg"
            },
            {
                "name": "Cook Islands",
                "callingCodes": "+682",
                "alpha3Code": "COK",
                "flag": "https://restcountries.eu/data/cok.svg"
            },
            {
                "name": "Costa Rica",
                "callingCodes": "+506",
                "alpha3Code": "CRI",
                "flag": "https://restcountries.eu/data/cri.svg"
            },
            {
                "name": "Croatia",
                "callingCodes": "+385",
                "alpha3Code": "HRV",
                "flag": "https://restcountries.eu/data/hrv.svg"
            },
            {
                "name": "Cuba",
                "callingCodes": "+53",
                "alpha3Code": "CUB",
                "flag": "https://restcountries.eu/data/cub.svg"
            },
            {
                "name": "Curaçao",
                "callingCodes": "+599",
                "alpha3Code": "CUW",
                "flag": "https://restcountries.eu/data/cuw.svg"
            },
            {
                "name": "Cyprus",
                "callingCodes": "+357",
                "alpha3Code": "CYP",
                "flag": "https://restcountries.eu/data/cyp.svg"
            },
            {
                "name": "Czech Republic",
                "callingCodes": "+420",
                "alpha3Code": "CZE",
                "flag": "https://restcountries.eu/data/cze.svg"
            },
            {
                "name": "Denmark",
                "callingCodes": "+45",
                "alpha3Code": "DNK",
                "flag": "https://restcountries.eu/data/dnk.svg"
            },
            {
                "name": "Djibouti",
                "callingCodes": "+253",
                "alpha3Code": "DJI",
                "flag": "https://restcountries.eu/data/dji.svg"
            },
            {
                "name": "Dominica",
                "callingCodes": "+1767",
                "alpha3Code": "DMA",
                "flag": "https://restcountries.eu/data/dma.svg"
            },
            {
                "name": "Dominican Republic",
                "callingCodes": "+1809",
                "alpha3Code": "DOM",
                "flag": "https://restcountries.eu/data/dom.svg"
            },
            {
                "name": "Ecuador",
                "callingCodes": "+593",
                "alpha3Code": "ECU",
                "flag": "https://restcountries.eu/data/ecu.svg"
            },
            {
                "name": "Egypt",
                "callingCodes": "+20",
                "alpha3Code": "EGY",
                "flag": "https://restcountries.eu/data/egy.svg"
            },
            {
                "name": "El Salvador",
                "callingCodes": "+503",
                "alpha3Code": "SLV",
                "flag": "https://restcountries.eu/data/slv.svg"
            },
            {
                "name": "Equatorial Guinea",
                "callingCodes": "+240",
                "alpha3Code": "GNQ",
                "flag": "https://restcountries.eu/data/gnq.svg"
            },
            {
                "name": "Eritrea",
                "callingCodes": "+291",
                "alpha3Code": "ERI",
                "flag": "https://restcountries.eu/data/eri.svg"
            },
            {
                "name": "Estonia",
                "callingCodes": "+372",
                "alpha3Code": "EST",
                "flag": "https://restcountries.eu/data/est.svg"
            },
            {
                "name": "Ethiopia",
                "callingCodes": "+251",
                "alpha3Code": "ETH",
                "flag": "https://restcountries.eu/data/eth.svg"
            },
            {
                "name": "Falkland Islands (Malvinas)",
                "callingCodes": "+500",
                "alpha3Code": "FLK",
                "flag": "https://restcountries.eu/data/flk.svg"
            },
            {
                "name": "Faroe Islands",
                "callingCodes": "+298",
                "alpha3Code": "FRO",
                "flag": "https://restcountries.eu/data/fro.svg"
            },
            {
                "name": "Fiji",
                "callingCodes": "+679",
                "alpha3Code": "FJI",
                "flag": "https://restcountries.eu/data/fji.svg"
            },
            {
                "name": "Finland",
                "callingCodes": "+358",
                "alpha3Code": "FIN",
                "flag": "https://restcountries.eu/data/fin.svg"
            },
            {
                "name": "France",
                "callingCodes": "+33",
                "alpha3Code": "FRA",
                "flag": "https://restcountries.eu/data/fra.svg"
            },
            {
                "name": "French Guiana",
                "callingCodes": "+594",
                "alpha3Code": "GUF",
                "flag": "https://restcountries.eu/data/guf.svg"
            },
            {
                "name": "French Polynesia",
                "callingCodes": "+689",
                "alpha3Code": "PYF",
                "flag": "https://restcountries.eu/data/pyf.svg"
            },
            {
                "name": "Gabon",
                "callingCodes": "241",
                "alpha3Code": "GAB",
                "flag": "https://restcountries.eu/data/gab.svg"
            },
            {
                "name": "Gambia",
                "callingCodes": "+220",
                "alpha3Code": "GMB",
                "flag": "https://restcountries.eu/data/gmb.svg"
            },
            {
                "name": "Georgia",
                "callingCodes": "+995",
                "alpha3Code": "GEO",
                "flag": "https://restcountries.eu/data/geo.svg"
            },
            {
                "name": "Germany",
                "callingCodes": "+49",
                "alpha3Code": "DEU",
                "flag": "https://restcountries.eu/data/deu.svg"
            },
            {
                "name": "Ghana",
                "callingCodes": "+233",
                "alpha3Code": "GHA",
                "flag": "https://restcountries.eu/data/gha.svg"
            },
            {
                "name": "Gibraltar",
                "callingCodes": "+350",
                "alpha3Code": "GIB",
                "flag": "https://restcountries.eu/data/gib.svg"
            },
            {
                "name": "Greece",
                "callingCodes": "+30",
                "alpha3Code": "GRC",
                "flag": "https://restcountries.eu/data/grc.svg"
            },
            {
                "name": "Greenland",
                "callingCodes": "+299",
                "alpha3Code": "GRL",
                "flag": "https://restcountries.eu/data/grl.svg"
            },
            {
                "name": "Grenada",
                "callingCodes": "+1473",
                "alpha3Code": "GRD",
                "flag": "https://restcountries.eu/data/grd.svg"
            },
            {
                "name": "Guadeloupe",
                "callingCodes": "590",
                "alpha3Code": "GLP",
                "flag": "https://restcountries.eu/data/glp.svg"
            },
            {
                "name": "Guam",
                "callingCodes": "+1671",
                "alpha3Code": "GUM",
                "flag": "https://restcountries.eu/data/gum.svg"
            },
            {
                "name": "Guatemala",
                "callingCodes": "+502",
                "alpha3Code": "GTM",
                "flag": "https://restcountries.eu/data/gtm.svg"
            },
            {
                "name": "Guernsey",
                "callingCodes": "+44",
                "alpha3Code": "GGY",
                "flag": "https://restcountries.eu/data/ggy.svg"
            },
            {
                "name": "Guinea",
                "callingCodes": "+224",
                "alpha3Code": "GIN",
                "flag": "https://restcountries.eu/data/gin.svg"
            },
            {
                "name": "Guinea-Bissau",
                "callingCodes": "+245",
                "alpha3Code": "GNB",
                "flag": "https://restcountries.eu/data/gnb.svg"
            },
            {
                "name": "Guyana",
                "callingCodes": "+592",
                "alpha3Code": "GUY",
                "flag": "https://restcountries.eu/data/guy.svg"
            },
            {
                "name": "Haiti",
                "callingCodes": "+509",
                "alpha3Code": "HTI",
                "flag": "https://restcountries.eu/data/hti.svg"
            },
            {
                "name": "Holy See",
                "callingCodes": "+379",
                "alpha3Code": "VAT",
                "flag": "https://restcountries.eu/data/vat.svg"
            },
            {
                "name": "Honduras",
                "callingCodes": "+504",
                "alpha3Code": "HND",
                "flag": "https://restcountries.eu/data/hnd.svg"
            },
            {
                "name": "Hong Kong",
                "callingCodes": "+852",
                "alpha3Code": "HKG",
                "flag": "https://restcountries.eu/data/hkg.svg"
            },
            {
                "name": "Hungary",
                "callingCodes": "+36",
                "alpha3Code": "HUN",
                "flag": "https://restcountries.eu/data/hun.svg"
            },
            {
                "name": "Iceland",
                "callingCodes": "+354",
                "alpha3Code": "ISL",
                "flag": "https://restcountries.eu/data/isl.svg"
            },
            {
                "name": "Indonesia",
                "callingCodes": "+62",
                "alpha3Code": "IDN",
                "flag": "https://restcountries.eu/data/idn.svg"
            },
            {
                "name": "Côte d'Ivoire",
                "callingCodes": "+225",
                "alpha3Code": "CIV",
                "flag": "https://restcountries.eu/data/civ.svg"
            },
            {
                "name": "Iran (Islamic Republic of)",
                "callingCodes": "+98",
                "alpha3Code": "IRN",
                "flag": "https://restcountries.eu/data/irn.svg"
            },
            {
                "name": "Iraq",
                "callingCodes": "+964",
                "alpha3Code": "IRQ",
                "flag": "https://restcountries.eu/data/irq.svg"
            },
            {
                "name": "Ireland",
                "callingCodes": "+353",
                "alpha3Code": "IRL",
                "flag": "https://restcountries.eu/data/irl.svg"
            },
            {
                "name": "Isle of Man",
                "callingCodes": "+44",
                "alpha3Code": "IMN",
                "flag": "https://restcountries.eu/data/imn.svg"
            },
            {
                "name": "Israel",
                "callingCodes": "+972",
                "alpha3Code": "ISR",
                "flag": "https://restcountries.eu/data/isr.svg"
            },
            {
                "name": "Italy",
                "callingCodes": "+39",
                "alpha3Code": "ITA",
                "flag": "https://restcountries.eu/data/ita.svg"
            },
            {
                "name": "Jamaica",
                "callingCodes": "+1876",
                "alpha3Code": "JAM",
                "flag": "https://restcountries.eu/data/jam.svg"
            },
            {
                "name": "Japan",
                "callingCodes": "+81",
                "alpha3Code": "JPN",
                "flag": "https://restcountries.eu/data/jpn.svg"
            },
            {
                "name": "Jersey",
                "callingCodes": "+44",
                "alpha3Code": "JEY",
                "flag": "https://restcountries.eu/data/jey.svg"
            },
            {
                "name": "Jordan",
                "callingCodes": "+962",
                "alpha3Code": "JOR",
                "flag": "https://restcountries.eu/data/jor.svg"
            },
            {
                "name": "Kazakhstan",
                "callingCodes": "+76",
                "alpha3Code": "KAZ",
                "flag": "https://restcountries.eu/data/kaz.svg"
            },
            {
                "name": "Kenya",
                "callingCodes": "+254",
                "alpha3Code": "KEN",
                "flag": "https://restcountries.eu/data/ken.svg"
            },
            {
                "name": "Kiribati",
                "callingCodes": "+686",
                "alpha3Code": "KIR",
                "flag": "https://restcountries.eu/data/kir.svg"
            },
            {
                "name": "Kuwait",
                "callingCodes": "+965",
                "alpha3Code": "KWT",
                "flag": "https://restcountries.eu/data/kwt.svg"
            },
            {
                "name": "Kyrgyzstan",
                "callingCodes": "+996",
                "alpha3Code": "KGZ",
                "flag": "https://restcountries.eu/data/kgz.svg"
            },
            {
                "name": "Lao People's Democratic Republic",
                "callingCodes": "+856",
                "alpha3Code": "LAO",
                "flag": "https://restcountries.eu/data/lao.svg"
            },
            {
                "name": "Latvia",
                "callingCodes": "+371",
                "alpha3Code": "LVA",
                "flag": "https://restcountries.eu/data/lva.svg"
            },
            {
                "name": "Lebanon",
                "callingCodes": "+961",
                "alpha3Code": "LBN",
                "flag": "https://restcountries.eu/data/lbn.svg"
            },
            {
                "name": "Lesotho",
                "callingCodes": "+266",
                "alpha3Code": "LSO",
                "flag": "https://restcountries.eu/data/lso.svg"
            },
            {
                "name": "Liberia",
                "callingCodes": "+231",
                "alpha3Code": "LBR",
                "flag": "https://restcountries.eu/data/lbr.svg"
            },
            {
                "name": "Libya",
                "callingCodes": "+218",
                "alpha3Code": "LBY",
                "flag": "https://restcountries.eu/data/lby.svg"
            },
            {
                "name": "Liechtenstein",
                "callingCodes": "+423",
                "alpha3Code": "LIE",
                "flag": "https://restcountries.eu/data/lie.svg"
            },
            {
                "name": "Lithuania",
                "callingCodes": "+370",
                "alpha3Code": "LTU",
                "flag": "https://restcountries.eu/data/ltu.svg"
            },
            {
                "name": "Luxembourg",
                "callingCodes": "+352",
                "alpha3Code": "LUX",
                "flag": "https://restcountries.eu/data/lux.svg"
            },
            {
                "name": "Macao",
                "callingCodes": "+853",
                "alpha3Code": "MAC",
                "flag": "https://restcountries.eu/data/mac.svg"
            },
            {
                "name": "Macedonia (the former Yugoslav Republic of)",
                "callingCodes": "+389",
                "alpha3Code": "MKD",
                "flag": "https://restcountries.eu/data/mkd.svg"
            },
            {
                "name": "Madagascar",
                "callingCodes": "+261",
                "alpha3Code": "MDG",
                "flag": "https://restcountries.eu/data/mdg.svg"
            },
            {
                "name": "Malawi",
                "callingCodes": "+265",
                "alpha3Code": "MWI",
                "flag": "https://restcountries.eu/data/mwi.svg"
            },
            {
                "name": "Malaysia",
                "callingCodes": "+60",
                "alpha3Code": "MYS",
                "flag": "https://restcountries.eu/data/mys.svg"
            },
            {
                "name": "Maldives",
                "callingCodes": "+960",
                "alpha3Code": "MDV",
                "flag": "https://restcountries.eu/data/mdv.svg"
            },
            {
                "name": "Mali",
                "callingCodes": "+223",
                "alpha3Code": "MLI",
                "flag": "https://restcountries.eu/data/mli.svg"
            },
            {
                "name": "Malta",
                "callingCodes": "+356",
                "alpha3Code": "MLT",
                "flag": "https://restcountries.eu/data/mlt.svg"
            },
            {
                "name": "Marshall Islands",
                "callingCodes": "+692",
                "alpha3Code": "MHL",
                "flag": "https://restcountries.eu/data/mhl.svg"
            },
            {
                "name": "Martinique",
                "callingCodes": "+596",
                "alpha3Code": "MTQ",
                "flag": "https://restcountries.eu/data/mtq.svg"
            },
            {
                "name": "Mauritania",
                "callingCodes": "+222",
                "alpha3Code": "MRT",
                "flag": "https://restcountries.eu/data/mrt.svg"
            },
            {
                "name": "Mauritius",
                "callingCodes": "+230",
                "alpha3Code": "MUS",
                "flag": "https://restcountries.eu/data/mus.svg"
            },
            {
                "name": "Mayotte",
                "callingCodes": "+262",
                "alpha3Code": "MYT",
                "flag": "https://restcountries.eu/data/myt.svg"
            },
            {
                "name": "Mexico",
                "callingCodes": "+52",
                "alpha3Code": "MEX",
                "flag": "https://restcountries.eu/data/mex.svg"
            },
            {
                "name": "Micronesia (Federated States of)",
                "callingCodes": "+691",
                "alpha3Code": "FSM",
                "flag": "https://restcountries.eu/data/fsm.svg"
            },
            {
                "name": "Moldova (Republic of)",
                "callingCodes": "+373",
                "alpha3Code": "MDA",
                "flag": "https://restcountries.eu/data/mda.svg"
            },
            {
                "name": "Monaco",
                "callingCodes": "+377",
                "alpha3Code": "MCO",
                "flag": "https://restcountries.eu/data/mco.svg"
            },
            {
                "name": "Mongolia",
                "callingCodes": "+976",
                "alpha3Code": "MNG",
                "flag": "https://restcountries.eu/data/mng.svg"
            },
            {
                "name": "Montenegro",
                "callingCodes": "+382",
                "alpha3Code": "MNE",
                "flag": "https://restcountries.eu/data/mne.svg"
            },
            {
                "name": "Montserrat",
                "callingCodes": "+1664",
                "alpha3Code": "MSR",
                "flag": "https://restcountries.eu/data/msr.svg"
            },
            {
                "name": "Morocco",
                "callingCodes": "+212",
                "alpha3Code": "MAR",
                "flag": "https://restcountries.eu/data/mar.svg"
            },
            {
                "name": "Mozambique",
                "callingCodes": "+258",
                "alpha3Code": "MOZ",
                "flag": "https://restcountries.eu/data/moz.svg"
            },
            {
                "name": "Myanmar",
                "callingCodes": "+95",
                "alpha3Code": "MMR",
                "flag": "https://restcountries.eu/data/mmr.svg"
            },
            {
                "name": "Namibia",
                "callingCodes": "+264",
                "alpha3Code": "NAM",
                "flag": "https://restcountries.eu/data/nam.svg"
            },
            {
                "name": "Nauru",
                "callingCodes": "+674",
                "alpha3Code": "NRU",
                "flag": "https://restcountries.eu/data/nru.svg"
            },
            {
                "name": "Nepal",
                "callingCodes": "+977",
                "alpha3Code": "NPL",
                "flag": "https://restcountries.eu/data/npl.svg"
            },
            {
                "name": "Netherlands",
                "callingCodes": "+31",
                "alpha3Code": "NLD",
                "flag": "https://restcountries.eu/data/nld.svg"
            },
            {
                "name": "New Caledonia",
                "callingCodes": "+687",
                "alpha3Code": "NCL",
                "flag": "https://restcountries.eu/data/ncl.svg"
            },
            {
                "name": "New Zealand",
                "callingCodes": "+64",
                "alpha3Code": "NZL",
                "flag": "https://restcountries.eu/data/nzl.svg"
            },
            {
                "name": "Nicaragua",
                "callingCodes": "+505",
                "alpha3Code": "NIC",
                "flag": "https://restcountries.eu/data/nic.svg"
            },
            {
                "name": "Niger",
                "callingCodes": "+227",
                "alpha3Code": "NER",
                "flag": "https://restcountries.eu/data/ner.svg"
            },
            {
                "name": "Nigeria",
                "callingCodes": "+234",
                "alpha3Code": "NGA",
                "flag": "https://restcountries.eu/data/nga.svg"
            },
            {
                "name": "Niue",
                "callingCodes": "+683",
                "alpha3Code": "NIU",
                "flag": "https://restcountries.eu/data/niu.svg"
            },
            {
                "name": "Norfolk Island",
                "callingCodes": "+672",
                "alpha3Code": "NFK",
                "flag": "https://restcountries.eu/data/nfk.svg"
            },
            {
                "name": "Korea (Democratic People's Republic of)",
                "callingCodes": "+850",
                "alpha3Code": "PRK",
                "flag": "https://restcountries.eu/data/prk.svg"
            },
            {
                "name": "Northern Mariana Islands",
                "callingCodes": "+1670",
                "alpha3Code": "MNP",
                "flag": "https://restcountries.eu/data/mnp.svg"
            },
            {
                "name": "Norway",
                "callingCodes": "+47",
                "alpha3Code": "NOR",
                "flag": "https://restcountries.eu/data/nor.svg"
            },
            {
                "name": "Oman",
                "callingCodes": "+968",
                "alpha3Code": "OMN",
                "flag": "https://restcountries.eu/data/omn.svg"
            },
            {
                "name": "Pakistan",
                "callingCodes": "+92",
                "alpha3Code": "PAK",
                "flag": "https://restcountries.eu/data/pak.svg"
            },
            {
                "name": "Palau",
                "callingCodes": "+680",
                "alpha3Code": "PLW",
                "flag": "https://restcountries.eu/data/plw.svg"
            },
            {
                "name": "Palestine, State of",
                "callingCodes": "+970",
                "alpha3Code": "PSE",
                "flag": "https://restcountries.eu/data/pse.svg"
            },
            {
                "name": "Panama",
                "callingCodes": "+507",
                "alpha3Code": "PAN",
                "flag": "https://restcountries.eu/data/pan.svg"
            },
            {
                "name": "Papua New Guinea",
                "callingCodes": "+675",
                "alpha3Code": "PNG",
                "flag": "https://restcountries.eu/data/png.svg"
            },
            {
                "name": "Paraguay",
                "callingCodes": "+595",
                "alpha3Code": "PRY",
                "flag": "https://restcountries.eu/data/pry.svg"
            },
            {
                "name": "Peru",
                "callingCodes": "+51",
                "alpha3Code": "PER",
                "flag": "https://restcountries.eu/data/per.svg"
            },
            {
                "name": "Philippines",
                "callingCodes": "+63",
                "alpha3Code": "PHL",
                "flag": "https://restcountries.eu/data/phl.svg"
            },
            {
                "name": "Pitcairn",
                "callingCodes": "+64",
                "alpha3Code": "PCN",
                "flag": "https://restcountries.eu/data/pcn.svg"
            },
            {
                "name": "Poland",
                "callingCodes": "+48",
                "alpha3Code": "POL",
                "flag": "https://restcountries.eu/data/pol.svg"
            },
            {
                "name": "Portugal",
                "callingCodes": "+351",
                "alpha3Code": "PRT",
                "flag": "https://restcountries.eu/data/prt.svg"
            },
            {
                "name": "Puerto Rico",
                "callingCodes": "+1787",
                "alpha3Code": "PRI",
                "flag": "https://restcountries.eu/data/pri.svg"
            },
            {
                "name": "Qatar",
                "callingCodes": "+974",
                "alpha3Code": "QAT",
                "flag": "https://restcountries.eu/data/qat.svg"
            },
            {
                "name": "Republic of Kosovo",
                "callingCodes": "+383",
                "alpha3Code": "KOS",
                "flag": "https://restcountries.eu/data/kos.svg"
            },
            {
                "name": "Réunion",
                "callingCodes": "+262",
                "alpha3Code": "REU",
                "flag": "https://restcountries.eu/data/reu.svg"
            },
            {
                "name": "Romania",
                "callingCodes": "+40",
                "alpha3Code": "ROU",
                "flag": "https://restcountries.eu/data/rou.svg"
            },
            {
                "name": "Russian Federation",
                "callingCodes": "+7",
                "alpha3Code": "RUS",
                "flag": "https://restcountries.eu/data/rus.svg"
            },
            {
                "name": "Rwanda",
                "callingCodes": "+250",
                "alpha3Code": "RWA",
                "flag": "https://restcountries.eu/data/rwa.svg"
            },
            {
                "name": "Saint Barthélemy",
                "callingCodes": "+590",
                "alpha3Code": "BLM",
                "flag": "https://restcountries.eu/data/blm.svg"
            },
            {
                "name": "Saint Helena, Ascension and Tristan da Cunha",
                "callingCodes": "+290",
                "alpha3Code": "SHN",
                "flag": "https://restcountries.eu/data/shn.svg"
            },
            {
                "name": "Saint Kitts and Nevis",
                "callingCodes": "+1869",
                "alpha3Code": "KNA",
                "flag": "https://restcountries.eu/data/kna.svg"
            },
            {
                "name": "Saint Lucia",
                "callingCodes": "+1758",
                "alpha3Code": "LCA",
                "flag": "https://restcountries.eu/data/lca.svg"
            },
            {
                "name": "Saint Martin (French part)",
                "callingCodes": "+590",
                "alpha3Code": "MAF",
                "flag": "https://restcountries.eu/data/maf.svg"
            },
            {
                "name": "Saint Pierre and Miquelon",
                "callingCodes": "+508",
                "alpha3Code": "SPM",
                "flag": "https://restcountries.eu/data/spm.svg"
            },
            {
                "name": "Saint Vincent and the Grenadines",
                "callingCodes": "+1784",
                "alpha3Code": "VCT",
                "flag": "https://restcountries.eu/data/vct.svg"
            },
            {
                "name": "Samoa",
                "callingCodes": "+685",
                "alpha3Code": "WSM",
                "flag": "https://restcountries.eu/data/wsm.svg"
            },
            {
                "name": "San Marino",
                "callingCodes": "+378",
                "alpha3Code": "SMR",
                "flag": "https://restcountries.eu/data/smr.svg"
            },
            {
                "name": "Sao Tome and Principe",
                "callingCodes": "+239",
                "alpha3Code": "STP",
                "flag": "https://restcountries.eu/data/stp.svg"
            },
            {
                "name": "Saudi Arabia",
                "callingCodes": "+966",
                "alpha3Code": "SAU",
                "flag": "https://restcountries.eu/data/sau.svg"
            },
            {
                "name": "Senegal",
                "callingCodes": "+221",
                "alpha3Code": "SEN",
                "flag": "https://restcountries.eu/data/sen.svg"
            },
            {
                "name": "Serbia",
                "callingCodes": "+381",
                "alpha3Code": "SRB",
                "flag": "https://restcountries.eu/data/srb.svg"
            },
            {
                "name": "Seychelles",
                "callingCodes": "+248",
                "alpha3Code": "SYC",
                "flag": "https://restcountries.eu/data/syc.svg"
            },
            {
                "name": "Sierra Leone",
                "callingCodes": "+232",
                "alpha3Code": "SLE",
                "flag": "https://restcountries.eu/data/sle.svg"
            },
            {
                "name": "Singapore",
                "callingCodes": "+65",
                "alpha3Code": "SGP",
                "flag": "https://restcountries.eu/data/sgp.svg"
            },
            {
                "name": "Sint Maarten (Dutch part)",
                "callingCodes": "+1721",
                "alpha3Code": "SXM",
                "flag": "https://restcountries.eu/data/sxm.svg"
            },
            {
                "name": "Slovakia",
                "callingCodes": "+421",
                "alpha3Code": "SVK",
                "flag": "https://restcountries.eu/data/svk.svg"
            },
            {
                "name": "Slovenia",
                "callingCodes": "+386",
                "alpha3Code": "SVN",
                "flag": "https://restcountries.eu/data/svn.svg"
            },
            {
                "name": "Solomon Islands",
                "callingCodes": "+677",
                "alpha3Code": "SLB",
                "flag": "https://restcountries.eu/data/slb.svg"
            },
            {
                "name": "Somalia",
                "callingCodes": "+252",
                "alpha3Code": "SOM",
                "flag": "https://restcountries.eu/data/som.svg"
            },
            {
                "name": "South Africa",
                "callingCodes": "+27",
                "alpha3Code": "ZAF",
                "flag": "https://restcountries.eu/data/zaf.svg"
            },
            {
                "name": "South Georgia and the South Sandwich Islands",
                "callingCodes": "+500",
                "alpha3Code": "SGS",
                "flag": "https://restcountries.eu/data/sgs.svg"
            },
            {
                "name": "Korea (Republic of)",
                "callingCodes": "+82",
                "alpha3Code": "KOR",
                "flag": "https://restcountries.eu/data/kor.svg"
            },
            {
                "name": "South Sudan",
                "callingCodes": "+211",
                "alpha3Code": "SSD",
                "flag": "https://restcountries.eu/data/ssd.svg"
            },
            {
                "name": "Spain",
                "callingCodes": "+34",
                "alpha3Code": "ESP",
                "flag": "https://restcountries.eu/data/esp.svg"
            },
            {
                "name": "Sri Lanka",
                "callingCodes": "+94",
                "alpha3Code": "LKA",
                "flag": "https://restcountries.eu/data/lka.svg"
            },
            {
                "name": "Sudan",
                "callingCodes": "+249",
                "alpha3Code": "SDN",
                "flag": "https://restcountries.eu/data/sdn.svg"
            },
            {
                "name": "Suriname",
                "callingCodes": "+597",
                "alpha3Code": "SUR",
                "flag": "https://restcountries.eu/data/sur.svg"
            },
            {
                "name": "Svalbard and Jan Mayen",
                "callingCodes": "+4779",
                "alpha3Code": "SJM",
                "flag": "https://restcountries.eu/data/sjm.svg"
            },
            {
                "name": "Swaziland",
                "callingCodes": "+268",
                "alpha3Code": "SWZ",
                "flag": "https://restcountries.eu/data/swz.svg"
            },
            {
                "name": "Sweden",
                "callingCodes": "+46",
                "alpha3Code": "SWE",
                "flag": "https://restcountries.eu/data/swe.svg"
            },
            {
                "name": "Switzerland",
                "callingCodes": "+41",
                "alpha3Code": "CHE",
                "flag": "https://restcountries.eu/data/che.svg"
            },
            {
                "name": "Syrian Arab Republic",
                "callingCodes": "+963",
                "alpha3Code": "SYR",
                "flag": "https://restcountries.eu/data/syr.svg"
            },
            {
                "name": "Taiwan",
                "callingCodes": "+886",
                "alpha3Code": "TWN",
                "flag": "https://restcountries.eu/data/twn.svg"
            },
            {
                "name": "Tajikistan",
                "callingCodes": "+992",
                "alpha3Code": "TJK",
                "flag": "https://restcountries.eu/data/tjk.svg"
            },
            {
                "name": "Tanzania, United Republic of",
                "callingCodes": "+255",
                "alpha3Code": "TZA",
                "flag": "https://restcountries.eu/data/tza.svg"
            },
            {
                "name": "Thailand",
                "callingCodes": "+66",
                "alpha3Code": "THA",
                "flag": "https://restcountries.eu/data/tha.svg"
            },
            {
                "name": "Timor-Leste",
                "callingCodes": "+670",
                "alpha3Code": "TLS",
                "flag": "https://restcountries.eu/data/tls.svg"
            },
            {
                "name": "Togo",
                "callingCodes": "+228",
                "alpha3Code": "TGO",
                "flag": "https://restcountries.eu/data/tgo.svg"
            },
            {
                "name": "Tokelau",
                "callingCodes": "+690",
                "alpha3Code": "TKL",
                "flag": "https://restcountries.eu/data/tkl.svg"
            },
            {
                "name": "Tonga",
                "callingCodes": "+676",
                "alpha3Code": "TON",
                "flag": "https://restcountries.eu/data/ton.svg"
            },
            {
                "name": "Trinidad and Tobago",
                "callingCodes": "+1868",
                "alpha3Code": "TTO",
                "flag": "https://restcountries.eu/data/tto.svg"
            },
            {
                "name": "Tunisia",
                "callingCodes": "+216",
                "alpha3Code": "TUN",
                "flag": "https://restcountries.eu/data/tun.svg"
            },
            {
                "name": "Turkey",
                "callingCodes": "+90",
                "alpha3Code": "TUR",
                "flag": "https://restcountries.eu/data/tur.svg"
            },
            {
                "name": "Turkmenistan",
                "callingCodes": "+993",
                "alpha3Code": "TKM",
                "flag": "https://restcountries.eu/data/tkm.svg"
            },
            {
                "name": "Turks and Caicos Islands",
                "callingCodes": "+1649",
                "alpha3Code": "TCA",
                "flag": "https://restcountries.eu/data/tca.svg"
            },
            {
                "name": "Tuvalu",
                "callingCodes": "+688",
                "alpha3Code": "TUV",
                "flag": "https://restcountries.eu/data/tuv.svg"
            },
            {
                "name": "Uganda",
                "callingCodes": "+256",
                "alpha3Code": "UGA",
                "flag": "https://restcountries.eu/data/uga.svg"
            },
            {
                "name": "Ukraine",
                "callingCodes": "+380",
                "alpha3Code": "UKR",
                "flag": "https://restcountries.eu/data/ukr.svg"
            },
            {
                "name": "United Arab Emirates",
                "callingCodes": "+971",
                "alpha3Code": "ARE",
                "flag": "https://restcountries.eu/data/are.svg"
            },
            {
                "name": "United Kingdom of Great Britain and Northern Ireland",
                "callingCodes": "+44",
                "alpha3Code": "GBR",
                "flag": "https://restcountries.eu/data/gbr.svg"
            },
            {
                "name": "United States of America",
                "callingCodes": "+1",
                "alpha3Code": "USA",
                "flag": "https://restcountries.eu/data/usa.svg"
            },
            {
                "name": "Uruguay",
                "callingCodes": "+598",
                "alpha3Code": "URY",
                "flag": "https://restcountries.eu/data/ury.svg"
            },
            {
                "name": "Uzbekistan",
                "callingCodes": "+998",
                "alpha3Code": "UZB",
                "flag": "https://restcountries.eu/data/uzb.svg"
            },
            {
                "name": "Vanuatu",
                "callingCodes": "+678",
                "alpha3Code": "VUT",
                "flag": "https://restcountries.eu/data/vut.svg"
            },
            {
                "name": "Venezuela (Bolivarian Republic of)",
                "callingCodes": "+58",
                "alpha3Code": "VEN",
                "flag": "https://restcountries.eu/data/ven.svg"
            },
            {
                "name": "Viet Nam",
                "callingCodes": "+84",
                "alpha3Code": "VNM",
                "flag": "https://restcountries.eu/data/vnm.svg"
            },
            {
                "name": "Wallis and Futuna",
                "callingCodes": "+681",
                "alpha3Code": "WLF",
                "flag": "https://restcountries.eu/data/wlf.svg"
            },
            {
                "name": "Western Sahara",
                "callingCodes": "+212",
                "alpha3Code": "ESH",
                "flag": "https://restcountries.eu/data/esh.svg"
            },
            {
                "name": "Yemen",
                "callingCodes": "+967",
                "alpha3Code": "YEM",
                "flag": "https://restcountries.eu/data/yem.svg"
            },
            {
                "name": "Zambia",
                "callingCodes": "+260",
                "alpha3Code": "ZMB",
                "flag": "https://restcountries.eu/data/zmb.svg"
            },
            {
                "name": "Zimbabwe",
                "callingCodes": "+263",
                "alpha3Code": "ZWE",
                "flag": "https://restcountries.eu/data/zwe.svg"
            }
        ];
        return countryCodes;
    };
    DbProvider.prototype.getDeviceInfo = function () {
        return this.device;
        // const data = { uuid: "1223645" };
        // return data;
    };
    DbProvider.prototype.getMonth = function () {
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return month;
    };
    DbProvider.prototype.isGuest = function () {
        return localStorage.getItem('isGuest');
    };
    DbProvider.prototype.chooseFile = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var isAndroid = (_this.device.platform.toLowerCase() === 'android') ? true : false;
            if (isAndroid) {
                _this.fileChooser.open().then(function (uri) {
                    _this.file.resolveLocalFilesystemUrl(uri).then(function (fileEntry) {
                        if (fileEntry) {
                            fileEntry.file(function (success) {
                                if (success.size <= '10000000') {
                                    var mimeType = success.type.split('/');
                                    if (success.type == 'application/pdf' ||
                                        mimeType[0] == 'image' ||
                                        mimeType[1] == 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
                                        if (success.type == 'application/pdf') {
                                            success['mime'] = 'pdf';
                                        }
                                        else if (mimeType[0] == 'image') {
                                            success['mime'] = 'image';
                                        }
                                        else {
                                            success['mime'] = 'word';
                                        }
                                        var imageName = success.localURL.split('/');
                                        var getName = decodeURIComponent(imageName[imageName.length - 1]);
                                        success['uploadFilePath'] = _this.sanitizer.bypassSecurityTrustResourceUrl(success.localURL);
                                        success['localURL'] = uri;
                                        success['getName'] = getName;
                                        resolve(success);
                                    }
                                    else {
                                        reject('Upload type .pdf,.jpg,.jpeg,.png,word document');
                                    }
                                }
                                else {
                                    reject('File size max 10 MB');
                                }
                            }, function (error) {
                                reject('Invalid File Format');
                            });
                        }
                    }, function (error) {
                        reject(error);
                    });
                }).catch(function (e) {
                    console.log(e);
                    reject();
                });
            }
            else {
                _this.iosFilePicker.pickFile()
                    .then(function (uri) {
                    resolve(uri);
                })
                    .catch(function (err) {
                    console.log(err);
                    reject();
                });
            }
        });
    };
    DbProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Events */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__["a" /* NativeStorage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__["a" /* NativeStorage */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__["a" /* LocalNotifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__["a" /* LocalNotifications */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_7__ionic_native_diagnostic__["a" /* Diagnostic */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ionic_native_diagnostic__["a" /* Diagnostic */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__["a" /* NativeGeocoder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__["a" /* NativeGeocoder */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_9__ionic_native_social_sharing__["a" /* SocialSharing */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__ionic_native_social_sharing__["a" /* SocialSharing */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__["a" /* InAppBrowser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__["a" /* InAppBrowser */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_11__ionic_native_calendar__["a" /* Calendar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11__ionic_native_calendar__["a" /* Calendar */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__["a" /* Device */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_13__utils_toast__["a" /* ToasterProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_13__utils_toast__["a" /* ToasterProvider */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_14__utils_loader__["a" /* LoadingProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_14__utils_loader__["a" /* LoadingProvider */]) === "function" && _q || Object, typeof (_r = typeof __WEBPACK_IMPORTED_MODULE_16__ionic_native_file_chooser__["a" /* FileChooser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_16__ionic_native_file_chooser__["a" /* FileChooser */]) === "function" && _r || Object, typeof (_s = typeof __WEBPACK_IMPORTED_MODULE_15__ionic_native_file_picker__["a" /* IOSFilePicker */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_15__ionic_native_file_picker__["a" /* IOSFilePicker */]) === "function" && _s || Object, typeof (_t = typeof __WEBPACK_IMPORTED_MODULE_17__ionic_native_file__["a" /* File */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_17__ionic_native_file__["a" /* File */]) === "function" && _t || Object, typeof (_u = typeof __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */]) === "function" && _u || Object, typeof (_v = typeof __WEBPACK_IMPORTED_MODULE_19__angular_platform_browser__["c" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_19__angular_platform_browser__["c" /* DomSanitizer */]) === "function" && _v || Object])
    ], DbProvider);
    return DbProvider;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v;
}());

//# sourceMappingURL=db.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This provider is responsible for calling all auth api.
 *
 * @export
 * @class AuthProvider
 * @author tathagata sur
 */
var AuthProvider = /** @class */ (function () {
    function AuthProvider(http) {
        this.http = http;
        this.REGISTRATION_URL = 'user_register.json';
        this.REGISTRATION_OTP_URL = 'user_register_otp.json';
        this.LOGIN_URL = 'user_login.json';
        this.TRAMSCONDITION_URL = 'term_of_use';
        this.FORGOTPASS_URL = 'user_forgot_pass.json';
        this.RESET_PASS_URL = 'user_new_pass.json';
        this.LOGOUT_URL = 'user_custom_logout.json';
        this.VERIFY_OTP_URL = 'otp_verify.json';
        this.UPDATE_DEVICE_TOKEN_URL = 'update_device_token.json';
    }
    AuthProvider.prototype.postRegistrationapi = function (postData) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.REGISTRATION_URL;
        return this.http.post(url, postData);
    };
    ;
    AuthProvider.prototype.postRegisterOtpApi = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.REGISTRATION_OTP_URL;
        return this.http.post(url, data);
    };
    ;
    AuthProvider.prototype.postLoginapi = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.LOGIN_URL;
        return this.http.post(url, data);
    };
    ;
    AuthProvider.prototype.getTermsConditionApi = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.TRAMSCONDITION_URL;
        return this.http.get(url);
    };
    ;
    AuthProvider.prototype.postForgotPasswordApi = function (postData) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.FORGOTPASS_URL;
        return this.http.post(url, postData);
    };
    ;
    AuthProvider.prototype.postResetPasswordApi = function (postData) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.RESET_PASS_URL;
        return this.http.post(url, postData);
    };
    ;
    AuthProvider.prototype.postLogoutApi = function (postData) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.LOGOUT_URL;
        return this.http.post(url, postData);
    };
    ;
    AuthProvider.prototype.postVerifyOtp = function (postData) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.VERIFY_OTP_URL;
        return this.http.post(url, postData);
    };
    ;
    AuthProvider.prototype.updateDeviceToken = function (postData) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.UPDATE_DEVICE_TOKEN_URL;
        return this.http.post(url, postData);
    };
    ;
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 187:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 187;

/***/ }),

/***/ 231:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/application-form/application-form.module": [
		728,
		7
	],
	"../pages/booking-summary/booking-summary.module": [
		729,
		32
	],
	"../pages/construction-status/construction-status.module": [
		730,
		15
	],
	"../pages/country-list/country-list.module": [
		731,
		31
	],
	"../pages/dashboard/dashboard.module": [
		732,
		14
	],
	"../pages/enquiry-form/enquiry-form.module": [
		733,
		6
	],
	"../pages/forgot-password/forgot-password.module": [
		734,
		30
	],
	"../pages/get-in-touch/get-in-touch.module": [
		735,
		5
	],
	"../pages/godrej-home/godrej-home.module": [
		736,
		0
	],
	"../pages/home/home.module": [
		759,
		29
	],
	"../pages/inventory-booking/inventory-booking.module": [
		737,
		4
	],
	"../pages/inventory-details/inventory-details.module": [
		738,
		13
	],
	"../pages/inventory-filter/inventory-filter.module": [
		739,
		16
	],
	"../pages/know-me/know-me.module": [
		740,
		28
	],
	"../pages/menu/menu.module": [
		741,
		27
	],
	"../pages/my-documents-add/my-documents-add.module": [
		742,
		12
	],
	"../pages/my-documents-list/my-documents-list.module": [
		743,
		3
	],
	"../pages/no-internet/no-internet.module": [
		744,
		26
	],
	"../pages/payment-summary/payment-summary.module": [
		745,
		25
	],
	"../pages/payment/payment.module": [
		746,
		1
	],
	"../pages/project-details/project-details.module": [
		760,
		2
	],
	"../pages/property-payment/property-payment.module": [
		747,
		11
	],
	"../pages/reset-password/reset-password.module": [
		748,
		24
	],
	"../pages/schedule-visit/schedule-visit.module": [
		749,
		10
	],
	"../pages/search-list/search-list.module": [
		750,
		9
	],
	"../pages/search/search.module": [
		751,
		23
	],
	"../pages/signup/signup.module": [
		752,
		22
	],
	"../pages/terms-condition/terms-condition.module": [
		753,
		21
	],
	"../pages/thank-you/thank-you.module": [
		754,
		20
	],
	"../pages/tutorial/tutorial.module": [
		755,
		19
	],
	"../pages/user-profile/user-profile.module": [
		756,
		8
	],
	"../pages/varify-otp/varify-otp.module": [
		757,
		18
	],
	"../pages/wishlist/wishlist.module": [
		758,
		17
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 231;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This provider is responsible for checking network state.
 *
 * @export
 * @class NetworkProvider
 * @author Mithun Sen
 */
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(network, platform) {
        var _this = this;
        this.network = network;
        this.platform = platform;
        this.platform.ready().then(function () {
            _this.isOnline();
        });
    }
    /**
     * Subscribe to the disconnect event to know when the network is not available
     */
    NetworkProvider.prototype.disconnectSub = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.disConnectionObj = _this.network.onDisconnect().subscribe(function () {
                console.log('No network connection');
                resolve('No network connection');
            });
        });
    };
    /**
     * Unsubscribe to the disconnect event to know when the network is not available
     */
    NetworkProvider.prototype.disconnectUnsub = function () {
        this.disConnectionObj.unsubscribe();
    };
    /**
     * Subscribe to the disconnect event to know when the network is available
     */
    NetworkProvider.prototype.connectSub = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connectionObj = _this.network.onConnect().subscribe(function () {
                console.log("network connected!");
                resolve("network connected!");
            });
        });
    };
    /**
     * Unsubscribe to the disconnect event to know when the network is not available
     */
    NetworkProvider.prototype.connectUnsub = function () {
        this.connectionObj.unsubscribe();
    };
    /**
     * Know the current network state
     */
    NetworkProvider.prototype.isOnline = function () {
        if (navigator && navigator["connection"] && navigator["connection"]["type"] === 'none') {
            return false;
        }
        else {
            return true;
        }
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This provider is responsible for calling property search api.
 *
 * @export
 * @class SearchApiProvider
 * @author tathagata sur
 */
var SearchApiProvider = /** @class */ (function () {
    function SearchApiProvider(http) {
        this.http = http;
        this.PROPERTY_FILTER_INFO = 'property_filter_info.json';
        this.PROPERTY_LISTING_URL = 'property-listing';
        this.PROJECT_SEARCH = 'property_api_search.json';
    }
    SearchApiProvider.prototype.propertySearchInfo = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.PROPERTY_FILTER_INFO;
        return this.http.post(url, {});
    };
    SearchApiProvider.prototype.propertysearch = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.PROPERTY_LISTING_URL;
        return this.http.get(url);
    };
    SearchApiProvider.prototype.projectSearch = function (searchdata) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.PROJECT_SEARCH;
        return this.http.post(url, searchdata);
    };
    SearchApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], SearchApiProvider);
    return SearchApiProvider;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WishListApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This provider is responsible for calling alll wish list Api  wish listpage
 *
 * @export
 * @class WishListApiProvider
 * @author tathagata sur
 */
var WishListApiProvider = /** @class */ (function () {
    function WishListApiProvider(http) {
        this.http = http;
        this.WISH_LIST_URL = 'res_wishlist_items.json';
        this.MODIFY_WISH_LIST = 'res_wishlist.json';
        this.COUNT_WISH_LIST = 'res_wishlist_count.json';
    }
    WishListApiProvider.prototype.getwishlisting = function (param) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.WISH_LIST_URL;
        return this.http.post(url, param);
    };
    WishListApiProvider.prototype.modifyWishlist = function (param) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.MODIFY_WISH_LIST;
        return this.http.post(url, param);
    };
    WishListApiProvider.prototype.countWishlist = function (param) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + this.COUNT_WISH_LIST;
        return this.http.post(url, param);
    };
    WishListApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], WishListApiProvider);
    return WishListApiProvider;
}());

//# sourceMappingURL=wishlistapi.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashordApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_constants__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * This provider is responsible for calling all dashbord api.
 *
 * @export
 * @class DashordApiProvider
 * @author tathagata sur
 */
var DashordApiProvider = /** @class */ (function () {
    function DashordApiProvider(http) {
        this.http = http;
    }
    DashordApiProvider.prototype.getRmContacts = function (payload) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_app_constants__["a" /* AppConst */].baseUrl + 'get_rm_info.json';
        return this.http.post(url, payload);
    };
    DashordApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], DashordApiProvider);
    return DashordApiProvider;
}());

//# sourceMappingURL=dashbord.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_apis_auth__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_db_db__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_storage__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_network__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_path__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_file_transfer__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_app_version__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_vibration__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_local_notifications__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_geolocation__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_diagnostic__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_native_geocoder__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_in_app_browser__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_calendar__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_device__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_utils_network__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_apis_search__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__agm_core__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_apis_wishlistapi__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_chooser__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_picker__ = __webpack_require__(241);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















/* import { FCM } from '@ionic-native/fcm'; */















/**
 * Add all dependency module here, don't forgot export them otherwise it will not accessable to other modules.
 * All Third party module should import here.
 * All Ionic native provider should declear here.
 *
 * @export
 * @class SharedModule
 */
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_24__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_27__agm_core__["a" /* AgmCoreModule */],
                __WEBPACK_IMPORTED_MODULE_27__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyAbepksU0CKU3PylVv71JAe9sMPxFnYmHE'
                }),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_24__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_27__agm_core__["a" /* AgmCoreModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_1__providers_apis_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_2__providers_utils_loader__["a" /* LoadingProvider */],
                __WEBPACK_IMPORTED_MODULE_3__providers_utils_toast__["a" /* ToasterProvider */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__providers_db_db__["a" /* DbProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_storage__["a" /* NativeStorage */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_10_ionic_angular__["q" /* ToastController */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_app_version__["a" /* AppVersion */],
                //FCM,
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_vibration__["a" /* Vibration */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_local_notifications__["a" /* LocalNotifications */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_calendar__["a" /* Calendar */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_25__providers_utils_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_apis_search__["a" /* SearchApiProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_apis_wishlistapi__["a" /* WishListApiProvider */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_picker__["a" /* IOSFilePicker */],
            ]
        })
    ], SharedModule);
    return SharedModule;
}());

//# sourceMappingURL=app.shared.module.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyHttpInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_toast__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_network__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_constants__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var defaultTimeout = 20000;
var MyHttpInterceptor = /** @class */ (function () {
    function MyHttpInterceptor(toast, network, events, loader) {
        this.toast = toast;
        this.network = network;
        this.events = events;
        this.loader = loader;
        this.islogout = false;
        console.log('MyHttpInterceptor');
    }
    MyHttpInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        var newRequest = req.clone({
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["e" /* HttpHeaders */]({
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
                //'Authorization': localStorage.getItem('Authorization') ? localStorage.getItem('Authorization') : '',
                'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
            })
        });
        /** send cloned request with header to the next handler. */
        if (this.network.isOnline()) {
            // console.log(newRequest);
            return next.handle(newRequest).timeout(defaultTimeout).do(function (event) {
                // do nothing
                if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["f" /* HttpResponse */]) {
                    _this.loader.hide();
                    if (Object.values(__WEBPACK_IMPORTED_MODULE_5__app_app_constants__["a" /* AppConst */].HTTP_SUCESS_STATUS).indexOf(event.status) > -1) {
                        var data = event.url.split('/');
                        if (data.length > 0 && data[data.length - 1] == 'user_login.json') {
                            _this.islogout = false;
                        }
                        else {
                            if (event.body.status == __WEBPACK_IMPORTED_MODULE_5__app_app_constants__["a" /* AppConst */].HTTP_ERROR_STATUS.SERVER_INTERNAL_ERROR && event.body.msg.toLowerCase() == 'unauthorised access' && !_this.islogout) {
                                _this.islogout = true;
                                _this.toast.show(event.body.msg);
                                _this.logout();
                            }
                        }
                    }
                    else if (Object.values(__WEBPACK_IMPORTED_MODULE_5__app_app_constants__["a" /* AppConst */].HTTP_SUCESS_STATUS).indexOf(event.status) == -1) {
                        _this.loader.hide();
                        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw('');
                    }
                }
            }, function (err) {
                if (err instanceof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpErrorResponse */]) {
                    _this.loader.hide();
                    // Check for common HTTP Error Response
                    if (Object.values(__WEBPACK_IMPORTED_MODULE_5__app_app_constants__["a" /* AppConst */].HTTP_ERROR_STATUS).indexOf(err.status) > -1) {
                        console.log('HTTP Error:::', err);
                        // this.toast.show(err.error.message);
                        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(err);
                    }
                    else {
                        console.log('Unknown HTTP Error:::', err);
                        // this.toast.show(err.error.message);
                        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(err);
                    }
                }
                else {
                    _this.loader.hide();
                    console.log('Invalid Server Response:::', err);
                    _this.toast.show('Invalid Server Response');
                    return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(err);
                }
            });
        }
        else {
            this.loader.hide();
            this.toast.show('No network connection found');
            return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].empty();
        }
    };
    MyHttpInterceptor.prototype.logout = function () {
        var data = { title: 'Logout', component: 'HomePage' };
        this.events.publish('afterLoginRedirect', data);
    };
    MyHttpInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__utils_toast__["a" /* ToasterProvider */], __WEBPACK_IMPORTED_MODULE_4__utils_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_6__utils_loader__["a" /* LoadingProvider */]])
    ], MyHttpInterceptor);
    return MyHttpInterceptor;
}());

//# sourceMappingURL=my-http-interceptor.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(401);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(725);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser_animations__ = __webpack_require__(726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_shared_module__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_interceptors_my_http_interceptor__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_apis_dashbord__ = __webpack_require__(392);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/application-form/application-form.module#ApplicationFormPageModule', name: 'ApplicationFormPage', segment: 'application-form', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/booking-summary/booking-summary.module#BookingSummaryPageModule', name: 'BookingSummaryPage', segment: 'booking-summary', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/construction-status/construction-status.module#ConstructionStatusPageModule', name: 'ConstructionStatusPage', segment: 'construction-status', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/country-list/country-list.module#CountryListPageModule', name: 'CountryListPage', segment: 'country-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/enquiry-form/enquiry-form.module#EnquiryFormPageModule', name: 'EnquiryFormPage', segment: 'enquiry-form', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgot-password/forgot-password.module#ForgotPasswordPageModule', name: 'ForgotPasswordPage', segment: 'forgot-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/get-in-touch/get-in-touch.module#GetInTouchPageModule', name: 'GetInTouchPage', segment: 'get-in-touch', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/godrej-home/godrej-home.module#GodrejHomePageModule', name: 'GodrejHomePage', segment: 'godrej-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inventory-booking/inventory-booking.module#InventoryBookingPageModule', name: 'InventoryBookingPage', segment: 'inventory-booking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inventory-details/inventory-details.module#InventoryDetailsPageModule', name: 'InventoryDetailsPage', segment: 'inventory-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inventory-filter/inventory-filter.module#InventoryFilterPageModule', name: 'InventoryFilterPage', segment: 'inventory-filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/know-me/know-me.module#KnowMePageModule', name: 'KnowMePage', segment: 'know-me', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/my-documents-add/my-documents-add.module#MyDocumentsAddPageModule', name: 'MyDocumentsAddPage', segment: 'my-documents-add', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/my-documents-list/my-documents-list.module#MyDocumentsListPageModule', name: 'MyDocumentsListPage', segment: 'my-documents-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/no-internet/no-internet.module#NoInternetPageModule', name: 'NoInternetPage', segment: 'no-internet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment-summary/payment-summary.module#PaymentSummaryPageModule', name: 'PaymentSummaryPage', segment: 'payment-summary', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/property-payment/property-payment.module#PropertyPaymentPageModule', name: 'PropertyPaymentPage', segment: 'property-payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reset-password/reset-password.module#ResetPasswordPageModule', name: 'ResetPasswordPage', segment: 'reset-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/schedule-visit/schedule-visit.module#ScheduleVisitPageModule', name: 'ScheduleVisitPage', segment: 'schedule-visit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-list/search-list.module#SearchListPageModule', name: 'SearchListPage', segment: 'search-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/terms-condition/terms-condition.module#TermsConditionPageModule', name: 'TermsConditionPage', segment: 'terms-condition', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/thank-you/thank-you.module#ThankYouPageModule', name: 'ThankYouPage', segment: 'thank-you', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-profile/user-profile.module#UserProfilePageModule', name: 'UserProfilePage', segment: 'user-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/varify-otp/varify-otp.module#VarifyOtpPageModule', name: 'VarifyOtpPage', segment: 'varify-otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/wishlist/wishlist.module#WishlistPageModule', name: 'WishlistPage', segment: 'wishlist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/project-details/project-details.module#ProjectDetailsPageModule', name: 'ProjectDetailsPage', segment: 'project-details', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_7__app_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["b" /* CommonModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_8__providers_interceptors_my_http_interceptor__["a" /* MyHttpInterceptor */],
                    multi: true
                },
                __WEBPACK_IMPORTED_MODULE_9__providers_apis_dashbord__["a" /* DashordApiProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppConst; });
/**
 * All application constant should define here.
 *
 * @export
 * @class AppConst
 * @author TATHAGATA SUR
 */
var AppConst = /** @class */ (function () {
    function AppConst() {
    }
    AppConst.baseUrl = "http://10.0.4.77/gpl-project/gpl-api/"; //Subhojit
    // public static readonly baseUrl = "http://10.0.4.182/gpl-project/gpl-api/"; //GARGI Di
    // public static readonly baseUrl = 'http://10.0.4.75/gpl-project/api/';
    //public static readonly baseUrl = 'http://43.242.212.209/gpl-project/gpl-api/'; //QA
    //public static readonly baseUrl = 'http://43.242.212.166/gpl-project/gpl-api/'; // UAT 
    //public static readonly baseUrl ="http://10.0.4.75/gpl-project/";
    /**  server HTTP header status code */
    AppConst.HTTP_ERROR_STATUS = {
        BAD_REQUEST: 400,
        UN_AUTHORIZED: 401,
        NOT_FOUND: 404,
        SERVER_INTERNAL_ERROR: 500
    };
    // public static readonly Authorization = 'Basic Z2FyZ2kuZGFzQGluZHVzbmV0LmNvLmluOjEyMzQ1Ng==';
    /** application's bussiness related api status code */
    AppConst.HTTP_SUCESS_STATUS = {
        OK: 200,
        CREATED: 201,
        ACCEPTED: 202,
        NO_RECORD: 204
    };
    /** application's bussiness related api status code */
    AppConst.API_STATUS = {
        OK: 200,
        NO_RECORD: 204
    };
    return AppConst;
}());

//# sourceMappingURL=app.constants.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * This provider is responsible for managing loader.
 *
 * @export
 * @class LoaderProvider
 * @author tathagata sur
 */
var LoadingProvider = /** @class */ (function () {
    function LoadingProvider(loaderCtrl) {
        this.loaderCtrl = loaderCtrl;
        this.loader = null;
    }
    /**
     * This method is responsible for show loader with message.
     *
     * @param {string} [message] pass message if you want to show custom message, otherwise it will show 'Please wait...'
     * @memberof LoadingProvider
     */
    LoadingProvider.prototype.show = function (message) {
        console.log(message);
        if (this.loader !== null) {
            this.loader.dismiss();
            this.loader = null;
        }
        this.loader = this.loaderCtrl.create({
            spinner: 'ios',
            content: message ? message : 'Please wait...',
            duration: 1000 * 60 * 2
        });
        this.loader.present();
    };
    /**
     * This method is responsible to hide loader.
     *
     * @memberof LoadingProvider
     */
    LoadingProvider.prototype.hide = function () {
        if (this.loader !== null) {
            this.loader.dismiss();
            this.loader = null;
        }
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loader.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToasterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * This provider is responsible for managing loader.
 *
 * @export
 * @class LoaderProvider
 * @author tathagata sur
 */
var ToasterProvider = /** @class */ (function () {
    function ToasterProvider(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    /**
     * Show toast message with 3 sec duration
     *
     * @param {string} message the toast message
     * @memberof ToasterProvider
     */
    ToasterProvider.prototype.show = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: "top"
        });
        toast.present();
    };
    /**
     * Toast with message and user defined duration
     *
     * @param {string} message the toast message
     * @param {number} duration the duration in milisecond
     * @memberof ToasterProvider
     */
    ToasterProvider.prototype.showWithDuration = function (message, duration) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: "top"
        });
        toast.present();
    };
    ToasterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */]])
    ], ToasterProvider);
    return ToasterProvider;
}());

//# sourceMappingURL=toast.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_db_db__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_app_version__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_vibration__ = __webpack_require__(248);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_local_notifications__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_apis_auth__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_utils_loader__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_utils_toast__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/* import { FCM } from '@ionic-native/fcm'; */





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, app, storagePrivider, network, modalCtrl, events, appVersion, 
        /* private fcm: FCM, */
        vibration, localNotifications, Authapi, loader, toast, config) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.app = app;
        this.storagePrivider = storagePrivider;
        this.network = network;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.appVersion = appVersion;
        this.vibration = vibration;
        this.localNotifications = localNotifications;
        this.Authapi = Authapi;
        this.loader = loader;
        this.toast = toast;
        this.config = config;
        this.pages = [
            { title: 'Profile', component: 'UserProfilePage' }
        ];
        this.showSubmenu = false;
        this.errorMessages = {
            offline: "Sorry! You are offline",
            slowNetwork: "Sorry! You are on a slow network. Please connect to a higher network.",
            backButton: "Press again to exit."
        };
        this.profileDetails = {};
        localStorage.setItem('isGuest', '0');
        this.initializeApp();
        this.initializesidepanel();
        this.events.subscribe('setUserDetails', function () {
            _this.getUserDetails();
        });
        this.events.subscribe('openSearchListPage', function (pageName) {
            _this.setRootPage(pageName);
        });
        this.events.subscribe('afterLoginRedirect', function (pageName) {
            _this.initializesidepanel();
            _this.openPage(pageName);
        });
        this.events.subscribe('guestToUser', function () {
            _this.initializesidepanel();
        });
    }
    MyApp.prototype.initializesidepanel = function () {
        this.isGuest = this.storagePrivider.isGuest();
        if (this.isGuest && this.isGuest == '1') {
            this.pages2 = [
                { title: 'Godrej Home', component: 'GodrejHomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/home-icon.png" },
                { title: 'Logout', component: 'HomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/logout.png" }
            ];
        }
        else {
            this.pages2 = [
                { title: 'Godrej Home', component: 'GodrejHomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/home-icon.png" },
                { title: 'My Dashboard', component: 'DashboardPage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-dashboard.png" },
                {
                    title: 'My Properties', component: '', submenu: [
                        { title: 'The Trees / GTR20154', click: false },
                        { title: 'Godrej Imerald / GEM20154', click: false }
                    ], icon: "assets/imgs/menuicon/my-properties.png"
                },
                { title: 'My Tasks', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-tasks.png" },
                { title: 'My Documents', component: 'MyDocumentsListPage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-documents.png" },
                { title: 'My Wishlist', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-wishlist.png" },
                { title: 'My Profile', component: 'UserProfilePage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-profile.png" },
                { title: 'My Rewards', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-reward.png" },
                { title: 'My Service Request', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-service-requests.png" },
                { title: 'Home loan Support', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/home-loan-support.png" },
                { title: 'Logout', component: 'HomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/logout.png" }
            ];
        }
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.count = 0;
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            //statusBar.styleDefault();
            _this.statusBar.styleLightContent();
            _this.statusBar.backgroundColorByHexString('#37b0de');
            _this.splashScreen.hide();
            //debugger
            /* Version Code & Version Number */
            _this.appVersion.getVersionCode().then(function (code) {
                localStorage.setItem('versionCode', code);
            }).catch(function (err) {
                console.log(err);
            });
            _this.config.set('backButtonIcon', 'ios-arrow-back-outline');
            _this.appVersion.getVersionNumber().then(function (vNum) {
                localStorage.setItem('versionNumber', vNum);
            }).catch(function (err) {
                console.log(err);
            });
            /* Version Code & Version Number */
            /* Network Change Status */
            _this.network.onchange().subscribe(function (value) {
                _this.storagePrivider.set("network", value.type);
                //this.noNetModal = this.modalCtrl.create("NoInternetPage", {});
                if (value.type == "online") {
                    //this.events.publish('dismissModal');
                    //this.storagePrivider.localNotificationNoNetwork("online");
                    /* this.network.onConnect().subscribe((type: any) => {
                      this.storagePrivider.set("networkType", this.network.type);
                      if (this.network.type == "2g") {
                        this.toast.show(this.errorMessages.slowNetwork);
                      }
                    }); */
                }
                else {
                    //this.storagePrivider.localNotificationNoNetwork("offline");
                    //this.noNetModal.onDidDismiss((item: any) => {
                    //console.log(overlayView);
                    //});
                    //this.noNetModal.present();
                    //this.storagePrivider.updateLocalNotification(0);
                }
            });
            /* Network Change Status */
            /* Set Root Page */
            if (localStorage.getItem('isLoggedIn') == '1') {
                _this.rootPage = 'GodrejHomePage';
                _this.pages2[0].click = true;
                _this.getUserDetails();
            }
            else {
                _this.pages2[0].click = false;
                _this.nav.setRoot('HomePage', {}, {
                    animate: true,
                    direction: 'forward'
                });
                //this.rootPage = 'HomePage';
            }
            /* Set Root Page */
            /* Back Button Exit / Pop Page */
            _this.platform.registerBackButtonAction(function (event) {
                var pageName = _this.nav.getActive().name;
                /* Modal Or Popup Checks */
                var overlayView = _this.app.getActiveNavs();
                if (overlayView[0]._views[0].data.hasOwnProperty("component") &&
                    overlayView[0]._views[0].data.component.name != "NoInternetPage" &&
                    overlayView[0]._views[0].data.component.name != "") {
                    if (overlayView[0]._views[0].data.component.name == "SearchPage") {
                        _this.events.publish('closeSearchModal');
                    }
                    else if (overlayView[0]._views[0].data.component.name == "InventoryFilterPage") {
                        _this.events.publish('closeInventoryModal');
                    }
                }
                else {
                    if (pageName == "HomePage" || pageName == "DashboardPage") {
                        if (_this.count < 2) {
                            _this.count++;
                            _this.toast.show(_this.errorMessages.backButton);
                            setTimeout(function () {
                                _this.count = 0;
                            }, 3000);
                        }
                        else {
                            _this.platform.exitApp();
                        }
                    }
                    else {
                        _this.nav.pop();
                    }
                }
            }, 0);
            /* FCM TOKEN GENERATION & PUSH TAP */
            /* this.fcm.getToken().then(token => {
              console.log(token);
            });
            this.fcm.onNotification().subscribe((data) => {
        
              if(data.wasTapped){
                console.log("Received in background");
              } else {
                this.events.publish('showNotiDot'); // Header notification dot show
                //this.vibration.vibrate(1000);
                this.storagePrivider.localNotificationWhenAppIsInForeGround(data);
                this.localNotifications.on('click').subscribe((noti)=>{
                  console.log(noti);
                },(eopts)=>{
                  console.log(eopts);
                });
        
                console.log("Received in foreground");
              };
            }); */
            /* FCM TOKEN GENERATION & PUSH TAP */
            _this.nav.viewDidEnter.subscribe(function (view) {
                /* if(localStorage.getItem("pageName") == "ProjectDetailsPage"){
                  this.events.publish('loadHomePage');
                }
                localStorage.setItem("pageName",view.instance.constructor.name); */
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        var _this = this;
        if (page.component == '') {
            alert('Page is under construction');
        }
        else {
            if (page.title == "Logout") {
                // localStorage.removeItem('token');
                // localStorage.removeItem('Cookie');
                localStorage.clear();
                var previousClick = this.pages2.findIndex(function (X) { return X.click == true; });
                if (previousClick > -1) {
                    this.pages2[previousClick].click = false;
                }
                this.storagePrivider.getVal("userData").then(function (userData) {
                    var userParseData = JSON.parse(userData);
                    var data = {
                        user_id: userParseData.data.userid
                    };
                    _this.storagePrivider.clear();
                    //localStorage.setItem("isTutorial", "1");
                    _this.Authapi.postLogoutApi(data).subscribe(function (response) {
                        if (response.status == "200") {
                            _this.toast.show(response.msg);
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }).catch(function (error) {
                    console.log("Error");
                });
                this.nav.setRoot(page.component);
            }
            else {
                /* this.pages2[0].click = true;
                console.log(this.pages2); */
                var clickIndex = this.pages2.findIndex(function (X) { return X.title == page.title; });
                var previousClick = this.pages2.findIndex(function (X) { return X.click == true; });
                //console.log(clickIndex, previousClick);
                if (clickIndex != previousClick) {
                    if (previousClick > -1) {
                        this.pages2[previousClick].click = false;
                    }
                    this.pages2[clickIndex].click = true;
                    if (page.title == "My Dashboard" || page.title == "Godrej Home" || page.title == "My Documents") {
                        this.nav.setRoot(page.component, {}, {
                            animate: true,
                            direction: 'forward'
                        });
                    }
                    else {
                        this.nav.push(page.component);
                    }
                }
            }
        }
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        //this.nav.setRoot(page.component);
    };
    MyApp.prototype.showSubMenu = function () {
        this.showSubmenu = !this.showSubmenu;
    };
    MyApp.prototype.getUserDetails = function () {
        //this.loader.show('Please wait...');
        /* this.storagePrivider.getVal('userData').then((data: any) => {
          this.loader.hide();
          if (data != "error") {
            this.profileDetails = data.userDetails;
            this.nameInitials = data.initials;
            this.userPic = data.image;
          } else {
            
          }
    
        }); */
        var userDetails = JSON.parse(localStorage.getItem('userData'));
        var fInitials = userDetails.data.first_name.substring(0, 1).toUpperCase();
        var lInitials = userDetails.data.last_name.substring(0, 1).toUpperCase();
        var fullInitial = fInitials + lInitials;
        this.profileDetails = userDetails;
        this.nameInitials = fullInitial;
        this.userPic = '';
        var newDate = new Date();
        this.date = newDate.getDate();
        this.time = newDate.getTime();
        this.year = newDate.getFullYear();
        var monthIndex = newDate.getMonth();
        this.month = this.storagePrivider.getMonth()[monthIndex];
    };
    MyApp.prototype.setRootPage = function (pageName) {
        var previousPage = this.pages2.findIndex(function (X) { return X.click == true; });
        if (previousPage > -1) {
            this.pages2[previousPage].click = false;
        }
        this.nav.setRoot(pageName, {}, {
            animate: true,
            direction: 'forward'
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\projects\gpl\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <!-- <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Pages</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header> -->\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <div class="profile_image" [ngClass]="{\'noBorder\': userPic == \'\'}">\n\n        <!-- <img class="imageshow" *ngIf="userPic != \'\' " src="{{userPic}}" alt=""> -->\n\n        <img class="imageshow" *ngIf="userPic != \'\' && isGuest != \'1\'" src="{{userPic}}" alt="">\n\n        <div class="initials" *ngIf="userPic == \'\'&& isGuest != \'1\'">\n\n          <div><div>{{nameInitials}}</div></div>\n\n        </div>\n\n        <div class="initials" *ngIf="isGuest == \'1\'">\n\n          GU\n\n        </div>\n\n      </div>\n\n      <div class="userDetails">\n\n        <div class="usrName">\n\n          <div *ngIf="isGuest != \'1\'">\n\n            {{profileDetails?.data?.first_name}} {{profileDetails?.data?.last_name}}\n\n          </div>\n\n          <div *ngIf="isGuest == \'1\'">\n\n            Guest\n\n          </div>\n\n          <div class="edit" menuClose (click)="openPage(pages2[6])" *ngIf="isGuest != \'1\'">\n\n            <!-- <ion-icon name="md-create"></ion-icon> -->\n\n            <img src="assets/imgs/penicon.png" alt="">\n\n          </div>\n\n          <div class="phoneNumber" *ngIf="isGuest != \'1\'">\n\n            <div class="icon">\n\n              <ion-icon name="ios-phone-portrait-outline"></ion-icon>\n\n            </div>\n\n            <div class="mobile">\n\n              {{profileDetails?.data?.mob_no}}\n\n            </div>\n\n          </div>\n\n          <div class="lastVisited">\n\n            Last visited {{date}} {{month}} {{year}} {{time | date:\'HH:mm\'}}\n\n          </div>\n\n        </div>\n\n      </div>\n\n      <div class="clearfix"></div>\n\n    </ion-list>\n\n\n\n    <ion-list class="menus">\n\n      <div ion-item *ngFor="let p of pages2; let i=index;" class="buttonMenu" [ngClass]="{\'active\': p.click == true}">\n\n        <div class="mainmenu" menuClose (click)="openPage(p)">\n\n          <div class="menuicon">\n\n            <img src="{{p.icon}}" alt="">\n\n          </div>\n\n          <div class="menutxt">{{p.title}}</div>\n\n        </div>\n\n        <div class="submenu" *ngIf="p?.submenu.length >0">\n\n          <div class="iconDown" (click)="showSubMenu()">\n\n            <ion-icon name="ios-arrow-down-outline" [ngClass]="{\'upArrow\': showSubmenu== true}" class="arrowCustom"></ion-icon>\n\n          </div>\n\n          <div *ngIf="showSubmenu">\n\n            <div class="subLoop" *ngFor="let sub of p.submenu" [ngClass]="{\'active\': sub.click == true}">\n\n              <div class="menuName" menuClose (click)="openPage(sub)">\n\n                <ion-icon name="md-arrow-dropright"></ion-icon> &nbsp;{{sub.title}}\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n<ion-nav #content [root]="rootPage"></ion-nav>'/*ion-inline-end:"D:\projects\gpl\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */],
            __WEBPACK_IMPORTED_MODULE_4__providers_db_db__["a" /* DbProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_vibration__["a" /* Vibration */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_9__providers_apis_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_utils_loader__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_11__providers_utils_toast__["a" /* ToasterProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Config */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[396]);
//# sourceMappingURL=main.js.map