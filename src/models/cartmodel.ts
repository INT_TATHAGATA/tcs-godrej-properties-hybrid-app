export class CartModel {
    
    public product_id : String;
    private product_name : String;
    private product_group_name : String;
    private product_price : Number;
    private product_discounted_price : Number;
    private product_discounted_percentage : String;
    private product_short_description : String;
    private price_saving : Number;
    private product_image : Number;
    private product_qnty : Number;
    public product_type : String;

    constructor(obj : any){

        this.product_id=obj.product_id;
        this.product_name=obj.product_name;
        this.product_group_name=obj.group_name;
        this.product_price=Number(obj.product_sell_price);
        this.product_discounted_price=Number(obj.offer_price);
        this.product_discounted_percentage=obj.offer_percentage;
        this.product_short_description=obj.highlights;
        this.price_saving=Number(obj.product_price)-Number(obj.product_sell_price);
        this.product_image=obj.product_image;
        this.product_qnty=1;
        this.product_type=obj.product_type;
            
    }

    public setitem(obj : any){

        this.product_id=obj.product_id;
        this.product_name=obj.product_name;
        this.product_group_name=obj.product_group_name;
        this.product_price=Number(obj.product_price);
        this.product_discounted_price=Number(obj.product_discounted_price);
        this.product_discounted_percentage=obj.product_discounted_percentage;
        this.product_short_description=obj.product_short_description;
        this.price_saving=Number(obj.price_saving);
        this.product_image=obj.product_image;
        this.product_qnty=Number(obj.product_qnty);
        this.product_type=obj.product_type;
            
    }

    public setquantity(qnty){
        this.product_qnty=qnty; 
    }

    public get_product_qnty(){

        return this.product_qnty;
    }
    public get_product_price(){

        return this.product_price;
    }
    public get_product_saving(){

        return this.price_saving;
    }

    public addquantity(){

       if(this.product_qnty<2){ 
        this.product_qnty=Number(this.product_qnty) + 1;
       }
    }

    public delquantity(){

        if(this.product_qnty > 1){    
        this.product_qnty=Number(this.product_qnty) - 1;

        }

    }

    

    
}