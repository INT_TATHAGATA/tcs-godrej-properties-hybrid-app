export interface HttpErrorStatus {
	NOT_FOUND: number;
	UN_AUTHORIZED: number;
	BAD_REQUEST: number;
	SERVER_INTERNAL_ERROR: number;
}

export interface HttpSuccessStatus {
	OK: number,
	CREATED: number,
	ACCEPTED: number,
	NO_RECORD: number
}
