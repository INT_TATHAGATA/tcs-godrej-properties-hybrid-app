export interface LogInInfo {
    userid: string;
    first_name: number;
    last_name: string;
    email: string;
    mob_no: string;
    pan_no: string;
    birth_dt: string;
    is_customer: string;
}
export interface LoginApiresponse {
    status: string;
    msg: string;
    sessionId: string;
    sessionName: string;
    token: string;
    data: LogInInfo;
}
export interface OtpApiresponse {
    status: string;
    msg: string;
    otp: string;
    user_id: string;    
    req_type: string,
    req_cont: string,
    req_name: string,
}
