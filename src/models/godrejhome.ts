export interface RecomProjectINFO {
    proj_id: string;
    proj_name: string;
    cities: string;
    proj_img: string;
    proj_logo_img: string;
    status: string;
    typologies: string;
    url: string;
}
export interface GodrejHomeRecomProjectApiresponse {
    status: string;
    msg: string;
    data: Array<RecomProjectINFO>;
}
