import { Component, Input } from '@angular/core';

/**
 * Generated class for the FootBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'progress-bar',
    templateUrl: 'progress-bar.html'

})
export class ProgressBar {

    @Input('progress') progress;

    constructor(

    ) {
        console.log('Hello FootBarComponent Component');
    }


}
