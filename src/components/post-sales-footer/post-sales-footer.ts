import { Component } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { SearchApiProvider } from '../../providers/apis/search';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';
/**
 * Generated class for the PostSalesFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'post-sales-footer',
  templateUrl: 'post-sales-footer.html'
})
export class PostSalesFooterComponent {

  text: string;
  public searchListArr: any = [];
  public addModal: any;
  constructor(
    public events: Events,
    public modalCtrl: ModalController,
    public storageProvider: DbProvider,
    public searchApi: SearchApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider
  ) {
    console.log('Hello PostSalesFooterComponent Component');
    this.text = 'Hello World';
  }

  openSearch() {
    this.loader.show('Loading');
    const searchParam = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      property_type_term_menu: "1",
      city_term_menu: "",
      sub_location: "",
      project_status: "",
      typology_term_menu: ""
    };
    this.searchApi.propertySearchInfo(searchParam).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.addModal = this.modalCtrl.create("SearchPage", { responseData: response,from:'footer' });
        this.addModal.onDidDismiss((item: any) => {
          if (item) {
            //this.searchListArr=item;
          }
        });
        this.addModal.present();
      } else {
        this.toast.show(response.msg);
      }

    }, (error: any) => {

      this.loader.hide();
      this.toast.show(error.statusText);
    });

  }
}
