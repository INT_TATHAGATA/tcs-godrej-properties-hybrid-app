import { NgModule } from '@angular/core';
//import { Component } from './component/component';
import { TestComponent } from './test/test';
import { HeadBarComponent } from './head-bar/head-bar';
import { FootBarComponent } from './foot-bar/foot-bar';
import { CommonModule } from '@angular/common'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MenuComponent } from './menu/menu';
import { ProgressBar } from './progress-bar/progress-bar';
import { PostSalesFooterComponent } from './post-sales-footer/post-sales-footer';


@NgModule({
    declarations: [
        TestComponent,
        HeadBarComponent,
        FootBarComponent,
        ProgressBar,
        MenuComponent,
    PostSalesFooterComponent],
    imports: [
        CommonModule,
        IonicModule
    ],
    exports: [
        TestComponent,
        HeadBarComponent,
        FootBarComponent,
        ProgressBar,
        MenuComponent,
    PostSalesFooterComponent]
})
export class ComponentsModule { }
