import { Component, Input } from '@angular/core';
import { Events, NavParams, NavController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/apis/auth';
import { AppConst } from '../../app/app.constants';
import { NotificationApiProvider } from '../../providers/apis/notification';
import { LoadingProvider } from '../../providers/utils/loader';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
/**
 * Generated class for the HeadBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'head-bar',
  templateUrl: 'head-bar.html'
})
export class HeadBarComponent {
  notificationcont: any;
  wishlistcont: any;

  notificationcontView: string = "";
  wishlistcontView: string = "";

  shownoti: boolean;
  Notificationlist: Array<any>;
  @Input("text")
  text: boolean;

  @Input("backTrue")
  backTrue: boolean;

  @Input("menuIcon")
  menuIcon: boolean;

  @Input("image")
  image: boolean;

  @Input("noti")
  noti: boolean;

  @Input("heart")
  heart: boolean;

  @Input("pin")
  pin: boolean;

  @Input("mail")
  mail: boolean;

  @Input("hTitle")
  hTitle: string;

  @Input("subHeading")
  subHeading: boolean;

  @Input("subHeadingText")
  subHeadingText: string;
  public isGuest: any;
  public notificationDot: boolean = false;

  public noNotificationMsg: string = "";

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageProvider: DbProvider,
    private Authapi: AuthProvider,
    public NotificationApi: NotificationApiProvider,
    private loader: LoadingProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
  ) {
    this.notificationcont = 0;
    this.wishlistcont = 0;
    this.shownoti = false;
    this.Notificationlist = [];
    this.isGuest = this.storageProvider.isGuest();

    this.events.subscribe('showNotiDot', () => {

      // this.notificationcont = 0;
      // this.wishlistcont = 0;
      this.getbadge();
      // this.notificationDot = true;
    });

    this.events.subscribe('checkHeaderIcon', () => {
      this.isGuest = this.storageProvider.isGuest();
      this.getbadge();
    });

    this.getbadge();
    //this.text = 'Hello World';
  }

  getbadge() {
    if (localStorage.getItem('userId') != null) {
      const param = {
        user_id: localStorage.getItem('userId'),
      };
      this.Authapi.BadgeCount(param).subscribe((response: any) => {
        console.log(response);
        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          this.notificationcont = response.data.unread_notifications_count;
          if (this.notificationcont > 99) {
            this.notificationcontView = "99+";
          } else {
            this.notificationcontView = "" + this.notificationcont;
          }

          this.wishlistcont = response.data.wishlist_count;
          if (this.wishlistcont > 99) {
            this.wishlistcontView =  "99+";
          } else {
            this.wishlistcontView = "" + this.wishlistcont;
          }
        } else {
          this.notificationcont = 0;
          this.notificationcontView = "";
          this.wishlistcont = 0;
          this.wishlistcontView = "";
        }
      }, (error: any) => {
        this.notificationcont = 0;
        this.notificationcontView = "";
        this.wishlistcont = 0;
        this.wishlistcontView = "";
        console.log(error);
      });
    }

  }

  toggleMenu() {

  }

  openwishlistpage() {
    this.navCtrl.push('WishlistPage');
  }

  opennotificationpage() {
    this.shownoti = false;
    this.navCtrl.push('NotificationlistPage');
  }

  shownotification() {
    this.shownoti = !this.shownoti;
    if (this.shownoti) {
      this.getnotification();
    }
  }

  getnotification() {
    const param = {
      user_id: localStorage.getItem('userId'),
      page: 0
    };
    this.loader.show('Please wait...');
    this.NotificationApi.getNotificationlisting(param).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        if (response.data.length > 5) {
          this.Notificationlist = response.data.slice(0, 5);
        } else {
          this.Notificationlist = response.data;
        }
        if (this.Notificationlist.length == 0) {
          this.noNotificationMsg = "No Notification Found.";
        } else {
          this.noNotificationMsg = "";
        }
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
      this.noNotificationMsg = "No Notification Found.";
    });
  }
  opennotification(notification) {
    console.log(notification);
    if (notification.field_is_message_read == 0) {
      this.goReadNotification(notification.id);
    }
    if (notification.field_mobile_app_data) {
      const PageApi = notification.field_mobile_app_data.target_link.split('/')[1];
      if (PageApi == 'get_user_data_api.json') {
        this.navCtrl.push('UserProfilePage');
      } else if (PageApi == 'list_document_api.json') {
        this.navCtrl.push('MyDocumentsListPage');
      } else if (PageApi == 'booking_confirmation_data.json') {
        this.boookingconfrim(notification.field_mobile_app_data.data.booking_id);
      } else if (PageApi == 'eoi_booking_confirmation_data.json') {
        this.eoiBoookingconfrim(notification.field_mobile_app_data.data.booking_id);
      }
    }

  }

  boookingconfrim(booking_id) {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: booking_id
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.postBookingConfrimDetails(data).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == '200') {
        this.navCtrl.push("PaymentPage", { details: response.data, bookingid: booking_id });
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  eoiBoookingconfrim(booking_id) {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: booking_id
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.postEoiBookingConfrimDetails(data).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == '200') {
        /* this.nav.push("PaymentPage", { details: response.data, bookingid: booking_id }); */
        this.navCtrl.push("PaymentPage", { details: response.data, bookingid: booking_id });
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  goReadNotification(notification_id) {
    const param = {
      user_id: localStorage.getItem('userId'),
      notification_id: notification_id
    };
    this.NotificationApi.readnotification(param).subscribe((response: any) => {
      const findIndex = this.Notificationlist.findIndex(x => x.id == notification_id);
      if (findIndex > -1) {
        this.Notificationlist[findIndex].field_is_message_read = 1;
      }
      this.events.publish('showNotiDot');
    }, (error: any) => {
      console.log(error);
    });
  }

}
