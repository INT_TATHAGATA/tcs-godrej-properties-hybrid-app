import { Component, Input, ViewChild } from '@angular/core';
import { ModalController, Events, NavController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { SearchApiProvider } from '../../providers/apis/search';
import { AppConst } from '../../app/app.constants';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
/**
 * Generated class for the FootBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'foot-bar',
  templateUrl: 'foot-bar.html',
  /* providers: [SearchApiProvider] */
})
export class FootBarComponent {

  public searchListArr: any = [];
  public addModal: any;
  public fontBold: boolean = false;

  constructor(
    public events: Events,
    public modalCtrl: ModalController,
    public storageProvider: DbProvider,
    public searchApi: SearchApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public navCtrl: NavController,
  ) {
    console.log('Hello FootBarComponent Component');
    this.events.subscribe('fontBold', () => {
      this.fontBold = true;
    })
  }

  openSearch() {
    this.loader.show('Loading');
    const searchParam = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      property_type_term_menu: "",
      city_term_menu: "",
      sub_location: "",
      project_status: "",
      typology_term_menu: "",
      min_price:'',
      max_price:'',
      lastSearch:'',
      lastDesect:''
    };
    this.searchApi.propertySearchInfo(searchParam).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.addModal = this.modalCtrl.create("SearchPage", { responseData: response.data,from:'footer' });
        this.addModal.onDidDismiss((item: any) => {
          if (item) {
            //this.searchListArr=item;
          }
        });
        this.addModal.present();
      } else {
        this.toast.show(response.msg);
      }

    }, (error: any) => {

      this.loader.hide();
      this.toast.show(error.statusText);
    });

  }

  goToPage(pageName: any) {
    if (pageName == 'Contact' || pageName == 'BookAVisitPage') {
      this.navCtrl.push('BookAVisitPage', {
        openView: pageName
      });
    } else if (pageName == 'BuyNowPage') {
      this.navCtrl.push('BuyNowPage', {
        openView: pageName
      });
    } else {
      this.events.publish('redirectFromFooter', pageName);
    }
  }
}
