import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';

/**
 * Generated class for the BookAVisitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-a-visit',
  templateUrl: 'book-a-visit.html',
})
export class BookAVisitPage implements OnInit {

  noti: boolean = false;
  heart: boolean = false;
  pin: boolean = false
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;

  public callUs: boolean = false;
  public callBck: boolean = false;
  public chat: boolean = false;
  public visitForm: FormGroup;
  public minDate: any;
  public maxDate: any;
  public disEndTime: boolean = true;
  public enTimeErr: boolean = false;
  public disableButton: boolean = true;
  public visit: boolean = false;
  public bookVisit: any;
  public siteVisit: boolean = true;

  public countryList: any = [];
  public code: any;
  public country: any;

  public requestCallForm: FormGroup;
  public addModal: any;

  public countryCodes: any = [];
  public countryInitials: any;
  public flag: any;
  public findIndia: any;

  public countryCodesReq: any = [];
  public countryInitialsReq: any;
  public flagReq: any;
  public findIndiaReq: any;
  public codeReq: any;
  public countryReq: any;

  public userParseData: any;
  public deviceinfo: any;
  public isValidPreferredTime = true;
  public openView: any;
  public headerText: string = "";

  public isGuest: any;
  public isReadOnly: boolean = true;
  public firstName: string = "";
  public lastName: string = "";
  public phoneNumber: string = "";
  public emailID: string = "";
  public indPhoneNumber: string = "";
  public otherPhoneNumber: string = "";

  public allProjectList: any;
  public projectID: any;
  public indiaNumber: any;
  public otherNumber: any;

  public selectedValue: any;

  public defaultCity = "";

  public isOpenModal: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public storageProvider: DbProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public modalCtrl: ModalController,
    private menu: MenuController,

  ) {
    this.countryList = this.storageProvider.getCountryCodes();
    // this.bookVisit = [{ 'name': "Yes", select: false }, { 'name': "No", select: false }];
    this.isGuest = this.storageProvider.isGuest();

    this.allProjectList = [];

    this.openView = navParams.get('openView');
    if (this.openView == "BookAVisitPage") {
      this.headerText = "Book a Visit";
    } else if (this.openView == "Contact") {
      this.headerText = "Contact";
    }
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad BookAVisitPage');
    // this.checkBox(0);
  }

  showHideAccordian(value: any) {
    if (value == "cu") {
      this.callUs = !this.callUs;
    } else if (value == "rcb") {
      this.callBck = !this.callBck;
    } else if (value == "cht") {
      this.chat = !this.chat;
    } else if (value == "bokv") {
      this.visit = !this.visit;
    }
  }

  ngOnInit() {

    this.defaultCity = "Enter City";

    this.getAllProjects();

    this.userParseData = JSON.parse(localStorage.getItem('userData'));
    this.deviceinfo = this.storageProvider.getDeviceInfo();

    this.userDetailsValidation();

    this.menu.swipeEnable(false);

    this.countryCodes = this.storageProvider.getCountryCodes();
    this.countryCodes.map((item: any) => {
      item["selected"] = false
    });
    this.findIndia = this.countryCodes.findIndex(x => x.callingCodes === "+91");
    this.countryCodes[this.findIndia].selected = true;
    this.countryInitials = this.countryCodes[this.findIndia];

    this.countryCodesReq = this.storageProvider.getCountryCodes();
    this.countryCodesReq.map((item: any) => {
      item["selected"] = false
    });
    this.findIndiaReq = this.countryCodes.findIndex(x => x.callingCodes === "+91");
    this.countryCodesReq[this.findIndia].selected = true;
    this.countryInitialsReq = this.countryCodes[this.findIndia];

    this.storageProvider.setMinMaxDate().then((minMaxDate: any) => {
      let splitDate = minMaxDate.split('~');
      this.minDate = splitDate[0];
      this.maxDate = splitDate[1];
    });

    if (this.allProjectList.length > 0) {
      this.selectedValue = this.allProjectList[0].proj_id;
      this.projectID = this.allProjectList[0].proj_id;
    }

    this.visitForm = this.fb.group({
      projectlist: new FormControl(this.selectedValue, [
        Validators.required
      ]),
      fname: new FormControl((this.isGuest == '1') ? '' : this.userParseData.data.first_name, [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern())
      ]),
      lname: new FormControl((this.isGuest == '1') ? '' : this.userParseData.data.last_name, [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern())
      ]),
      mobile: new FormControl((this.isGuest == '1') ? '' : this.userParseData.data.mob_no, [
        Validators.required
      ]),
      email: new FormControl((this.isGuest == '1') ? '' : this.userParseData.data.email, [
        Validators.required,
        Validators.pattern(this.storageProvider.emailPattern())
      ]),
      country: new FormControl(this.countryList[0].name, []),
      city: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.alphabetPattren())
      ]),
      preferedDate: new FormControl('', [
        Validators.required
      ]),
      startTime: new FormControl('', [
        Validators.required
      ]),
      countryCode: new FormControl(this.countryList[0].callingCodes, [])

    });

    if (this.isGuest && this.isGuest == '1') {
      this.visitForm.controls["mobile"].setValidators([
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.required,
        Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
      ]);
    }

    this.visitForm.valueChanges.subscribe((v: any) => {
      this.disableButton = true;
      if (this.visitForm.value.city == "Enter City") {
        this.disableButton = true;
      } else {
        if (this.visitForm.valid) {
          this.disableButton = false;
        }
      }

    });

    this.code = this.countryList[0].callingCodes;
    this.country = this.countryList[0].name;

    this.codeReq = this.countryList[0].callingCodes;
    this.countryReq = this.countryList[0].name;

    this.requestCallForm = this.fb.group({
      mobile: new FormControl('', [
        Validators.required
      ])
    });
    if (this.isGuest && this.isGuest == '1') {
      this.requestCallForm.controls["mobile"].setValidators([
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.required,
        Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
      ]);
    }

    this.flagChange(this.countryCodes[this.findIndia]);
    this.flagChangeReq(this.countryCodesReq[this.findIndiaReq]);
  }

  get f() {
    return this.visitForm.controls;
  }

  get g() {
    return this.requestCallForm.controls;
  }

  ionViewDidEnter() {
    if (this.openView == "BookAVisitPage") {
      this.showHideAccordian('bokv');
    } else if (this.openView == "Contact") {
      this.showHideAccordian('cu');
    } else {
      this.showHideAccordian('bokv');
    }
  }

  timeChange(value: any) {

    const preferedTime = this.visitForm.value.startTime;

    if (Number(preferedTime.split(':')[0]) >= 10 && Number(preferedTime.split(':')[0]) < 18) {
      console.log("timeChange : Valid Prefered Time");
      this.isValidPreferredTime = true;
      if (this.visitForm.value.city == "Enter City") {
        this.disableButton = true;
      } else {
        if (this.visitForm.valid) {
          this.disableButton = false;
        }
      }
    } else if (Number(preferedTime.split(':')[0]) == 18 && Number(preferedTime.split(':')[1]) == 0) {
      console.log("timeChange : Valid Prefered Time");
      this.isValidPreferredTime = true;
      if (this.visitForm.value.city == "Enter City") {
        this.disableButton = true;
      } else {
        if (this.visitForm.valid) {
          this.disableButton = false;
        }
      }
    } else {
      console.log("Blank : " + preferedTime);
      console.log("timeChange : Invalid Prefered Time");
      this.isValidPreferredTime = false;
      this.disableButton = true;
    }

    if (value == "st") {
      this.disEndTime = false;
    } else {
      this.enTimeErr = false;
      if (this.visitForm.value.endTime < this.visitForm.value.startTime) {
        this.enTimeErr = true;
      }
    }

  }

  flagChange(event: any) {
    this.flag = event.flag
  }

  flagChangeReq(event: any) {
    this.flagReq = event.flag
  }

  submit() {

    let mobNumber: any;
    let userID: any;
    let deviceID: any;
    if (this.isGuest && this.isGuest == '1') {
      mobNumber = this.countryInitialsReq.callingCodes + this.visitForm.value.mobile;
      deviceID = this.deviceinfo.uuid;
      userID = ""
    } else {
      mobNumber = this.phoneNumber;
      userID = this.userParseData.data.userid;
      deviceID = ""
    }

    const jsonData = {
      user_id: userID,
      proj_id: this.projectID,
      field_name: this.visitForm.value.fname + " " + this.visitForm.value.lname,
      field_email: this.visitForm.value.email,
      field_mobile_no: mobNumber,
      field_country: this.country,
      field_contact_us_city: this.visitForm.value.city,
      field_book_your_site_visit: "",
      field_contact_date: this.visitForm.value.preferedDate,
      field_contact_time: this.visitForm.value.startTime,
      device_id: deviceID
    };
    this.loader.show('Please wait...');
    this.projectdetailsApi.postVisitBook(jsonData).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
       /*  this.ngOnInit(); */
       this.navCtrl.pop();
      }
      this.toast.show(response.msg);
      this.loader.hide();
    });
  }

  request() {

    let mobNumber: any;
    let userID: any;
    let deviceID: any;
    if (this.isGuest && this.isGuest == '1') {
      mobNumber = this.countryInitialsReq.callingCodes + this.requestCallForm.value.mobile;
      deviceID = this.deviceinfo.uuid;
      userID = ""
    } else {
      mobNumber = this.phoneNumber;
      userID = this.userParseData.data.userid;
      deviceID = ""
    }

    let jsonData = {
      user_id: userID,
      mob_no: mobNumber,
      device_id: deviceID
    };
    this.loader.show('Please wait...');
    this.projectdetailsApi.postRequestCall(jsonData).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        if (this.isGuest && this.isGuest == '1') {
          this.ngOnInit();
        }
      }
      this.toast.show(response.msg);
    });
  }

  openCountry(openMode) {
    if (!this.isOpenModal) {
      this.isOpenModal = !this.isOpenModal;
      /*  this.loader.show('Please wait..'); */
      if (openMode == 'fullName') {
        this.addModal = this.modalCtrl.create("CountryListPage", { showCodes: false });
      } else {
        this.addModal = this.modalCtrl.create("CountryListPage", { showCodes: true });
      }
      this.addModal.onDidDismiss((item: any) => {
        if (item) {
          this.countryCodes = item;
          const findSelected = item.findIndex(x => x.selected == true);
          if (openMode == 'fullName') {
            this.country = item[findSelected].name;
          } else if (openMode == 'codeName') {
            this.countryInitials = item[findSelected];
            this.flag = this.countryInitials.flag;
            if (this.countryInitials.callingCodes == '+91') {
              this.visitForm.controls["mobile"].setValidators([
                Validators.minLength(10),
                Validators.maxLength(10),
                Validators.required,
                Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
              ]);
            } else {
              this.visitForm.controls["mobile"].setValidators([
                Validators.required,
                Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
              ]);
            }
            this.visitForm.controls['mobile'].updateValueAndValidity();
          } else if (openMode == 'codeNameRequest') {
            this.countryInitialsReq = item[findSelected];
            this.flagReq = this.countryInitialsReq.flag;
            if (this.countryInitialsReq.callingCodes == '+91') {
              this.requestCallForm.controls["mobile"].setValidators([
                Validators.minLength(10),
                Validators.maxLength(10),
                Validators.required,
                Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
              ]);
            } else {
              this.requestCallForm.controls["mobile"].setValidators([
                Validators.required,
                Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
              ]);
            }
            this.requestCallForm.controls['mobile'].updateValueAndValidity();
          }
        }
        this.isOpenModal = !this.isOpenModal;
      });
      this.addModal.present();
    }
  }

  userDetailsValidation() {
    console.log("User isGuest : " + this.isGuest);

    if (this.isGuest && this.isGuest == '1') {
      this.isReadOnly = false;
    } else {

      this.isReadOnly = true;

      const userDetails = JSON.parse(localStorage.getItem('userData'));
      this.firstName = userDetails.data.first_name;
      this.lastName = userDetails.data.last_name;
      this.phoneNumber = userDetails.data.mob_no;
      this.emailID = userDetails.data.email;
    }
  }

  getAllProjects() {
    this.loader.show('Please wait...');
    this.projectdetailsApi.getAllProjectList().subscribe((response: any) => {
      if (response.status == '200') {
        this.allProjectList = response.data;
        this.indiaNumber = response.india_number;
        this.otherNumber = response.other_number;
        this.selectedValue = this.allProjectList[0].proj_id;
        this.projectID = this.allProjectList[0].proj_id;
      }
      this.loader.hide();
    });
  }

  onSelectChange(projectId: any) {
    this.projectID = projectId;
  }

  clearCity() {
    if (this.defaultCity = 'Enter City') {
      this.defaultCity = "";
    }
  }

}