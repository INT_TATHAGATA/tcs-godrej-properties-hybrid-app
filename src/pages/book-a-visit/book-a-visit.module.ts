import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookAVisitPage } from './book-a-visit';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BookAVisitPage,
  ],
  imports: [
    IonicPageModule.forChild(BookAVisitPage),
    ComponentsModule
  ],
})
export class BookAVisitPageModule {}
