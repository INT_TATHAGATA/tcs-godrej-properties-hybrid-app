import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplicationFormPage } from './application-form';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
@NgModule({
  declarations: [
    ApplicationFormPage,
  ],
  imports: [
    IonicPageModule.forChild(ApplicationFormPage),
    ComponentsModule
  ],
  providers: [ProjectDetailsApiProvider]
})
export class ApplicationFormPageModule { }
