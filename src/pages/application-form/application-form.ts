import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events, Nav } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AppConst } from '../../app/app.constants';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

/**
 * Generated class for the ApplicationFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-application-form',
  templateUrl: 'application-form.html',
})
export class ApplicationFormPage {

  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = true;
  subHeadingText: string = "";

  isOpen: boolean = true;
  enquiryFormData: any;
  projectDetails: any;
  parameter: any;
  public addModal: any;
  public bookingId: any;
  public count = 0;
  public detailsResponseData: any;
  public applicantsArr: any;

  public termsData: any;
  public footerAccordian: any = {
    terms: false
  };
  public temsCheck: boolean = true;
  public termsError: any;
  public isDisabledProceedBtn = true;

  public uploadCount = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageProvider: DbProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public events: Events,
    public modalCtrl: ModalController,
    private iab: InAppBrowser,
    private transfer: FileTransfer,
    private menu: MenuController,
    private nav: Nav
  ) {
    this.menu.swipeEnable(false);
    this.bookingId = '';
    this.projectDetails = this.navParams.get('projectDetails');
    this.parameter = this.navParams.get('parameter');
    console.log("Inventory Data: " + JSON.stringify(this.projectDetails));
    this.enquiryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));
    this.subHeadingText = this.projectDetails.pName;

    /* this.getTermsData(); */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApplicationFormPage');

  }

  getCollapse(value: any) {
    this.isOpen = !this.isOpen;
  }

  goToPay() {
    if (localStorage.getItem('isGuest') == '1') {
      this.toast.show('Please Login/Register To View This Page.');
      this.addModal = this.modalCtrl.create("HomePage", {});
      this.addModal.onDidDismiss((item: any) => {
        if (item == 'loggedIn') {
          this.events.publish('guestToUser');
          this.postBookingDetails();
        }
      });
      localStorage.setItem('fromPage', 'BookingSummary');
      this.addModal.present();
    } else {
      if (this.temsCheck) {
        this.postBookingDetails();
      } else {
        this.termsError = true;
      }
    }

  }

  postBookingDetails() {

    this.loader.show();

    const enqueryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));

    if (enqueryFormData.applicantlist.length > 0) {
      for (let i = 0; i < enqueryFormData.applicantlist.length; i++) {
        enqueryFormData.applicantlist[i].applicantMobile = enqueryFormData.applicantlist[i].countryInitials + enqueryFormData.applicantlist[i].applicantMobile;
        if (enqueryFormData.applicantlist[i].countryInitials = '+91') {
          enqueryFormData.applicantlist[i].appPassport = '';
        } else {
          enqueryFormData.applicantlist[i].appPan = '';
        }
      }
    }

    const jsonData = {
      nationality: enqueryFormData.nationality,
      name: enqueryFormData.name,
      mobile: enqueryFormData.mobile,
      email: enqueryFormData.email,
      dob: enqueryFormData.dob,
      permanentAddress: enqueryFormData.permanentAddress,
      street: enqueryFormData.street,
      locality: enqueryFormData.locality,
      country: enqueryFormData.country,
      city: enqueryFormData.city,
      pincode: enqueryFormData.pincode,
      communicationHouseNo: enqueryFormData.communicationHouseNo,
      communicationStreet: enqueryFormData.communicationStreet,
      communicationLocality: enqueryFormData.communicationLocality,
      communicationCountry: enqueryFormData.communicationCountry,
      communicationCity: enqueryFormData.communicationCity,
      communicationPincode: enqueryFormData.communicationPincode,
      pan: enqueryFormData.pan,
      passport: enqueryFormData.passport,
      purposeOfPurchase: enqueryFormData.purposeOfPurchase,
      applicantlist: enqueryFormData.applicantlist,
      device_id: this.storageProvider.getDeviceInfo().uuid,
      inventory_id: this.projectDetails.inv.nid,
      project_id: localStorage.getItem('projectId'),
      user_id: localStorage.getItem('userId')
    }

    this.projectdetailsApi.postBookingDetails(jsonData).subscribe((response: any) => {
      /* this.loader.hide(); */
      if (response.status == '200') { // 200
        this.bookingId = response.booking_id;
        this.applicantsArr = response;
        const userData = JSON.parse(localStorage.getItem('userData'));
        if (!userData.data.hasOwnProperty('birth_dt')) {
          userData.data['birth_dt'] = enqueryFormData.dob;
        } else {
          userData.data.birth_dt = enqueryFormData.dob;
        }
        localStorage.setItem('userData', JSON.stringify(userData));
        this.loader.hide();
        this.OpenPaymentPage(response.booking_id, response.booking_no, response.amount);
      }
      else if (response.status == '505') { //505 - Cost Sheet
        this.loader.hide();
        this.toast.show(response.msg);
        const startIndex = this.navCtrl.getActive().index - 1;
        this.navCtrl.remove(startIndex, 0).then((data: any) => {
          console.log(data);
        });
        this.navCtrl.popTo("InventoryDetailsPage");
      } else if (response.status == '506') { //506 - Inventory List
        this.loader.hide();
        this.toast.show(response.msg);
        const data = {
          project_id: localStorage.getItem('projectId'),
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
          page: 0
        };
        const extradata = {
          pName: this.projectDetails.pName
        };
        setTimeout(() => {
          this.nav.push('InventoryBookingPage', { formData: data, img: this.projectDetails.pImage, extdata: extradata }).then(() => {
            const index = this.nav.getActive().index;
            this.nav.remove(index - 4, 4);
          });
        }, 6000);
      }
      else {
        this.loader.hide();
        this.toast.show(response.msg);
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  editEnquiry() {
    //this.navCtrl.push("EnquiryFormPage", { details: this.projectDetails,formData:this.navParams.get('formData'), imgObj:this.navParams.get('imgObj') });
    const startIndex = this.navCtrl.getActive().index - 1;
    this.navCtrl.remove(startIndex, 3).then((data: any) => {
      console.log(data);
    });
    this.navCtrl.popTo("EnquiryFormPage");
  }

  OpenPaymentPage(booking_id, booking_no, amount) {
    const sessonid = localStorage.getItem('Cookie').split('=')[1];
    const param = '?auth=' + sessonid + '&user_id=' + localStorage.getItem('userId') + '&booking_id=' + booking_id + '&booking_no=' + booking_no + '&amount=' + amount;
    const self = this;
    const url = AppConst.baseUrl + 'api_payment_redirect' + param;
    console.log('url', url);
    const option: InAppBrowserOptions = {
      location: 'no',
      hidden: 'no',
      toolbar: 'no',
    };
    const browser = this.iab.create(url, '_blank', option);
    browser.on('loadstop').subscribe(event => {
      console.log(event.url);
      console.log(AppConst.baseUrl + 'ccavSuccessHandler')
      if (event.url == AppConst.baseUrl + 'ccavSuccessHandler') {
        browser.close();
        this.loader.show();
        /* self.toast.show('Your booking has been successful');
         this.boookingconfrim(); */

        this.getBookingStatus(true, booking_id);
      } else if (event.url == AppConst.baseUrl + 'ccavCancelHandler') {
        browser.close();
        /* self.toast.show('Some thing went wrong in payment'); */

        this.loader.show();
        this.getBookingStatus(false, booking_id);
      }
    });
  }

  getBookingStatus(isSuccess: boolean, bookingId: any) {
    const data = {
      booking_id: this.bookingId
    }
    this.projectdetailsApi.getBookingStatus(data).subscribe((response: any) => {
      console.log("BOOKING_STATUS : " + response);
      if (response.status == '200' && response.booking_status == 'Completed') {
        if (isSuccess) {
          this.toast.show('Your Booking Has Been Successful.');
          this.boookingconfrim();
        } else {
          this.loader.hide();
          this.toast.show('Booking Failed. Please Try Again Later.');
        }
      } else {
        this.loader.hide();
        this.toast.show('Booking Failed. Please Try Again Later.');
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  boookingconfrim() {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: this.bookingId
    }
    /* this.loader.show('Please wait...'); */
    this.projectdetailsApi.postBookingConfrimDetails(data).subscribe((response: any) => {
      /* this.loader.hide(); */
      console.log(response);
      if (response.status == '200') {
        this.uploadImage();
        this.detailsResponseData = response.data;
        /* this.navCtrl.push("PaymentPage", { details: response.data, bookingid: this.bookingId }); */
      } else {
        this.loader.hide();
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  uploadImage() {
    const enqueryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));
    const imageDataApplicantArr = JSON.parse(JSON.stringify(this.navParams.get('imageDataApplicantArr')));
    let imgObjArr = [];
    let jsonData;
    let applicantId = "";

    if (this.uploadCount < imageDataApplicantArr.length) {
      if (imageDataApplicantArr[this.uploadCount].applicantType == 0) {
        applicantId = this.applicantsArr.applications[0];
      } else if (imageDataApplicantArr[this.uploadCount].applicantType == 1) {
        applicantId = this.applicantsArr.applications[1];
      } else {
        applicantId = this.applicantsArr.applications[2];
      }
      jsonData = {
        applicant_id: applicantId,
        type: imageDataApplicantArr[this.uploadCount].type,
        file_uploaded: imageDataApplicantArr[this.uploadCount].file_uploaded,
        fid: imageDataApplicantArr[this.uploadCount].fid,
        user_uploaded: imageDataApplicantArr[this.uploadCount].user_uploaded
      };
      imgObjArr.push(jsonData);

      let optionData = {
        user_id: localStorage.getItem('userId'),
        applicant_arr: imgObjArr
      }
      
      const url = AppConst.baseUrl + 'upload_applicant_doc.json';
      this.projectdetailsApi.applicantExistingImgUpload(url, optionData).subscribe((responseData: any) => {
        console.log(JSON.stringify(responseData));
        this.uploadCount++;
        this.uploadImage();
      }, (error: any) => {
        console.log(error);
        this.uploadCount++;
        this.uploadImage();
      });
    } else {
      localStorage.setItem("indian_APP_2ND_ID_F_ID", '');
      localStorage.setItem("indian_APP_2ND_ADD_F_ID", '');
      localStorage.setItem("indian_APP_3RD_ID_F_ID", '');
      localStorage.setItem("indian_APP_3RD_ADD_F_ID", '');
      this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'ApplicationFormPage' });
    }
  }

 /*  uploadImage() {
    const enqueryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));
    const imageDataApplicantArr = JSON.parse(JSON.stringify(this.navParams.get('imageDataApplicantArr')));
    let imgObjArr = [];
    let jsonData;
    imageDataApplicantArr.map((item: any) => {
      let applicantId = "";
      if (item.applicantType == 0) {
        applicantId = this.applicantsArr.applications[0];
      } else if (item.applicantType == 1) {
        applicantId = this.applicantsArr.applications[1];
      } else {
        applicantId = this.applicantsArr.applications[2];
      }
      jsonData = {
        applicant_id: applicantId,
        type: item.type,
        file_uploaded: item.file_uploaded,
        fid: item.fid,
        user_uploaded: item.user_uploaded
      };
      imgObjArr.push(jsonData);
    });

    let optionData = {
      user_id: localStorage.getItem('userId'),
      applicant_arr: imgObjArr
    }

    console.log("Application Form : imgObjArr : " + JSON.stringify(optionData));
    const url = AppConst.baseUrl + 'upload_applicant_doc.json';
    this.projectdetailsApi.applicantExistingImgUpload(url, optionData).subscribe((responseData: any) => {
      console.log(JSON.stringify(responseData));
      if (responseData.status == '200') {
        console.log("Applicant Doc Upload : " + responseData.status);
      }
      localStorage.setItem("indian_APP_2ND_ID_F_ID", '');
      localStorage.setItem("indian_APP_2ND_ADD_F_ID", '');
      localStorage.setItem("indian_APP_3RD_ID_F_ID", '');
      localStorage.setItem("indian_APP_3RD_ADD_F_ID", '');
      this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'ApplicationFormPage' });
    }, (error: any) => {
      console.log(error);
      localStorage.setItem("indian_APP_2ND_ID_F_ID", '');
      localStorage.setItem("indian_APP_2ND_ADD_F_ID", '');
      localStorage.setItem("indian_APP_3RD_ID_F_ID", '');
      localStorage.setItem("indian_APP_3RD_ADD_F_ID", '');
      this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'ApplicationFormPage' });
    });
  } */


  toggleFooter(from) {
    if (from == 'termsfooter') {
      this.footerAccordian = {
        terms: true,
      }
    } else {
      this.footerAccordian = {
        terms: (this.footerAccordian.terms) ? false : true
      }
    }
  }

  checkboxTerms() {
    this.temsCheck = !this.temsCheck;
    this.isDisabledProceedBtn = !this.isDisabledProceedBtn;
    (this.temsCheck) ? this.termsError = false : this.termsError = true;
  }

  getTermsData() {
    this.loader.show();
    this.projectdetailsApi.getBookingSummeryTermsData().subscribe((result: any) => {
      this.termsData = result[0].page_content;
      setTimeout(() => {
        this.loader.hide();
      }, 850);
    }, (error) => {
      this.loader.hide();
      console.log('Error:::', error);
    });
  }

}
