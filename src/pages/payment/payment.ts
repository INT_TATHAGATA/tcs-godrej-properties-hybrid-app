import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import moment from 'moment';
import { File } from '@ionic-native/file';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { DbProvider } from '../../providers/db/db';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ViewChild } from '@angular/core';
import { Navbar } from 'ionic-angular';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  isOpen: boolean = true;
  isOwner: boolean = true;
  temsCheck: boolean = true;

  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = false;
  parameter: any;
  subHeadingText: string = "";
  enquiryFormData: any;
  projectDetails: any;
  Bokkingid: any;

  public fromPage: string;
  public fullName: string = '';

  @ViewChild(Navbar) navBar: Navbar;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private file: File,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    public events: Events,
    public storageProvider: DbProvider,
    private menu: MenuController,
  ) {
    this.menu.swipeEnable(false);
    this.loader.hide();

    this.fullName = "";
    this.getUserDetails();
    this.parameter = this.navParams.get('details');
    // this.enquiryFormData = JSON.parse(localStorage.getItem('EnquiryFormData'));
    // this.projectDetails = this.navParams.get('projectDetails');
    this.Bokkingid = this.navParams.get('bookingid');

    try {
      this.fromPage = this.navParams.get('fromPage');
    }
    catch {
      this.fromPage = '';
    }

    localStorage.setItem('isCustomer', '1');
    this.events.publish('refreshSidepanelAfterPayment');
  }

  ionViewDidLoad() {
    if (this.fromPage == 'ApplicationFormPage' || this.fromPage == 'EOIEnquiryFormPage') {
      this.navBar.backButtonClick = (e: UIEvent) => {
        const pageDetails = 'GodrejHomePage';
        this.events.publish('openSearchListPage', pageDetails);
      }
    }
  }

  getCollapse(value: any) {
    if (value == "pd") {
      this.isOpen = !this.isOpen;
    } else if (value == "own") {
      this.isOwner = !this.isOwner;
    }

  }

  checkboxTerms() {
    this.temsCheck = !this.temsCheck;
  }
  // onPrint() {
  //   const self = this;
  //   debugger
  //   html2canvas(document.getElementById('print-section')).then(function (canvas) {
  //     const img = canvas.toDataURL('image/png');
  //     const doc = new jsPDF();
  //     doc.addImage(img, 'JPEG', 10, 10, 190, 262);
  //     const d = moment().format('dddd, MMMM Do YYYY, h:mm:ss a').replace(new RegExp(':', 'g'), '.');
  //     doc.save('Dashboard_' + d + '.pdf');
  //     // self.toast.success('Dashboard Screenshot Pdf Download Successfully');

  //   });

  // }

  onPrint() {

    const div = document.getElementById('print-section');

    const options = { background: "white", height: div.clientHeight, width: div.clientWidth };
    html2canvas(div, options).then((canvas) => {
      //Initialize JSPDF
      var doc = new jsPDF("p", "mm", "a4");
      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      //Add image Canvas to PDF
      doc.addImage(imgData, 'JPEG', 10, 10, 190, 262);

      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }


      //This is where the PDF file will stored , you can change it as you like
      // for more information please visit https://ionicframework.com/docs/native/file/
      const directory = this.file.externalApplicationStorageDirectory;

      //Name of pdf
      const fileName = "example1.pdf";

      //Writing File to Device
      this.file.writeFile(directory, fileName, buffer)
        .then((success) => console.log("File created Succesfully" + JSON.stringify(success)))
        .catch((error) => console.log("Cannot Create File " + JSON.stringify(error)));


    });
  }

  sendemailinvoice() {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: this.Bokkingid
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.sendInvoice(data, this.fromPage).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      /* this.toast.show(response.msg); */
      this.toast.show("An E-Mail Has Been Successfully Sent.");
    }, (error: any) => {
      this.loader.hide();
    });
  }

  ionViewWillLeave() {
    /* if (this.fromPage == 'ApplicationFormPage' || this.fromPage == 'EOIEnquiryFormPage') {
      const pageDetails = 'GodrejHomePage';
      this.events.publish('openSearchListPage', pageDetails);
    } */
  }

  getUserDetails() {
    const userDetails = JSON.parse(localStorage.getItem('userData'));
    let fInitials = userDetails.data.first_name.toUpperCase();
    let lInitials = userDetails.data.last_name.toUpperCase();
    this.fullName = fInitials + " " + lInitials;
  }
}
