import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AuthProvider } from '../../providers/apis/auth';
/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  public otpData: any;
  public resetPasswordForm: FormGroup;
  //public paswordReg="^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{6,20})";
  public openClose: boolean = false;
  public openCloseConf: boolean = false;
  public typeText: string = "password";
  public confType: string = "password";
  public notMatched: boolean = false;
  public disableButton: boolean = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public storageProvider: DbProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    private Authapi: AuthProvider,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
    this.otpData = this.navParams.get("formData");
  }

  ngOnInit() {
    this.resetPasswordForm = this.fb.group({
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.passwordPattern())
      ]),
      cpassword: new FormControl('', [
        Validators.required
      ])
    });
    this.notMatched = false;
    this.resetPasswordForm.valueChanges.subscribe((v) => {
      /* this.disableButton=false; 
      this.notMatched=false;
      if((v.password != v.cpassword) && v.cpassword !='' && v.password != ''){
        this.notMatched=true;
        this.disableButton=true;
      } else {
        this.disableButton=false;
      }*/


      if (this.resetPasswordForm.valid) {
        this.disableButton = false;
      } else {
        this.disableButton = true;
      }
      this.notMatched = false;
      if (v.password != v.cpassword && v.cpassword != "") {
        this.notMatched = true;
        this.disableButton = true;
      }
    });
  }

  get f() {
    return this.resetPasswordForm.controls;
  }

  resetPassword() {
    let data = {
      uid: this.otpData.uid,
      otp: this.otpData.otp,
      pass: this.resetPasswordForm.value.password
    };
    this.loader.show('Please wait...');
    this.Authapi.postResetPasswordApi(data).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == "200") {
        this.navCtrl.popToRoot();
      }
      this.toast.show(response.msg);
    }, (error: any) => {
      console.log(error);
    });
  }

  eyeOpenClose(value: any, type: any) {
    if (value == "O") {
      if (type == "pass") {
        this.openClose = false;
        this.typeText = "password";
      } else {
        this.openCloseConf = false;
        this.confType = "password";
      }
    } else {
      if (type == "pass") {
        this.openClose = true;
        this.typeText = "text";
      } else {
        this.openCloseConf = true;
        this.confType = "text";
      }

    }
  }

}
