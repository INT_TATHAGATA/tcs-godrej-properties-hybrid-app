import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events, ModalController, AlertController } from 'ionic-angular';
import { SearchApiProvider } from '../../providers/apis/search';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';
import { DbProvider } from '../../providers/db/db';

/**
 * Generated class for the SearchListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-list',
  templateUrl: 'search-list.html',
})
export class SearchListPage {
  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  mail: boolean = false;
  menuIcon: boolean = true;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = false;
  subHeadingText: string = "Project Name";


  data: any;
  users: string[];
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  searchparam: any;
  searchlist: Array<any>;
  public isGuest: any;
  public title: string = "Properties";
  public addModal: any;
  public possessionList: any;
  public typologyList: any;
  public posseNotSelected: boolean = false;
  public typoNotSelected: boolean = false;
  public minPrice: any = "";
  public maxPrice: any = "";
  public showTypologyData: any = [];
  public showPossessionData: any = [];
  public budgetPriceList: any = [];

  public noDataMsg: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    public searchApi: SearchApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    private wishlistApi: WishListApiProvider,
    public storagePrivider: DbProvider,
    public events: Events,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
  ) {
    this.searchparam = JSON.parse(localStorage.getItem('tempsearchdata'));
    this.searchlist = [];
    console.log(this.searchparam);
    this.isGuest = this.storagePrivider.isGuest();
  }

  /* ionViewDidEnter */ionViewDidLoad() {
    this.menu.swipeEnable(true);
    this.showPossessionData = [];
    this.showTypologyData = [];
    const searchedData = JSON.parse(localStorage.getItem('searchItems'));
    const tempData = JSON.parse(localStorage.getItem('tempsearchdata'));
    this.minPrice = tempData.min_price;
    this.maxPrice = tempData.max_price;

    this.setPriceListData(searchedData);

    const selectPossession = tempData.possession.split(',');
    this.typoNotSelected = true;
    this.posseNotSelected = true;
    searchedData.project_status.map((item: any) => {
      item.selected = false;
      if (selectPossession.includes(item.id)) {
        this.posseNotSelected = false;
        item.selected = true;
        this.showPossessionData.push(item);
      }
    });
    this.possessionList = searchedData.project_status;

    const selectTypology = tempData.typology.split(',');
    searchedData.typology_term_menu.map((item: any) => {
      item.selected = false;
      if (selectTypology.includes(item.id)) {
        this.typoNotSelected = false;
        item.selected = true;
        this.showTypologyData.push(item);
      }
    });

    this.typologyList = searchedData.typology_term_menu;

    this.searchparam.page = 0;
    if (this.searchlist.length <= 0) {
      //console.log('Search list data blank : ' + this.searchlist.length);
      this.searchlist = [];
      this.getsearchresult(this.searchparam);
    } else {
      //console.log('Search list : ' + this.searchlist.length);
      const projectDetails_P_ID = localStorage.getItem("ProjectDetailsWishlist_P_ID");
      const projectDetailsWishlist_P_Wishlist = localStorage.getItem("ProjectDetailsWishlist_P_Wishlist");
      //console.log('Search list : ProjectDetailsWishlist_P_ID : ' + projectDetails_P_ID);
      this.searchlist.map((item: any) => {
        if (projectDetails_P_ID != null && projectDetails_P_ID != ""
          && projectDetails_P_ID == item.nid
          && projectDetailsWishlist_P_Wishlist != null) {
          item.wishlist_status = projectDetailsWishlist_P_Wishlist;
        }
        // console.log(item.title + " = > Wish list status: " + item.wishlist_status);
      });
    }
    console.log('ionViewDidLoad SearchListPage');
  }

  getsearchresult(params: any) {
    if (this.searchparam.page == 0) {
      this.loader.show();
    }
    this.minPrice = params.min_price;
    this.maxPrice = params.max_price;
    this.searchApi.projectSearch(params).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {

        // this.searchlist.push(response.list_data);
        // this.searchlist = response.list_data;
        /* response.list_data.map((item: any, index) => {
            if (item.field_typology_tags != null && item.field_typology_tags.split(',').length > 2) {
             item['field_typology_tags'] = item.field_typology_tags.split(',').slice(1, 3).toString();
           } 
           if (item.field_typology_tags != null) {
             item['field_typology_tags'] = item.field_typology_tags;
           }
           this.searchlist.push(item);
         });*/

        if (this.searchlist.length > 0) {
          this.searchlist = this.searchlist.concat(response.list_data);
        } else {
          this.searchlist = response.list_data;
        }

        if (response.total_count > 0) {
          if (response.total_count > 1) {
            this.title = response.total_count + " Properties";
          } else {
            this.title = response.total_count + " Property";
          }
        }
        this.totalPage = response.total_page;

        this.noDataMsg = "";

      } else {
        this.noDataMsg = "No Projects Found. Please Modify Your Search.";
      }
    }, (error: any) => {
      this.noDataMsg = "No Projects Found. Please Modify Your Search.";
      this.title = " Property";
      this.loader.hide();
    });


  }

  doInfinite(infiniteScroll) {
    if (this.totalPage > 0 && this.searchparam.page < this.totalPage) {
      this.searchparam.page = this.searchparam.page + 1;
      this.getsearchresult(this.searchparam);
      setTimeout(() => {
        infiniteScroll.complete();
      }, 1000);
    } else {
      infiniteScroll.complete();
    }
  }

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  addRmWishList(item) {
    console.log(item)
    const data = {
      user_id: localStorage.getItem('userId'),
      proj_id: item.nid,
      type: item.wishlist_status == 0 ? 'add' : 'delete'
    }
    this.loader.show('Please wait...');
    this.wishlistApi.modifyWishlist(data).subscribe((response: any) => {
      console.log(response);
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.events.publish('showNotiDot');
        this.searchlist.map((data) => {
          if (data.nid == item.nid) {
            data.wishlist_status = (data.wishlist_status == '0') ? 1 : 0
          }
        });
        this.toast.show(response.msg);
      } else {
        this.toast.show(response.msg);
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    });
  }

  goToPage(page: any, pageName: any, projectId: any, ltem: any) {
    if (page == 'gotopage') {
      projectId['proj_id'] = projectId.nid;
      const extradata = {
        pName: projectId.title
      };
      this.navCtrl.push(pageName, { fullData: projectId, img: projectId.project_image != '' ? projectId.project_image : '', extdata: extradata });
    } else if (page == 'share') {
      this.share(ltem);
    } else {
      if (localStorage.getItem('isGuest') == '1') {
        this.toast.show('Please Login/Register To View This Page.');
        this.addModal = this.modalCtrl.create("HomePage", {});
        this.addModal.onDidDismiss((item: any) => {
          if (item == 'loggedIn') {
            this.isGuest = this.storagePrivider.isGuest();
            this.events.publish('guestToUser');

            this.addRmWishList(ltem);
          }
        });
        localStorage.setItem('fromPage', 'ProjectDetailsPage');
        this.addModal.present();
      } else {
        this.addRmWishList(ltem);
      }
    }

  }

  share(item?) {
    this.loader.show('Please wait...');
    // const url = AppConst.baseUrl + item.url;
    const url = item.url;
    this.storagePrivider.socialShare(item.title, "", item.project_image, url);
  }

  openSearch() {
    this.addModal = this.modalCtrl.create("SearchPage", { responseData: JSON.parse(localStorage.getItem('searchItems')), from: 'searchList' });
    this.addModal.present();
  }

  openPossession() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Select Possession');
    this.possessionList.map((item: any) => {
      alert.addInput({
        type: 'checkbox',
        label: item.name,
        value: item.id,
        checked: item.selected
      });
    });
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Checkbox data:', data);
        this.showPossessionData = [];
        if (data.length > 0) {
          this.posseNotSelected = true;
          this.possessionList.map((item: any) => {
            item['selected'] = false;
            if (data.includes(item.id)) {
              this.posseNotSelected = false;
              item['selected'] = true;
              this.showPossessionData.push(item);
            }
          });
        } else {
          this.possessionList.map((item: any) => {
            item['selected'] = false;
            this.posseNotSelected = true;
          });
        }
        const selectedTempData = JSON.parse(localStorage.getItem('tempsearchdata'));
        const jsonData = {
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
          project_status: selectedTempData.project_status,
          location: selectedTempData.location,
          lat: selectedTempData.lat,
          lng: selectedTempData.lng,
          min_price: selectedTempData.min_price,
          max_price: selectedTempData.max_price,
          possession: data.toString(),
          typology: selectedTempData.typology,
          sub_location: selectedTempData.sub_location,
          page: 0
        };
        localStorage.setItem('tempsearchdata', JSON.stringify(jsonData));
        this.searchparam = JSON.parse(localStorage.getItem('tempsearchdata'));
        this.searchlist = [];
        this.getsearchresult(this.searchparam);
      }
    });
    alert.present();
  }

  openTypology() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Select Typology');
    this.typologyList.map((item: any) => {
      alert.addInput({
        type: 'checkbox',
        label: item.name,
        value: item.id,
        checked: item.selected
      });
    });
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Checkbox data:', data);
        this.showTypologyData = [];
        if (data.length > 0) {
          this.typoNotSelected = true;

          this.typologyList.map((item: any) => {
            item['selected'] = false;
            if (data.includes(item.id)) {
              this.typoNotSelected = false;
              item['selected'] = true;
              this.showTypologyData.push(item);
            }
          });
        } else {
          this.typologyList.map((item: any) => {
            item['selected'] = false;
            this.typoNotSelected = true;
          });
        }


        const selectedTempData = JSON.parse(localStorage.getItem('tempsearchdata'));
        const jsonData = {
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
          project_status: selectedTempData.project_status,
          location: selectedTempData.location,
          lat: selectedTempData.lat,
          lng: selectedTempData.lng,
          min_price: selectedTempData.min_price,
          max_price: selectedTempData.max_price,
          possession: selectedTempData.possession,
          typology: data.toString(),
          sub_location: selectedTempData.sub_location,
          page: 0
        };
        localStorage.setItem('tempsearchdata', JSON.stringify(jsonData));
        this.searchparam = JSON.parse(localStorage.getItem('tempsearchdata'));
        this.searchlist = [];
        this.getsearchresult(this.searchparam);
      }
    });
    alert.present();
  }

  openPricelist() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Price Range');
    this.budgetPriceList.map((item: any) => {
      alert.addInput({
        type: 'checkbox',
        label: item.label,
        value: item,
        checked: item.selected
      });
    });
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        let firstIndex = 0;
        let lastIndex = 0;

        firstIndex = this.budgetPriceList.indexOf(data[0]);
        lastIndex = this.budgetPriceList.indexOf(data[data.length - 1]);

        if (data.length > 0) {
          this.budgetPriceList.map((item: any) => {
            item['selected'] = false;
          });

          for (let i = firstIndex; i <= lastIndex; i++) {
            this.budgetPriceList[i].selected = true;
          }
        } else {
          this.budgetPriceList.map((item: any, index: any) => {
            item['selected'] = false;
            /* if(index == 0){
              item['selected'] = true;
            } */

          });
        }

        let lowestPrice = Number.POSITIVE_INFINITY;
        let highestPrice = Number.NEGATIVE_INFINITY;
        let tmpLowest;
        let tmpHighest;
        for (let i = this.budgetPriceList.length - 1; i >= 0; i--) {
          if (this.budgetPriceList[i].selected == true) {
            tmpLowest = this.budgetPriceList[i].minPrice;
            if (tmpLowest < lowestPrice) lowestPrice = tmpLowest;
            if (tmpLowest > highestPrice) highestPrice = tmpLowest;
          }
        }
        for (let i = this.budgetPriceList.length - 1; i >= 0; i--) {
          if (this.budgetPriceList[i].selected == true) {
            tmpHighest = this.budgetPriceList[i].maxPrice;
            if (tmpHighest < lowestPrice) lowestPrice = tmpHighest;
            if (tmpHighest > highestPrice) highestPrice = tmpHighest;
          }
        }

        if (lowestPrice === Number.POSITIVE_INFINITY) {
          lowestPrice = 0;
        }
        if (highestPrice === Number.NEGATIVE_INFINITY) {
          highestPrice = 0;
        }

        const selectedTempData = JSON.parse(localStorage.getItem('tempsearchdata'));
        const jsonData = {
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
          project_status: selectedTempData.project_status,
          location: selectedTempData.location,
          lat: selectedTempData.lat,
          lng: selectedTempData.lng,
          min_price: lowestPrice.toString(),
          max_price: highestPrice.toString(),
          possession: selectedTempData.possession,
          typology: selectedTempData.typology,
          sub_location: selectedTempData.sub_location,
          page: 0
        };
        localStorage.setItem('tempsearchdata', JSON.stringify(jsonData));
        this.searchparam = JSON.parse(localStorage.getItem('tempsearchdata'));
        this.searchlist = [];
        this.getsearchresult(this.searchparam);
      }
    });
    alert.present();
  }

  setPriceListData(searchedData) {
    const minPriceValue = searchedData.budget_data.min;
    const maxPriceValue = searchedData.budget_data.max;

    let divisibleVal = 1500000;
    if (parseInt(maxPriceValue) < 10000000) {
      divisibleVal = 1500000;
    } else {
      divisibleVal = 5000000;
    }
    let num = parseInt(maxPriceValue) / divisibleVal;

    if (parseInt(minPriceValue) < divisibleVal) {
      this.budgetPriceList.push(
        {
          label: "< " + this.storagePrivider.getSearchListIntCurrency(divisibleVal),
          checked: false,
          minPrice: parseInt(minPriceValue),
          maxPrice: divisibleVal
        }
      )
      for (let i = 1; i <= num; i++) {
        if (parseInt(maxPriceValue) >= (i + 1) * divisibleVal) {
          this.budgetPriceList.push(
            {
              label: this.storagePrivider.getSearchListIntCurrency(i * divisibleVal) + ' - ' + this.storagePrivider.getSearchListIntCurrency((i + 1) * divisibleVal),
              checked: false,
              minPrice: i * divisibleVal,
              maxPrice: (i + 1) * divisibleVal
            });
        } else {
          this.budgetPriceList.push(
            {
              label: "> " + this.storagePrivider.getSearchListIntCurrency(i * divisibleVal),
              checked: false,
              minPrice: i * divisibleVal,
              maxPrice: maxPriceValue
            }
          )
        }
      }
    } else {
      for (let i = 1; i <= num; i++) {
        if (parseInt(maxPriceValue) >= (i + 1) * divisibleVal) {
          this.budgetPriceList.push(
            {
              label: this.storagePrivider.getSearchListIntCurrency(i * divisibleVal) + ' - ' + this.storagePrivider.getSearchListIntCurrency((i + 1) * divisibleVal),
              checked: false,
              minPrice: i * divisibleVal,
              maxPrice: (i + 1) * divisibleVal
            });
        } else {
          this.budgetPriceList.push(
            {
              label: "> " + this.storagePrivider.getSearchListIntCurrency(i * divisibleVal),
              checked: false,
              minPrice: i * divisibleVal,
              maxPrice: maxPriceValue
            }
          )
        }
      }
    }

    /* console.log("Main min : " + parseInt(this.minPrice) + " and max : " + parseInt(this.maxPrice)); */
    this.budgetPriceList.map((item: any, index: any) => {
      /* console.log("Item min : " + item.minPrice + " and max : " + item.maxPrice); */
      if ((parseInt(this.minPrice) >= item.minPrice && parseInt(this.minPrice) < item.maxPrice
        || parseInt(this.minPrice) < item.minPrice && parseInt(this.maxPrice) > item.minPrice) && this.maxPrice > 0) {
        item.selected = true;
      }
    });
  }

}
