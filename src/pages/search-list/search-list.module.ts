import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchListPage } from './search-list';
import { ComponentsModule } from '../../components/components.module';
import { SearchApiProvider } from '../../providers/apis/search';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';

@NgModule({
  declarations: [
    SearchListPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchListPage),
    ComponentsModule
  ],
  providers: [SearchApiProvider, WishListApiProvider]
})
export class SearchListPageModule { }
