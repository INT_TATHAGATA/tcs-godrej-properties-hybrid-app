import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, AlertController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { SearchApiProvider } from '../../providers/apis/search';
import { LoadingProvider } from '../../providers/utils/loader';
import { AppConst } from '../../app/app.constants';
import { ToasterProvider } from '../../providers/utils/toast';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  public goalProgress: any;
  public lower: any;
  public upper: any;
  public commonClas: any;
  public location: string = "";
  public locationName: any = "";
  public possetionList: any = [];
  public cityList: any = [];
  public typologyList: any = [];
  public searchValues: any = [];
  public findCitySelected: any;
  public responseData: any;
  public listCity: any = [];
  public isGuest: any;
  public lat: any;
  public long: any;
  public step: any;
  public selectedFrom: any;
  public fromWhere: boolean = false;
  public disableRange: boolean = false;

  public isSetLastPrice = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public storagePrivider: DbProvider,
    public events: Events,
    public searchApi: SearchApiProvider,
    private loader: LoadingProvider,
    public alertCtrl: AlertController,
    private toast: ToasterProvider,
  ) {

    this.events.subscribe('closeSearchModal', () => {
      this.myDismiss('d');
    });

    this.lat = '';
    this.long = '';
    this.isGuest = this.storagePrivider.isGuest();
    this.responseData = this.navParams.get('responseData');
    this.selectedFrom = this.navParams.get('from');
    (this.selectedFrom == 'footer') ? this.fromWhere = false : this.fromWhere = true;
    localStorage.setItem('searchItems', JSON.stringify(this.responseData));
    if (typeof this.responseData.budget_data.min != 'undefined' && this.responseData.budget_data.min != null
      && typeof this.responseData.budget_data.max != 'undefined' && this.responseData.budget_data.max != null
      /* && this.responseData.budget_data.max != 0 && this.responseData.budget_data.min != 0 */
    ) {
      this.responseData.budget_data = {
        lower: this.responseData.budget_data.min,
        upper: this.responseData.budget_data.max
      };
      const lowerData = this.storagePrivider.getSearchCurrency(this.responseData.budget_data.lower);
      if (typeof lowerData != 'undefined') {
        if (lowerData.split(' ')[1] == "Lakh") {
          this.step = 500000;
        } else if (lowerData.split(' ')[1] == "Crore") {
          this.step = 1000000;
        } else {
          this.step = 10000;
        }
      } else {
        this.step = 500000;
      }
    } else {
      this.responseData.budget_data = {
        lower: 0,
        upper: 0
      };
      this.disableRange = true;
      this.step = 0;
    }
    this.goalProgress = this.responseData.budget_data;
    if (this.responseData.budget_data.lower == this.responseData.budget_data.upper) {
      this.disableRange = true;
    }
    if (this.selectedFrom == 'footer') {
      this.getlocation(); // Location First

      this.responseData.project_status.map((item: any) => {
        item['selected'] = false;
      });
      this.possetionList = this.responseData.project_status;

      this.responseData.typology_term_menu.map((item: any) => {
        item['selected'] = false;
      });
      this.typologyList = this.responseData.typology_term_menu;

      this.listCity = this.responseData.city_term_menu;

      this.responseData.sub_location_term_menu.map((item: any) => {
        item['selected'] = false;
      });
      this.cityList = this.responseData.sub_location_term_menu;

      this.responseData.property_type_term_menu.map((item: any, index: any) => {
        item['selected'] = false;
        if (index == 0) {
          item['selected'] = true;
        }
      });
      this.commonClas = this.responseData.property_type_term_menu;

      // this.selectcitybydefalt();
    } else {

      this.isSetLastPrice = false;

      const selectedTempData = JSON.parse(localStorage.getItem('tempsearchdata'));
      const searchedData = JSON.parse(localStorage.getItem('searchItems'));

      this.commonClas = searchedData.property_type_term_menu;
      this.listCity = searchedData.city_term_menu;
      this.possetionList = searchedData.project_status;
      this.typologyList = searchedData.typology_term_menu;
      this.cityList = searchedData.sub_location_term_menu;

      this.commonClas.map((item: any) => {
        item['selected'] = false;
        if (item.id == selectedTempData.project_status) {
          item['selected'] = true;
        }
      });

      const splitData = selectedTempData.location.split(',');
      let locationName = [];
      let locationId = [];
      this.listCity.map((item: any, index: any) => {
        item['selected'] = false;
        if (splitData.includes(item.id)) {
          item['selected'] = true;
          locationName.push(item.name);
          locationId.push(item.id);
        }
        this.locationName = locationName.toString();
        this.location = locationId.toString();
      });
      /* (this.locationName == '') ? this.locationName = 'Select Location' : ''; */

      const subLocData = selectedTempData.sub_location.split(',');
      this.cityList.map((item: any) => {
        item['selected'] = false;
        if (subLocData.includes(item.id)) {
          item['selected'] = true;
        }
      });

      const possessionData = selectedTempData.possession.split(',');
      this.possetionList.map((item: any) => {
        item['selected'] = false;
        if (possessionData.includes(item.id)) {
          item['selected'] = true;
        }
      });
      const typologyData = selectedTempData.typology.split(',');
      this.typologyList.map((item: any) => {
        item['selected'] = false;
        if (typologyData.includes(item.id)) {
          item['selected'] = true;
        }
      });
      this.goalProgress = {
        lower: selectedTempData.min_price,
        upper: selectedTempData.max_price
      };
      /* this.callSearchAPI(); */
      this.onChangeStop();
    }
    this.change();
  }

  selectcitybydefalt() {
    /* this.listCity.map((item: any, index: any) => {
    item['selected'] = false;
    if (index == 0) {
    item['selected'] = true;
    }
    }); */
    this.location = '';
    this.locationName = '';
    localStorage.setItem('lastSearched', 'location');
    if (this.fromWhere) {
      this.callSearchAPI();
    }
    // this.getSubLocation(this.location);
  }

  getlocation() { //Main Method  // Location First
    this.loader.show();
    this.storagePrivider.getLocation().then((res: any) => {
      if (res.coords.latitude && res.coords.longitude) {
        this.lat = '';
        this.long = '';
        this.storagePrivider.getReverseGeoCode(res.coords.latitude, res.coords.longitude).then((result: any) => {
          this.location = this.matchcity(result.locality);
          //this.loader.hide(); Here
          this.fromWhere = false;
          localStorage.setItem('lastSearched', 'location');
          this.callSearchAPI();
        }).catch((err) => {
          /* this.selectcitybydefalt(); */
          this.loader.hide();
          console.log(err);
        });
      } else {
        /* this.selectcitybydefalt(); */
        this.loader.hide();
      }
    }).catch((err) => {
      /* this.selectcitybydefalt(); */
      this.loader.hide();
      console.log(err);
    });

  }

  ionViewDidLoad() {
    /* this.goalProgress = this.responseData.budget_data;
    this.change(); */
  }

  matchcity(param) {
    console.log("Locality Param : " + JSON.stringify(param));

    const serachCityName = this.listCity.findIndex(x => x.name.toLowerCase() == param.toLowerCase());
    if (serachCityName > -1) {
      this.listCity[serachCityName].selected = true;
      this.locationName = param;
      return this.listCity[serachCityName].id;
    } else {
      this.locationName = "";
      /* return this.listCity[this.listCity.length - 1].id; */
      return 0;
    }

  }

  myDismiss(value: any) {
    this.viewCtrl.dismiss(this.storagePrivider.getValue());
    if (value == "c") {
      this.events.publish('openSearchListPage', "SearchListPage");
    }
  }

  change() {
    const lowerData = this.storagePrivider.getSearchCurrency(this.goalProgress.lower);
    if (typeof lowerData != 'undefined') {
      if (lowerData.split(' ')[1] == "Lakh") {
        this.step = 500000;
      } else if (lowerData.split(' ')[1] == "Crore") {
        this.step = 1000000;
      } else {
        this.step = 10000;
      }
    } else {
      this.step = 500000;
    }
    this.lower = this.goalProgress.lower;
    this.upper = this.goalProgress.upper;
    /* if (this.lower == 0 && this.upper == 0) {
      this.disableRange = true;
    } else {
      this.disableRange = false;
    } */
    this.fromWhere = false;
  }

  resOrCom(index: any) {
    if (this.commonClas.length > 1) {
      let findSelected = this.commonClas.findIndex(x => x.selected == true);
      this.commonClas[index].selected = true;
      if (findSelected > -1 && (index != findSelected)) {
        this.commonClas[findSelected].selected = false;
      }
      this.fromWhere = false;
      localStorage.setItem('lastSearched', 'project_status');
      this.callSearchAPI();
    }
  }

  click(pageName: any) {
    this.navCtrl.push(pageName);
  }

  getSelected(type: any, index: any) {
    if (type == "city") {
      (this.cityList[index].selected == false) ? this.cityList[index].selected = true : this.cityList[index].selected = false;
      localStorage.setItem('lastSearched', 'sub_location');
    } else if (type == "possession") {
      (this.possetionList[index].selected == false) ? this.possetionList[index].selected = true : this.possetionList[index].selected = false;
      localStorage.setItem('lastSearched', 'possession');
    } else if (type == "typology") {
      (this.typologyList[index].selected == false) ? this.typologyList[index].selected = true : this.typologyList[index].selected = false;
      localStorage.setItem('lastSearched', 'typology');
    }
    this.fromWhere = false;
    this.callSearchAPI();
  }

  onChangeStop() {

    let minPrice = this.goalProgress.lower;
    let maxPrice = this.goalProgress.upper;

    console.log("onChangeStop() : " + Number(this.goalProgress.lower));
    console.log("onChangeStop() : " + Number(this.goalProgress.upper));

    if (maxPrice != 0 && maxPrice > minPrice) {
      minPrice = this.goalProgress.lower;
      maxPrice = this.goalProgress.upper;
    } else {
      minPrice = "";
      maxPrice = "";
    }
    console.log("onChangeStop() : " + minPrice);
    console.log("onChangeStop() : " + maxPrice);

    /*   if (this.isSetLastPrice) {
        localStorage.setItem('lastSearched', 'price');
      }
      this.callSearchAPI(); */

    if (maxPrice > 0) {
      if (this.isSetLastPrice) {
        localStorage.setItem('lastSearched', 'price');
      }
      this.callSearchAPI();
    } else {
      this.disableRange = false;
    }
  }

  search() {
    let location = this.location;
    if (location != '') {
      this.lat = '';
      this.long = '';
    }

    let locality = this.cityList.filter(x => x.selected == true);
    const localityArr = [];
    locality.map((item: any) => {
      localityArr.push(item.id);
    });

    let possession = this.possetionList.filter(x => x.selected == true);
    const possessionArr = [];
    possession.map((item: any) => {
      possessionArr.push(item.id);
    });

    let typology = this.typologyList.filter(x => x.selected == true);
    const typoArr = [];
    typology.map((item: any) => {
      typoArr.push(item.id);
    });

    let projectStatus = this.commonClas.findIndex(x => x.selected == true);
    const statusId = this.commonClas[projectStatus].id;
    if (Array.isArray(this.locationName)) {
      if (this.locationName.length == 0) {
        location = '';
      }
    } else {
      if (this.locationName == 'Select Location') {
        location = '';
      }
    }



    let minPrice = this.goalProgress.lower;
    let maxPrice = this.goalProgress.upper;

    console.log(Number(this.goalProgress.lower));
    console.log(Number(this.goalProgress.upper));

    if (maxPrice != 0 && maxPrice > minPrice) {
      minPrice = this.goalProgress.lower;
      maxPrice = this.goalProgress.upper;
    } else {
      minPrice = "";
      maxPrice = "";
    }
    console.log(minPrice);
    console.log(maxPrice);

    const jsonData = {
      //user_id: localStorage.getItem('userId'),
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
      //device_id: '8234a52c10d15b2b',
      project_status: statusId,
      location: location.toString(),
      lat: this.lat,
      lng: this.long,
      min_price: minPrice /* this.goalProgress.lower */,
      max_price: maxPrice /* this.goalProgress.upper */,
      possession: possessionArr.toString(),
      typology: typoArr.toString(),
      sub_location: localityArr.toString(),
      page: 0
    };
    localStorage.setItem('tempsearchdata', JSON.stringify(jsonData));

    // console.log(jsonData);

    this.myDismiss('c');
  }

  onChange(value: any) {
    console.log(value);
  }

  showCheckbox() {
    if (this.listCity != null && this.listCity.length > 0) {

      let alert = this.alertCtrl.create();
      alert.setTitle('City List');
      alert._cssClass = "city_checkbox";
      this.listCity.map((item: any) => {
        alert.addInput({
          type: 'checkbox',
          label: item.name,
          value: item.id,
          checked: item.selected,
        });
      });
      alert.addButton({ cssClass: 'cancelBtn', text: 'Cancel' });
      alert.addButton({
        cssClass: 'okBtn',
        text: 'Ok',
        handler: data => {
          console.log('Checkbox data:', data);
          this.locationName = [];
          this.listCity.map((selected: any) => {
            const findIndex = data.findIndex(x => x == selected.id);
            if (findIndex > -1) {
              selected.selected = true;
              this.locationName.push(selected.name);
            } else {
              selected.selected = false;
            }
          });
          this.location = data;
          this.locationName = this.locationName.toString();
          // this.getSubLocation(this.location);
          this.fromWhere = false;
          localStorage.setItem('lastSearched', 'location');
          this.callSearchAPI();
        }
      });
      alert.present();
    } else {

      let alert = this.alertCtrl.create();
      alert.setTitle('City List');
      alert.setMessage("No City Found");
      alert._cssClass = "city_checkbox";
      alert.addButton({ cssClass: 'cancelBtn', text: 'Cancel' });
      alert.addButton({
        cssClass: 'okBtn',
        text: 'Ok',
        handler: data => {
        }
      });
      alert.present();
    }
  }

  callSearchAPI() {

    this.isSetLastPrice = true;

    let projectStatus = this.commonClas.findIndex(x => x.selected == true);
    const statusId = this.commonClas[projectStatus].id;

    let location = this.location;

    let locality = this.cityList.filter(x => x.selected == true);
    const localityArr = [];
    locality.map((item: any) => {
      localityArr.push(item.id);
    });

    let possession = this.possetionList.filter(x => x.selected == true);
    const possessionArr = [];
    possession.map((item: any) => {
      possessionArr.push(item.id);
    });

    let typology = this.typologyList.filter(x => x.selected == true);
    const typoArr = [];
    typology.map((item: any) => {
      typoArr.push(item.id);
    });

    this.getSearch(
      statusId,
      location,
      localityArr,
      possessionArr,
      typoArr
    );
  }

  getSearch(propertyTypeTermMenu, cityTermMenu, subLocation, projectStatus, typologyTermMenu) {
    if (!this.fromWhere) {
      this.loader.show();
      let lastSearchDesect = [];
      if (localStorage.getItem('lastSearched') == 'location') {
        this.listCity.map((item: any) => {
          if (!item.selected) {
            lastSearchDesect.push(item);
          }
        });
        /* if (lastSearchDesect.length == this.listCity.length) {
        lastSearchDesect = [];
        } */

      } else if (localStorage.getItem('lastSearched') == 'possession') {
        this.possetionList.map((item: any) => {
          if (!item.selected) {
            lastSearchDesect.push(item);
          }
        });
        /* if (lastSearchDesect.length == this.possetionList.length) {
        lastSearchDesect = [];
        } */
      } else if (localStorage.getItem('lastSearched') == 'typology') {
        this.typologyList.map((item: any) => {
          if (!item.selected) {
            lastSearchDesect.push(item);
          }
        });
        /* if (lastSearchDesect.length == this.typologyList.length) {
        lastSearchDesect = [];
        } */
      } else if (localStorage.getItem('lastSearched') == 'sub_location') {
        this.cityList.map((item: any) => {
          if (!item.selected) {
            lastSearchDesect.push(item);
          }
        });
        /* if (lastSearchDesect.length == this.cityList.length) {
        lastSearchDesect = [];
        } */
      } else if (localStorage.getItem('lastSearched') == 'price') {
        lastSearchDesect = [{
          min: this.responseData.budget_data.lower
        }, {
          max: this.responseData.budget_data.upper
        }];

      }

      let newCityTerm = [];
      try {
        if (Array.isArray(cityTermMenu)) {
          cityTermMenu.map((item: any) => {
            if (item != '' || item != null) {
              newCityTerm.push(item);
            }
          });
        } else {
          const splitData = cityTermMenu.split(',');
          splitData.map((item: any) => {
            if (item != '' || item != null) {
              newCityTerm.push(item);
            }
          });
        }
        cityTermMenu = newCityTerm;
      } catch (err) {
        cityTermMenu = [];
      }


      let searchParam;
      searchParam = {
        user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
        device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
        property_type_term_menu: propertyTypeTermMenu,
        city_term_menu: cityTermMenu.toString(),
        sub_location: subLocation.toString(),
        project_status: projectStatus.toString(),
        typology_term_menu: typologyTermMenu.toString(),
        min_price: this.goalProgress.lower,
        max_price: this.goalProgress.upper,
        lastSearch: localStorage.getItem('lastSearched'),
        lastDesect: lastSearchDesect
      };
      if (localStorage.getItem('lastSearched') == 'project_status') {
        searchParam = {
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
          property_type_term_menu: propertyTypeTermMenu,
          city_term_menu: '',
          sub_location: '',
          project_status: '',
          typology_term_menu: '',
          min_price: '',
          max_price: '',
          lastSearch: localStorage.getItem('lastSearched'),
          lastDesect: lastSearchDesect
        };
      }
      this.searchApi.propertySearchInfo(searchParam).subscribe((response: any) => {

        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          this.loader.hideNoTimeout();
          const searchData = JSON.parse(localStorage.getItem('searchItems'));
          const newJsonData = {
            budget_data: searchData.budget_data,
            city_term_menu: response.data.city_term_menu,
            project_status: response.data.project_status,
            property_type_term_menu: response.data.property_type_term_menu,
            sub_location_term_menu: response.data.sub_location_term_menu,
            typology_term_menu: response.data.typology_term_menu
          };
          localStorage.setItem('searchItems', JSON.stringify(newJsonData));
          if (response.data.property_type_term_menu.length > 0) {
            response.data.property_type_term_menu.map((item: any, index: any) => {
              if (propertyTypeTermMenu == item.id) {
                item['selected'] = true;
              } else {
                item['selected'] = false;
              }
            });
          }

          this.commonClas = response.data.property_type_term_menu;
          if (response.data.city_term_menu.length > 0 && localStorage.getItem('lastSearched') != 'project_status') {
            response.data.city_term_menu.map((selected: any) => {
              if (Array.isArray(cityTermMenu)) {
                const findIndex = cityTermMenu.findIndex(x => x == selected.id);
                if (findIndex > -1) {
                  selected.selected = true;
                } else {
                  selected.selected = false;
                }
              } else {
                const splidData = cityTermMenu.split(',');
                selected.selected = false;
                if (splidData.includes(selected.id)) {
                  selected.selected = true;
                } else {
                  selected.selected = false;
                }
              }
            });
          } else {
            this.location = '';
            this.locationName = '';
          }

          this.listCity = response.data.city_term_menu;

          if (!Array.isArray(cityTermMenu)) {
            const splidData = cityTermMenu.split(',');
            cityTermMenu = splidData;
          }

          if (cityTermMenu.length > 0) {
            cityTermMenu.map((item: any, index: any) => {
              if (this.listCity.findIndex(x => x.id == item) == -1) {
                delete cityTermMenu[index];
              }
            });
            let locName = [];
            let locationId = [];
            this.listCity.map((item: any) => {
              if (item.selected) {
                locName.push(item.name);
                locationId.push(item.id);
              }
            });
            this.location = locationId.toString();
            this.locationName = locName;
          }


          if (response.data.sub_location_term_menu.length > 0 && localStorage.getItem('lastSearched') != 'project_status') {
            response.data.sub_location_term_menu.map((item: any) => {
              const findIndex = subLocation.findIndex(x => x == item.id);
              if (findIndex > -1) {
                item['selected'] = true;
              } else {
                item['selected'] = false;
              }
            });
          }
          if (response.data.sub_location_term_menu.length > 0 && localStorage.getItem('lastSearched') == 'project_status') {
            response.data.sub_location_term_menu.map((item: any) => {
              item['selected'] = false;
            });
          }

          this.cityList = response.data.sub_location_term_menu;
          if (response.data.project_status.length > 0 && localStorage.getItem('lastSearched') != 'project_status') {
            response.data.project_status.map((item: any) => {
              const findIndex = projectStatus.findIndex(x => x == item.id);
              if (findIndex > -1) {
                item['selected'] = true;
              } else {
                item['selected'] = false;
              }
            });
          }
          if (response.data.project_status.length > 0 && localStorage.getItem('lastSearched') == 'project_status') {
            response.data.project_status.map((item: any) => {
              item['selected'] = false;
            });
          }

          this.possetionList = response.data.project_status;
          if (response.data.typology_term_menu.length > 0 && localStorage.getItem('lastSearched') != 'project_status') {
            response.data.typology_term_menu.map((item: any) => {
              const findIndex = typologyTermMenu.findIndex(x => x == item.id);
              if (findIndex > -1) {
                item['selected'] = true;
              } else {
                item['selected'] = false;
              }
            });
          }
          if (response.data.typology_term_menu.length > 0 && localStorage.getItem('lastSearched') == 'project_status') {
            response.data.typology_term_menu.map((item: any) => {
              item['selected'] = false;
            });
          }

          this.typologyList = response.data.typology_term_menu;

          if (localStorage.getItem('lastSearched') != 'price') {
            if (typeof response.data.budget_data.min != 'undefined' && response.data.budget_data.min != null
              && typeof response.data.budget_data.max != 'undefined' && response.data.budget_data.max != null
            ) {
              if (response.data.budget_data.min != response.data.budget_data.max) {
                this.disableRange = false;
              } else {
                this.disableRange = true;
              }
              response.data.budget_data = {
                lower: response.data.budget_data.min,
                upper: response.data.budget_data.max
              };
              this.goalProgress = {
                lower: response.data.budget_data.lower,
                upper: response.data.budget_data.upper
              };
              console.log(JSON.stringify(this.goalProgress));

              const lowerData = this.storagePrivider.getSearchCurrency(response.data.budget_data.lower);
              if (typeof lowerData != 'undefined' && lowerData != 0) {
                if (lowerData.split(' ')[1] == "Lakh") {
                  this.step = 500000;
                } else if (lowerData.split(' ')[1] == "Crore") {
                  this.step = 1000000;
                } else {
                  this.step = 10000;
                }
              } else {
                this.step = 500000;
              }
            } else {
              this.responseData.budget_data = {
                lower: 0,
                upper: 0
              };
              this.goalProgress = {
                lower: 0,
                upper: 0
              };
              this.step = 0;
              this.disableRange = true;
            }
            this.responseData.budget_data = {
              lower: this.responseData.budget_data.min,
              upper: this.responseData.budget_data.max
            };
            this.lower = this.responseData.budget_data.lower;
            this.upper = this.responseData.budget_data.upper;
          }

        } else {
          this.loader.hide();
          this.myDismiss('d');
          /* this.toast.show(response.msg); */
        }
      }, (error: any) => {
        this.loader.hide();
        this.myDismiss('d');
        /* this.toast.show(error.statusText); */
      });
    } else {
      this.loader.hideNoTimeout();
      this.commonClas.map((item: any, index: any) => {
        if (propertyTypeTermMenu == item.id) {
          item['selected'] = true;
        } else {
          item['selected'] = false;
        }
      });

      this.listCity.map((selected: any, index: any) => {
        if (Array.isArray(cityTermMenu)) {
          const findIndex = cityTermMenu.findIndex(x => x == selected.id);
          if (findIndex > -1) {
            selected.selected = true;
          }
          /*else {
            delete this.listCity[index]; 
         }*/
        } else {
          const splidData = cityTermMenu.split(',');
          selected.selected = false;
          if (splidData.includes(selected.id)) {
            selected.selected = true;
          } /*else {
             delete this.listCity[index]; 
          }*/
        }
      });

      this.fromWhere = false;
      //this.forEditCallApi(propertyTypeTermMenu, cityTermMenu, subLocation, projectStatus, typologyTermMenu);
    }
  }

  forEditCallApi(propertyTypeTermMenu, cityTermMenu, subLocation, projectStatus, typologyTermMenu) {

    const searchParam = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storagePrivider.getDeviceInfo().uuid : '',
      property_type_term_menu: propertyTypeTermMenu,
      city_term_menu: cityTermMenu.toString(),
      sub_location: subLocation.toString(),
      project_status: projectStatus.toString(),
      typology_term_menu: typologyTermMenu.toString(),
      min_price: this.goalProgress.lower,
      max_price: this.goalProgress.upper,
    };
    this.searchApi.propertySearchInfo(searchParam).subscribe((response: any) => {
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        response.data.sub_location_term_menu.map((item: any) => {
          const findIndex = subLocation.findIndex(x => x == item.id);
          if (findIndex > -1) {
            item['selected'] = true;
          } else {
            item['selected'] = false;
          }
        });
        this.cityList = response.data.sub_location_term_menu;

        response.data.project_status.map((item: any) => {
          const findIndex = projectStatus.findIndex(x => x == item.id);
          if (findIndex > -1) {
            item['selected'] = true;
          } else {
            item['selected'] = false;
          }
        });
        this.possetionList = response.data.project_status;
        response.data.typology_term_menu.map((item: any) => {
          const findIndex = typologyTermMenu.findIndex(x => x == item.id);
          if (findIndex > -1) {
            item['selected'] = true;
          } else {
            item['selected'] = false;
          }
        });
        this.typologyList = response.data.typology_term_menu;
      }
    })

  }

  getSubLocation(locationData) {
    const param = {
      city_id: locationData.toString()
    };

    this.loader.show();
    this.searchApi.getSubLocation(param).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == '200') {
        response.data.map((item: any) => {
          item['selected'] = false;
        });
        this.cityList = response.data;
      }

      this.callSearchAPI();
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    });
  }

}