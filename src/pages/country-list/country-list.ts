import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import { LoadingProvider } from '../../providers/utils/loader';
import { DbProvider } from '../../providers/db/db';
/**
 * Generated class for the CountryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-country-list',
  templateUrl: 'country-list.html',
})
export class CountryListPage {
  public countryList: any;
  searchTerm: string = '';
  items: any;
  public shouldShowCancel: boolean = false;
  searchControl: FormControl;
  searching: any = false;
  isShowSearchBar: boolean = false;
  public showCountryCodes:boolean=true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loader: LoadingProvider,
    public viewCtrl: ViewController,
    public storageProvider: DbProvider, 
    public events: Events
    ) {
    this.searchControl = new FormControl();

    // this.countryList = this.navParams.get('countryList');
    // this.items = this.countryList

    this.countryList = [];
    this.items = [];
    this.getCountryList();
    this.events.subscribe('closeCountryModal',()=>{
      this.myDismiss();
    });
    this.showCountryCodes=this.navParams.data.showCodes;
  }

  getCountryList() {
   /*  this.loader.show('Please wait..'); */
    this.countryList = this.storageProvider.getCountryCodes();
    if (this.countryList.length > 0) {
      this.items = this.countryList;
      this.isShowSearchBar = true;
    } else {
      this.items = [];
      this.isShowSearchBar = false;
    }
    if(this.navParams.data.hasOwnProperty('from')){
      if(this.navParams.data.from== 'nri'){
        let newCountryList=[];
        this.countryList.map((item:any)=>{
          if(item.callingCodes != '+91'){
            newCountryList.push(item);
          }
        });
        this.countryList=[];
        this.items = [];
        this.items = newCountryList;
        this.countryList=newCountryList;
      }
    }
  }

  ionViewDidLoad() {
    // this.loader.hide();
  }

  ionViewDidEnter() {
    this.loader.hide();
  }

  onInput() {
    this.searching = true;
    this.items = this.countryList.filter(x => (x.name).toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
  }

  selectCountry(item: any) {
    const findSelected = this.countryList.findIndex(x => x.selected == true);
    if (findSelected > -1) {
      this.countryList[findSelected].selected = false;
    }
    const findToBSelected = this.countryList.findIndex(x => x.alpha3Code == item.alpha3Code);
    this.countryList[findToBSelected].selected = true;
    this.viewCtrl.dismiss(this.countryList);
  }

  myDismiss() {
    this.viewCtrl.dismiss();
  }
}
