import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/apis/auth';
import { OtpApiresponse } from '../../models/auth';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';


/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {

  public gender: object = {
    male: true,
    female: false
  };
  public temsCheck: boolean = false;
  public signUpForm: FormGroup;

  public disableButton: boolean = true;
  public fName: boolean = false;
  public notMatched: boolean = false;

  public openClose: boolean = false;
  public openCloseConf: boolean = false;
  public countryCodes: any = [];
  public flag: any;
  public deviceinfo: any;
  public findIndia: any;
  public addModal: any;
  public countryInitials: any;

  public isShowforgotpassDiv: boolean = false;

  public preUserId: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public storageProvider: DbProvider,
    public modalCtrl: ModalController,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public events: Events,
  ) {
    this.gender = {
      male: true,
      female: false
    };
  }

  ionViewDidLoad() {
    this.isShowforgotpassDiv = false;
    this.gender = {
      male: true,
      female: false
    };
    console.log('ionViewDidLoad SignupPage');
  }

  ngOnInit() {
    /* Signup form initialization */
    this.deviceinfo = this.storageProvider.getDeviceInfo();


    this.countryCodes = this.storageProvider.getCountryCodes();

    this.countryCodes.map((item: any) => {
      item["selected"] = false
    });
    this.findIndia = this.countryCodes.findIndex(x => x.callingCodes === "+91");
    this.countryCodes[this.findIndia].selected = true;
    this.countryInitials = this.countryCodes[this.findIndia];


    this.signUpForm = this.fb.group({
      fname: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern())
      ]),
      lname: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern())
      ]),
      countryCode: new FormControl(this.countryInitials, [
        Validators.required
      ]),
      mobile: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyNumberPatternsignup()),
        Validators.minLength(10),
        Validators.maxLength(10)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.emailPattern())
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.passwordPattern())
      ]),
      cpassword: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });

    this.signUpForm.valueChanges.subscribe((v) => {
      if (this.temsCheck && this.signUpForm.valid) {
        this.disableButton = false;
      }
      this.notMatched = false;
      if (v.password != v.cpassword && v.cpassword != "") {
        this.notMatched = true;
        this.disableButton = true;
      }
    });

    /* Select India By Default  */
    this.onChange(this.countryCodes[this.findIndia]);
  }

  checkVerification() {

    const selectedCountry = this.countryCodes.findIndex(x => x.selected == true);

    let data = {
      mobile_no: this.countryCodes[selectedCountry].callingCodes + this.signUpForm.value.mobile,
      email: this.signUpForm.value.email,
      device_id: this.deviceinfo.uuid,
      first_name: this.signUpForm.value.fname,
      last_name: this.signUpForm.value.lname,
      password: this.signUpForm.value.password,
      first_time: "true",
      device_type: "android"
    };
    this.loader.show('Please wait...');
    this.Authapi.postRegisterOtpApi(data).subscribe((response: OtpApiresponse) => {
      this.loader.hide();
      if (response.status == "200") {
        let postData = {
          first_name: this.signUpForm.value.fname,
          last_name: this.signUpForm.value.lname,
          email: this.signUpForm.value.email,
          mob_no: this.countryCodes[selectedCountry].callingCodes + this.signUpForm.value.mobile,
          password: this.signUpForm.value.password,
          device_type: "Android",
          device_id: this.deviceinfo.uuid,
          response_otp: response.otp,
          responseMsg: response.msg,
          user_id: "",
          req_type: response.req_type,
          req_cont: response.req_cont,
          req_name: response.req_name,
        };
        
        this.events.publish('startOTPAutoRead');
        this.navCtrl.push("VarifyOtpPage", { formData: postData });
      } else if (response.status == "204") {
        this.toast.show(response.msg);
        this.isShowforgotpassDiv = true;
      } else if (response.status == "209") {
        this.toast.show(response.msg);
        this.preUserId = response.user_id;
        let postData = {
          first_name: this.signUpForm.value.fname,
          last_name: this.signUpForm.value.lname,
          email: this.signUpForm.value.email,
          mob_no: this.countryCodes[selectedCountry].callingCodes + this.signUpForm.value.mobile,
          password: this.signUpForm.value.password,
          device_type: "Android",
          device_id: this.deviceinfo.uuid,
          response_otp: response.otp,
          responseMsg: response.msg,
          user_id: response.user_id,
          req_type: response.req_type,
          req_cont: response.req_cont,
          req_name: response.req_name,
        };
        
        this.events.publish('startOTPAutoRead');
        this.navCtrl.push("VarifyOtpPage", { formData: postData });
      } else {
        this.toast.show(response.msg);
      }

    }, (error: any) => {
      console.log(error);
      this.loader.hide();
      this.navCtrl.popToRoot();
      this.toast.show('Some Thing Went Wrong, Please Try Again After Some Time.');
    });
  }

  register() {
  }

  get f() {
    return this.signUpForm.controls;
  }

  checkBox(value: any) {
    (value == "M") ? this.gender = {
      male: true,
      female: false
    } : this.gender = {
      male: false,
      female: true
    }
  }

  checkboxTerms() {
    this.temsCheck = !this.temsCheck;
    this.disableButton = true;
    if (this.temsCheck && this.signUpForm.valid) {
      this.disableButton = false;
    }

  }

  loginPage() {
    this.navCtrl.popToRoot();
  }

  eyeOpenClose(value: any, type: any) {
    if (value == "O") {
      if (type == "pass") {
        this.openClose = true;
      } else {
        this.openCloseConf = true;
      }
    } else {
      if (type == "pass") {
        this.openClose = false;
      } else {
        this.openCloseConf = false;
      }

    }
  }

  onChange(event: any) {
    this.flag = event.flag
  }

  openTerms() {
    this.addModal = this.modalCtrl.create("TermsConditionPage", {});
    this.addModal.onDidDismiss((item: any) => {
    });
    this.addModal.present();
  }

  goBack() {
    this.navCtrl.pop();
  }

  openCountry() {
    /* this.loader.show('Please wait..'); */
    // this.addModal = this.modalCtrl.create("CountryListPage", { countryList: this.countryCodes });
    this.addModal = this.modalCtrl.create("CountryListPage", { showCodes: true });
    this.addModal.onDidDismiss((item: any) => {
      if (item) {
        this.countryCodes = item;
        const findSelected = item.findIndex(x => x.selected == true);
        this.countryInitials = item[findSelected];
        this.flag = this.countryInitials.flag;
        if (this.countryInitials.callingCodes == '+91') {
          this.signUpForm.controls["mobile"].setValidators([
            Validators.minLength(10),
            Validators.maxLength(10),
            Validators.required,
            Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
          ]);
        } else {
          this.signUpForm.controls["mobile"].setValidators([
            Validators.required,
            Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
          ]);
        }
        this.signUpForm.controls['mobile'].updateValueAndValidity();
      }
    });
    this.addModal.present();
    // setTimeout(() => {
    //   this.loader.hide();
    // }, 5000);
  }

  goToPage(pageName: any) {
    this.navCtrl.push(pageName);
  }

}
