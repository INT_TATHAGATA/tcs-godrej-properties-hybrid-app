import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnquiryFormPage } from './enquiry-form';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';


@NgModule({
  declarations: [
    EnquiryFormPage,
  ],
  imports: [
    IonicPageModule.forChild(EnquiryFormPage),
    ComponentsModule
  ],
  providers: [ProjectDetailsApiProvider]
})
export class EnquiryFormPageModule { }
