import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WishlistPage } from './wishlist';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';

@NgModule({
  declarations: [
    WishlistPage,
  ],
  imports: [
    IonicPageModule.forChild(WishlistPage),
  ],
  providers: [WishListApiProvider]
})
export class WishlistPageModule { }
