import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, MenuController, AlertController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';
import { AppConst } from '../../app/app.constants';
import { DbProvider } from '../../providers/db/db';

/**
 * Generated class for the WishlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wishlist',
  templateUrl: 'wishlist.html',
})
export class WishlistPage {
  wishlist: Array<any>;

  public noDataMsg: string = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private loader: LoadingProvider,
    public events: Events,
    private toast: ToasterProvider,
    private wishlistApi: WishListApiProvider,
    public storageProvider: DbProvider,
    private menu: MenuController,
    private alertCtrl: AlertController
  ) {
    this.menu.swipeEnable(false);
    this.wishlist = [];
  }

  ionViewWillEnter() {
    this.wishlist = [];
    this.getwishlist();
  }

  ionViewDidEnter() {
    /* this.wishlist = [];
    this.getwishlist(); */
    /*  console.log('ionViewDidLoad WishlistPage'); */
  }

  getwishlist() {
    const param = {
      user_id: localStorage.getItem('userId')
    };
    this.loader.show();
    this.wishlistApi.getwishlisting(param).subscribe((response: any) => {
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.wishlist = response.data;
        if (this.wishlist.length == 0) {
          this.noDataMsg = "No Wishlist Found.";
        } else {
          this.noDataMsg = "";
        }
        this.wishlist.map((item: any) => {
          if (item.typologies.split(',').length > 4) {
            item['typologies'] = item.typologies.split(',').slice(1, 5).toString() + '...';
          }
        });
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    },
      () => {
        this.loader.hideNoTimeout();
      });
  }

  RamoveWishList(item) {
    const data = {
      user_id: localStorage.getItem('userId'),
      proj_id: item.proj_id,
      type: 'delete'
    }
    this.loader.show('Please wait...');
    this.wishlistApi.modifyWishlist(data).subscribe((response: any) => {
      console.log(response);
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.events.publish('showNotiDot');
        this.getwishlist();
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    });
  }

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  goToPage(pageName: any, project: any) {
    if (pageName == 'ProjectDetailsPage') {
      const data = { fullData: project };
      this.navCtrl.push(pageName, data);
    } else {
      if (project.field_is_available_for_booking != null && project.field_is_available_for_booking == '0') {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          message: 'Online booking for this project is currently unavailable.',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
      } else {

        const data = {
          project_id: project.proj_id,
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : ''
        };
        const extradata = {
          pName: project.proj_name
        };
        if (project.eoi_enabled == '1') {
          this.navCtrl.push('EoiPage', { formData: data, img: project.proj_img != '' ? project.proj_img : '', extdata: extradata });
        } else {
          this.navCtrl.push(pageName, { formData: data, img: project.proj_img != '' ? project.proj_img : '', extdata: extradata });
        }
      }
    }
  }

}
