import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { DbProvider } from '../../providers/db/db';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { LoadingProvider } from '../../providers/utils/loader';
import { MyProfileApiProvider } from '../../providers/apis/myprofile';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';
import { AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/apis/auth';
import moment from 'moment';
import { DatePicker } from '@ionic-native/date-picker';
/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage implements OnInit {

  noti: boolean = false;
  heart: boolean = false;
  pin: boolean = false;
  menuIcon: boolean = true;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = false;
  subHeadingText: string = "";
  public profileForm: FormGroup;
  public selectedImage: any = "";
  public onlyAlphabet = new RegExp("^[a-zA-Z]+$");
  @ViewChild('fileInput') fileInput;
  public cordova: any;
  public lastImage: string = null;
  public isReadyToSave: boolean = false;
  public initialNames: any;
  public minDate: any;
  public maxDate: any;
  public maxDateDob: any;
  public aniDateDisable: boolean = true;
  public getImage: any;
  fileTransfer: FileTransferObject;
  public userInfo: any;
  public deviceId: any;
  public changePassForm: FormGroup;
  public openClose: boolean = false;
  public openClose2: boolean = false;
  public openClose3: boolean = false;
  public notMatched: boolean = false;


  public addModal: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public camera: Camera,
    public storageProvider: DbProvider,
    private transfer: FileTransfer,
    private file: File,
    private filePath: FilePath,
    private DomSanitizer: DomSanitizer,
    public events: Events,
    private loader: LoadingProvider,
    public profileApi: MyProfileApiProvider,
    private toast: ToasterProvider,
    public alertCtrl: AlertController,
    private Authapi: AuthProvider,
    private datePicker: DatePicker,
    public modalCtrl: ModalController,
  ) {
  }

  ionViewDidLoad() {
    this.deviceId = this.storageProvider.getDeviceInfo().uuid;
    const P5AppDeviceInfo = {
      P5AppDeviceInfo: this.storageProvider.getDeviceInfo().model,
      DeviceId: this.storageProvider.getDeviceInfo().uuid,
      Locality: "User Profile",
      City: "City",
      State: "State",
      Country: "Country",
      CountryCode: "CountryCode",
      Latitude: "0.0",
      Longitude: "0.0",
      IsObjectReady: true
    };
  }

  ngOnInit() {
    const userDetails = JSON.parse(localStorage.getItem('userData'));
    let fInitials = userDetails.data.first_name.substring(0, 1).toUpperCase();
    let lInitials = userDetails.data.last_name.substring(0, 1).toUpperCase();
    let fullInitial = fInitials + lInitials;
    this.initialNames = fullInitial;
    const dobYr = new Date().getFullYear() - 20;
    const dobMn = (new Date().getMonth() + 1).toString();
    (dobMn < '10') ? '0' + dobMn.toString() : dobMn.toString();
    const full = (dobYr + '-' + (dobMn) + '-' + new Date().getDate()).toString();

    this.maxDateDob = (moment().subtract(18, 'years')).format('YYYY-MM-DD');//"1999-12-31";
    console.log(this.maxDateDob);
    this.profileForm = this.fb.group({
      salutaion: new FormControl('', [
        Validators.required
      ]),
      fname: new FormControl(userDetails.data.first_name, [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern())
      ]),
      lname: new FormControl(userDetails.data.last_name, [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern())
      ]),
      dob: new FormControl('', []),
      anniversary: new FormControl('', [])
    });

    this.profileForm.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.profileForm.valid;
    });
    this.getProfileInfo();

    this.changePassForm = this.fb.group({
      oldpassword: new FormControl('', [
        Validators.required
      ]),
      newpassword: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.passwordPattern())
      ]),
      confpassword: new FormControl('', [
        Validators.required
      ]),
    });

    this.changePassForm.valueChanges.subscribe((v: any) => {
      this.notMatched = false;
      if (v.confpassword != '') {
        if (v.newpassword != v.confpassword) {
          this.notMatched = true;
        }
      }
    });
  }

  getProfileInfo() {
    const jsonData = {
      user_id: localStorage.getItem('userId')
    };
    this.loader.show();
    this.profileApi.getProfileList(jsonData).subscribe((response: any) => {
      if (response.status == '200') {
        const userData = response.data[0];
        this.userInfo = userData;
        const getUserSavedData = JSON.parse(localStorage.getItem('userData'));
        getUserSavedData.data.first_name = userData.first_name;
        getUserSavedData.data.last_name = userData.last_name;
        getUserSavedData.data.mob_no = userData.mobile;
        getUserSavedData.data.pan_no = userData.pan;
        getUserSavedData.data.email = userData.email;
        getUserSavedData.data.user_image = userData.user_image;
        getUserSavedData.data.profile_pic = userData.user_image;


        if (userData.birth_dt != '' && userData.birth_dt != null) {
          if (!getUserSavedData.data.hasOwnProperty('birth_dt')) {
            getUserSavedData.data['birth_dt'] = userData.birth_dt;
          } else {
            getUserSavedData.data.birth_dt = userData.birth_dt;
          }
        }

        localStorage.setItem('userData', JSON.stringify(getUserSavedData));
        this.profileForm.patchValue({
          salutaion: (userData.salutation) ? userData.salutation : '',
          fname: userData.first_name,
          lname: userData.last_name,
          dob: (userData.birth_dt) ? userData.birth_dt : '',
          anniversary: (userData.annivesary_dt) ? userData.annivesary_dt : ''
        });
        this.selectedImage = userData.user_image;
        if (userData.birth_dt != '') {
          this.updateMyDate('', 'dob');
        }
        this.events.publish("setUserDetails");
      }
      this.loader.hide();
    }, (error: any) => {
      this.loader.hide();
      this.toast.show('Please Try Again.');
    });
  }

  get f() {
    return this.profileForm.controls;
  }
  get c() {
    return this.changePassForm.controls;
  }

  updateMyDate($event, from: any) {
    if (from == 'dob') {
      this.minDate = (moment(this.profileForm.value.dob).add(1, 'days')).format('YYYY-MM-DD');
      this.maxDate = (moment()).format('YYYY-MM-DD');
      this.aniDateDisable = false;
      this.profileForm.value.anniversary = '';
    }
  }

  getPicture(value: any) {
    if (Camera['installed']()) {
      let options = this.storageProvider.getFromCamera(value);
      this.camera.getPicture(options).then((imageData) => {
        let path = imageData;
        this.storageProvider.checkOnlyImage(imageData).then((response: any) => {
          this.selectedImage = path;
          this.getImage = response;
          this.uploadImage();
        }).catch((error: any) => {

        });


        this.camera.cleanup().then((clean) => {
          console.log(clean);
        });
      }, (err) => {
        // Handle error
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  getProfileImageStyle() {
    let image = this.selectedImage;
    /* if(image == ""){
      image="assets/imgs/logo.png";
    } */
    return 'url(' + image + ')';
  }

  cancel() {
    this.selectedImage = "";
    this.profileForm.reset();
  }

  done() {
    let param = {
      salutation: this.profileForm.value.salutaion,
      first_name: this.profileForm.value.fname,
      last_name: this.profileForm.value.lname,
      user_id: localStorage.getItem('userId'),
      dob: this.profileForm.value.dob,
      anniversary_date: this.profileForm.value.anniversary
    };
    this.loader.show();
    this.profileApi.updateUserProfile(param).subscribe((response: any) => {
      if (response.status == '200') {
        this.getProfileInfo();
      }
      this.toast.show(response.msg);
      this.loader.hide();
    }, (error: any) => {
      this.toast.show('Please Try Again.');
      this.loader.hide();
    });
  }

  uploadImage() {
    const fileTransfer: FileTransferObject = this.transfer.create();
    const fileName = localStorage.getItem('userId') + '_' + new Date().getTime() + '.' + this.getImage.extention;
    const url = AppConst.baseUrl + 'update_user_api.json';
    const jsonData = {
      user_id: localStorage.getItem('userId'),
      salutation: this.profileForm.value.salutaion,
      first_name: this.profileForm.value.fname,
      last_name: this.profileForm.value.lname,
      dob: this.profileForm.value.dob,
      anniversary_date: this.profileForm.value.anniversary
    };
    let options: FileUploadOptions = {
      fileKey: 'myDoc',
      fileName: fileName,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      headers: {
        'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
        'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
      },
      params: jsonData
    };
    this.loader.show();
    fileTransfer.upload(this.selectedImage, url, options).then((data: any) => {
      const responseData = JSON.parse(data.response);
      this.toast.show(responseData.msg);
      if (responseData.status == '200') {
        this.getProfileInfo();
      }
      this.loader.hide();
    }).catch((err) => {
      console.log(err);
      this.loader.hide();
    });
  }

  openProfileVerificationModal(otpType) {
    const data = {
      type: otpType,
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      operation: 'send_otp',
      first_time: "true"
    };

    this.loader.show();
    this.profileApi.updateEmailMobile(data, 'E').subscribe((response: any) => {
      if(response.status == '200') {
        this.loader.hide();
  
        this.addModal = this.modalCtrl.create("ProfileVerificationPage", {dataParams: data});
        this.addModal.onDidDismiss((item: any) => {
          if(item == "success") {
            this.userInfo.is_verified_email = 1;
          }
        });
        this.addModal.present();
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  sendOtp(type: any, from: any) {
    let jSonData;
    if (type == "Email") {
      jSonData = {
        type: 'send_otp',
        user_id: localStorage.getItem('userId'),
        email: ''
      };
    } else if (type == "Mobile") {
      jSonData = {
        type: 'send_otp',
        user_id: localStorage.getItem('userId'),
        mobile: ''
      };
    }

    if (from == "verify") {
      if (type == "E") {
        jSonData = {
          type: 'email',
          user_id: localStorage.getItem('userId'),
          operation: 'send_otp'
        };
      } else if (type == "EM") {
        jSonData = {
          type: 'email',
          user_id: localStorage.getItem('userId'),
          operation: 'verify'
        };
      } else if (type == "MM") {
        jSonData = {
          type: 'mobile',
          user_id: localStorage.getItem('userId'),
          operation: 'verify'
        };
      } else {
        jSonData = {
          type: 'mobile',
          user_id: localStorage.getItem('userId'),
          operation: 'send_otp'
        };
      }
    }

    this.loader.show();
    this.profileApi.updateEmailMobile(jSonData, type).subscribe((response: any) => {
      this.loader.hide();
      if (type != "EM" && type != "MM") {
        this.showPromptForOtp(type, from);
      } else if (type == "EM" || type == "MM") {
        this.toast.show(response.msg);
        this.getProfileInfo();
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  showPromptForOtp(type: any, from: any) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Verify OTP');
    alert.addInput({
      type: 'number',
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Verify',
      handler: data => {
        this.verifyOtp(data, type, from);
      }
    });
    alert.present();
  }

  verifyOtp(data: any, type: any, from: any) {
    let jsonData = {
      device_id: '',
      user_id: localStorage.getItem('userId'),
      otp: data[0]
    };

    this.loader.show();
    this.Authapi.postVerifyOtp(jsonData).subscribe((response: any) => {
      if (response.status == '200') {
        if (type == "E" || type == "M") {
          let newType;
          (type == "E") ? newType = "EM" : newType = "MM";
          this.sendOtp(newType, from);
        } else {
          this.showPromptForValue(type, from);
        }
      } else {
        this.showPromptForOtp(type, from);
      }
      this.toast.show(response.msg);
    });
  }

  showPromptForValue(type: any, from: any) {
    let alert = this.alertCtrl.create();
    let title, inputType;
    if (type == 'Email') {
      title = 'Update Email';
      inputType = 'email';
    } else {
      title = 'Update Mobile';
      inputType = 'tel';
    }
    alert.setTitle(title);
    alert.addInput({
      type: inputType,
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Update',
      handler: data => {
        this.updateValue(type, from, data);
      }
    });
    alert.present();
  }

  updateValue(type: any, from: any, data: any) {
    let jsonData;
    if (type == 'Email') {
      jsonData = {
        type: 'update_email',
        user_id: localStorage.getItem('userId'),
        email: data[0]
      };
    } else {
      jsonData = {
        type: 'update_mobile',
        user_id: localStorage.getItem('userId'),
        mobile: data[0]
      };
    }

    this.loader.show();
    this.profileApi.updateEmailMobile(jsonData, type).subscribe((response: any) => {
      this.loader.hide();
      if (response.status != '200') {
        this.showPromptForValue(type, from);
      } else if (response.status == '200') {
        this.getProfileInfo();
      }
      this.toast.show(response.msg)
    }, (error: any) => {
      this.loader.hide();
    });
  }

  eyeOpenClose(type: any) {
    if (type == 'oldpassword') {
      this.openClose = !this.openClose;
    } else if (type == 'newpassword') {
      this.openClose2 = !this.openClose2;
    } else {
      this.openClose3 = !this.openClose3;
    }

  }

  changePassword() {
    const jsonData = {
      user_id: localStorage.getItem('userId'),
      old_pass: this.changePassForm.value.oldpassword,
      new_pass: this.changePassForm.value.newpassword,
      cnf_pass: this.changePassForm.value.confpassword
    };

    this.loader.show();
    this.profileApi.changePassword(jsonData).subscribe((response: any) => {
      if (response.status == '200') {
        this.changePassForm.value.oldpassword = '';
        this.changePassForm.value.newpassword = '';
        this.changePassForm.value.confpassword = '';
      }
      this.toast.show(response.msg);
      this.loader.hide();
    }, (error: any) => {
      this.loader.hide();
    });
  }

  // openDOBPicker(from: any) {
  //   let maximumDate = new Date((moment().subtract(18, 'years')).format('YYYY-MM-DD')).valueOf();
  //   let minimumDate: any;
  //   this.aniDateDisable = true;
  //   if (from == 'ani') {
  //     minimumDate = new Date((moment(this.profileForm.value.dob).add(10, 'years')).format('YYYY-MM-DD')).valueOf();
  //     maximumDate = new Date((moment()).format('YYYY-MM-DD')).valueOf();
  //     this.aniDateDisable = false;
  //   }
  //   this.datePicker.show({
  //     date: new Date(),
  //     mode: 'date',
  //     minDate: minimumDate,
  //     maxDate: maximumDate,
  //     androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
  //   }).then(
  //     (date: any) => {
  //       if (from == 'ani') {
  //         this.profileForm.value.anniversary = moment(date).format('YYYY-MM-DD');
  //       } else {
  //         this.profileForm.value.dob = moment(date).format('YYYY-MM-DD');
  //       }

  //       console.log('Got date: ', moment(date).format('YYYY-MM-DD'));
  //     },
  //     err => console.log('Error occurred while getting date: ', err)
  //   );
  // }

}
