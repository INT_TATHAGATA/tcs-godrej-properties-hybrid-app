import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfilePage } from './user-profile';
import { ComponentsModule } from '../../components/components.module';
import { MyProfileApiProvider } from '../../providers/apis/myprofile';
import { AuthProvider } from '../../providers/apis/auth';

@NgModule({
  declarations: [
    UserProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfilePage),
    ComponentsModule
  ],
  providers: [MyProfileApiProvider, AuthProvider],

})
export class UserProfilePageModule { }
