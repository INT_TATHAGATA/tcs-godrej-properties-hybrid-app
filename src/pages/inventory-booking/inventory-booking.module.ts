import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventoryBookingPage } from './inventory-booking';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
@NgModule({
  declarations: [
    InventoryBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryBookingPage),
    ComponentsModule
  ],
  providers: [ProjectDetailsApiProvider]
})
export class InventoryBookingPageModule { }
