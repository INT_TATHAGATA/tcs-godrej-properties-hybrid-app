import { Component, Input } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, Events, MenuController, AlertController } from 'ionic-angular';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';
import { DbProvider } from '../../providers/db/db';
import { Observable } from 'rxjs';

/**
 * Generated class for the InventoryBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inventory-booking',
  templateUrl: 'inventory-booking.html',
})
export class InventoryBookingPage {
  passedId = null;
  inventoryList: Array<any>;
  /* noti:boolean=true;
  heart:boolean=true;
  pin:boolean=true; */

  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = true;
  subHeadingText: string = "";
  formData: any;
  totalPage: any;
  projectimg: string;
  isfiter: boolean;
  fiterdata: any;
  extraData: any;
  public addModal: any;
  public initResult: boolean = false;
  public initData: any;

  public isShowBlankMSG = false;

  public totalSalesCosiderationCost: string = '0';

  public noDataMsg: string = "";

  public paymentPlat: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public storageProvider: DbProvider,
    private menu: MenuController,
    private alertCtrl: AlertController
  ) {
    this.menu.swipeEnable(false);
    this.formData = this.navParams.get('formData');
    localStorage.setItem('projectId', this.formData.project_id)
    this.projectimg = this.navParams.get('img');
    this.extraData = this.navParams.get('extdata');
    this.subHeadingText = this.extraData.pName;
    console.log(this.projectimg);
    this.inventoryList = [];
    this.totalPage = 0;
    this.isfiter = false;

    this.isShowBlankMSG = false;
  }

  ionViewWillEnter() {
    this.fetchInventoryData();
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad InventoryBookingPage');
  }

  fetchInventoryData() {
    this.inventoryList = [];

    this.isShowBlankMSG = false;

    this.initData = {
      project_id: this.formData.project_id,
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      typology: '',
      tower: '',
      facing: '',
      carpet_area: '',
      floor: '',
      min_price: '',
      max_price: '',
      page: 0
    };
    this.getprojectInventoryBookinglist();
  }

  getprojectInventoryBookinglist() {

    this.noDataMsg = "";
    if (this.inventoryList.length == 0) {
      this.loader.show();
    }
    /* this.projectdetailsApi.getinventorylist(this.formData).subscribe((response: any) => { */
    this.projectdetailsApi.getinventorysearch(this.initData).subscribe((response: any) => {
      this.isShowBlankMSG = true;
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.initResult = false;
        if (this.initData.page == '0') {
          this.inventoryList = response.list_data;
          if (this.inventoryList.length > 0) {
            this.noDataMsg = "";
          } else {
            this.noDataMsg = "All The Inventories Are Sold Out For Online Booking, Please Select Another Project.";
          }
          if (response.list_data.length == 0) {
            this.initResult = true;
          } else {
            this.initResult = false;
          }
        } else {
          this.inventoryList = this.inventoryList.concat(response.list_data);
        }
        this.totalPage = response.total_page;
      } else {
        if (this.inventoryList.length > 0) {
          this.noDataMsg = "";
        } else {
          this.inventoryList = [];
          this.initResult = true;
          this.noDataMsg = "All The Inventories Are Sold Out For Online Booking, Please Select Another Project.";
        }
      }
    }, (error: any) => {
      console.log(error);
      this.isShowBlankMSG = true;
      this.initResult = true;
      this.inventoryList = [];
      this.noDataMsg = "All The Inventories Are Sold Out For Online Booking, Please Select Another Project.";
      this.loader.hide();
    },
      () => {
        this.loader.hideNoTimeout();
      });
  }

  openFilter() {
    if (localStorage.getItem('Selectedinventoryfiterdata') == null) {
      this.loader.show('Please wait...');
      const data = {
        user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
        device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
        project_id: this.formData.project_id,
        tower: ''
      };
      this.projectdetailsApi.getinventoryfiterdata(data).subscribe((response: any) => {
        this.loader.hide();

        let addModal = this.modalCtrl.create("InventoryFilterPage", { responseData: response, projectId: this.formData.project_id });
        addModal.onDidDismiss(item => {

          if (item) {
            item["project_id"] = this.formData.project_id;
            this.inventoryList = [];
            this.fiterdata = item;
            this.isfiter = true;
            this.noDataMsg = "";
            this.getinventarysearch();
          }
        });
        addModal.present();

      }, (error: any) => {
        console.log(error);
        this.loader.hide();
      });
    } else {
      let addModal = this.modalCtrl.create("InventoryFilterPage");

      addModal.onDidDismiss(item => {

        if (item) {
          this.inventoryList = [];
          item["project_id"] = this.formData.project_id;
          this.fiterdata = item;
          this.isfiter = true;
          this.noDataMsg = "";
          this.getinventarysearch();
        }
      });
      addModal.present();
    }
  }

  getinventarysearch() {
    if (this.fiterdata.page == 0) {
      this.loader.show('Please wait...');
    }
    this.projectdetailsApi.getinventorysearch(this.fiterdata).subscribe((response: any) => {
      console.log(response);
      this.loader.hide();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        if (this.fiterdata.page == '0') {
          this.inventoryList = response.list_data;
          if (response.list_data.length == 0) {
            //this.initResult = true;
            this.noDataMsg = "No Inventories Found. Please Modify Your Search.";
          } else {
            this.noDataMsg = "";
          }
        } else {
          this.inventoryList = this.inventoryList.concat(response.list_data);
        }
        this.totalPage = response.total_page;
      } else {
        if (this.inventoryList.length == 0) {
          this.inventoryList = [];
          //this.initResult = true;
          this.noDataMsg = "No Inventories Found. Please Modify Your Search.";
        } else {
          this.noDataMsg = "";
        }
        this.totalPage = 0;
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    });
  }

  goToProjectDetails(value: any, inventory: any) {
    if (localStorage.getItem('userId') == null) {
      this.toast.show('Please Login/Register To View This Page.');
      this.addModal = this.modalCtrl.create("HomePage", {});
      this.addModal.onDidDismiss((item: any) => {
        if (item == 'loggedIn') {
          this.goToProjectDetails(value, inventory);
        }
      });
      localStorage.setItem('fromPage', 'ProjectDetailsPage');
      this.addModal.present();
    } else {
      const jsonData = {
        user_id: localStorage.getItem('userId'),
        inventory_id: inventory.nid
      };

      this.loader.show();
      Observable.forkJoin([
        this.getInventoryAvailablity(jsonData),
        this.getPaymentPlantStatus()
      ]).subscribe((result: any) => {
        if (result[0].status == AppConst.HTTP_SUCESS_STATUS.OK) {
          if (result[1].status == AppConst.HTTP_SUCESS_STATUS.OK) {
            if (result[1].data.curl_response.hasOwnProperty('errors')) {
              if (result[1].data.curl_response.errors.status == '1') {
                this.loader.hideNoTimeout();
                this.toast.show("Apologies For Inconvenience Caused. Please Try With Another Unit.");
                this.fetchInventoryData();
              } else {

                this.paymentPlat = result[1].data.curl_response;

                this.getCostSheetBreakupStatus(inventory).subscribe((result: any) => {
                  if (result.status == AppConst.HTTP_SUCESS_STATUS.OK) {
                    if (result.data.curl_response.hasOwnProperty('errors')) {
                      if (result.data.curl_response.errors.status == '1') {
                        this.loader.hideNoTimeout();
                        this.toast.show("Oops! Something Went Wrong. Try With Some Other Unit.");
                        this.fetchInventoryData();
                      } else {
                        this.totalSalesCosiderationCost = result.data.curl_response.project_price[0].value;

                        const data = {
                          pName: this.extraData.pName,
                          pImage: this.projectimg,
                          inv: inventory,
                          totalSalesConsiderationPrice: this.totalSalesCosiderationCost,
                          paymentPlan: this.paymentPlat
                        }
                        localStorage.removeItem('EnquiryFormData');
                        this.navCtrl.push(value, { details: data });
                      }
                    } else {
                      this.loader.hideNoTimeout();
                      this.toast.show("Oops! Something Went Wrong. Try With Some Other Unit.");
                      this.fetchInventoryData();
                    }
                  } else {
                    this.loader.hideNoTimeout();
                    this.toast.show("Oops! Something Went Wrong. Try With Some Other Unit.");
                    this.fetchInventoryData();
                  }
                }, (error) => {
                  console.log('Error:::', error);
                  this.loader.hideNoTimeout();
                  this.toast.show("Oops! Something Went Wrong. Try With Some Other Unit. ");
                  this.fetchInventoryData();
                });
              }
            } else {
              this.loader.hideNoTimeout();
              this.toast.show("Apologies For Inconvenience Caused. Please Try With Another Unit.");
              this.fetchInventoryData();
            }
          } else {
            this.loader.hideNoTimeout();
            this.toast.show("Apologies For Inconvenience Caused. Please Try With Another Unit.");
            this.fetchInventoryData();
          }
        } else {
          this.loader.hideNoTimeout();
          this.toast.show("Looks Like Unit Is Already Booked. Please Proceed With Any Other Unit. ");
          this.fetchInventoryData();
        }
      }, (error) => {
        console.log('Error:::', error);
        this.loader.hideNoTimeout();
        this.toast.show("Looks Like Unit Is Already Booked. Please Proceed With Any Other Unit. ");
        this.fetchInventoryData();
      },
        () => {
          /* this.loader.hideNoTimeout(); */
        });
    }
  }

  ionViewDidLeave() {
    this.loader.hideNoTimeout();
  }

  getInventoryAvailablity(jsonData) {
    return this.projectdetailsApi.getInventoryAvailable(jsonData);
  }

  getCostSheetBreakupStatus(inventory) {
    const data = {
      proj_id: localStorage.getItem('projectId'),
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      inventory_id: inventory.nid
    };
    return this.projectdetailsApi.getcostsheetbrkup(data);
  }

  getPaymentPlantStatus() {
    const jSonData = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      proj_id: localStorage.getItem('projectId'),
      device_id: this.storageProvider.getDeviceInfo().uuid
    };
    return this.projectdetailsApi.getPaymentPlan(jSonData);
  }

  getcostsheetbrkup(inventory) {
    return new Promise((resolve, reject) => {
      const data = {
        proj_id: localStorage.getItem('projectId'),
        user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
        device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
        inventory_id: inventory.nid
      };
      this.projectdetailsApi.getcostsheetbrkup(data).subscribe((response: any) => {
        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          if (response.data.curl_response.hasOwnProperty('errors')) {
            if (response.data.curl_response.errors.status == '1') {
              reject(false);
            } else {
              this.totalSalesCosiderationCost = response.data.curl_response.project_price[0].value;
              resolve(true);
            }
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      }, (error: any) => {
        this.loader.hide();
        reject(false);
      });
    })
  }

  getPaymentPlan() {
    return new Promise((resolve, reject) => {
      const userData = JSON.parse(localStorage.getItem('userData'));
      const jSonData = {
        user_id: userData.data.userid,
        proj_id: localStorage.getItem('projectId'),
        device_id: this.storageProvider.getDeviceInfo().uuid
      };
      this.projectdetailsApi.getPaymentPlan(jSonData).subscribe((response: any) => {
        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          if (response.data.curl_response.hasOwnProperty('errors')) {
            if (response.data.curl_response.errors.status == '1') {
              reject(false);
            } else {
              resolve(true);
            }
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      }, (error: any) => {
        this.loader.hide();
        reject(false);
      });
    })
  }

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  doInfinite(infiniteScroll) {
    if (!this.isfiter) {
      if (this.totalPage > 0 && this.initData.page < this.totalPage) {
        this.initData.page = this.initData.page + 1;

        this.getprojectInventoryBookinglist();
        setTimeout(() => {
          infiniteScroll.complete();
        }, 1000);
      } else {
        infiniteScroll.complete();
      }
    } else {
      if (this.totalPage > 0 && this.fiterdata.page < this.totalPage) {
        this.fiterdata.page = this.fiterdata.page + 1;

        this.getinventarysearch();
        setTimeout(() => {
          infiniteScroll.complete();
        }, 1000);
      } else {
        infiniteScroll.complete();
      }
    }

  }

  showInfoText() {
    let alert = this.alertCtrl.create({

      title: 'Price Info',
      message: 'Exclusive of Other Charges and All Govt. Taxes',
      buttons: [
        {
          text: 'GOT IT',
          handler: () => {
          }
        }
      ],
    });
    alert._cssClass = "inventory-info-alert";
    alert.present();
  }
}
