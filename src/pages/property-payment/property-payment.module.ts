import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertyPaymentPage } from './property-payment';
import { ComponentsModule } from '../../components/components.module';
import { MypropertypaymentProvider } from '../../providers/apis/mypropertypayment';

@NgModule({
  declarations: [
    PropertyPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(PropertyPaymentPage),
    ComponentsModule
  ], 
  providers: [
    MypropertypaymentProvider
  ]
})
export class PropertyPaymentPageModule { }
