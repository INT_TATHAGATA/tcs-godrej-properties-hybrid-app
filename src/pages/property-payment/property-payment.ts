import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events } from 'ionic-angular';
import { LoadingProvider } from '../../providers/utils/loader';
import { MypropertypaymentProvider } from '../../providers/apis/mypropertypayment';

/**
 * Generated class for the PropertyPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-property-payment',
  templateUrl: 'property-payment.html',
})
export class PropertyPaymentPage {

  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  mail: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = true;
  subHeadingText: string = "";
  //public projectName:any[]=[];
  public myHeadingList: any[] = [];
  public projectId: any;
  public welcomePlus: boolean = false;


  public applicantList: any = [];
  public propertyImg: string = "";
  public locationName: string = "";
  public projectName: string = "";
  public totalValue: string = "";
  public carpetArea: string = "";
  public plannedHandoverDate: string = "";

  public totalValueBilled: any;
  public amountReceived: any;
  public lastBilledAmount: any;
  public lastBilledDate: any;
  public lastPaymentReceivedAmount: any;
  public lastPaymentReceivedDate: any;
  public totalAmountOverdue: any;
  public interestPayableAsOnDate: any;
  public totalPayable: any;
  public totalPriceResponse:any;
  public initHide: boolean = false;
  public applicantListHeight: any;
  public bookingId: any;
  public responseDataReceive:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    public myPropertyPaymentProvider: MypropertypaymentProvider,
    private loader: LoadingProvider,
    public events: Events
  ) {
    this.bookingId = navParams.get('bookingId');
    this.subHeadingText = this.navParams.get('subHeadingText');
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(false);
    /* this.projectName=[
      {name:"The Trees / GTR2014",id:"1",selected:false},
      {name:"Godrej Emerland / GEM2014",id:"2",selected:false},
      {name:"Godrej Emerland / GEM2015",id:"3",selected:false},
    ] */
    //console.log('ionViewDidLoad PropertyPaymentPage');
    this.myHeadingList = [
      { name: "Overview", id: "1", selected: false },
      { name: "My Account", id: "2", selected: false },
      { name: "My Journey", id: "3", selected: false },
    ];
    this.selectProject(this.myHeadingList[0]);
  }

  selectProject(project: any) {
    let selectedProjectIndex = this.myHeadingList.findIndex(X => X.selected == true);
    if (selectedProjectIndex > -1) {
      this.myHeadingList[selectedProjectIndex].selected = false;
    }
    let activeProject = this.myHeadingList.findIndex(X => X.id == project.id);
    this.myHeadingList[activeProject].selected = true;
    this.projectId = project.id;
    console.log("Project ID : " + this.projectId);

    if (this.projectId === '1') {
      this.getProjectOverview();
    } else if (this.projectId === '2') {
      this.getMyAccount();
    } else {
      this.getMyJurney();
    }
  }

  getProjectOverview() {
    console.log("bookingId : " + this.bookingId);
    console.log("Project  : Overview");
    let data = {
      user_id: localStorage.getItem('userId'),
      booking_id: this.bookingId
    };
    this.loader.show();
    this.myPropertyPaymentProvider.getPropertyOverview(data).subscribe((response: any) => {
      console.log("Property Overview Response : " + JSON.stringify(response));
      this.loader.hide();
      if (response.status == '200') {
        (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          this.propertyImg = response.propertyImg;
        this.locationName = response.project_city;
        this.projectName = response.property_name;

        if(response.price != null && response.price != ''){
          if(response.price % 1 != 0){
            const tVBill=response.price.split('.');
            response['price_rupee']=tVBill[0];
            response['price_paisa']=tVBill[1];
          } else if(response.price % 1 == 0){
            response['price_rupee']="00";
            response['price_paisa']="00";
          }
        }

        this.totalPriceResponse=response;
        this.totalValue = response.price;
        this.carpetArea = response.carpet_area;
        this.plannedHandoverDate = response.handover_date;
        this.applicantList = response.applicant;
      }
    }, (err: any) => {
      this.loader.hide();
      console.log('Error:::', err);
    });
  }

  getMyAccount() {
    console.log("Project  : My Account"); let data = {
      user_id: localStorage.getItem('userId'),
      booking_id: this.bookingId
    };
    this.loader.show();
    this.myPropertyPaymentProvider.getMyAccount(data).subscribe((response: any) => {
      console.log("Property My Account Response : " + JSON.stringify(response));
      this.loader.hide();
      if (response.status == '200') {
        if(response.total_billed != null && response.total_billed != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.total_billed.split('.');
            response['total_billed_rupee']=tVBill[0];
            response['total_billed_paisa']=tVBill[1];
          } else if(response.total_billed % 1 == 0){
            response['total_billed_rupee']="00";
            response['total_billed_paisa']="00";
          }
        }

        if(response.last_billed.amount != null && response.last_billed.amount != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.last_billed.amount.split('.');
            response['last_billed_amount_rupee']=tVBill[0];
            response['last_billed_amount_paisa']=tVBill[1];
          } else if(response.last_billed.amount % 1 == 0){
            response['last_billed_amount_rupee']="00";
            response['last_billed_amount_paisa']="00";
          }
        }

        if(response.total_overdue != null && response.total_overdue != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.total_overdue.split('.');
            response['total_overdue_rupee']=tVBill[0];
            response['total_overdue_paisa']=tVBill[1];
          } else if(response.total_overdue % 1 == 0){
            response['total_overdue_rupee']="00";
            response['total_overdue_paisa']="00";
          }
        }


        //response.total_received
        if(response.total_received != null && response.total_received != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.total_received.split('.');
            response['total_received_rupee']=tVBill[0];
            response['total_received_paisa']=tVBill[1];
          } else if(response.total_received % 1 == 0){
            response['total_received_rupee']="00";
            response['total_received_paisa']="00";
          }
        }

        //response.last_payment_recieved.amount
        if(response.last_payment_recieved.amount != null && response.last_payment_recieved.amount != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.last_payment_recieved.amount.split('.');
            response['last_payment_recieved_amount_rupee']=tVBill[0];
            response['last_payment_recieved_amount_paisa']=tVBill[1];
          } else if(response.last_payment_recieved.amount % 1 == 0){
            response['last_payment_recieved_amount_rupee']="00";
            response['last_payment_recieved_amount_paisa']="00";
          }
        }

        //response.total_overdue
        if(response.total_overdue != null && response.total_overdue != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.total_overdue.split('.');
            response['total_overdue_rupee']=tVBill[0];
            response['total_overdue_paisa']=tVBill[1];
          } else if(response.total_overdue % 1 == 0){
            response['total_overdue_rupee']="00";
            response['total_overdue_paisa']="00";
          }
        }
        //response.total_payable
        if(response.total_payable != null && response.total_payable != ''){
          if(response.total_billed % 1 != 0){
            const tVBill=response.total_payable.split('.');
            response['total_payable_rupee']=tVBill[0];
            response['total_payable_paisa']=tVBill[1];
          } else if(response.total_payable % 1 == 0){
            response['total_payable_rupee']="00";
            response['total_payable_paisa']="00";
          }
        }


        this.responseDataReceive=response;
        this.totalValueBilled = response.total_billed;
        this.amountReceived = response.total_received;
        this.lastBilledAmount = response.last_billed.amount;
        this.lastBilledDate = response.last_billed.date;
        this.lastPaymentReceivedAmount = response.last_payment_recieved.amount;
        this.lastPaymentReceivedDate = response.last_payment_recieved.date;
        
        this.totalAmountOverdue = response.total_overdue;
        this.interestPayableAsOnDate = response.total_pay;
        this.totalPayable = response.total_payable;
      }
    }, (err: any) => {
      this.loader.hide();
      console.log('Error:::', err);
    });
  }

  getMyJurney() {
    console.log("Project  : My Jurney");
  }

  openCloseAccordian(section: any, type: any) {

    if (section == 'welcomePlus') {
      (type == 'S') ? this.welcomePlus = true : this.welcomePlus = false;
    }
  }

  ordinalSuffixOf(index: any) {
    if (index != null && index > -1) {
      let changeIndex = index + 1;
      let j = changeIndex % 10;
      if (j == 1) {
        return changeIndex + "st";
      }
      if (j == 2) {
        return changeIndex + "nd";
      }
      if (j == 3) {
        return changeIndex + "rd";
      }
      return changeIndex + "th";
    }
  }

  openClose() {
    this.initHide = !this.initHide;
    if (this.initHide) {
      this.applicantListHeight = 350;
    } else {
      this.applicantListHeight = 235;
    }
  }

  goToPage(pageName: any, rmData: any) {
    if (pageName == "My Documents") {
      const page = {
        title: 'My Documents', component: 'MyDocumentsListPage'
      };
      this.events.publish('afterLoginRedirect', page)
    }
  }

}
