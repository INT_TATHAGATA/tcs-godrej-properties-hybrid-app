import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventoryDetailsPage } from './inventory-details';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    InventoryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryDetailsPage),
    ComponentsModule,
  ],
})
export class InventoryDetailsPageModule {}
