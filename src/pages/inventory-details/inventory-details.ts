import { Component, OnInit, ViewChild, Renderer, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { LoadingProvider } from '../../providers/utils/loader';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer';
import { ToasterProvider } from '../../providers/utils/toast';
import { File, FileEntry } from '@ionic-native/file';
import { AppConst } from '../../app/app.constants';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePipe } from '@angular/common';

/**
 * Generated class for the InventoryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inventory-details',

  templateUrl: 'inventory-details.html',
})
export class InventoryDetailsPage implements OnInit {
  parameter: any;
  isOpen: boolean = true;
  salesConci: boolean = false;
  otherChar: boolean = false;
  govtChar: boolean = false;
  coupon: boolean = false;
  couponCode: any = { code: '' };
  offer: any = false;
  projectdetails: any;
  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = false;
  subHeadingText: string = "";

  /* TEST */
  accordionExapanded: boolean = true;
  @ViewChild("cc") cardContent: any;
  icon: string = "ios-arrow-up-outline";

  /* TEST */

  public termsData: any;
  public footerAccordian: any = {
    terms: false
  };
  public temsCheck: boolean = true;
  public termsError: any;
  public isDisabledProceedBtn = true;

  public isDisableDownloadButton: boolean = false;

  public count = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    public storageProvider: DbProvider,
    private menu: MenuController,
    private loader: LoadingProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private transfer: FileTransfer,
    private toast: ToasterProvider,
    public file: File,
    private alertCtrl: AlertController,
    private fileOpener: FileOpener,
    private datePipe: DatePipe
  ) {
    this.menu.swipeEnable(false);
    this.loader.hide();

    this.parameter = this.navParams.get('details');
    this.parameter.data.curl_response.project_price.map((eachtype) => {
      eachtype['salesConci'] = false;
      if (!eachtype.hasOwnProperty('breakups')) {
        eachtype['breakups'] = [];
      }
    });
    this.projectdetails = this.navParams.get('extradata');

    /* this.getTermsData(); */
  }

  ionViewDidLoad() {
    console.log(this.parameter);
    console.log(this.projectdetails);
    console.log('ionViewDidLoad InventoryDetailsPage');

  }

  ngOnInit() {
    this.renderer.setElementStyle(this.cardContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
  }

  getCollapse(value: any) {

    if (value == "pd") {
      this.isOpen = !this.isOpen;
    } else if (value == "coupon") {
      this.coupon = !this.coupon;
    } else if (value == "offer") {
      this.offer = !this.offer;
    }
  }

  getCollapse2(value: any, index: any) {
    value = !value;
    this.parameter.data.curl_response.project_price[index].salesConci = value;
  }

  apply() {
    console.log(this.couponCode);
  }

  proceedBooking() {
    /* if (this.temsCheck) { */
      //this.navCtrl.push("ApplicationFormPage", { projectDetails: this.projectdetails, parameter: this.parameter, formData: this.navParams.get('formData'), imgObj: this.navParams.get('imgObj') });
      this.navCtrl.push("ApplicationFormPage", { projectDetails: this.projectdetails, parameter: this.parameter, imageDataApplicantArr: this.navParams.get('imageDataApplicantArr') });
      this.temsCheck = false;
      this.loader.hide();
    /* } else {
      console.log("Here..Not");
      this.termsError = true;
    } */
  }

  toggleAccordion() {
    if (this.accordionExapanded) {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 16px");
    } else {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "500px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "13px 16px");
    }
    this.accordionExapanded = !this.accordionExapanded;
    this.icon = this.icon == "ios-arrow-up-outline" ? "ios-arrow-down-outline" : "ios-arrow-up-outline";
  }

  toggleFooter(from) {
    if (from == 'termsfooter') {
      this.footerAccordian = {
        terms: true,
      }
    } else {
      this.footerAccordian = {
        terms: (this.footerAccordian.terms) ? false : true
      }
    }
  }

  checkboxTerms() {
    this.temsCheck = !this.temsCheck;
    this.isDisabledProceedBtn = !this.isDisabledProceedBtn;
    (this.temsCheck) ? this.termsError = false : this.termsError = true;
  }

  getTermsData() {
    this.loader.show();
    this.projectdetailsApi.getCostSheetTermsData().subscribe((result: any) => {
      this.termsData = result[0].page_content;
      setTimeout(() => {
        this.loader.hide();
      }, 850);
    }, (error) => {
      this.loader.hide();
      console.log('Error:::', error);
    });
  }

  downloadCostSheet() {
    const fileTransfer: FileTransferObject = this.transfer.create();
    this.storageProvider.getpermitionAndroid().then((entry) => {
      console.log("Permition Android: " + entry);
      this.isDisableDownloadButton = true;
      if (this.count == 0) {
        this.loader.show();
      }

      /* let options = {
        headers: {
          'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
          'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
        }
      } */

      const downloadFileURL = encodeURI(AppConst.baseUrl + "get_costsheet_download/" + this.projectdetails.inv.nid);
      let date = new Date();
      const fileName = "GPL/" + this.projectdetails.inv.unit_no + this.datePipe.transform(date, "yyyyMMddhhmmss") + "_CostSheet.pdf";
      fileTransfer.download(downloadFileURL, this.file.externalRootDirectory + fileName, true).then((entry) => {
        setTimeout(() => {
          this.loader.hide();
          this.isDisableDownloadButton = false;
          // this.toast.show("File Downloaded in " + entry.filesystem.name + entry.fullPath);
          //this.openUserOption(entry.filesystem.name + entry.fullPath);
          this.fileOpener.open(entry.filesystem.name + entry.fullPath, 'application/pdf')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
          this.count++;
        }, 850);
      }, (error: any) => {
        if (this.count == 1) {
          this.count = 0;
          this.downloadCostSheet();
        } else {
          setTimeout(() => {
            this.loader.hide();
            this.isDisableDownloadButton = false;
            console.log("Cost Sheet Download Error : " + JSON.stringify(error));
            this.toast.show("Failed To Download Cost Sheet.");
          }, 200);
        }
      });
    }, (error: any) => {
      //this.toast.show('Storage Permission is needed to download the file');
    });

    /*  fileTransfer.onProgress((progress: any) => {
       //setTimeout(() => {
       const downProgress = ((progress.loaded / progress.total) * 100).toFixed(2);
       this.docList[index].width = parseInt(downProgress);
       if (this.docList[index].width == 100) {
         this.docList[index].width = 0;
       }
       //}, 10);
     }); */
  }

  openUserOption(filePath) {
    let alert = this.alertCtrl.create({
      /*  title: 'Cost Sheet Download', */
      message: "Click on 'Open' to view the downloaded Cost Sheet.",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Open',
          handler: () => {
            this.fileOpener.open(filePath, 'application/pdf')
              .then(() => console.log('File is opened'))
              .catch(e => console.log('Error opening file', e));
          }
        }
      ]
    });
    alert.present();
  }

}
