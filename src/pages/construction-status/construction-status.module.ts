import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConstructionStatusPage } from './construction-status';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ConstructionStatusPage,
  ],
  imports: [
    IonicPageModule.forChild(ConstructionStatusPage),
    ComponentsModule
  ],
})
export class ConstructionStatusPageModule {}
