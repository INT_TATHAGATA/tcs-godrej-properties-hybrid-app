import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConstructionStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-construction-status',
  templateUrl: 'construction-status.html',
})
export class ConstructionStatusPage {

  noti:boolean=true;
  heart:boolean=true;
  pin:boolean=true
  menuIcon:boolean=false;
  image:boolean=false;
  text:boolean=true;
  public showDropdown:boolean=false;
  public event:any={
    timeStarts:""
  };
  public project:any={
    name:""
  };
  public showBigImage:boolean=false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConstructionStatusPage');
  }

  openDropdown(){
    this.showDropdown= !this.showDropdown;
  }

  dateChanged(){
    setTimeout(()=>{
      console.log(this.event.timeStarts);
    },1000);
  }

  getProjectName(){
    setTimeout(()=>{
      console.log(this.project);
    },1000);
  }
  openImage(value:any){
    (value == "S") ? this.showBigImage = true : this.showBigImage = false;
  }
}
