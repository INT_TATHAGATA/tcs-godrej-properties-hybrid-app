import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController, Events, Platform, App } from 'ionic-angular';
import { Network } from '@ionic-native/network';
/**
 * Generated class for the NoInternetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-no-internet',
  templateUrl: 'no-internet.html',
})
export class NoInternetPage {

  public wifiColor:boolean=false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private network: Network,
    public events: Events,
    public platform: Platform,
    public app: App,
    ) {
      events.subscribe('dismissModal', () => {
        this.wifiColor=true;
        setTimeout(() => {
          this.tryAgain();
        }, 3000);

      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoInternetPage');
    /* let overLays = this.app.getActiveNavs();
    overLays[0]._views[0].data.opts.enableBackdropDismiss=false; */
  }
  tryAgain(){
    if(this.network.type != "none" && this.network.type!="2g"){
      this.viewCtrl.dismiss();
    }
  }

}
