import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, Events, Nav, AlertController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { LoadingProvider } from '../../providers/utils/loader';
import { GodrejHomeApiProvider } from '../../providers/apis/godrejhome';
import { AppConst } from '../../app/app.constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { ToasterProvider } from '../../providers/utils/toast';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';
import { Slides } from 'ionic-angular'
import moment from 'moment';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
/**
 * Generated class for the GodrejHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-godrej-home',
  templateUrl: 'godrej-home.html'
})
export class GodrejHomePage {
  lat: any;
  long: any;
  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  mail: boolean = false;
  menuIcon: boolean = true;
  image: boolean = true;
  text: boolean = false;
  public searchListArr: any = [];
  public addModal: any;

  RecommendForU: Array<any> = [];
  resentSerchproperty: Array<any> = [];
  public eventsArr: any = [];

  public isGuest: any;
  public userName: any;
  public searchTime: any;

  @ViewChild(Slides) slides: Slides;
  @ViewChild('Slides2') slides2: Slides;
  @ViewChild('RecomendedSlides') RecomendedSlides: Slides;
  public loop: boolean = false;
  public eventsOffers: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    public storageProvider: DbProvider,
    public modalCtrl: ModalController,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    private GodrejHomneApi: GodrejHomeApiProvider,
    private wishlistApi: WishListApiProvider,
    public events: Events,
    private screenOrientation: ScreenOrientation,
    private alertCtrl: AlertController
  ) {

    /*  this.RecommendForU = [];
     this.resentSerchproperty = [];
     this.eventsArr = []; */
    this.lat = '';
    this.long = '';

    this.events.subscribe('deepLinkDirect', (response: any) => {
      this.navCtrl.push('ProjectDetailsPage', { fullData: { proj_id: response } });
    });

    this.events.subscribe('locationPermissionReject', () => {
      this.getrecentSearchProject();
    });

    this.events.subscribe('locationPermissionResolve', (res: any) => {
      if (this.lat == '' && this.lat == '') {
        if (res.coords.latitude && res.coords.longitude) {
          this.lat = res.coords.latitude;
          this.long = res.coords.longitude;
        } else {
          this.lat = '';
          this.long = '';
        }
        this.getrecentSearchProject();
      }
    });
  }

  ionViewWillEnter() {
    this.slides.autoplayDisableOnInteraction = false;
    this.isGuest = this.storageProvider.isGuest();
    if (this.isGuest != '1') {
      const userDetails = JSON.parse(localStorage.getItem('userData'));
      this.userName = userDetails.data.first_name;
    }

    const projectDetails_P_ID = localStorage.getItem("ProjectDetailsWishlist_P_ID");
    const projectDetailsWishlist_P_Wishlist = localStorage.getItem("ProjectDetailsWishlist_P_Wishlist");
    if (this.resentSerchproperty.length > 0) {
      this.resentSerchproperty.map((item: any) => {
        if (projectDetails_P_ID != null && projectDetails_P_ID != ""
          && projectDetails_P_ID == item.nid
          && projectDetailsWishlist_P_Wishlist != null) {
          item.RecommendForU = projectDetailsWishlist_P_Wishlist;
        }
      });
    }
    if (this.RecommendForU.length > 0) {
      this.resentSerchproperty.map((item: any) => {
        if (projectDetails_P_ID != null && projectDetails_P_ID != ""
          && projectDetails_P_ID == item.nid
          && projectDetailsWishlist_P_Wishlist != null) {
          item.wishlist_status = projectDetailsWishlist_P_Wishlist;
        }
      });
    }

    /* this.getrecentSearchProject(); */

    this.storageProvider.getLocation().then((res: any) => {
      console.log("Get location Try : " + JSON.stringify(res));
      if (res.coords.latitude && res.coords.longitude) {
        this.lat = res.coords.latitude;
        this.long = res.coords.longitude;
        this.getrecentSearchProject();
      } else {
        this.getrecentSearchProject();
      }
    }).catch((err) => {
      console.log("Get location Catch : " + JSON.stringify(err));
      this.getrecentSearchProject();
    });

    this.screenOrientation.onChange().subscribe((data: any) => {
      this.RecommendForU = [];
      this.resentSerchproperty = [];
      this.eventsArr = [];
      this.loader.show();
      this.getrecentSearchProject();
    });
  }

  ionViewWillLeave() {
    this.slides.autoplayDisableOnInteraction = false;
    this.slides.stopAutoplay();
    /* this.resentSerchproperty = []; */
  }

  /* allApiGetInt() {
    Observable.forkJoin([
      this.getRecommededProject(),
      this.getrecentSearchProject()
    ]).subscribe((result: any) => {

      if (result[0].status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.RecommendForU = result[0].data;
        localStorage.setItem('recomendedProjects', JSON.stringify(result[0].data));
        this.RecommendForU.map((item: any) => {
          if (item.typologies.split(',').length > 2) {
            item['typologies'] = item.typologies.split(',').slice(1, 3).toString();
          }
          if (item.min_price != null && item.min_price) {
            if (item.min_price == "0.00" || item.min_price == "0") {
              item.min_price = "On Request";
            } else {
              const getCurrency = this.storageProvider.getCurrency(item.min_price);
              item.min_price = getCurrency;
            }
          }
        });
      }

      if (result[1].status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.resentSerchproperty = result[1].data;
        localStorage.setItem('recentProjects', JSON.stringify(result[1].data));
        if (this.resentSerchproperty.length > 1) {
          this.loop = true;
          this.slides.startAutoplay();
          this.slides.update();
        }

        this.slideChanged();
      }

      setTimeout(() => {
        this.eventsArr = [{
          imgs: 'assets/imgs/godrej-offer-src.jpg',
        }];
      }, 3000);
      this.loader.hide();

    }, (error) => {
      this.loader.hide();
      console.log('Error:::', error);
    },
      () => {
        console.log("complete callback");
        this.loader.hide();
      });
  } */

  /* afterInitFunction(showLoader) {
    this.searchListArr = [];
    this.menu.swipeEnable(true);
    if (showLoader) {
      this.loader.show();
    } else {
      this.RecommendForU = JSON.parse(localStorage.getItem('recomendedProjects'));
      this.resentSerchproperty = JSON.parse(localStorage.getItem('recentProjects'));
      setTimeout(() => {
        this.eventsArr = [{
          imgs: 'assets/imgs/godrej-offer-src.jpg',
        }];
      }, 3000);
    }

    this.storageProvider.getLocation().then((res: any) => {

      if (res.coords.latitude && res.coords.longitude) {
        this.lat = res.coords.latitude;
        this.long = res.coords.longitude;
        this.allApiGetInt();
      } else {
        this.allApiGetInt();
      }
    }).catch((err) => {
      this.allApiGetInt();
      console.log(err);
    });
  } */

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  addRmWishList(item) {
    const data = {
      user_id: localStorage.getItem('userId'),
      proj_id: item.proj_id,
      type: item.wishlist_status == 0 ? 'add' : 'delete'
    }
    this.loader.show();
    this.wishlistApi.modifyWishlist(data).subscribe((response: any) => {
      this.loader.hideNoTimeout();
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.events.publish('showNotiDot');
        this.RecommendForU.map((data) => {
          if (data.proj_id == item.proj_id) {
            data.wishlist_status = (data.wishlist_status == '0') ? 1 : 0;
          }
        });
        this.resentSerchproperty.map((data) => {
          if (data.proj_id == item.proj_id) {
            data.wishlist_status = (data.wishlist_status == '0') ? 1 : 0;
          }
        });
        this.toast.show(response.msg);
      } else {
        this.toast.show(response.msg);
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hideNoTimeout();
    });
  }

  openSearch(value: any) {
    if (value != "ND") {
      let params = {};
      if (this.searchListArr.length == 0) {
        localStorage.setItem("fromDashboard", "N")
        this.storageProvider.setValue(this.searchListArr);
        params = {
          type: "C",// R => Residential C => Commertial
          location: "Kolkata",
          cityLocality: this.storageProvider.getCityList(),
          possession: this.storageProvider.getPossessionList(),
          typology: this.storageProvider.getTypologyList(),
          price: this.storageProvider.getPrice()
        };
      } else {
        params = this.searchListArr;
      }
      this.addModal = this.modalCtrl.create("SearchPage", { params: params });
    } else {
      localStorage.setItem("fromDashboard", "Y")
      this.addModal = this.modalCtrl.create("SearchPage", {});
    }

    this.addModal.onDidDismiss((item: any) => {

      if (item) {
        let pageName = this.navCtrl.getActive();
        if (pageName.name == "ProjectDetailsPage") {
          this.navCtrl.pop();
        }

        this.searchListArr = item;
      }
    });
    this.addModal.present();
  }

  goToPage(pageName: any, project: any, type: any, indiaPhNo: any, otherPhNo: any) {
    if (type == "pageRedirect") {
      if (pageName == "GetInTouchPage") {
        const data = {
          projectId: project.proj_id,
          projectName: project.proj_name,
          userName: localStorage.getItem('userData'),
          userId: localStorage.getItem('userId'),
          indiaPhNo: indiaPhNo,
          otherPhNo: otherPhNo
        };
        this.navCtrl.push(pageName, { formData: data });
      } else {
        this.navCtrl.push(pageName, { fullData: project });
      }
    } else if (type == "share") {
      this.share(project);
    } else if (type == "wishList") {

      if (localStorage.getItem('isGuest') == '1') {
        this.toast.show('Please Login/Register To View This Page.');
        this.addModal = this.modalCtrl.create("HomePage", {});
        this.addModal.onDidDismiss((item: any) => {
          if (item == 'loggedIn') {
            this.isGuest = this.storageProvider.isGuest();
            this.events.publish('guestToUser');

            this.addRmWishList(project);
          }
        });
        localStorage.setItem('fromPage', 'ProjectDetailsPage');
        this.addModal.present();
      } else {
        this.addRmWishList(project);
      }
    }
  }

  reDirectTo(pageName: any, project: any, type: any, indiaPhNo: any, otherPhNo: any) {
    if (project.field_is_available_for_booking != null && project.field_is_available_for_booking == '0') {
      let alert = this.alertCtrl.create({
        title: 'Oops!',
        message: 'Online booking for this project is currently unavailable.',
        buttons: [
          {
            text: 'Ok',
            handler: () => {
            }
          }
        ],
        enableBackdropDismiss: false
      });
      alert.present();
    } else {
      localStorage.removeItem('Selectedinventoryfiterdata');
      const data = {
        project_id: project.proj_id,
        user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
        device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
        page: 0
      };
      const extradata = {
        pName: project.proj_name
      };
      if (localStorage.getItem('isGuest') == '1') {
        this.toast.show('Please Login/Register To View This Page.');
        this.addModal = this.modalCtrl.create("HomePage", {});
        this.addModal.onDidDismiss((item: any) => {
          if (item == 'loggedIn') {
            this.isGuest = this.storageProvider.isGuest();
            this.events.publish('guestToUser');
            if (project.eoi_enabled == '1') {
              this.navCtrl.push('EoiPage', { formData: data, img: project.proj_img != '' ? project.proj_img : '', extdata: extradata });
            } else {
              this.navCtrl.push(pageName, { formData: data, img: project.proj_img != '' ? project.proj_img : '', extdata: extradata });
            }
          }
        });
        localStorage.setItem('fromPage', 'ProjectDetailsPage');
        this.addModal.present();
      } else {
        if (project.eoi_enabled == '1') {
          this.navCtrl.push('EoiPage', { formData: data, img: project.proj_img != '' ? project.proj_img : '', extdata: extradata });
        } else {
          this.navCtrl.push(pageName, { formData: data, img: project.proj_img != '' ? project.proj_img : '', extdata: extradata });
        }
      }
    }

  }

  share(item?) {
    this.loader.show('Please wait...');
    const url = item.url;
    this.storageProvider.socialShare(item.proj_name, "", item.proj_img, url);
  }

  getrecentSearchProject() {

    if (this.resentSerchproperty.length == 0) {
      this.loader.show();
    }

    const data = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : ''
    };
    /*  return this.GodrejHomneApi.recentSearchProject(data); */
    this.GodrejHomneApi.recentSearchProject(data).subscribe((result: any) => {
      if (result.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.resentSerchproperty = result.data;

        if (this.resentSerchproperty.length > 1) {
          this.loop = true;
          try {
            this.slides.startAutoplay();
            this.slides.update();
          } catch (exception) {
            console.log("Recent Search Slider Error : " + JSON.stringify(exception));
          }
        }
        this.slideChanged();
      }
    }, (error) => {
      this.loader.hideNoTimeout();
      console.log('Error:::', error);
    },
      () => {
        this.getRecommededProject();
      });
  }

  getRecommededProject() {
    const data = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      lat: this.lat,
      lng: this.long
    }
    /* return this.GodrejHomneApi.RecommededProject(data); */
    this.GodrejHomneApi.RecommededProject(data).subscribe((result: any) => {
      if (result.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.RecommendForU = result.data;

        this.RecommendForU.map((item: any) => {
          if (item.typologies.split(',').length > 2) {
            item['typologies'] = item.typologies.split(',').slice(1, 3).toString();
          }
          if (item.min_price != null && item.min_price) {
            if (item.min_price == "0.00" || item.min_price == "0") {
              item.min_price = "On Request";
            } else {
              const getCurrency = this.storageProvider.getCurrency(item.min_price);
              item.min_price = getCurrency;
            }
          }
        });
      }
    }, (error) => {
      this.loader.hideNoTimeout();
      console.log('Error:::', error);
    },
      () => {
        this.getEventsProject();
      });
  }

  getEventsProject() {
    setTimeout(() => {
      this.eventsArr = [{
        imgs: 'assets/imgs/godrej-offer-src.jpg',
      }];
    }, 100);
    this.loader.hideNoTimeout();
  }

  slideChanged() {
    this.slides.autoplayDisableOnInteraction = false;
    let currentIndex = this.slides.realIndex;
    if (typeof currentIndex != 'undefined') {
      this.searchTime = moment(this.resentSerchproperty[currentIndex].last_viewd).startOf('second').fromNow();
    } else {
      if (this.resentSerchproperty != null && this.resentSerchproperty.length > 0) {
        this.searchTime = moment(this.resentSerchproperty[0].last_viewd).startOf('second').fromNow();
      }
    }
    this.slides.startAutoplay();
  }
}
