import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GodrejHomePage } from './godrej-home';
import { ComponentsModule } from '../../components/components.module';
import { GodrejHomeApiProvider } from '../../providers/apis/godrejhome';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';

@NgModule({
  declarations: [
    GodrejHomePage,
  ],
  imports: [
    IonicPageModule.forChild(GodrejHomePage),
    ComponentsModule
  ],
  providers: [GodrejHomeApiProvider, WishListApiProvider]
})
export class GodrejHomePageModule { }
