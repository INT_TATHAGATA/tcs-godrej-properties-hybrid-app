import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

/**
 * Generated class for the KnowMePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-know-me',
  templateUrl: 'know-me.html',
})
export class KnowMePage {

  public knowMeForm: FormGroup;
  public epattern = new RegExp("[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}");
  public onlyAlphabet = new RegExp("^[a-zA-Z ]+$");
  public onlyNumber = new RegExp("^[0-9]+$");
  public gender:any={
    male:true,
    female:false
  };
  public status:any={
    single:true,
    married:false
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    private menu: MenuController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KnowMePage');
  }
  ngOnInit(){
    this.menu.swipeEnable(false);
    this.knowMeForm = this.fb.group({
      fullName:new FormControl('',[
        Validators.required,
        Validators.pattern(this.onlyAlphabet)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.epattern)
      ]),
      mobile:new FormControl('',[
        Validators.required,
        Validators.pattern(this.onlyNumber),
        /* Validators.minLength(10),
        Validators.maxLength(10) */
      ]),
      hobby:new FormControl('',[
        Validators.required
      ]),
      leisureActivity:new FormControl('',[
        Validators.required
      ]),
      nOfKids:new FormControl('',[]),
      school:new FormControl('',[]),
      familyIncome:new FormControl('',[]),
      favHangout: new FormControl('', [])
    });
  }

  get f() {
    return this.knowMeForm.controls;
  }

  checkBox(type:any,value:any){
    if(type=="Gender"){
      (value == "M") ? this.gender={
        male:true,
        female:false
      } : this.gender={
        male:false,
        female:true
      }
    } else if(type == "mStatus"){
      (value == "S") ? this.status={
        single:true,
        married:false
      } : this.status={
          single:false,
          married:true
        };
    }

  }
  submit(){
    let formValue=this.knowMeForm.value;
    (this.gender.male) ? formValue["gender"]="M" : formValue["gender"]="F";
    (this.status.single) ? formValue["status"]="S" : formValue["status"]="M";
    console.log(formValue);
  }
  goToLogin(){
    this.navCtrl.setRoot('HomePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}
