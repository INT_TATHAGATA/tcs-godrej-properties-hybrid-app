import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KnowMePage } from './know-me';

@NgModule({
  declarations: [
    KnowMePage,
  ],
  imports: [
    IonicPageModule.forChild(KnowMePage),
  ],
})
export class KnowMePageModule {}
