import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleVisitPage } from './schedule-visit';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ScheduleVisitPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleVisitPage),
    ComponentsModule
  ],
})
export class ScheduleVisitPageModule {}
