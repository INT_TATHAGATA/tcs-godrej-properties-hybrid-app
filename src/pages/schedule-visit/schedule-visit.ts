import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

/**
 * Generated class for the ScheduleVisitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule-visit',
  templateUrl: 'schedule-visit.html',
})
export class ScheduleVisitPage implements OnInit  {

  noti:boolean=true;
  heart:boolean=true;
  pin:boolean=true
  menuIcon:boolean=false;
  image:boolean=false;
  text:boolean=true;
  public scheduleVisitForm: FormGroup;
  public minDate:any;
  public maxDate:any;
  public isReadyToSubmit:boolean=false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder
    ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScheduleVisitPage');
  }

  ngOnInit() {
    /* Set Date Minimum of tomorrow to maximum of same date next year */
    let day = new Date();
    let nextDay = new Date(day);
    nextDay.setDate(day.getDate()+1);
    this.minDate = nextDay.toISOString();
    let nextYear=nextDay.setFullYear(nextDay.getFullYear() + 1);;
    this.maxDate= new Date(nextYear).toISOString();
    /* Set Date Minimum of tomorrow to maximum of same date next year */

    this.scheduleVisitForm = this.fb.group({
      property:new FormControl('',[
        Validators.required
      ]),
      purpose:new FormControl('',[
        Validators.required
      ]),
      datetime:new FormControl('',[
        Validators.required
      ]),
      persons: new FormControl('', [
        Validators.required
      ]),
      comment: new FormControl('', [])
    });

    this.scheduleVisitForm.valueChanges.subscribe((v) => {
      console.log(this.scheduleVisitForm)
      this.isReadyToSubmit = this.scheduleVisitForm.valid;
    });
  }

  get f() {
    return this.scheduleVisitForm.controls;
  }

  submitVisit(){
    console.log(this.scheduleVisitForm.value);
  }

}
