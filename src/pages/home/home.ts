import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events, ViewController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
//import { DashboardPage } from '../dashboard/dashboard';
import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/apis/auth';
import { LoginApiresponse } from '../../models/auth';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { Device } from '@ionic-native/device';
import moment from 'moment';
import { DatePicker } from '@ionic-native/date-picker';
import { DatePipe } from '@angular/common';
import { AppVersion } from '@ionic-native/app-version';
import { AppConst } from '../../app/app.constants';

//import { SignupPage } from '../signup/signup';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {

  //form: FormGroup;
  loginForm: FormGroup;
  isReadyToLogin: boolean = true;
  validation_messages: any;

  public type: any = {
    email: true,
    mobile: false,
    pan: false
  };
  public openClose: boolean = false;

  @ViewChild('inputRef') inputRef;

  @ViewChild('mobile') mobileRef;
  @ViewChild('email') emailRef;

  public onlyNumberAllowed: boolean = false;
  public validEmail: boolean = false;
  public panNumber: boolean = false;
  public deviceinfo: any;
  public invalid: boolean = true;
  public previousPage: any;
  public isGuest: any;

  public isCountryFieldShow: boolean = false;
  public isEmailFieldShow: boolean = true;

  public countryCodes: any = [];
  public flag: any;
  public findIndia: any;
  public addModal: any;
  public countryInitials: any;
  public validLength: boolean = false;
  public copyRightDate;

  public buildType: string = "";
  public appVersionNumber: any = "";
  public typeText: string = "password";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    private menu: MenuController,
    public storageProvider: DbProvider,
    public events: Events,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public viewCtrl: ViewController,
    private device: Device,
    public modalCtrl: ModalController,
    private datePicker: DatePicker,
    public datepipe: DatePipe,
    private appVersion: AppVersion,
  ) {

    this.loader.hide();

    this.isGuest = localStorage.getItem('isGuest');
    this.events.subscribe('updateDeviceToken', () => {
      this.updateDeviceToken();
    });

    /* this.appVersionNumber = storageProvider.getAppVersionNumber().then((res: any) => {
      this.appVersionNumber = res;
    }).catch((err) => {
      this.appVersionNumber = err;
    }); */

    if (storageProvider.getAppBuildType() == "") {
      this.appVersionNumber = AppConst.appVersionProd;
    } else {
      this.appVersionNumber = AppConst.appVersion;
    }
    this.buildType = storageProvider.getAppBuildType();

    this.copyRightDate = new Date();

    this.events.subscribe('closeLoginPageModal',()=>{
      this.closeModal();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ngOnInit() {
    this.deviceinfo = this.storageProvider.getDeviceInfo();
    //localStorage.setItem("isTutorial", "1");
    this.menu.swipeEnable(false);

    this.countryCodes = this.storageProvider.getCountryCodes();

    this.countryCodes.map((item: any) => {
      item["selected"] = false
    });
    this.findIndia = this.countryCodes.findIndex(x => x.callingCodes === "+91");
    this.countryCodes[this.findIndia].selected = true;
    this.countryInitials = this.countryCodes[this.findIndia];

    this.loginForm = this.fb.group({
      textvalue: new FormControl('', [
        Validators.required
      ]),
      pan: new FormControl(),
      password: new FormControl('', [Validators.required]),
    });
    this.loginForm.valueChanges.subscribe((v) => {
      this.onlyNumberAllowed = false;
      this.validEmail = false;
      this.invalid = true;

      if (v.textvalue != "") {
        let checkEmail = this.loginForm.value.textvalue.indexOf("@");
        if (!isNaN(v.textvalue) && checkEmail == -1) {
          let regEx = this.storageProvider.onlyNumberPatternsignup();
          let result = v.textvalue.search(regEx);
          this.onlyNumberAllowed = false;

          if (v.textvalue.length > 4) {
            this.isCountryFieldShow = true;
            this.isEmailFieldShow = false;
          } else {
            this.isCountryFieldShow = false;
            this.isEmailFieldShow = true;
          }

          if (result == -1) {
            this.invalid = true;
            this.onlyNumberAllowed = true;
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.validLength = false;
              } else {
                this.validLength = true;
              }
            } else {
              this.validLength = false;
            }
          } else {
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.invalid = false;
                this.validLength = false;
              } else {
                this.invalid = true;
                this.validLength = true;
              }
            } else {
              this.invalid = false;
              this.validLength = false;
            }
            // this.invalid = false;
          }
        } else if (isNaN(v.textvalue)) {

          let regEx = this.storageProvider.emailPattern();
          let result = v.textvalue.search(regEx);
          this.validEmail = false;
          this.validLength = false;
          if (result == -1) {
            this.invalid = true;
            this.validEmail = true;
            this.isCountryFieldShow = false;
            this.isEmailFieldShow = true;
          } else {
            this.invalid = false;
          }
        }
      } else {
        this.isCountryFieldShow = false;
        this.isEmailFieldShow = true;
      }
      if (this.loginForm.valid && v.password != "") { //this.loginForm.valid && 
        this.invalid = false;
      } else {
        this.invalid = true;
      }

    });

    /* Select India By Default  */
    this.onChange(this.countryCodes[this.findIndia]);
  }

  get f() {
    return this.loginForm.controls;
  }

  goToPage(pageName: any) {
    this.navCtrl.push(pageName);
  }

  doLogin() {
    /* this.storageProvider.clear();
    localStorage.clear(); */
    /*  */
    let type, postData;
    if (this.panNumber) {
      type = "pan";
      postData = {
        username: this.loginForm.value.pan,
        type: type,
        password: this.loginForm.value.password,
        device_type: "Android",
        device_id: this.deviceinfo.uuid
      };
    } else {
      let checkEmail = this.loginForm.value.textvalue.indexOf("@");
      if (checkEmail > -1) {
        // type = "email";
        postData = {
          username: this.loginForm.value.textvalue,
          type: "email",
          password: this.loginForm.value.password,
          device_type: "Android",
          device_id: this.deviceinfo.uuid
        };
      } else {
        // type = "mob";
        postData = {
          username: this.countryInitials.callingCodes + this.loginForm.value.textvalue,
          type: "mob",
          password: this.loginForm.value.password,
          device_type: "Android",
          device_id: this.deviceinfo.uuid
        };
      }
    }
    this.loader.show('Please wait...');
    this.Authapi.postLoginapi(postData).subscribe((response: LoginApiresponse) => {
      this.loader.hide();
      if (response.status == "200") {
        localStorage.setItem('isGuest', "0");

        let data;
        if (localStorage.getItem('fromPage') == 'ProjectDetailsPage'
          || localStorage.getItem('fromPage') == 'BookingSummary'
          || localStorage.getItem('fromPage') == 'BuyNowPage') {

          data = { title: '', component: localStorage.getItem('fromPage') };
          localStorage.setItem('isCustomer', response.data.is_customer);
          this.events.publish('afterLoginRedirect', data);
          this.viewCtrl.dismiss('loggedIn');

        } else {
          /*  if (response.data.is_customer == '1') {
             data = { title: 'My Dashboard', component: 'DashboardPage' };
           } else {
             data = { title: 'Godrej Home', component: 'GodrejHomePage' };
           } */
          data = { title: 'Godrej Home', component: 'GodrejHomePage' };
          localStorage.setItem('isCustomer', response.data.is_customer);
          this.events.publish('afterLoginRedirect', data);
        }

        if (response.data.birth_dt == null || response.data.birth_dt == "") {
          response.data.birth_dt = '';
        } else/*  if (response.data.birth_dt != null || response.data.birth_dt != "") */ {
          response.data.birth_dt = this.datepipe.transform(response.data.birth_dt, 'yyyy-MM-dd');
        }

        localStorage.setItem('userData', JSON.stringify(response));
        localStorage.setItem('isLoggedIn', "1");
        this.events.publish('setUserDetails');
        localStorage.setItem('token', response.token);
        localStorage.setItem('Cookie', response.sessionName + '=' + response.sessionId);
        localStorage.setItem('Authorization', btoa(this.loginForm.value.textvalue + '/' + this.loginForm.value.password));
        localStorage.setItem('userId', response.data.userid);

        this.events.publish('checkHeaderIcon', data);

        if (localStorage.getItem('deviceToken') != "" &&
          typeof (localStorage.getItem('deviceToken')) != "undefined" &&
          localStorage.getItem('deviceToken') != null) {
          this.updateDeviceToken();
        } else {
          this.events.publish('getDeviceToken');
        }
        if (response.data.is_customer == '1') {
          this.events.publish('getSubmenu');
        }
      } else {
        if (response.status == "501") {
          this.panNumber = true;
          this.invalid = true;
          this.loginForm = this.fb.group({
            textvalue: new FormControl(this.loginForm.value.textvalue, [
              Validators.required
            ]),
            pan: new FormControl('', [
              Validators.required,
              Validators.pattern(this.storageProvider.panPattern()),
            ]),
            password: new FormControl('', [Validators.required]),
          });

          this.loginForm.valueChanges.subscribe((v) => {
            this.onlyNumberAllowed = false;
            this.validEmail = false;
            this.invalid = true;

            if (v.textvalue != "") {
              let checkEmail = this.loginForm.value.textvalue.indexOf("@");
              if (!isNaN(v.textvalue) && checkEmail == -1) {
                let regEx = this.storageProvider.onlyNumberPatternsignup();
                let result = v.textvalue.search(regEx);
                this.onlyNumberAllowed = false;

                if (v.textvalue.length > 4) {
                  this.isCountryFieldShow = true;
                  this.isEmailFieldShow = false;
                } else {
                  this.isCountryFieldShow = false;
                  this.isEmailFieldShow = true;
                }

                if (result == -1) {
                  this.invalid = true;
                  this.onlyNumberAllowed = true;
                  if (this.countryInitials.callingCodes == '+91') {
                    if (v.textvalue.length == 10) {
                      this.validLength = false;
                    } else {
                      this.validLength = true;
                    }
                  } else {
                    this.validLength = false;
                  }
                } else {
                  if (this.countryInitials.callingCodes == '+91') {
                    if (v.textvalue.length == 10) {
                      this.invalid = false;
                      this.validLength = false;
                    } else {
                      this.invalid = true;
                      this.validLength = true;
                    }
                  } else {
                    this.invalid = false;
                    this.validLength = false;
                  }
                  // this.invalid = false;
                }
              } else if (isNaN(v.textvalue)) {

                let regEx = this.storageProvider.emailPattern();
                let result = v.textvalue.search(regEx);
                this.validEmail = false;
                this.validLength = false;
                if (result == -1) {
                  this.invalid = true;
                  this.validEmail = true;
                  this.isCountryFieldShow = false;
                  this.isEmailFieldShow = true;
                } else {
                  this.invalid = false;
                }
              }
            } else {
              this.isCountryFieldShow = false;
              this.isEmailFieldShow = true;
            }

            if (v.pan != "") {
              let panRegEx = this.storageProvider.panPattern();
              let result = v.pan.search(panRegEx);
              if (result == -1) {
                this.invalid = true;
              } else {
                this.invalid = false;
              }
            }

            if (this.loginForm.valid && v.password != "") { //this.loginForm.valid && 
              this.invalid = false;
            } else {
              this.invalid = true;
            }

          });
        }
        this.toast.show(response.msg);
      }

    }, (error: any) => {
      console.log(error);
    })

    //localStorage.setItem("isLoggedIn","1");
    /* this.navCtrl.setRoot('GodrejHomePage', {}, {
      animate: true,
      direction: 'forward'
    }); */
  }

  checkEmailORMobile() {
    this.panNumber = false;
    this.loginForm.valueChanges.subscribe((v) => {
      this.onlyNumberAllowed = false;
      this.validEmail = false;
      this.invalid = true;
      if (v.textvalue != "") {
        let checkEmail = this.loginForm.value.textvalue.indexOf("@");
        if (!isNaN(v.textvalue) && checkEmail == -1) {
          let regEx = this.storageProvider.onlyNumberPatternsignup();
          let checkPlus = v.textvalue.indexOf("+");
          let result = v.textvalue.search(regEx);
          this.onlyNumberAllowed = false;

          if (v.textvalue.length > 4) {
            this.isCountryFieldShow = true;
            this.isEmailFieldShow = false;
          } else {
            this.isCountryFieldShow = false;
            this.isEmailFieldShow = true;
          }

          if (result == -1) {
            this.invalid = true;
            this.onlyNumberAllowed = true;
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.validLength = false;
              } else {
                this.validLength = true;
              }
            } else {
              this.validLength = false;
            }
          } else {
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.invalid = false;
                this.validLength = false;
              } else {
                this.invalid = true;
                this.validLength = true;
              }
            } else {
              this.invalid = false;
              this.validLength = false;
            }
            // this.invalid = false;
          }
        } else if (isNaN(v.textvalue)) {
          let regEx = this.storageProvider.emailPattern();
          let result = v.textvalue.search(regEx);
          this.validEmail = false;
          this.validLength = false;
          if (result == -1) {
            this.invalid = true;
            this.validEmail = true;
          } else {
            this.invalid = false;
          }
        }
      } else {
        this.isCountryFieldShow = false;
        this.isEmailFieldShow = true;
      }
      if (v.password == "") {
        this.invalid = true;
      }
    });
  }


  updateDeviceToken() {
    const isAndroid = (this.device.platform.toLowerCase() === 'android') ? true : false;
    const data = {
      device_token: JSON.parse(localStorage.getItem('deviceToken')),
      user_id: localStorage.getItem('userId'),
      device_type: (isAndroid) ? 'Android' : 'IOS'
    };
    this.Authapi.updateDeviceToken(data).subscribe((response: any) => {

    })
  }

  checkBox(type: any) {
    /* if(type == "E"){
      this.type={
        email:true,
        mobile:false,
        pan:false
      };
      this.loginForm = this.fb.group({
        emails: new FormControl('', [
          Validators.required,
          Validators.pattern(this.storageProvider.emailPattern())
        ]),
        password: new FormControl('', [Validators.required]),
      });
    } else if(type == "M"){
      this.type={
        email:false,
        mobile:true,
        pan:false
      };
      this.loginForm = this.fb.group({
        mobile: new FormControl('', [
          Validators.required,
          Validators.pattern(this.storageProvider.onlyNumberPattern()),
        ]),
        password: new FormControl('', [Validators.required]),
      });
    } else {
      this.type={
        email:false,
        mobile:false,
        pan:true
      };
      this.loginForm = this.fb.group({
        pan: new FormControl('', [
          Validators.required,
          Validators.pattern(this.storageProvider.panPattern()),
        ]),
        password: new FormControl('', [Validators.required]),
      });
    } */

  }

  onkeyDown() {
    let newPan = this.loginForm.value.pan.toUpperCase();
    this.loginForm.setValue({ textvalue: this.loginForm.value.textvalue, pan: newPan, password: this.loginForm.value.password });

  }

  eyeOpenClose(value: any) {
    if (value == "O") {
      this.openClose = true;
      this.typeText = "text";
    } else {
      this.openClose = false;
      this.typeText = "password";
    }
  }

  guestLogin() {
    let data = { title: 'Godrej Home', component: 'GodrejHomePage' };
    localStorage.setItem('isGuest', '1');
    this.events.publish('afterLoginRedirect', data);
    this.events.publish('updateLastLoginDate');
  }

  onChange(event: any) {
    this.flag = event.flag
  }

  openCountry() {
    /* this.loader.show('Please wait..'); */
    // this.addModal = this.modalCtrl.create("CountryListPage", { countryList: this.countryCodes });
    this.addModal = this.modalCtrl.create("CountryListPage", { showCodes: true });
    this.addModal.onDidDismiss((item: any) => {
      if (item) {
        this.countryCodes = item;
        const findSelected = item.findIndex(x => x.selected == true);
        this.countryInitials = item[findSelected];
        this.flag = this.countryInitials.flag;
        if (this.countryInitials.callingCodes == '+91') {
          this.loginForm.controls["textvalue"].setValidators([
            Validators.minLength(10),
            Validators.maxLength(10),
            Validators.required,
            Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
          ]);
        } else {
          this.loginForm.controls["textvalue"].setValidators([
            Validators.required,
            Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
          ]);
        }
        this.loginForm.controls['textvalue'].updateValueAndValidity();
      }
    });
    this.addModal.present();
    this.loader.hide();
  }

  getMobileNoValidation() {

    if (this.panNumber) {
      this.panNumber = false;
    }

    this.validLength = false;

    if (this.countryInitials.callingCodes == '+91') {
      this.loginForm.controls["textvalue"].setValidators([
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.required,
        Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
      ]);
      if (!this.loginForm.controls.textvalue.valid && this.loginForm.value.textvalue.length > 4) {
        this.validLength = true;
        this.invalid = true;
      } else {
        this.validLength = false;
        this.invalid = false;
      }
    } else {
      this.loginForm.controls["textvalue"].setValidators([
        Validators.required
      ]);
      // this.loginForm.controls["textvalue"].setValidators([
      //   Validators.required,
      //   Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
      // ]);
    }
    this.loginForm.controls['textvalue'].updateValueAndValidity();
  }

  closeModal() {
    this.viewCtrl.dismiss('');
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  openCalendar() {
    // let maximumDate = new Date((moment().subtract(18, 'years')).format('YYYY-MM-DD')).valueOf();
    // let minimumDate= new Date((moment().add(2, 'years')).format('YYYY-MM-DD')).valueOf();;
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      // minDate: minimumDate,
      // maxDate: maximumDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      (date: any) => {

        console.log('Got date: ', moment(date).format('YYYY-MM-DD'));
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }


}
