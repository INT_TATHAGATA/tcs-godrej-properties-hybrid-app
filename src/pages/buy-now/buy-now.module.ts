import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyNowPage } from './buy-now';
import { GodrejHomeApiProvider } from '../../providers/apis/godrejhome';

@NgModule({
  declarations: [
    BuyNowPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyNowPage),
  ],
  providers: [GodrejHomeApiProvider]
})
export class BuyNowPageModule {}
