import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { DbProvider } from '../../providers/db/db';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { GodrejHomeApiProvider } from '../../providers/apis/godrejhome';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { InAppBrowserOptions, InAppBrowser } from '@ionic-native/in-app-browser';
import { AppConst } from '../../app/app.constants';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

/**
 * Generated class for the BuyNowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buy-now',
  templateUrl: 'buy-now.html',
})
export class BuyNowPage implements OnInit {

  public buyNowProjectlist: Array<any>;

  public isGuest: any;
  public addModal: any;
  public userData: any;
  public previousPage: any;

  public noData: string = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    public storageProvider: DbProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public modalCtrl: ModalController,
    private buyNowAPI: GodrejHomeApiProvider,
    private iab: InAppBrowser,
    public projectdetailsApi: ProjectDetailsApiProvider,
    public events: Events,
    private alertCtrl: AlertController
  ) {
    this.buyNowProjectlist = [];
    this.isGuest = this.storageProvider.isGuest();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyNowPage');
  }

  ngOnInit() {
    this.menu.swipeEnable(false);
    this.getBuyNowProjectList();
  }

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  goToPage(pageName: any, project: any) {
    let data = {};
    if (pageName == "ProjectDetailsPage") {
      this.navCtrl.push(pageName, { fullData: { proj_id: project.proj_id } });
    } else if (pageName == 'InventoryBookingPage') {

      if (project.hasOwnProperty('field_is_available_for_booking')
        && project.field_is_available_for_booking != null
        && project.field_is_available_for_booking == '0') {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          message: 'Online booking for this project is currently unavailable.',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
      } else {

        localStorage.removeItem('Selectedinventoryfiterdata');
        const extradata = {
          pName: project.name
        };
        data = {
          project_id: project.proj_id,
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
          page: 0
        };

        if (localStorage.getItem('isGuest') == '1') {
          this.toast.show('Please Login/Register To View This Page.');
          this.addModal = this.modalCtrl.create("HomePage", {});
          this.addModal.onDidDismiss((item: any) => {
            if (item == 'loggedIn') {
              this.isGuest = this.storageProvider.isGuest();
              this.events.publish('guestToUser');

              if (project.hasOwnProperty('field_foyr_url') && project.field_foyr_url != null && project.field_foyr_url != '') {
                this.openFOYRPage(project);
              } else {
                if (project.hasOwnProperty('eoi_enabled') && project.eoi_enabled == '1') {
                  this.navCtrl.push('EoiPage', { formData: data, img: project.img != '' ? project.img : '', extdata: extradata });
                } else {
                  this.navCtrl.push(pageName, { formData: data, img: project.img != '' ? project.img : '', extdata: extradata });
                }
              }
            }
          });
          localStorage.setItem('fromPage', 'BuyNowPage');
          this.addModal.present();
        } else {

          if (project.hasOwnProperty('field_foyr_url') && project.field_foyr_url != null && project.field_foyr_url != '') {
            this.openFOYRPage(project);
          } else {
            if (project.hasOwnProperty('eoi_enabled') && project.eoi_enabled == '1') {
              this.navCtrl.push('EoiPage', { formData: data, img: project.img != '' ? project.img : '', extdata: extradata });
            } else {
              this.navCtrl.push(pageName, { formData: data, img: project.img != '' ? project.img : '', extdata: extradata });
            }
          }
        }

      }
    }
  }

  openFOYRPage(item) {
    /* debugger */
    if (localStorage.getItem('userId') == null && localStorage.getItem('isGuest') == '1') {
      this.toast.show('Please Login/Register To View This Page.');
      this.addModal = this.modalCtrl.create("HomePage", {});
      this.addModal.onDidDismiss((item: any) => {
        if (item == 'loggedIn') {
          this.openFOYRPage(item);
        } else {
          console.log("ITEM : " + item);
        }
      });
      localStorage.setItem('fromPage', 'BuyNowPage');
      this.addModal.present();
    } else {

      const sessonid = localStorage.getItem('Cookie').split('=')[1];
      const param = '?auth=' + sessonid + '&user_id=' + localStorage.getItem('userId');
      const self = this;
      //item.field_foyr_url
      const url = AppConst.baseUrl + 'virtual_grid_mobile/' + item.sfdc_code;
      console.log('url', url);
      const option: InAppBrowserOptions = {
        location: 'no',
        hidden: 'no',
        toolbar: 'no',
        clearcache: 'yes',
      };
      const browser = this.iab.create(url, '_blank', option);
      browser.on('loadstop').subscribe(event => {
        const path = event.url.split('inv_id=');
        if (path.length > 1) {
          browser.close();
          this.getInventoryObj(path[1], item);
        }
      });

    }
  }

  getInventoryObj(invId: any, item) {
    const jsonPayload = {
      inventory_id: invId
    };
    this.projectdetailsApi.getInvData(jsonPayload).subscribe((response: any) => {
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.goToProjectDetails(response.data, item.name);
      }
    });
  }

  getBuyNowProjectList() {
    this.loader.show('Please wait...');
    this.buyNowAPI.getBuyNowProjectList().subscribe((response: any) => {
      if (response.status == '200') {
        this.buyNowProjectlist = response.data;
        if (this.buyNowProjectlist.length > 0) {
          this.noData = "";
        } else {
          this.noData = "No Projects Found.";
        }
      } else {
        this.noData = "No Projects Found.";
      }
      this.loader.hideNoTimeout();
    });
  }

  goToProjectDetails(inventory: any, projectName: any) {

    const jsonData = {
      user_id: localStorage.getItem('userId'),
      inventory_id: inventory[0].nid
    };
    this.loader.show();
    this.projectdetailsApi.getInventoryAvailable(jsonData).subscribe((response: any) => {
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.getcostsheetbrkup(inventory).then((response: any) => {
          if (response) {
            this.getPaymentPlan().then((response: any) => {
              if (response) {
                this.loader.hide();
                const data = {
                  pName: projectName,
                  inv: inventory
                }
                localStorage.removeItem('EnquiryFormData');
                this.navCtrl.push('EnquiryFormPage', { details: data });
              } else {
                this.loader.hide();
                this.toast.show("Apologies for Inconvenience Caused. Please try with other Unit.");
              }
            }).catch((error: any) => {
              this.loader.hide();
              this.toast.show("Apologies for Inconvenience Caused. Please try with other Unit.");
            });
          } else {
            this.loader.hide();
            this.toast.show("Oops! Something went wrong. Try with some other Unit.");
          }
        }).catch((error: any) => {
          this.loader.hide();
          this.toast.show("Oops! Something went wrong. Try with some other Unit.");
        });
      } else {
        this.loader.hide();
        this.toast.show(response.msg);
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
      this.toast.show("Looks like Unit is already booked. Please Proceed with any other Unit.");
    });
  }

  getcostsheetbrkup(inventory) {
    return new Promise((resolve, reject) => {
      const data = {
        proj_id: localStorage.getItem('projectId'),
        user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
        device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
        inventory_id: inventory.nid
      };
      this.projectdetailsApi.getcostsheetbrkup(data).subscribe((response: any) => {
        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          if (response.data.curl_response.hasOwnProperty('errors')) {
            if (response.data.curl_response.errors.status == '1') {
              reject(false);
            } else {
              resolve(true);
            }
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      }, (error: any) => {
        this.loader.hide();
        reject(false);
      });
    })
  }

  getPaymentPlan() {
    return new Promise((resolve, reject) => {
      const userData = JSON.parse(localStorage.getItem('userData'));
      const jSonData = {
        user_id: userData.data.userid,
        proj_id: localStorage.getItem('projectId'),
        device_id: this.storageProvider.getDeviceInfo().uuid
      };
      this.projectdetailsApi.getPaymentPlan(jSonData).subscribe((response: any) => {
        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          if (response.data.curl_response.hasOwnProperty('errors')) {
            if (response.data.curl_response.errors.status == '1') {
              reject(false);
            } else {
              resolve(true);
            }
          } else {
            reject(false);
          }
        } else {
          reject(false);
        }
      }, (error: any) => {
        this.loader.hide();
        reject(false);
      });
    })
  }

}
