import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationlistPage } from './notificationlist';
import { NotificationApiProvider } from '../../providers/apis/notification';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

@NgModule({
  declarations: [
    NotificationlistPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationlistPage),
  ],
  providers: [NotificationApiProvider, ProjectDetailsApiProvider]
})
export class NotificationlistPageModule { }
