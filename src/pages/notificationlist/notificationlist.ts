import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { NotificationApiProvider } from '../../providers/apis/notification';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

/**
 * Generated class for the NotificationlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notificationlist',
  templateUrl: 'notificationlist.html',
})
export class NotificationlistPage {
  Notificationlist: Array<any>;
  totalPage: any;
  gopage: any;

  public noDataMsg: string = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public NotificationApi: NotificationApiProvider,
    private loader: LoadingProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private toast: ToasterProvider,
    public events: Events, ) {
    this.Notificationlist = [];
    this.totalPage = 0;
    this.gopage = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationlistPage');
    this.getnotification();
  }

  getnotification() {
    const param = {
      user_id: localStorage.getItem('userId'),
      page: this.gopage
    };
    if (this.Notificationlist.length == 0) {
      this.loader.show();
    }
    this.NotificationApi.getNotificationlisting(param).subscribe((response: any) => {
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        if (this.gopage == '0') {
          this.Notificationlist = response.data;
          if (this.Notificationlist.length == 0) {
            this.noDataMsg = "No Notification Found.";
          } else {
            this.noDataMsg = "";
          }
        } else {
          this.Notificationlist = this.Notificationlist.concat(response.data);
        }
        this.totalPage = response.page_count;
      }

    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    },
      () => {
        this.loader.hideNoTimeout();
      });
  }

  opennotification(notification) {
    console.log(notification);
    if (notification.field_is_message_read == 0) {
      this.goReadNotification(notification.id);
    }
    if (notification.field_mobile_app_data) {
      const PageApi = notification.field_mobile_app_data.target_link.split('/')[1];
      if (PageApi == 'get_user_data_api.json') {
        this.navCtrl.push('UserProfilePage');
      } else if (PageApi == 'list_document_api.json') {
        this.navCtrl.push('MyDocumentsListPage');
      } else if (PageApi == 'booking_confirmation_data.json') {
        this.boookingconfrim(notification.field_mobile_app_data.data.booking_id);
      } else if (PageApi == 'eoi_booking_confirmation_data.json') {
        this.eoiBoookingconfrim(notification.field_mobile_app_data.data.booking_id);
      }
    }
  }

  boookingconfrim(booking_id) {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: booking_id
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.postBookingConfrimDetails(data).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == '200') {
        this.navCtrl.push("PaymentPage", { details: response.data, bookingid: booking_id, fromPage: 'ApplicationFormPageN' });
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  eoiBoookingconfrim(booking_id) {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: booking_id
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.postEoiBookingConfrimDetails(data).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == '200') {
        this.navCtrl.push("PaymentPage", { details: response.data, bookingid: booking_id, fromPage: 'EOIEnquiryFormPageN' });
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  goReadNotification(notification_id) {
    const param = {
      user_id: localStorage.getItem('userId'),
      notification_id: notification_id
    };
    this.NotificationApi.readnotification(param).subscribe((response: any) => {
      this.events.publish('showNotiDot');
      const findIndex = this.Notificationlist.findIndex(x => x.id == notification_id);
      if (findIndex > -1) {
        this.Notificationlist[findIndex].field_is_message_read = 1;
      }
    }, (error: any) => {
      console.log(error);
    });
  }

  doInfinite(infiniteScroll) {
    console.log("Notification gopage : " + this.gopage);
    if (this.totalPage > 0 && this.gopage < this.totalPage) {
      this.gopage = this.gopage + 1;
      this.getnotification();
      setTimeout(() => {
        infiniteScroll.complete();
      }, 1000);
    } else {
      infiniteScroll.complete();
    }

  }

}
