import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, Slides, AlertController } from 'ionic-angular';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { LoadingProvider } from '../../providers/utils/loader';
import { AppConst } from '../../app/app.constants';
import { ToasterProvider } from '../../providers/utils/toast';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';
import { DbProvider } from '../../providers/db/db';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
/**
 * Generated class for the ProjectDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-details',
  templateUrl: 'project-details.html',
})
export class ProjectDetailsPage {
  name: string;
  lat: any;
  lng: any;
  url: string;
  wistliststatus: any;
  public division: any = {
    buyNow: true,
    gInTouch: false
  };
  projectbanar: Array<any>;
  projectDetails: any = [];
  projectAmenities: Array<any>;
  projectGallery: Array<any>;
  public sectionList: any[];
  projectid: any;
  public selectedSection: boolean = true;
  listIndex: any;
  sectionImg: any;
  public userData: any;
  public locationData: any;
  public previousPage: any;
  public isGuest: any;
  public addModal: any;
  @ViewChild(Slides) slides: Slides;

  public overviewData: any;

  public markerInfoWindowText: any;

  public noDataOverView: String = "";
  public noDataAmenities: String = "";
  public noDataGallery: String = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    private wishlistApi: WishListApiProvider,
    public storageProvider: DbProvider,
    public events: Events,
    public modalCtrl: ModalController,
    public sanitizer: DomSanitizer,
    private menu: MenuController,
    private alertCtrl: AlertController
  ) {
    this.menu.swipeEnable(false);

    this.projectid = this.navParams.get('fullData');
    /*  console.log("Project Details : " + JSON.stringify(this.projectid)); */

    this.projectbanar = [];
    this.projectDetails = [];
    this.projectAmenities = [];
    this.projectGallery = [];
    this.lat = '';
    this.lng = '';

    this.wistliststatus = '0';
    try {
      this.url = this.projectid.url;
      if (this.projectid.hasOwnProperty('wishlist_status')) {
        this.wistliststatus = this.projectid.wishlist_status;
      } else {
        this.wistliststatus = '0';
      }
    } catch (error) {
      console.log(error);
      this.wistliststatus = '0';
      this.url = '';
    }
  }

  ionViewWillEnter() {
    this.isGuest = this.storageProvider.isGuest();
    this.userData = localStorage.getItem('userData');

    if (this.projectbanar.length == 0) {
      this.loader.show();
      this.getprojetbanar();
    }
  }

  ionViewDidLoad() {
    /*  this.isGuest = this.storageProvider.isGuest();
       this.customInit();
       this.updateView();
      this.userData = localStorage.getItem('userData'); */
  }

  /*  customInit() {
     this.loader.show();
     Observable.forkJoin([
       this.getprojectdetails(),
       this.getprojetbanar(),
       this.getprojectlocation()
     ]).subscribe((result: any) => {
       this.projectDetails = result[0];
 
       if (this.projectDetails[0].Overview) {
         this.noDataOverView = "";
         this.overviewData = this.sanitizer.bypassSecurityTrustHtml(this.projectDetails[0].Overview);
       } else {
         this.noDataOverView = "No Data Available."
       }
 
       this.projectbanar = result[1];
       if (result[2].status == AppConst.HTTP_SUCESS_STATUS.OK) {
         this.locationData = result[2].data;
 
         let location = "";
         if (this.projectDetails[0].field_sub_location != '') {
           location = this.projectDetails[0].field_sub_location + ", " + this.projectDetails[0].field_city;
         } else {
           location = this.projectDetails[0].field_city;
         }
 
         this.markerInfoWindowText = this.sanitizer.bypassSecurityTrustHtml("<b>" + this.projectDetails[0].title + "</b><br/>" + location);
 
         this.lat = Number(result[2].data.lat);
         this.lng = Number(result[2].data.lng);
         this.url = result[2].data.url;
         this.wistliststatus = result[2].data.wishlist_status;
       }
       this.loader.hide();
     }, (error) => {
       this.loader.hide();
       console.log('Error:::', error);
     });
     this.division = {
       buyNow: true,
       gInTouch: false
     };
 
     this.sectionList = [
       { name: "Overview", iconName: "home", selected: true, img: "assets/imgs/overview-icon.png", grey_img: "assets/imgs/overview-icon-b.png" },
       { name: "Location", iconName: "pin", selected: false, img: "assets/imgs/location-icon.png", grey_img: "assets/imgs/location-icon-b.png" },
       { name: "Amenities", iconName: "alarm", selected: false, img: "assets/imgs/amenities-icon.png", grey_img: "assets/imgs/amenities-icon-b.png" },
       { name: "Gallery", iconName: "images", selected: false, img: "assets/imgs/gallery-icon.png", grey_img: "assets/imgs/gallery-icon-b.png" }
     ];
     this.showDetails(0);
   } */

  openDevivceMap(lat, lng) {
    let destination = lat + ',' + lng;
    let location = "";
    if (this.projectDetails[0].field_sub_location != '') {
      location = this.projectDetails[0].field_sub_location + ", " + this.projectDetails[0].field_city;
    } else {
      location = this.projectDetails[0].field_city;
    }
    let label = encodeURI(this.projectDetails[0].title + "\n" + location);
    window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
  }

  getprojetbanar() {
    this.projectdetailsApi.getprojectbanar(this.projectid.proj_id).subscribe((result) => {
      this.projectbanar = result;
    }, (error) => {
      console.log('Error:::', error);
      this.loader.hide();
    },
      () => {
        this.division = {
          buyNow: true,
          gInTouch: false
        };

        this.sectionList = [
          { name: "Overview", iconName: "home", selected: true, img: "assets/imgs/overview-icon.png", grey_img: "assets/imgs/overview-icon-b.png" },
          { name: "Location", iconName: "pin", selected: false, img: "assets/imgs/location-icon.png", grey_img: "assets/imgs/location-icon-b.png" },
          { name: "Amenities", iconName: "alarm", selected: false, img: "assets/imgs/amenities-icon.png", grey_img: "assets/imgs/amenities-icon-b.png" },
          { name: "Gallery", iconName: "images", selected: false, img: "assets/imgs/gallery-icon.png", grey_img: "assets/imgs/gallery-icon-b.png" }
        ];
        this.showDetails(0);
        if (this.projectbanar != null && this.projectbanar.length > 0) {
          this.getprojectdetails();
        } else {
          this.getprojetbanarImage();
        }
      });
  }

  getprojetbanarImage() {
    this.projectdetailsApi.getprojectbanarImage(this.projectid.proj_id).subscribe((result) => {
      this.projectbanar = result;
      if (this.projectbanar != null && this.projectbanar.length > 0) {
        this.projectbanar.map((item: any) => {
          item['banner_image_url'] = item.field_project_image;
        });
      }
    }, (error) => {
      console.log('Error:::', error);
      this.loader.hide();
    },
      () => {
        this.getprojectdetails()
      });
  }

  getprojectdetails() {
    /* return this.projectdetailsApi.goprojectdetails(this.projectid.proj_id); */
    this.projectdetailsApi.goprojectdetails(this.projectid.proj_id).subscribe((result) => {
      this.projectDetails = result;

      if (this.projectDetails[0].Overview) {
        this.noDataOverView = "";
        this.overviewData = this.sanitizer.bypassSecurityTrustHtml(this.projectDetails[0].Overview);
      } else {
        this.noDataOverView = "No Data Available."
      }
    }, (error) => {
      console.log('Error:::', error);
      this.loader.hide();
    },
      () => {
        /* this.getprojectlocation(); */
        /* this.loader.hideNoTimeout(); */
        this.getprojectlocation();
        this.updateView();
      });
  }

  getprojectlocation() {
    if (this.lat == '' && this.lng == '') {
      /* this.loader.show(); */
      const data = {
        proj_id: this.projectid.proj_id,
        user_id: localStorage.getItem('userId')
      }
      /* return this.projectdetailsApi.getprojectlocation(data); */
      return this.projectdetailsApi.getprojectlocation(data).subscribe((result) => {
        if (result.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          this.locationData = result.data;

          let location = "";
          if (this.projectDetails[0].field_sub_location != '') {
            location = this.projectDetails[0].field_sub_location + ", " + this.projectDetails[0].field_city;
          } else {
            location = this.projectDetails[0].field_city;
          }

          this.markerInfoWindowText = this.sanitizer.bypassSecurityTrustHtml("<b>" + this.projectDetails[0].title + "</b><br/>" + location);

          this.lat = Number(result.data.lat);
          this.lng = Number(result.data.lng);
          this.url = result.data.url;
          this.wistliststatus = result.data.wishlist_status;
        }
      }, (error) => {
        console.log('Error:::', error);
      },
        () => {
          this.loader.hideNoTimeout();
        });
    }
  }

  getprojectaminities() {
    if (this.projectAmenities != null && this.projectAmenities.length == 0) {
      this.loader.show();
      this.projectdetailsApi.getprojectaminities(this.projectid.proj_id).subscribe((response: any) => {
        this.projectAmenities = response;
        if (this.projectAmenities.length > 0) {
          this.noDataAmenities = "";
        } else {
          this.noDataAmenities = "No Data Available."
        }
      }, (error: any) => {
        console.log(error);
        this.loader.hide();
      },
        () => {
          this.loader.hideNoTimeout();
        });
    }
  }

  getprojectgalleries() {
    if (this.projectGallery != null && this.projectGallery.length == 0) {
      this.loader.show();
      this.projectdetailsApi.getprojectgalleries(this.projectid.proj_id).subscribe((response: any) => {
        this.projectGallery = response;
        if (this.projectGallery.length > 0) {
          this.noDataGallery = "";
        } else {
          this.noDataGallery = "No Data Available."
        }
      }, (error: any) => {
        console.log(error);
        this.loader.hide();
      },
        () => {
          this.loader.hideNoTimeout();
        });
    }
  }

  updateView() {
    const data = {
      proj_id: this.projectid.proj_id,
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : ''
    };
    this.projectdetailsApi.updateProjectDetails(data).subscribe((response: any) => {

    }, (error: any) => {

    });
  }

  selected(value: any) {
    (value == "b") ? this.division = {
      buyNow: true,
      gInTouch: false
    } : this.division = {
      buyNow: false,
      gInTouch: true
    };
  }

  goBack() {
    this.navCtrl.pop();
  }

  goToPage(page: any) {
    let data = {};
    const extradata = {
      pName: this.projectDetails[0].title
    };
    if (page == 'GetInTouchPage') {
      data = {
        projectId: this.projectid.proj_id,
        projectName: this.projectDetails[0].title,
        userName: this.userData,
        userId: localStorage.getItem('userId'),
        indiaPhNo: this.locationData.callus_india,
        otherPhNo: this.locationData.callus_other_country
      };
      this.navCtrl.push(page, { formData: data, img: this.projectbanar.length > 0 ? this.projectbanar[0].banner_image_url : '', extdata: extradata });
    } else if (page == 'InventoryBookingPage') {

      if (this.projectDetails[0].field_is_available_for_booking != null && this.projectDetails[0].field_is_available_for_booking == '0') {
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          message: 'Online booking for this project is currently unavailable.',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
      } else {
        localStorage.removeItem('Selectedinventoryfiterdata');
        data = {
          project_id: this.projectid.proj_id,
          user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
          device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
          page: 0
        };
        if (localStorage.getItem('isGuest') == '1') {
          this.toast.show('Please Login/Register To View This Page.');
          this.addModal = this.modalCtrl.create("HomePage", {});
          this.addModal.onDidDismiss((item: any) => {
            if (item == 'loggedIn') {
              this.isGuest = this.storageProvider.isGuest();
              this.events.publish('guestToUser');
              if (this.projectid.eoi_enabled == '1') {
                this.navCtrl.push('EoiPage', { formData: data, img: this.projectid.proj_img != '' ? this.projectid.proj_img : '', extdata: extradata });
              } else {
                this.navCtrl.push(page, { formData: data, img: this.projectbanar.length > 0 ? this.projectbanar[0].banner_image_url : '', extdata: extradata });
              }
            }
          });
          localStorage.setItem('fromPage', 'ProjectDetailsPage');
          this.addModal.present();
        } else {
          if (this.projectid.eoi_enabled == '1') {
            this.navCtrl.push('EoiPage', { formData: data, img: this.projectid.proj_img != '' ? this.projectid.proj_img : '', extdata: extradata });
          } else {
            this.navCtrl.push(page, { formData: data, img: this.projectbanar.length > 0 ? this.projectbanar[0].banner_image_url : '', extdata: extradata });
          }
        }
      }
    }
  }

  showDetails(index: any) {
    console.log(index);
    this.name = this.sectionList[index].name;
    this.sectionImg = this.sectionList[index].grey_img;
    this.listIndex = index;
    let findPreviousSelected = this.sectionList.findIndex(X => X.selected == true);
    if (findPreviousSelected > -1) {
      this.sectionList[findPreviousSelected].selected = false;
    }
    this.sectionList[index].selected = true;
    if (index == 0) {

    } else if (index == 1) {
      this.getprojectlocation();
    } else if (index == 2) {
      this.getprojectaminities();
    } else if (index == 3) {
      this.getprojectgalleries();
    }
  }

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  addOrRemoveWishlist() {
    if (localStorage.getItem('isGuest') == '1') {
      this.toast.show('Please Login/Register To View This Page.');
      this.addModal = this.modalCtrl.create("HomePage", {});
      this.addModal.onDidDismiss((item: any) => {
        if (item == 'loggedIn') {
          this.isGuest = this.storageProvider.isGuest();
          this.events.publish('guestToUser');

          this.addRmWishList();
        }
      });
      localStorage.setItem('fromPage', 'ProjectDetailsPage');
      this.addModal.present();
    } else {
      this.addRmWishList();
    }
  }

  addRmWishList() {
    const data = {
      user_id: localStorage.getItem('userId'),
      proj_id: this.projectid.proj_id,
      type: this.wistliststatus == 0 ? 'add' : 'delete'
    }
    this.loader.show();
    this.wishlistApi.modifyWishlist(data).subscribe((response: any) => {
      console.log(response);
      /* this.loader.hide(); */
      if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.events.publish('showNotiDot');
        this.wistliststatus = this.wistliststatus == 0 ? 1 : 0
        localStorage.setItem("ProjectDetailsWishlist_P_ID", this.projectid.proj_id);
        localStorage.setItem("ProjectDetailsWishlist_P_Wishlist", this.wistliststatus);
      }
      this.toast.show(response.msg);
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    },
      () => {
        console.log("Final");
        this.loader.hideNoTimeout();
      });
  }

  share() {
    const url = this.url;
    if (this.projectbanar != null && this.projectbanar.length > 0) {
      this.storageProvider.socialShare(this.projectDetails[0].title, "", this.projectbanar[0].banner_image_url, url);
    } else {
      this.storageProvider.socialShare(this.projectDetails[0].title, "", "", url);
    }
  }

  slidestop() {
    let currentIndex = this.slides.getActiveIndex();
    if (currentIndex == (this.projectAmenities.length - 1)) {
      this.slides.lockSwipeToNext(true);
    }
  }

  ionViewDidLeave() {
    this.loader.hideNoTimeout();
  }
}
