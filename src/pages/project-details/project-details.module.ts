import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectDetailsPage } from './project-details';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { SharedModule } from '../../app/app.shared.module';
import { WishListApiProvider } from '../../providers/apis/wishlistapi';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from '../../providers/interceptors/my-http-interceptor';

@NgModule({
  declarations: [
    ProjectDetailsPage,

  ],
  imports: [
    IonicPageModule.forChild(ProjectDetailsPage),
    SharedModule,
    ComponentsModule
  ],
  providers: [ProjectDetailsApiProvider, WishListApiProvider, {
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true
  },]

})
export class ProjectDetailsPageModule { }
