import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetInTouchPage } from './get-in-touch';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

@NgModule({
  declarations: [
    GetInTouchPage,
  ],
  imports: [
    IonicPageModule.forChild(GetInTouchPage),
    ComponentsModule
  ],
  providers:[ProjectDetailsApiProvider]
})
export class GetInTouchPageModule {}
