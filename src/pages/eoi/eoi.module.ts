import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EoiPage } from './eoi';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

@NgModule({
  declarations: [
    EoiPage,
  ],
  imports: [
    IonicPageModule.forChild(EoiPage),
    ComponentsModule
  ],
  providers: [ProjectDetailsApiProvider]
})
export class EoiPageModule {}