import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events, AlertController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { DbProvider } from '../../providers/db/db';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { AppConst } from '../../app/app.constants';

/**
 * Generated class for the EoiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eoi',
  templateUrl: 'eoi.html',
})
export class EoiPage {

  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = true;
  subHeadingText: string = "";
  projectimg: string;
  extraData: any;
  formData: any;

  public typologyList: any;
  public towerList: any;
  public floorBandList: any;

  public invalid: boolean = true;
  public selectedType: string = 'typology';
  public selectedId: object = {
    typology: '',
    tower: '',
    floor: ''
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public storageProvider: DbProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private menu: MenuController,
    public events: Events,
    private alertCtrl: AlertController
  ) {
    this.menu.swipeEnable(false);
    this.formData = this.navParams.get('formData');
    this.projectimg = this.navParams.get('img');
    this.extraData = this.navParams.get('extdata');
    this.subHeadingText = this.navParams.get('extdata').pName;
    this.typologyList = [];
    this.towerList = [];
    this.floorBandList = [];
  }

  ionViewDidLoad() {
    this.typologyList = [];
    this.towerList = [];
    this.floorBandList = [];
    this.getAllList();
  }

  getimgUrl(imagename) {
    if (imagename && imagename !== '') {
      return imagename;
    } else {
      return `assets/imgs/no-image.png`;
    }
  }

  goToEnquiryPage(value: any) {
    this.loader.show();
    const stockJson = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      project_id: this.formData.project_id,
      typology_id: this.selectedId['typology']
    };
    
    this.projectdetailsApi.getEoiStock(stockJson).subscribe((response: any) => {
      if (response.status == AppConst.API_STATUS.OK && response.msg.toLowerCase() == 'available') {
        const data = {
          projectId: this.formData.project_id,
          pName: this.extraData.pName,
          isSelectedId: this.selectedId
        };
        this.navCtrl.push(value, { details: data });
        /* this.loader.hide(); */
      }
    });
  }

  selectDiv(index, type) {
    this.invalid = true;
    if (type == "Typology") {
      this.selectedType = 'tower';
      this.typologyList.map((item: any, mapIndex: any) => {
        if (index == mapIndex) {
          this.typologyList[index].selected = !this.typologyList[index].selected;
        } else {
          this.typologyList[mapIndex].selected = false;
        }
      });
      const typoArrLength = this.typologyList.filter(x => x.selected == true);

      if (typoArrLength.length > 0) {
        this.selectedId = {
          typology: typoArrLength[0].id,
          tower: '',
          floor: ''
        }
        if (typoArrLength[0].name.toLowerCase() == 'villa' ||
          typoArrLength[0].name.toLowerCase() == 'plot' ||
          typoArrLength[0].name.toLowerCase() == 'studio'
        ) {
          this.invalid = false;
        }
        this.towerList = [];
        this.floorBandList = [];
        this.getAllList();
      } else {
        this.floorBandList = [];
        this.towerList = [];
      }
    } else if (type == "Tower") {
      this.selectedType = 'floor_band';
      this.towerList.map((item: any, mapIndex: any) => {
        if (index == mapIndex) {
          this.towerList[index].selected = !this.towerList[index].selected;
        } else {
          this.towerList[mapIndex].selected = false;
        }
      });
      const towerArrLength = this.towerList.filter(x => x.selected == true);

      if (towerArrLength.length > 0) {
        this.selectedId = {
          typology: this.selectedId['typology'],
          tower: towerArrLength[0].id,
          floor: ''
        }
        this.getAllList();
      } else {
        this.floorBandList = [];
      }
    } else if (type == "FloorBand") {
      this.floorBandList.map((item: any, mapIndex: any) => {
        if (index == mapIndex) {
          this.floorBandList[index].selected = !this.floorBandList[index].selected;
        } else {
          this.floorBandList[mapIndex].selected = false;
        }
      });

      const floorArr = this.floorBandList.filter(x => x.selected == true);
      if (floorArr.length > 0) {
        this.selectedId = {
          typology: this.selectedId['typology'],
          tower: this.selectedId['tower'],
          floor: floorArr[0].id
        };
      }
      if (floorArr.length > 0) {
        this.invalid = false;
      } else {
        this.invalid = true;
      }
    }
  }

  getAllList() {
    this.loader.show();

    const data = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      project_id: this.formData.project_id,
      type: this.selectedType,
      typology_id: this.selectedId['typology'],
      tower_id: this.selectedId['tower']
    };
    this.projectdetailsApi.getEoiList(data).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == '200') {
        console.log("Status 200...");
        if (this.selectedType == 'typology') {
          this.typologyList = response.data;
          this.typologyList.map((item: any) => {
            item['selected'] = false;
          });
        } else if (this.selectedType == 'tower') {
          this.towerList = response.data;
          this.towerList.map((item: any) => {
            item['selected'] = false;
          });
        } else if (this.selectedType == 'floor_band') {
          this.floorBandList = response.data;
          this.floorBandList.map((item: any) => {
            item['selected'] = false;
          });
        }
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    });
  }

}