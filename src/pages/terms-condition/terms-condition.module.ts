import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermsConditionPage } from './terms-condition';
import { AuthProvider } from '../../providers/apis/auth';

@NgModule({
  declarations: [
    TermsConditionPage,
  ],
  imports: [
    IonicPageModule.forChild(TermsConditionPage),
  ],
  providers: [AuthProvider]
})
export class TermsConditionPageModule { }
