import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthProvider } from '../../providers/apis/auth';
import { LoadingProvider } from '../../providers/utils/loader';

/**
 * Generated class for the TermsConditionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terms-condition',
  templateUrl: 'terms-condition.html',
})
export class TermsConditionPage {
  public termsData: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public storageProvider: DbProvider,
    public sanitizer: DomSanitizer,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    public events: Events
  ) {
    this.events.subscribe('closeTermsConditionPage',()=>{
      this.myDismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsConditionPage');
  }

  ngOnInit() {
    this.loader.show('Please wait...');
    this.Authapi.getTermsConditionApi().subscribe((response: any) => {
      this.loader.hide();
      this.termsData = this.sanitizer.bypassSecurityTrustHtml(response[0].page_content);
    }, (error: any) => {
      this.loader.hide();
      console.log(error);
    });

  }

  myDismiss() {
    this.viewCtrl.dismiss();
  }

}
