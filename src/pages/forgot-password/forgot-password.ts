import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/apis/auth';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  public forgotPasswordForm: FormGroup;
  public disableButton: boolean = true;
  public panNumber: boolean = false;
  public onlyNumberAllowed: boolean = false;
  public validEmail: boolean = false;
  public invalid: boolean = true;

  public isCountryFieldShow: boolean = false;
  public isEmailFieldShow: boolean = true;

  public countryCodes: any = [];
  public flag: any;
  public findIndia: any;
  public addModal: any;
  public countryInitials: any;
  public validLength: boolean = false;

  public otpMsg: any = "";


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public storageProvider: DbProvider,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public modalCtrl: ModalController,
    public events: Events,
  ) {
    this.otpMsg = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  ngOnInit() {

    this.countryCodes = this.storageProvider.getCountryCodes();

    this.countryCodes.map((item: any) => {
      item["selected"] = false
    });
    this.findIndia = this.countryCodes.findIndex(x => x.callingCodes === "+91");
    this.countryCodes[this.findIndia].selected = true;
    this.countryInitials = this.countryCodes[this.findIndia];

    this.forgotPasswordForm = this.fb.group({
      textvalue: new FormControl('', [
        Validators.required,
      ]),
      pan: new FormControl()
    });
    this.forgotPasswordForm.valueChanges.subscribe((v) => {
      this.onlyNumberAllowed = false;
      this.validEmail = false;
      this.invalid = true;
      if (v.textvalue != "") {
        let checkEmail = this.forgotPasswordForm.value.textvalue.indexOf("@");
        if (!isNaN(v.textvalue) && checkEmail == -1) {
          let regEx = this.storageProvider.onlyNumberPatternsignup();
          let checkPlus = v.textvalue.indexOf("+");
          let result = v.textvalue.search(regEx);
          this.onlyNumberAllowed = false;

          if (v.textvalue.length > 4) {
            this.isCountryFieldShow = true;
            this.isEmailFieldShow = false;
          } else {
            this.isCountryFieldShow = false;
            this.isEmailFieldShow = true;
          }

          if (result == -1) {
            this.invalid = true;
            this.onlyNumberAllowed = true;
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.validLength = false;
              } else {
                this.validLength = true;
              }
            } else {
              this.validLength = false;
            }
          } else {
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.invalid = false;
                this.validLength = false;
              } else {
                this.invalid = true;
                this.validLength = true;
              }
            } else {
              this.invalid = false;
              this.validLength = false;
            }
            // this.invalid = false;
          }
        } else if (isNaN(v.textvalue)) {
          let regEx = this.storageProvider.emailPattern();
          let result = v.textvalue.search(regEx);
          this.validEmail = false;
          this.validLength = false;
          if (result == -1) {
            this.invalid = true;
            this.validEmail = true;
          } else {
            this.invalid = false;
          }
        }
      } else {
        this.isCountryFieldShow = false;
        this.isEmailFieldShow = true;
      }
      if (v.password == "") {
        this.invalid = true;
      }
    });

    /* Select India By Default  */
    this.onChange(this.countryCodes[this.findIndia]);
  }

  get f() {
    return this.forgotPasswordForm.controls;
  }

  forgotPassword() {
    //
    let type, postData;
    let email = "";
    let mobile = "";
    if (this.panNumber) {
      type = "pan";

      let checkEmail = this.forgotPasswordForm.value.textvalue.indexOf("@");
      if (checkEmail > -1) {
        email = this.forgotPasswordForm.value.textvalue;
        mobile = "";
      } else {
        email = "";
        mobile = this.countryInitials.callingCodes + this.forgotPasswordForm.value.textvalue;
      }
      postData = {
        type: type,
        username: this.forgotPasswordForm.value.pan,
        first_time: "true",
        email: email,
        mobile: mobile,
        device_type: "android"
      };
    } else {
      let checkEmail = this.forgotPasswordForm.value.textvalue.indexOf("@");

      if (checkEmail > -1) {
        // type = "email";
        postData = {
          type: "email",
          username: this.forgotPasswordForm.value.textvalue,
          first_time: "true",
          device_type: "android"
        };
      } else {
        // type = "mob";
        postData = {
          type: "mob",
          username: this.countryInitials.callingCodes + this.forgotPasswordForm.value.textvalue,
          first_time: "true",
          device_type: "android"
        };
      }
    }

    this.loader.show('Please wait...');
    this.Authapi.postForgotPasswordApi(postData).subscribe((response: any) => {
      this.loader.hide();
      if (response.status == "200") {
        let data = {
          response_otp: response.otp,
          uid: response.uid,
          postd: postData,
          responseMsg: response.msg,
          email: email,
          mobile: mobile
        }
        this.otpMsg = response.msg;
        /* this.toast.show(response.msg); */
        this.events.publish('startOTPAutoRead');
        this.navCtrl.push("VarifyOtpPage", { formData: data });
      } else {
        if (response.status == "501") {
          this.invalid = true;
          this.panNumber = true;
          this.forgotPasswordForm = this.fb.group({
            textvalue: new FormControl(this.forgotPasswordForm.value.textvalue, [
              Validators.required
            ]),
            pan: new FormControl('', [
              Validators.required,
              Validators.pattern(this.storageProvider.panPattern()),
            ])
          });


          this.forgotPasswordForm.valueChanges.subscribe((v) => {
            this.onlyNumberAllowed = false;
            this.validEmail = false;
            this.invalid = true;

            if (v.textvalue != "") {
              let checkEmail = this.forgotPasswordForm.value.textvalue.indexOf("@");
              if (!isNaN(v.textvalue) && checkEmail == -1) {
                let regEx = this.storageProvider.onlyNumberPatternsignup();
                let checkPlus = v.textvalue.indexOf("+");
                let result = v.textvalue.search(regEx);
                this.onlyNumberAllowed = false;

                if (v.textvalue.length > 4) {
                  this.isCountryFieldShow = true;
                  this.isEmailFieldShow = false;
                } else {
                  this.isCountryFieldShow = false;
                  this.isEmailFieldShow = true;
                }

                if (result == -1) {
                  this.invalid = true;
                  this.onlyNumberAllowed = true;
                  if (this.countryInitials.callingCodes == '+91') {
                    if (v.textvalue.length == 10) {
                      this.validLength = false;
                    } else {
                      this.validLength = true;
                    }
                  } else {
                    this.validLength = false;
                  }
                } else {
                  if (this.countryInitials.callingCodes == '+91') {
                    if (v.textvalue.length == 10) {
                      this.invalid = false;
                      this.validLength = false;
                    } else {
                      this.invalid = true;
                      this.validLength = true;
                    }
                  } else {
                    this.invalid = false;
                    this.validLength = false;
                  }
                  // this.invalid = false;
                }
              } else if (isNaN(v.textvalue)) {
                let regEx = this.storageProvider.emailPattern();
                let result = v.textvalue.search(regEx);
                this.validEmail = false;
                this.validLength = false;
                if (result == -1) {
                  this.invalid = true;
                  this.validEmail = true;
                } else {
                  this.invalid = false;
                }
              }
            } else {
              this.isCountryFieldShow = false;
              this.isEmailFieldShow = true;
            }
            if (v.pan != "") {
              let panRegEx = this.storageProvider.panPattern();
              let result = v.pan.search(panRegEx);
              if (result == -1) {
                this.invalid = true;
              } else {
                this.invalid = false;
              }
            }
          });
        }

        this.toast.show(response.msg);
      }
    }, (error: any) => {
      console.log(error);
    });

    /*
  } else {

  } */

  }

  checkEmailORMobile() {
    this.panNumber = false;
    this.forgotPasswordForm.valueChanges.subscribe((v) => {
      this.onlyNumberAllowed = false;
      this.validEmail = false;
      this.invalid = true;
      if (v.textvalue != "") {
        let checkEmail = this.forgotPasswordForm.value.textvalue.indexOf("@");
        if (!isNaN(v.textvalue) && checkEmail == -1) {
          let regEx = this.storageProvider.onlyNumberPatternsignup();
          let checkPlus = v.textvalue.indexOf("+");
          let result = v.textvalue.search(regEx);
          this.onlyNumberAllowed = false;

          if (v.textvalue.length > 4) {
            this.isCountryFieldShow = true;
            this.isEmailFieldShow = false;
          } else {
            this.isCountryFieldShow = false;
            this.isEmailFieldShow = true;
          }

          if (result == -1) {
            this.invalid = true;
            this.onlyNumberAllowed = true;
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.validLength = false;
              } else {
                this.validLength = true;
              }
            } else {
              this.validLength = false;
            }
          } else {
            if (this.countryInitials.callingCodes == '+91') {
              if (v.textvalue.length == 10) {
                this.invalid = false;
                this.validLength = false;
              } else {
                this.invalid = true;
                this.validLength = true;
              }
            } else {
              this.invalid = false;
              this.validLength = false;
            }
            // this.invalid = false;
          }
        } else if (isNaN(v.textvalue)) {
          let regEx = this.storageProvider.emailPattern();
          let result = v.textvalue.search(regEx);
          this.validEmail = false;
          this.validLength = false;
          if (result == -1) {
            this.invalid = true;
            this.validEmail = true;
          } else {
            this.invalid = false;
          }
        }
      } else {
        this.isCountryFieldShow = false;
        this.isEmailFieldShow = true;
      }
      if (v.password == "") {
        this.invalid = true;
      }
    });
  }

  onkeyDown() {
    let newPan = this.forgotPasswordForm.value.pan.toUpperCase();
    this.forgotPasswordForm.setValue({ textvalue: this.forgotPasswordForm.value.textvalue, pan: newPan });
  }

  onChange(event: any) {
    this.flag = event.flag
  }

  openCountry() {
    /* this.loader.show('Please wait..'); */
    // this.addModal = this.modalCtrl.create("CountryListPage", { countryList: this.countryCodes });
    this.addModal = this.modalCtrl.create("CountryListPage", { showCodes: true });
    this.addModal.onDidDismiss((item: any) => {
      if (item) {
        this.countryCodes = item;
        const findSelected = item.findIndex(x => x.selected == true);
        this.countryInitials = item[findSelected];
        this.flag = this.countryInitials.flag;
        if (this.countryInitials.callingCodes == '+91') {
          this.forgotPasswordForm.controls["textvalue"].setValidators([
            Validators.minLength(10),
            Validators.maxLength(10),
            Validators.required,
            Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
          ]);
        } else {
          this.forgotPasswordForm.controls["textvalue"].setValidators([
            Validators.required,
            Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
          ]);
        }
        this.forgotPasswordForm.controls['textvalue'].updateValueAndValidity();
      }
    });
    this.addModal.present();
    this.loader.hide();
  }

  getMobileNoValidation() {
    this.validLength = false;

    if (this.countryInitials.callingCodes == '+91') {
      this.forgotPasswordForm.controls["textvalue"].setValidators([
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.required,
        Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
      ]);
      if (!this.forgotPasswordForm.controls.textvalue.valid && this.forgotPasswordForm.value.textvalue.length > 4) {
        this.validLength = true;
        this.invalid = true;
      } else {
        this.validLength = false;
        this.invalid = false;
      }
    } else {
      this.forgotPasswordForm.controls["textvalue"].setValidators([
        Validators.required
      ]);
      // this.forgotPasswordForm.controls["textvalue"].setValidators([
      //   Validators.required,
      //   Validators.pattern(this.storageProvider.onlyNumberPatternsignup())
      // ]);
    }
    this.forgotPasswordForm.controls['textvalue'].updateValueAndValidity();
  }

}
