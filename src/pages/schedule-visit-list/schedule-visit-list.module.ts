import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleVisitListPage } from './schedule-visit-list';

@NgModule({
  declarations: [
    ScheduleVisitListPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleVisitListPage),
  ],
})
export class ScheduleVisitListPageModule {}
