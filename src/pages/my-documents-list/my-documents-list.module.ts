import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyDocumentsListPage } from './my-documents-list';
import { ComponentsModule } from '../../components/components.module';
import { DocumentsApiProvider } from '../../providers/apis/documents';

@NgModule({
  declarations: [
    MyDocumentsListPage,
  ],
  imports: [
    IonicPageModule.forChild(MyDocumentsListPage),
    ComponentsModule
  ],
  providers: [DocumentsApiProvider]
})
export class MyDocumentsListPageModule { }
