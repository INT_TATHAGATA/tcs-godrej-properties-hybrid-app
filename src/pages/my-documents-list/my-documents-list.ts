import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Alert } from 'ionic-angular';
import { ToasterProvider } from '../../providers/utils/toast';
import { LoadingProvider } from '../../providers/utils/loader';
import { DbProvider } from '../../providers/db/db';
import { DocumentsApiProvider } from '../../providers/apis/documents';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File, FileEntry } from '@ionic-native/file';
import { DashordApiProvider } from '../../providers/apis/dashbord';
/**
 * Generated class for the MyDocumentsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-documents-list',
  templateUrl: 'my-documents-list.html',
})
export class MyDocumentsListPage {
  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = true;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = false;
  subHeadingText: string = "";
  public docList: any = [];
  bookingitems: any;
  public otherProjects: boolean = true;
  public propertyText: string = "";

  public propertyDocList: any = [];

  public selectedValue: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public storageProvider: DbProvider,
    public documentAPi: DocumentsApiProvider,
    private transfer: FileTransfer,
    public DashbordApi: DashordApiProvider,
    public file: File
  ) {
    this.bookingitems = [];
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(true);
    console.log('ionViewDidLoad MyDocumentsListPage');
    let data = {
      user_id: localStorage.getItem('userId')
    };
    this.loader.show();
    this.documentAPi.getDocumentList(data).subscribe((response: any) => {
      if (response.status == '200') {
        response.data.map((item: any) => {
          const extention = item.attachment_file.split('.');
          item['ext'] = (extention[extention.length - 1]).toLowerCase();
          item['width'] = 0;
        });
        this.docList = response.data;
      }
      this.getAccountSummary();
    }, (err: any) => {
      this.loader.hide();
    });
  }

  getAccountSummary() {
    const jsonData = {
      user_id: localStorage.getItem('userId')
    };
    return this.DashbordApi.getAccountSummary(jsonData).subscribe((result: any) => {
      // console.log("App Component : getAccountSummary() : " + JSON.stringify(result));

      if (result.status == '200') {
        try {
          this.bookingitems = result.data.booking_items;
          if (this.bookingitems != null && this.bookingitems.length > 0) {
            this.propertyText = this.bookingitems[0].project_code + " / " + this.bookingitems[0].inventory_code;
            this.getPropertyDocumentList(this.bookingitems[0].booking_id);
            this.selectedValue = this.bookingitems[0].booking_id;
          }
        }
        catch (e) {
          console.log("My Doc List : getAccountSummary() error : " + e);
        }
      }
    }, (error) => {
      console.log('Error:::', error);
      this.loader.hide();
    });
  }

  onSelectChange(projectBookingId: any) {
    console.log('Selected', projectBookingId);
    this.getPropertyDocumentList(projectBookingId);
  } 

  getPropertyDocumentList(projectBookingId) {
    this.openOtherProjects();
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: projectBookingId
    }
    this.documentAPi.getPropertyDocumentList(data).subscribe((response: any) => {

      console.log("App Component : getPropertyDocumentList() : " + JSON.stringify(response));
      this.loader.hide();
      if (response.status == '200') {
        response.document.map((item: any) => {
          const extention = item.attachment.split('.');
          item['ext'] = (extention[extention.length - 1]).toLowerCase();
          item['width'] = 0;
        });
        this.propertyDocList = response.document;
      } else {
        this.propertyDocList = [];
      }
    }, (error: any) => {
      console.log(error);
      this.loader.hide();
    });
  }

  openOtherProjects() {
    this.otherProjects = !this.otherProjects;
    console.log(this.otherProjects);
  }

  goToPage(pageName: any, type: any, index: any) {
    if (type == "Edit") {
      const nId = {
        document_id: this.docList[index].nid,
        user_id: localStorage.getItem('userId')
      };
      this.loader.show();
      this.documentAPi.getDocumentData(nId).subscribe((response: any) => {
        if (response.status == '200') {
          response.data.map((item: any) => {
            const extention = item.attachment_file.split('.');
            item['ext'] = (extention[extention.length - 1]).toLowerCase();
          });
          this.navCtrl.push(pageName, { data: response.data });
        }
        this.loader.hide();
      }, (error: any) => {
        this.toast.show('Please try again');
        this.loader.hide();
      });
    } else if (type == 'download') {
      this.storageProvider.getpermitionAndroid().then((entry) => {
        this.download(index, pageName);
      }, (error: any) => {
        this.toast.show('Storage Permission is needed to download the file');
      });
    } else {
      this.navCtrl.push(pageName, { data: '' });
    }
  }

  openInApp(index: any) {
    this.storageProvider.openInAppBrowser(this.docList[index].attachment_file);
  }

  download(index: any, downloadType: string) {
    let fileName, fileUrl;
    if (downloadType == 'KYCDoc') {
      this.docList[index].width = 1;
      fileName = this.docList[index].attachment_file.split('/');
      fileUrl = this.docList[index].attachment_file;
    } else {
      this.propertyDocList[index].width = 1;
      fileName = this.propertyDocList[index].attachment.split('/');
      fileUrl = this.propertyDocList[index].attachment;
    }
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(fileUrl, this.file.externalRootDirectory + fileName[fileName.length - 1]).then((entry) => {
      this.toast.show("File Downloaded in " + entry.filesystem.name + entry.fullPath);
    }, (error: any) => {
      this.toast.show(error);
    });
    fileTransfer.onProgress((progress: any) => {
      //setTimeout(() => {
      const downProgress = ((progress.loaded / progress.total) * 100).toFixed(2);
      this.docList[index].width = parseInt(downProgress);
      if (this.docList[index].width == 100) {
        this.docList[index].width = 0;
      }
      //}, 10);
    });
  }

}