import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, Events } from 'ionic-angular';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';
import { DbProvider } from '../../providers/db/db';


/**
 * Generated class for the InventoryFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inventory-filter',
  templateUrl: 'inventory-filter.html',
})
export class InventoryFilterPage {
  passedId: null;
  public goalProgress: any; // declare your variables as a number
  public effortValue: number = 0;
  public icons: any;
  public typologyList: any;
  public towerList: any;
  public facing: any;
  public carpetList: any;
  public availableList: any;
  public floorList: any;
  public maxval: any;
  public minval: any;
  public responseData: any;
  public lower: any;
  public upper: any;
  public step: any;
  public carpetArea: any;
  public carpetAreaChange: any;
  public disableRange: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalController: ModalController,
    public viewCtrl: ViewController,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public storageProvider: DbProvider,
    public events: Events,
  ) {

    this.events.subscribe('closeInventoryModal', () => {
      this.myDismiss('d');
    });

    if (this.navParams.get('responseData')) {
      const data = this.navParams.get('responseData');

      if (typeof data.data.budget_data.min != 'undefined' && data.data.budget_data.min != null
        && typeof data.data.budget_data.max != 'undefined' && data.data.budget_data.max != null) {

        data.budget_data = {
          min: parseInt(data.data.budget_data.min),
          max: parseInt(data.data.budget_data.max)
        };
      } else {
        this.responseData.budget_data = {
          lower: 0,
          upper: 0
        };
        this.step = 0;
      }
      /* const lowerData=this.storageProvider.getSearchCurrency(data.budget_data.min);
      if(lowerData.split(' ')[1] == "Lakh"){
        this.step = 500000;
      } else if(lowerData.split(' ')[1] == "Crore") {
        this.step = 1000000;
      } else {
        this.step = 10000;
      } */
      this.responseData = data;
      this.typologyList = this.responseData.data.typology_term_menu;
      this.towerList = this.responseData.data.tower;
      this.facing = this.responseData.data.facing;

      let carpetAreaArr = [];
      if (this.responseData.data.carpet_area.length > 0) {
        this.responseData.data.carpet_area.map((item: any) => {
          let numbers;
          if (item.id % 1 > .5) {
            numbers = Math.ceil(Number(item.id));
          } else {
            numbers = parseInt(item.id);
          }
          carpetAreaArr.push(numbers);
        });
        /* this.carpetArea = {
          minValue: Math.min(...carpetAreaArr) - 1,
          maxValue: Math.max(...carpetAreaArr) + 1
        }; */
        this.carpetArea = {
          minValue: Math.min(...carpetAreaArr),
          maxValue: Math.max(...carpetAreaArr)
        };
        if (this.carpetArea.minValue > 0) {
          this.carpetArea.minValue = this.carpetArea.minValue - 1;
        }
        if (this.carpetArea.maxValue > 0) {
          this.carpetArea.maxValue = this.carpetArea.maxValue + 1;
        }
      } else {
        this.carpetArea = {
          minValue: 0,
          maxValue: 0
        };
      }

      this.carpetAreaChange = {
        lower: this.carpetArea.minValue,
        upper: this.carpetArea.maxValue
      };
      this.carpetList = this.responseData.data.carpet_area;
      this.floorList = this.responseData.data.floor;
      this.goalProgress = {
        lower: Number(this.responseData.data.budget_data.min),
        upper: Number(this.responseData.data.budget_data.max)
      };
      this.typologyList.map((item: any) => {
        item['selected'] = false;
      });
      this.towerList.map((item: any) => {
        item['selected'] = false;
      });
      this.facing.map((item: any) => {
        item['selected'] = false;
      });
      this.carpetList.map((item: any) => {
        item['selected'] = false;
      });
      this.floorList.map((item: any) => {
        item['selected'] = false;
      });
    } else if (!this.navParams.get('responseData') && localStorage.getItem('Selectedinventoryfiterdata')) {
      const data = JSON.parse(localStorage.getItem('Selectedinventoryfiterdata'));
      this.typologyList = data.type;
      this.towerList = data.tower;
      this.facing = data.facing;
      this.carpetList = data.carpet;
      this.floorList = data.floors;
      this.goalProgress = data.goalProgr;
      this.responseData = {
        budget_data: {
          min: this.goalProgress.lower,
          max: this.goalProgress.upper,
          setmin: data.setgolprogr.lower,
          setmax: data.setgolprogr.upper
        }
      }
    }
    this.change();

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad InventoryFilterPage');
    //this.passedId = this.navParams.get('userId');
  }
  myDismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }


  change() {
    const lowerData = this.storageProvider.getSearchCurrency(this.goalProgress.lower);
    if (lowerData) {
      if (lowerData.split(' ')[1] == "Lakh") {
        this.step = 500000;
      } else if (lowerData.split(' ')[1] == "Crore") {
        this.step = 1000000;
      } else {
        this.step = 10000;
      }
    } else {
      this.step = 500000;
    }
    this.lower = this.goalProgress.lower;
    this.upper = this.goalProgress.upper;
  }

  selectDiv(index, type) {
    if (type == "Typology") {
      this.typologyList[index].selected = (this.typologyList[index].selected) ? false : true;
    } else if (type == "Tower") {
      this.towerList[index].selected = (this.towerList[index].selected) ? false : true;

      const towerArr = this.towerList.filter(x => x.selected == true);
      const towerReArr = [];
      towerArr.map((item: any) => {
        towerReArr.push(item.id);
      });
      console.log("Tower Re Arr : " + towerReArr.toString());
      this.getFloorFilter(towerReArr.toString());

    } else if (type == "Facing") {
      this.facing[index].selected = (this.facing[index].selected) ? false : true;
    } else if (type == "Carpet") {
      this.carpetList[index].selected = (this.carpetList[index].selected) ? false : true;
    } else if (type == "Available") {
      this.availableList[index].selected = (this.availableList[index].selected) ? false : true;
    } else if (type == "Floor") {
      /* let findIndex=this.floorList.findIndex(x => x.selected == true);
      (findIndex > -1 ) ? this.floorList[findIndex].selected = false : ''; */
      this.floorList[index].selected = (this.floorList[index].selected) ? false : true;
    }
  }

  getFilter() {
    const typologyArr = this.typologyList.filter(x => x.selected == true);
    const towerArr = this.towerList.filter(x => x.selected == true);
    const facingArr = this.facing.filter(x => x.selected == true);
    const carpetArr = this.carpetList.filter(x => x.selected == true);
    const floor = this.floorList.filter(x => x.selected == true);
    const typoArr = [];
    const towerReArr = [];
    const facingReArr = [];
    const carpetResArr = [];
    const floorResArr = [];

    typologyArr.map((item: any) => {
      typoArr.push(item.id);
    });
    towerArr.map((item: any) => {
      towerReArr.push(item.id);
    });
    facingArr.map((item: any) => {
      facingReArr.push(item.id);
    });
    carpetArr.map((item: any) => {
      carpetResArr.push(item.id);
    });
    floor.map((item: any) => {
      floorResArr.push(item.id);
    });
    let payloadData = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      typology: typoArr.toString(),
      tower: towerReArr.toString(),
      facing: facingReArr.toString(),
      carpet_area: carpetResArr.toString(),
      floor: floorResArr.toString(),
      min_price: this.goalProgress.lower,
      max_price: this.goalProgress.upper,
      min_carpetArea: this.carpetAreaChange.lower,
      max_carpetArea: this.carpetAreaChange.upper,
      page: 0
    };
    /*  const data = {
       type: this.typologyList,
       tower: this.towerList,
       facing: this.facing,
       carpet: this.carpetList,
       floors: this.floorList,
       goalProgr: {
         lower: Number(this.responseData.budget_data.min),
         upper: Number(this.responseData.budget_data.max),
         setlower: this.goalProgress.lower,
         setuper: this.goalProgress.upper,
       }
     } */
    // localStorage.setItem('Selectedinventoryfiterdata', JSON.stringify(data));
    // console.log(payloadData);
    this.myDismiss(payloadData);
  }

  getFloorFilter(towerID) {
    this.loader.show('Please wait...');
    const data = {
      user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
      device_id: (localStorage.getItem('userId') == null) ? this.storageProvider.getDeviceInfo().uuid : '',
      project_id: this.navParams.get('projectId'),
      tower: towerID
    };
    this.projectdetailsApi.getinventoryfiterdata(data).subscribe((response: any) => {
      this.loader.hide();
      this.floorList = [];
      if (response.status == '200') {
        this.floorList = response.data.floor;
      }
    }, (error: any) => {
      console.log(error);
      this.myDismiss();
      this.loader.hide();
    });
  }
}
