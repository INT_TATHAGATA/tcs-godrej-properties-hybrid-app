import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventoryFilterPage } from './inventory-filter';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

@NgModule({
  declarations: [
    InventoryFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryFilterPage),
  ],
  providers: [ProjectDetailsApiProvider]
})
export class InventoryFilterPageModule { }
