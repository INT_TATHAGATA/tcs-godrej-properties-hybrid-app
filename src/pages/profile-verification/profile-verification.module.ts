import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileVerificationPage } from './profile-verification';
import { AuthProvider } from '../../providers/apis/auth';
import { MyProfileApiProvider } from '../../providers/apis/myprofile';

@NgModule({
  declarations: [
    ProfileVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileVerificationPage),
  ],
  providers: [MyProfileApiProvider, AuthProvider]
})
export class ProfileVerificationPageModule { }
