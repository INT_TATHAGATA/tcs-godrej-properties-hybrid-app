import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/apis/auth';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
import { MyProfileApiProvider } from '../../providers/apis/myprofile';

/**
 * Generated class for the ProfileVerificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-verification',
  templateUrl: 'profile-verification.html',
})
export class ProfileVerificationPage implements OnInit {

  public dataParams: any;
  public otpMsg: any = "OTP Successfully Sent To Your Email";
  public verifyForm: FormGroup;
  public previousPage: string = "";
  public registerData: any;
  public otpNotCorrect: boolean = false;
  public deviceinfo: any;
  public errorMsg: any;

  public isDisableResend = true;

  public timeLeft: number;
  public interval;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public storageProvider: DbProvider,
    public events: Events,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    public viewCtrl: ViewController,
    private toast: ToasterProvider,
    public profileApi: MyProfileApiProvider,
  ) {

    this.events.subscribe('closeProfileVerificationPage',()=>{
      this.myDismiss();
    });

    this.dataParams = this.navParams.data.dataParams;

    this.errorMsg = "";
    this.timeLeft = 15;
    this.delayResend();
    console.log(JSON.stringify(this.dataParams));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileVerificationPage');
  }

  ngOnInit() {
    this.verifyForm = this.fb.group({
      first: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      second: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      third: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      fourth: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ])
    });
  }

  checkAndMove(event, element, input, nextEl, previousEle, prEL) {
    if (event.key == 'Backspace' || event.key == 'Unidentified') {
      if (prEL) {
        previousEle.focus();
      }
    }
    if (event.target.value.toString().length > 1) {
      const previousdata = event.target.value.toString().slice(-1);
      this.verifyForm.get(input).setValue(parseInt(previousdata));
      console.log(this.verifyForm.value);
    }
    if (this.verifyForm.controls[input].status == "VALID") {
      if (nextEl) {
        element.focus();
      }
    } else {
      console.log(this.verifyForm.value);
    }
  }

  delayResend() {
    this.interval = setInterval(() => {
      if (this.timeLeft == 1) {
        clearInterval(this.interval);
        this.isDisableResend = !this.isDisableResend;
      } else if (this.timeLeft > 1) {
        this.timeLeft--;
      } else {
        this.timeLeft = 15;
      }
    }, 1000)
  }

  myDismiss() {
    this.viewCtrl.dismiss();
  }

  verifyOtp() {
    let jsonData = {
      device_id: '',
      user_id: localStorage.getItem('userId'),
      otp: this.verifyForm.value.first.toString() + this.verifyForm.value.second.toString() + this.verifyForm.value.third.toString() + this.verifyForm.value.fourth.toString()
    };

    this.loader.show();
    this.Authapi.postVerifyOtp(jsonData).subscribe((response: any) => {
      if (response.status == '200') {
        let payload = {
          type: "email",
          user_id: localStorage.getItem('userId'),
          operation: "verify"
        };
        this.profileApi.verifyEmailOtp(payload).subscribe((response: any) => {
          this.loader.hide();
          if (response.status == '200') {
            this.viewCtrl.dismiss("success");
          }
          this.toast.show(response.msg);
        }, (error: any) => {
          this.loader.hide();
          this.viewCtrl.dismiss();
        });
      } else if (response.status == '502') {
        this.otpNotCorrect = true;
        this.errorMsg = response.msg;
        this.loader.hide();
      }
      this.loader.hide();
      /* this.toast.show(response.msg); */
    }, (error: any) => {
      this.loader.hide();
      this.viewCtrl.dismiss();
    });
  }

  resendOtp() {
    if (!this.isDisableResend) {
      this.isDisableResend = !this.isDisableResend;
      this.timeLeft = 15;
      this.delayResend();
      const data = {
        type: this.dataParams.type,
        user_id: (localStorage.getItem('userId')) ? localStorage.getItem('userId') : '',
        operation: 'send_otp',
        first_time: ""
      };

      this.loader.show();
      this.profileApi.updateEmailMobile(data, 'E').subscribe((response: any) => {
        this.loader.hide();
        if (response.status == '200') {
          this.otpNotCorrect = false;
          this.toast.show(this.otpMsg);
        } else {
          this.viewCtrl.dismiss();
        }
      }, (error: any) => {
        this.loader.hide();
        this.viewCtrl.dismiss();
      });
    }
  }

}
