import { Component, ViewChild } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams, Slides } from 'ionic-angular';
//import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { ToasterProvider } from '../../providers/utils/toast';
/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  public showSkip: boolean = true;
  @ViewChild('slides') slides: Slides;
  private _ionViewDidEnter_is_complete: boolean;
  public ciltyList: any = [];
  public typologyList: any = [];
  public ready: any = {
    wait: true,
    move: false
  };


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public storagePrivider: DbProvider,
    private toast: ToasterProvider,
  ) {
  }

  ionViewDidLoad() {
    this._ionViewDidEnter_is_complete = true;
    this.menu.enable(false);
    this.ready = {
      wait: true,
      move: false
    };
  }

  ngOnInit(): void {
    this.ciltyList = [
      { id: 1, name: "Pune", selected: false },
      { id: 2, name: "Mumbai", selected: false },
      { id: 3, name: "Delhi", selected: false },
      { id: 4, name: "Kolkata", selected: false },
      { id: 5, name: "Begaluru", selected: false },
    ];

    this.typologyList = [
      { id: 1, name: "2 Bhk", selected: false },
      { id: 2, name: "3 Bhk", selected: true },
      { id: 3, name: "3+ Bhk", selected: false },
      { id: 4, name: "4 Bhk", selected: false },
      { id: 5, name: "5 Bhk", selected: false },
    ];
    //this.slides.ionSlideProgress.subscribe(progress => this.onSliderProgress(progress));
    //this.slides.ionSlideDidChange.subscribe(data => this.onSliderProgress(data));

  }

  onSliderProgress(progress) {
    /* if (this._ionViewDidEnter_is_complete && this.slides.getActiveIndex() == 0) {
        console.log('% progress: ' + progress);
        // do work here...
    } */
  }

  nextSlide() {
    this.slides.slideNext();
  }

  startApp() {
    localStorage.setItem("isTutorial", "1");
    this.navCtrl.setRoot('HomePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  onSlideChangeStart(slider: any) {
    /* debugger;
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
    this.showSkip = !slider.isEnd(); */
  }


  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  select(index: any, type: any, extra: any) {
    if (type == "city") {
      let findSelected = this.ciltyList.findIndex(X => X.selected == true)
      if (findSelected > -1) {
        this.ciltyList[findSelected].selected = false;
      }
      this.ciltyList[index].selected = true;

    } else if (type == "typology") {
      let findSelected = this.typologyList.findIndex(X => X.selected == true)
      if (findSelected > -1) {
        this.typologyList[findSelected].selected = false;
      }
      this.typologyList[index].selected = true;
    } else if (type == "ready") {
      (extra == "wait") ? this.ready = {
        wait: true,
        move: false
      } : this.ready = {
        wait: false,
        move: true
      };
    }

    this.nextSlide();

  }

  goToPage(page: any) {
    this.navCtrl.push(page);
  }

  getLocation() {

    this.storagePrivider.getLocation().then((coOrd: any) => {
      let lat = coOrd.coords.latitude;
      let long = coOrd.coords.longitude;
      this.storagePrivider.getReverseGeoCode(lat, long).then((result: any) => {
        console.log(result);
        let findCityNAme = this.ciltyList.findIndex(x => x.name == result.locality);
        if (findCityNAme > -1) {
          let findSelected = this.ciltyList.findIndex(x => x.selected == true);
          if (findSelected > -1) {
            this.ciltyList[findSelected].selected = false;
          }
          this.ciltyList[findCityNAme].selected = true;

        } else {
          this.toast.show('Please select your prefered city.');
        }
        this.nextSlide();
        //result.locality
      }, (err) => {
        console.log(err);
      }).catch((error) => {
        console.log(error);
      });
    }, (err) => {
      console.log(err);
    }).catch((error) => {
      console.log(error);
    });
  }

}
