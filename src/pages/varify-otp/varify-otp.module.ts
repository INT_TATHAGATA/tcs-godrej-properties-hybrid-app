import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VarifyOtpPage } from './varify-otp';
import { AuthProvider } from '../../providers/apis/auth';

@NgModule({
  declarations: [
    VarifyOtpPage,
  ],
  imports: [
    IonicPageModule.forChild(VarifyOtpPage),
  ],
  providers: [AuthProvider]
})
export class VarifyOtpPageModule { }
