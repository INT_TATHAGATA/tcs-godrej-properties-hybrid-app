import { Component, OnInit, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { AuthProvider } from '../../providers/apis/auth';
import { LoadingProvider } from '../../providers/utils/loader';
import { ToasterProvider } from '../../providers/utils/toast';
/**
 * Generated class for the VarifyOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-varify-otp',
  templateUrl: 'varify-otp.html',
})
export class VarifyOtpPage implements OnInit {
  public verifyForm: FormGroup;
  public previousPage: string = "";
  public registerData: any;
  public otpNotCorrect: boolean = false;
  public deviceinfo: any;
  public errorMsg: any;
  public otpMsg: any = "";

  public isDisableResend = true;

  public timeLeft: number;
  public interval;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public storageProvider: DbProvider,
    public events: Events,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    private ngZone: NgZone,
  ) {
    this.errorMsg = "";
    this.timeLeft = 15;
    this.delayResend();
    this.registerData = this.navParams.get("formData");
    this.otpMsg = this.registerData.responseMsg;
  }

  ionViewWillEnter() {
    this.events.subscribe('getOTPAutoReadValue', (otpValue: any) => {
      this.ngZone.run(() => {
        const otpMSG = JSON.parse(JSON.stringify(otpValue));
        try {
          let otpMsg = otpMSG.Message;
          let sptitOTPMsg = otpMsg.split(" ");
          this.verifyForm = this.fb.group({
            first: new FormControl(sptitOTPMsg[6].toString().substring(0, 1), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ]),
            second: new FormControl(sptitOTPMsg[6].toString().substring(1, 2), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ]),
            third: new FormControl(sptitOTPMsg[6].toString().substring(2, 3), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ]),
            fourth: new FormControl(sptitOTPMsg[6].toString().substring(3, 4), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ])
          });
        } catch (error) {
          console.log(error);
        }
      });
    });
  }

  ionViewDidLoad() {
    // debugger
    console.log('ionViewDidLoad VarifyOtpPage');
  }

  ngOnInit() {
    //debugger
    this.previousPage = this.navCtrl.getPrevious().id;
    this.deviceinfo = this.storageProvider.getDeviceInfo();
    this.verifyForm = this.fb.group({
      first: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      second: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      third: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      fourth: new FormControl('', [
        Validators.required,
        // Validators.minLength(1),
        Validators.maxLength(1),
        Validators.pattern(this.storageProvider.numberOnly())
      ]),
      // fifth: new FormControl('', [
      //   Validators.required,
      //   // Validators.minLength(1),
      //   Validators.maxLength(1),
      //   Validators.pattern(this.storageProvider.numberOnly())
      // ]),
      // sixth: new FormControl('', [
      //   Validators.required,
      //   // Validators.minLength(1),
      //   Validators.maxLength(1),
      //   Validators.pattern(this.storageProvider.numberOnly())
      // ])
    });

   // this.startOTPWatching();
  }

  getApplicationHASHKey() {
    /*  var appHash = {
       getAppHash: function () {
         window['cordova']['plugins']['smsRetriever']['getAppHash'](
           (result) => {
             // Once you get this hash code of your app. Please remove this code.
             console.log('Varify Hash : ', result);
           },
           (err) => {
             console.log(err);
           }
         );
       }
     };
     appHash.getAppHash();
  */
  }

  startOTPWatching() {
    console.log("Start Watching");
    window['cordova']['plugins']['smsRetriever']['startWatching']((result) => {
      this.ngZone.run(() => {
        const otpMSG = JSON.parse(JSON.stringify(result));
        try {
          let otpMsg = otpMSG.Message;
          let sptitOTPMsg = otpMsg.split(" ");
          this.verifyForm = this.fb.group({
            first: new FormControl(sptitOTPMsg[6].toString().substring(0, 1), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ]),
            second: new FormControl(sptitOTPMsg[6].toString().substring(1, 2), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ]),
            third: new FormControl(sptitOTPMsg[6].toString().substring(2, 3), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ]),
            fourth: new FormControl(sptitOTPMsg[6].toString().substring(3, 4), [
              Validators.required,
              Validators.maxLength(1),
              Validators.pattern(this.storageProvider.numberOnly())
            ])
          });
        } catch (error) {
          console.log(error);
        }
      });
    }, (error) => {
      console.log("OTP Auto Read Error : " + error);
    });
  }

  checkAndMove(event, element, input, nextEl, previousEle, prEL) {
    if (event.key == 'Backspace' || event.key == 'Unidentified') {
      /* this.verifyForm.get(input).patchValue({input:''}); */
      /* this.verifyForm.patchValue({
        input:''
      }); */
      if (prEL) {
        previousEle.focus();

      }
    }
    if (event.target.value.toString().length > 1) {
      const previousdata = event.target.value.toString().slice(-1);
      this.verifyForm.get(input).setValue(parseInt(previousdata));
      console.log(this.verifyForm.value);
    }
    if (this.verifyForm.controls[input].status == "VALID") {
      if (nextEl) {
        element.focus();
      }
    } else {
      console.log(this.verifyForm.value);
      //this.verifyForm.get(input).setValue("");
    }
  }

  checkVerification() {
    /* this.otpNotCorrect = false; */
    this.errorMsg = "";
    let otp = this.verifyForm.value.first.toString() + this.verifyForm.value.second.toString() + this.verifyForm.value.third.toString() + this.verifyForm.value.fourth.toString();
    let uid = '';
    let deviceId = '';
    if (this.previousPage == "ForgotPasswordPage") {
      uid = this.registerData.uid;
    } else {
      deviceId = this.registerData.device_id;
    }
    let data = {
      device_id: deviceId,
      user_id: uid,
      otp: otp
    }
    this.loader.show();

    this.Authapi.postVerifyOtp(data).subscribe((response: any) => {
      this.loader.hide();

      if (response.status == '200') {
        if (this.previousPage == "ForgotPasswordPage") {
          /* Go to Reset password */
          const postData = {
            otp: this.registerData.otp,
            uid: this.registerData.uid
          };
          this.navCtrl.push("ResetPasswordPage", { formData: postData });
        } else {
          /* Submit Registration Data */
          let postData = {
            first_name: this.registerData.first_name,
            last_name: this.registerData.last_name,
            email: this.registerData.email,
            mob_no: this.registerData.mob_no,
            password: this.registerData.password,
            device_type: this.registerData.device_type,
            device_id: this.deviceinfo.uuid,
            user_id: this.registerData.user_id
          };

          this.loader.show('Please wait...');
          this.Authapi.postRegistrationapi(postData).subscribe((response: any) => {
            this.loader.hideNoTimeout();
            if (response.status == "200") {
              localStorage.setItem('isGuest', "0");
              let data = { title: 'Godrej Home', component: 'GodrejHomePage' };
              localStorage.setItem('isCustomer', '0');
              this.events.publish('afterLoginRedirect', data);
              localStorage.setItem('userData', JSON.stringify(response));
              localStorage.setItem('isLoggedIn', "1");
              this.events.publish('setUserDetails');
              localStorage.setItem('token', response.token);
              localStorage.setItem('Cookie', response.sessionName + '=' + response.sessionId);
              localStorage.setItem('Authorization', btoa(this.registerData.mob_no + '/' + this.registerData.password));
              localStorage.setItem('userId', response.data.userid);
              if (localStorage.getItem('deviceToken') != "") {
                this.events.publish('updateDeviceToken');
              } else {
                this.events.publish('getDeviceToken');
              }
              if (response.data.is_customer == '1') {
                this.events.publish('getSubmenu');
              }
            } else {
              this.loader.hideNoTimeout();
              this.toast.show(response.msg);
              this.navCtrl.popToRoot();
            }
          }, (error: any) => {
            this.loader.hideNoTimeout();
            console.log(error);
            this.navCtrl.popToRoot();
            this.toast.show('Some Thing Went Wrong, Please Try Again After Some Time.');
          });
        }
      } else if (response.status == '502') {
        this.otpNotCorrect = true;
        this.errorMsg = response.msg;
        this.loader.hideNoTimeout();
        this.ngOnInit();
      } else if (response.status == '507') {
        this.otpNotCorrect = true;
        this.errorMsg = response.msg;
        this.verifyForm.disable();
        this.ngOnInit();

        this.loader.hideNoTimeout();
      }
    }, (error: any) => {
      this.errorMsg = "";
      this.loader.hide();
      this.navCtrl.popToRoot();
      this.toast.show('Some Thing Went Wrong, Please Try Again After Some Time.');
    });
  }

  resendOtp() {
    if (!this.isDisableResend) {
      this.isDisableResend = !this.isDisableResend;
      this.timeLeft = 15;     
      this.otpNotCorrect = false; 
      this.verifyForm.enable();
      this.delayResend();
      this.startOTPWatching();
      if (this.previousPage == "ForgotPasswordPage") {
        this.loader.show('Please wait...');
        let postData;

        if (this.registerData.postd.type == 'pan') {
          postData = {
            type: this.registerData.postd.type,
            username: this.registerData.postd.username,
            first_time: "",
            email: this.registerData.postd.email,
            mobile: this.registerData.postd.mobile,
            device_type: "android"
          };
        } else {
          postData = {
            type: this.registerData.postd.type,
            username: this.registerData.postd.username,
            first_time: "",
            device_type: "android"
          };
        }
        this.Authapi.postForgotPasswordApi(postData).subscribe((response: any) => { /* this.registerData.postd */
          this.loader.hide();
          if (response.status == "200") {
            this.registerData.response_otp = response.otp;
          }
          this.toast.show(response.msg)
        }, (error: any) => {
          this.loader.hide();
          this.navCtrl.popToRoot();
          this.toast.show('Some Thing Went Wrong, Please Try Again After Some Time.');
        });
      } else {
        let data = {
          mobile_no: this.registerData.mob_no,
          email: this.registerData.email,
          first_name: this.registerData.first_name,
          last_name: this.registerData.last_name,
          req_type: this.registerData.req_type,
          req_cont: this.registerData.req_cont,
          req_name: this.registerData.req_name,
          user_id: this.registerData.user_id,
          first_time: "",
          device_id: this.deviceinfo.uuid,
          device_type: "android"
        };
        this.loader.show('Please wait...');
        this.Authapi.postRegisterOtpApi(data).subscribe((response: any) => {
          this.loader.hide();
          if (response.status == "200") {
            let postData = {
              first_name: this.registerData.first_name,
              last_name: this.registerData.last_name,
              email: this.registerData.email,
              mob_no: this.registerData.mob_no,
              password: this.registerData.password,
              device_type: this.registerData.device_type,
              device_id: this.registerData.device_id,
              response_otp: response.otp,
              req_type: this.registerData.req_type,
              req_cont: this.registerData.req_cont,
              req_name: this.registerData.req_name,
              user_id: this.registerData.user_id,
              first_time: "",
            };
            this.registerData = postData;
            this.toast.show(response.msg)
          } else {
            this.loader.hide();
            this.navCtrl.popToRoot();
            this.toast.show('Some Thing Went Wrong, Please Try Again After Some Time.');
          }
        }, (error: any) => {
          this.loader.hide();
          this.navCtrl.popToRoot();
          this.toast.show('Some Thing Went Wrong, Please Try Again After Some Time.');
        });
      }
    }
  }

  delayResend() {
    this.interval = setInterval(() => {
      if (this.timeLeft == 1) {
        clearInterval(this.interval);
        this.isDisableResend = !this.isDisableResend;
      } else if (this.timeLeft > 1) {
        this.timeLeft--;
      } else {
        this.timeLeft = 15;
      }
    }, 1000)
  }

}
