import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, Slides } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Events, PopoverController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { DashordApiProvider } from '../../providers/apis/dashbord';
import { LoadingProvider } from '../../providers/utils/loader';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { ToasterProvider } from '../../providers/utils/toast';
import { AppConst } from '../../app/app.constants';

/**
 * Generated class f  or the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  // noti:boolean=true;
  // heart:boolean=true;
  // pin:boolean=true
  // menuIcon:boolean=true;
  // image:boolean=false;
  // text:boolean=false;
  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  mail: boolean = false;
  menuIcon: boolean = true;
  image: boolean = true;
  text: boolean = true;
  subHeading: boolean = false;
  subHeadingText: string = "";
  IndividualPropertyText: string = 'All Properties';

  public makeWishList: boolean = false;
  public searchListArr: any = [];
  public addModal: any;

  countries: string[];
  errorMessage: string;
  public username: any;
  public popOverDiv: boolean = false;
  public initHide: boolean = false;
  public otherProjects: boolean = false;
  public rmCallFeature: any;
  public accountSummarydata: any;
  bookingitems: any;

  public isEnablePayNow: boolean = true;

  @ViewChild(Slides) slides: Slides;

  public profileDetails: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private socialSharing: SocialSharing,
    public modalCtrl: ModalController,
    public events: Events,
    public DashbordApi: DashordApiProvider,
    public loadingCtrl: LoadingController,
    public storageProvider: DbProvider,
    public popoverCtrl: PopoverController,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
  ) {
    this.accountSummarydata = {};
    this.bookingitems = [];

    this.getUserDetails();
  }

  ionViewDidLoad() {
    this.storageProvider.getUserDetails().then((userdata: any) => {
      this.username = userdata.userDetails;
    }).catch((error) => {
      console.log(error);
    })

    this.searchListArr = [];
    this.menu.swipeEnable(true);
    let cityList = [
      { name: "New Town", selected: false },
      { name: "Salt Lake", selected: true },
      { name: "Tollygaunge", selected: false },
      { name: "Ballygaunge", selected: false },
    ];
    let possesionList = [
      { name: "New Launch", selected: false },
      { name: "Ready To Move", selected: true },
      { name: "Under Construction", selected: false },
      { name: "Within 6 Months", selected: false }
    ];
    let typologyList = [
      { name: "2 Bhk", selected: false },
      { name: "3 Bhk", selected: true },
      { name: "3 Bhk+", selected: false },
      { name: "4 Bhk", selected: false },
    ]
    let price = {
      lower: "35",
      upper: "45"
    };
    this.storageProvider.setcityList(cityList);
    this.storageProvider.setPossessionList(possesionList);
    this.storageProvider.setTypologyList(typologyList);
    this.storageProvider.setPrice(price);

    this.afterInitCall();
  }

  afterInitCall() {
    this.loader.show('Please wait');
    Observable.forkJoin([
      this.getRmInfo(),
      this.getAccountSummary()
    ]).subscribe((result: any) => {
      console.log(result);
      this.loader.hide();
      if (result[0].status == '200') {
        this.rmCallFeature = result[0].data;

        this.rmCallFeature.map((item: any) => {
          item["isPopupShow"] = false
        });

        console.log("rmCallFeature : " + JSON.stringify(this.rmCallFeature));
      }
      if (result[1].status == '200') {
        let returnedData = result[1].data;
        if (returnedData.total_billed % 1 != 0) {
          const rupee = returnedData.total_billed.split('.');
          result[1].data['total_billed_rupee'] = rupee[0];
          result[1].data['total_billed_paisa'] = rupee[1];
        } else if(returnedData.total_billed % 1 == 0){
          result[1].data['total_billed_rupee'] = "00";
          result[1].data['total_billed_paisa'] = "00";
        }

        if (returnedData.last_billed % 1 != 0) {
          const rupee = returnedData.last_billed.split('.');
          result[1].data['last_billed_rupee'] = rupee[0];
          result[1].data['last_billed_paisa'] = rupee[1];
        } else if (returnedData.last_billed % 1 == 0) {
          result[1].data['last_billed_rupee'] = "00";
          result[1].data['last_billed_paisa'] = "00";
        }
        
        if (returnedData.total_overdue % 1 != 0) {
          const rupee = returnedData.total_overdue.split('.');
          result[1].data['total_overdue_rupee'] = rupee[0];
          result[1].data['total_overdue_paisa'] = rupee[1];
        } else if(returnedData.total_overdue % 1 == 0){
          result[1].data['total_overdue_rupee'] = "00";
          result[1].data['total_overdue_paisa'] = "00";
        }

        

        if (returnedData.total_payable % 1 != 0) {
          const rupee = returnedData.total_payable.split('.');
          result[1].data['total_payable_rupee'] = rupee[0];
          result[1].data['total_payable_paisa'] = rupee[1];
        } else if(returnedData.total_payable % 1 == 0){
          result[1].data['total_payable_rupee'] = "00";
          result[1].data['total_payable_paisa'] = "00";
        }

        if (returnedData.total_received % 1 != 0) {
          const rupee = returnedData.total_received.split('.');
          result[1].data['total_received_rupee'] = rupee[0];
          result[1].data['total_received_paisa'] = rupee[1];
        } else if (returnedData.total_received % 1 == 0) {
          result[1].data['total_received_rupee'] = "00";
          result[1].data['total_received_paisa'] = "00";
        }

        

        if (returnedData.last_payment % 1 != 0) {
          const rupee = returnedData.last_payment.split('.');
          result[1].data['last_payment_rupee'] = rupee[0];
          result[1].data['last_payment_paisa'] = rupee[1];
        } else if (returnedData.last_payment % 1 == 0) {
          result[1].data['last_payment_rupee'] = "00";
          result[1].data['last_payment_paisa'] = "00";
        }

        

        if (returnedData.total_payable % 1 != 0) {
          const rupee = returnedData.total_payable.split('.');
          result[1].data['total_payable_rupee'] = rupee[0];
          result[1].data['total_payable_paisa'] = rupee[1];
        } else if (returnedData.total_payable % 1 == 0) {
          result[1].data['total_payable_rupee'] = "00";
          result[1].data['total_payable_paisa'] = "00";
        }

        this.accountSummarydata = result[1].data;
        this.bookingitems = result[1].data.booking_items;
      }
    }, (error) => {
      this.loader.hide();
      console.log('Error:::', error);
    });
  }

  getRmInfo() {
    const jsonData = {
      user_id: localStorage.getItem('userId')
    };
    return this.DashbordApi.getRmContacts(jsonData);
  }

  getAccountSummary() {
    const jsonData = {
      user_id: localStorage.getItem('userId')
    };
    return this.DashbordApi.getAccountSummary(jsonData);
  }

  getSingleAccountSummary(project) {
    if (project != '') {
      this.loader.show('Please wait...');
      const data = {
        user_id: localStorage.getItem('userId'),
        booking_id: project.booking_id
      }
      this.DashbordApi.getSingleAccountSummary(data).subscribe((response: any) => {
        console.log(response);
        this.loader.hide();
        if (response.status == AppConst.HTTP_SUCESS_STATUS.OK) {
          

        let returnedData = response.data;
        if (returnedData.total_billed % 1 != 0) {
          const rupee = returnedData.total_billed.split('.');
          response.data['total_billed_rupee'] = rupee[0];
          response.data['total_billed_paisa'] = rupee[1];
        } else if(returnedData.total_billed % 1 == 0){
          response.data['total_billed_rupee'] = "00";
          response.data['total_billed_paisa'] = "00";
        }

        if (returnedData.last_billed % 1 != 0) {
          const rupee = returnedData.last_billed.split('.');
          response.data['last_billed_rupee'] = rupee[0];
          response.data['last_billed_paisa'] = rupee[1];
        } else if (returnedData.last_billed % 1 == 0) {
          response.data['last_billed_rupee'] = "00";
          response.data['last_billed_paisa'] = "00";
        }
        
        if (returnedData.total_overdue % 1 != 0) {
          const rupee = returnedData.total_overdue.split('.');
          response.data['total_overdue_rupee'] = rupee[0];
          response.data['total_overdue_paisa'] = rupee[1];
        } else if(returnedData.total_overdue % 1 == 0){
          response.data['total_overdue_rupee'] = "00";
          response.data['total_overdue_paisa'] = "00";
        }

        

        if (returnedData.total_payable % 1 != 0) {
          const rupee = returnedData.total_payable.split('.');
          response.data['total_payable_rupee'] = rupee[0];
          response.data['total_payable_paisa'] = rupee[1];
        } else if(returnedData.total_payable % 1 == 0){
          response.data['total_payable_rupee'] = "00";
          response.data['total_payable_paisa'] = "00";
        }

        

        if (returnedData.total_received % 1 != 0) {
          const rupee = returnedData.total_received.split('.');
          response.data['total_received_rupee'] = rupee[0];
          response.data['total_received_paisa'] = rupee[1];
        } else if (returnedData.total_received % 1 == 0) {
          response.data['total_received_rupee'] = "00";
          response.data['total_received_paisa'] = "00";
        }

        

        if (returnedData.last_payment % 1 != 0) {
          const rupee = returnedData.last_payment.split('.');
          response.data['last_payment_rupee'] = rupee[0];
          response.data['last_payment_paisa'] = rupee[1];
        } else if (returnedData.last_payment % 1 == 0) {
          response.data['last_payment_rupee'] = "00";
          response.data['last_payment_paisa'] = "00";
        }

        

        if (returnedData.total_payable % 1 != 0) {
          const rupee = returnedData.total_payable.split('.');
          response.data['total_payable_rupee'] = rupee[0];
          response.data['total_payable_paisa'] = rupee[1];
        } else if (returnedData.total_payable % 1 == 0) {
          response.data['total_payable_rupee'] = "00";
          response.data['total_payable_paisa'] = "00";
        }
        this.accountSummarydata = response.data;
        this.otherProjects = !this.otherProjects;
        this.IndividualPropertyText = project.project_code;
          this.isEnablePayNow = false;
        }
      }, (error: any) => {
        console.log(error);
        this.loader.hide();
      });
    } else {
      this.otherProjects = false
      this.IndividualPropertyText = 'All Properties';
      this.afterInitCall();
    }

  }

  getCountries() {
    let payload = [
      { "key": "email", "value": "subhendu.pal@indusnet.co.in", "description": "" },
      { "key": "password", "value": "1234", "description": "" },
      { "key": "devicetoken", "value": "test", "description": "" },
      { "key": "deviceid", "value": "1234", "description": "" },
      { "key": "devicetype", "value": "1", "description": "" }
    ];


  }

  goToPage(pageName: any, rmData: any) {
    if (pageName == "My Documents") {
      const page = {
        title: 'My Documents', component: 'MyDocumentsListPage'
      };
      this.events.publish('afterLoginRedirect', page)
    } else {
      if (pageName == "PropertyPaymentPage") {
        const findOpenPopUp = this.rmCallFeature.findIndex(x => x.isPopupShow == true);
        if (findOpenPopUp > -1) {
          this.rmCallFeature[findOpenPopUp].isPopupShow = false;
        }

        this.navCtrl.push(pageName, {
          bookingId: rmData.booking_id,
          subHeadingText: rmData.property_name
        });
      } else {
        this.navCtrl.push(pageName);
      }
    }
  }

  share() {
    let message = "message";
    let subject = "subject";
    let file = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png";
    let url = "https://ionicframework.com/docs/native/social-sharing/"

    this.socialSharing.share(message, subject, file, url).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }

  makeWish() {
    this.makeWishList = !this.makeWishList;
  }
  /* openSearch(value:any){
    
    if(value!="ND"){
      let params={};
      if(this.searchListArr.length == 0){
        localStorage.setItem("fromDashboard","N")
        this.storageProvider.setValue(this.searchListArr);
        params={
          type:"C",// R => Residential C => Commertial
          location: "Kolkata",
          cityLocality:this.storageProvider.getCityList(),
          possession:this.storageProvider.getPossessionList(),
          typology:this.storageProvider.getTypologyList(),
          price:this.storageProvider.getPrice()
        };
      } else {
        params=this.searchListArr;
      }
      this.addModal = this.modalCtrl.create("SearchPage",{ params: params });
    } else {
      localStorage.setItem("fromDashboard","Y")
      this.addModal = this.modalCtrl.create("SearchPage",{ });
    }

    this.addModal.onDidDismiss((item:any) => {

      if (item) {
        let pageName=this.navCtrl.getActive();
        if(pageName.name=="ProjectDetailsPage"){
          this.navCtrl.pop();
        }

        this.searchListArr=item;
      }
    });
    this.addModal.present();
  } */
  displayCounter(value: any) {
    console.log(value);
  }

  openPopOver(rmitem) {
    rmitem.isPopupShow = !rmitem.isPopupShow;
  }

  openClose() {
    this.initHide = !this.initHide;
  }

  openOtherProjects() {
    this.otherProjects = !this.otherProjects;
    console.log(this.otherProjects);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    const findOpenPopUp = this.rmCallFeature.findIndex(x => x.isPopupShow == true);
    if (findOpenPopUp > -1) {
      this.rmCallFeature[findOpenPopUp].isPopupShow = false;
    }
    console.log('Current index is', currentIndex);
  }
  closePopOverFromImage(rmData: any) {
    this.goToPage('PropertyPaymentPage', rmData);
    const findOpenPopUp = this.rmCallFeature.findIndex(x => x.isPopupShow == true);
    if (findOpenPopUp > -1) {
      this.rmCallFeature[findOpenPopUp].isPopupShow = false;
    }
  }

  getUserDetails() {
    const userDetails = JSON.parse(localStorage.getItem('userData'));
    this.profileDetails = userDetails;
  }

}