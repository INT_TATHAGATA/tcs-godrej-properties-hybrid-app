import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyDocumentsAddPage } from './my-documents-add';
import { ComponentsModule } from '../../components/components.module';
import { DocumentsApiProvider } from '../../providers/apis/documents';

@NgModule({
  declarations: [
    MyDocumentsAddPage,
  ],
  imports: [
    IonicPageModule.forChild(MyDocumentsAddPage),
    ComponentsModule
  ],
  providers: [DocumentsApiProvider]
})
export class MyDocumentsAddPageModule { }
