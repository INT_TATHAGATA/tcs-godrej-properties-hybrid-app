import { Component, OnInit, } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DbProvider } from '../../providers/db/db';
import { ToasterProvider } from '../../providers/utils/toast';
import { LoadingProvider } from '../../providers/utils/loader';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { AppConst } from '../../app/app.constants';
import { DocumentsApiProvider } from '../../providers/apis/documents';

/**
 * Generated class for the MyDocumentsAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-documents-add',
  templateUrl: 'my-documents-add.html',
})
export class MyDocumentsAddPage implements OnInit {
  noti: boolean = false;
  heart: boolean = false;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = false;
  subHeadingText: string = "";
  public disabled: boolean = true;
  public uploadDocuments: FormGroup;
  fileTransfer: FileTransferObject;
  public selectedData: any;
  public errorMsg: any;
  public pageData: any;
  public chooseFile: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    public storageProvider: DbProvider,
    public fb: FormBuilder,
    private transfer: FileTransfer,
    public documentAPi: DocumentsApiProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyDocumentsAddPage');
  }

  ngOnInit() {
    this.pageData = this.navParams.get('data');
    this.uploadDocuments = this.fb.group({
      docType: new FormControl((this.pageData == '') ? '' : this.pageData[0].document_type, [Validators.required]),
      docTitle: new FormControl((this.pageData == '') ? '' : this.pageData[0].title, [Validators.required]),
      docNo: new FormControl((this.pageData == '') ? '' : this.pageData[0].field_document_no, [Validators.required]),
      docFile: new FormControl((this.pageData == '') ? '' : this.pageData[0].attachment_file, [Validators.required])
    });
    this.selectedData = ''
    this.chooseFile = false;
    if (this.pageData != '') {
      this.selectedData = {
        localURL: this.pageData[0].attachment_file,
        mime: this.pageData[0].ext,
        uploadFilePath: this.pageData[0].attachment_file
      };
    }
  }

  get f() {
    return this.uploadDocuments.controls;
  }
  openDocs() {
    this.storageProvider.chooseFile().then((uri: any) => {
      this.errorMsg = '';
      this.selectedData = uri;
      this.chooseFile = true;
      this.uploadDocuments.patchValue({ docFile: uri });
      /* fileTransfer.onProgress((prog: any) => {
        console.log(prog);
      }); */
    }).catch((error: any) => {
      this.errorMsg = error;
    });

  }
  submit() {
    const url = AppConst.baseUrl + 'create_document_api.json';
    const dataObj = '';
    const fileTransfer: FileTransferObject = this.transfer.create();
    let jsonData;
    let fileName;
    if (this.pageData == '') {
      jsonData = {
        user_id: localStorage.getItem('userId'),
        title: this.uploadDocuments.value.docTitle,
        document_type: this.uploadDocuments.value.docType,
        document_no: this.uploadDocuments.value.docNo
      };
      fileName = 'myDocuments_' + new Date().getTime() + '.' + this.selectedData.extention
    } else {
      jsonData = {
        user_id: localStorage.getItem('userId'),
        title: this.uploadDocuments.value.docTitle,
        document_type: this.uploadDocuments.value.docType,
        document_no: this.uploadDocuments.value.docNo,
        document_id: this.pageData[0].nid
      };
      fileName = 'myDocuments_' + new Date().getTime() + '.' + this.selectedData.mime;
    }

    this.loader.show();
    if (this.chooseFile) {
      let options: FileUploadOptions = {
        fileKey: 'myDoc',
        fileName: fileName,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        headers: {
          'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
          'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
        },
        params: jsonData
      };
      fileTransfer.upload(this.selectedData.uploadFilePath, url, options).then((data: any) => {
        const responseData = JSON.parse(data.response);
        this.toast.show(responseData.msg);
        if (responseData.status == '200') {
          this.navCtrl.pop();
        }
        this.loader.hide();
      }).catch((err) => {
        console.log(err);
        this.loader.hide();
      });
    } else {
      this.documentAPi.createDocApi(jsonData).subscribe((response: any) => {
        this.toast.show(response.msg);
        if (response.status == '200') {
          this.navCtrl.pop();
        }
        this.loader.hide();
      }, (err: any) => {
        this.loader.hide();
      });
    }

  }

}
