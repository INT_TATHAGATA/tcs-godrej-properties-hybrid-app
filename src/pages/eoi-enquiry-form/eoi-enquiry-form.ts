import { Component, OnInit, ViewChild, Renderer, Input, ElementRef, NgZone, ViewChildren } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, ActionSheetController, Content, AlertController, Select, Navbar } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { DbProvider } from '../../providers/db/db';
import { ToasterProvider } from '../../providers/utils/toast';
import { LoadingProvider } from '../../providers/utils/loader';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';
import moment from 'moment';
import { Camera } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { AppConst } from '../../app/app.constants';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { File, FileEntry } from '@ionic-native/file';

/**
 * Generated class for the EoiEnquiryFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eoi-enquiry-form',
  templateUrl: 'eoi-enquiry-form.html',
})
export class EoiEnquiryFormPage implements OnInit {

  noti: boolean = true;
  heart: boolean = true;
  pin: boolean = false;
  menuIcon: boolean = false;
  image: boolean = false;
  text: boolean = true;
  subHeading: boolean = true;
  subHeadingText: string = "";

  public detailsForm: FormGroup;
  public isReadonly: boolean = true;
  public types: any[];
  public userDetails: any;
  public projectDetails: any;
  public previousPage: any;
  public maxDateDob: any;
  public countryList: any = [];
  public addModal: any;
  public countryCodes: any = [];
  public countryInitials: any;
  public flag: any;
  public findIndia: any;


  accordionExapanded = true;
  @ViewChild("cc") cardContent: any;
  @Input('title') title: string;

  icon: string = "substract.svg";

  @ViewChild(Content) rows: Content;
  @ViewChild('select') select: Select;
  public addApplicant: boolean = false;
  public imageObj: any = {
    primary: {
      idProof: '',
      addressProof: ''
    },
    secondary: {
      0: {
        idProof: '',
        addressProof: ''
      },
      1: {
        idProof: '',
        addressProof: ''
      }
    }
  };

  public accordionExapandedApp: any = {
    secondary: {
      0: {
        isExpand: false
      },
      1: {
        isExpand: false
      }
    }
  };

  public countryError: boolean = false;
  public readonly: boolean = false;
  public completed: boolean = true;
  public passpostPattern: any = this.storageProvider.passportPattern();
  public panPattern: any = this.storageProvider.panPattern();
  public imageError: boolean = false;
  public previousPageData: any;
  public paymentPlat: any;
  public projectInfoDetails: any;
  public termsData: any;
  public footerAccordian: any = {
    payment: false,
    terms: false,
    propertyDetails: false
  };
  public imageSizeErr: any;
  public deleteApplicant: boolean = false;
  public dateOB: boolean = false;
  public errrorPan: any = '';
  public errorPassport: any = '';
  public errrorApplicantPan: any = '';
  public errorApplicantPassport: any = '';
  public temsCheck: boolean = false;
  public termsError: any;
  public primaryIdFrontErr: string = '';
  public primaryIdBackErr: string = '';
  public mimeType: string;

  public bookingId: any;
  public applicantsArr: any;
  public count = 0;
  public detailsResponseData: any;
  public purposeData: any;
  public relationshipData: any;

  public isPrefilled: boolean = false;

  public isOpenModal: boolean = false;

  public actionSheet = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storageProvider: DbProvider,
    public fb: FormBuilder,
    private toast: ToasterProvider,
    private menu: MenuController,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private loader: LoadingProvider,
    public renderer: Renderer,
    public modalCtrl: ModalController,
    public camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    private ngZone: NgZone,
    private sanitizer: DomSanitizer,
    private iab: InAppBrowser,
    private transfer: FileTransfer,
    public alertCtrl: AlertController,
    public file: File,
    private fileOpener: FileOpener,
  ) {
    this.userDetails = JSON.parse(localStorage.getItem('userData'));
    this.menu.swipeEnable(false);
    this.projectDetails = this.navParams.get('details');
    this.subHeadingText = this.projectDetails.pName;
    this.countryList = this.storageProvider.getCountryCodes();
    this.bookingId = '';

    this.loader.show();
  }

  ionViewDidLoad() {
    this.maxDateDob = (moment().subtract(18, 'years')).format('YYYY-MM-DD');
  }

  ngOnInit() {

    this.previousPageData = this.navParams.get('details');
    this.previousPage = this.navCtrl.getPrevious().id;
    this.countryCodes = this.storageProvider.getCountryCodes();
    this.countryCodes.map((item: any) => {
      item["selected"] = false
    });

    const userData = JSON.parse(localStorage.getItem('userData'));

    let nation = '';
    if (userData.data.mob_no.substring(0, 3) == '+91') {
      nation = 'Indian';

      this.findIndia = this.countryCodes.findIndex(x => x.callingCodes === "+91");
      this.countryCodes[this.findIndia].selected = true;
      this.countryInitials = this.countryCodes[this.findIndia];
      this.flagChange(this.countryCodes[this.findIndia]);
    } else {
      nation = 'NRI';

      this.findIndia = this.countryCodes.findIndex(x => x.callingCodes === "+93");
      this.countryCodes[this.findIndia].selected = true;
      this.countryInitials = this.countryCodes[this.findIndia];
      this.flagChange(this.countryCodes[this.findIndia]);
    }

    if (userData.data.hasOwnProperty('birth_dt')) {
      if (userData.data.birth_dt != '') {
        this.dateOB = true;
      }
    }

    this.detailsForm = this.fb.group({
      nationality: new FormControl('', []),
      name: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.emailPattern())
      ]),
      dob: new FormControl((this.dateOB) ? userData.data.birth_dt : '', [
        Validators.required
      ]),
      permanentAddress: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ]),
      street: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ]),
      locality: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ]),
      country: new FormControl('', [
        Validators.required
      ]),
      city: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern()),
        Validators.maxLength(25)
      ]),
      pincode: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.numberOnly()),
        Validators.minLength(6),
        Validators.maxLength(6)
      ]),
      same: new FormControl(false, []),
      communicationHouseNo: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ]),
      communicationStreet: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ]),
      communicationLocality: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ]),
      communicationCountry: new FormControl('', [
        Validators.required
      ]),
      communicationCity: new FormControl('', [
        Validators.required,
        Validators.pattern(this.storageProvider.onlyAlphabetPattern()),
        Validators.maxLength(25)
      ]),
      communicationPincode: new FormControl('', [
        Validators.required
      ]),
      pan: new FormControl('', []),
      passport: new FormControl('', []),
      purposeOfPurchase: new FormControl('', [
        Validators.required
      ]),
      idProof: new FormControl('', []),
      addressProof: new FormControl('', []),
      idProofName: new FormControl('', []),
      addressProofName: new FormControl('', []),
      idProofMime: new FormControl('', []),
      addressProofMime: new FormControl('', []),
      applicantlist: this.fb.array([this.initPackage()])
    });

    this.deletePackageRow(0);
    this.addApplicant = true;

    this.detailsForm.patchValue({
      name: this.userDetails ? this.userDetails.data.first_name + ' ' + this.userDetails.data.last_name : '',
      mobile: this.userDetails ? this.userDetails.data.mob_no : '',
      email: this.userDetails ? this.userDetails.data.email : '',
      dob: userData.data.birth_dt
    });

    this.detailsForm.valueChanges.subscribe((v: any) => {
      this.completed = true;
      if (v.permanentAddress != '' &&
        v.street != '' &&
        v.locality != '' &&
        v.country != '' &&
        v.country != '' &&
        v.city != '' &&
        v.pincode != ''
      ) {
        this.completed = false;
      }

      if (this.detailsForm.value.same == true) {
        this.readonly = true;
        this.detailsForm.value.communicationHouseNo = this.detailsForm.value.permanentAddress;
        this.detailsForm.value.communicationStreet = this.detailsForm.value.street;
        this.detailsForm.value.communicationLocality = this.detailsForm.value.locality;
        this.detailsForm.value.communicationCountry = this.detailsForm.value.country;
        this.detailsForm.value.communicationCity = this.detailsForm.value.city;
        this.detailsForm.value.communicationPincode = this.detailsForm.value.pincode;
      } else {
        this.readonly = false;
      }

      if (this.detailsForm.value.applicantlist.length > 0) {
        this.errrorPan = '';
        this.errorPassport = '';
        this.errrorApplicantPan = '';
        this.errorApplicantPassport = '';
        for (let i = 0; i < this.detailsForm.value.applicantlist.length; i++) {
          if (this.detailsForm.value.applicantlist[i].applicantSameAddress) {
            this.detailsForm.value.applicantlist[i].applicantComuHouseNo = this.detailsForm.value.applicantlist[i].applicantPAddress;
            this.detailsForm.value.applicantlist[i].applicantComuStreet = this.detailsForm.value.applicantlist[i].applicantStreet;
            this.detailsForm.value.applicantlist[i].applicantComuLocality = this.detailsForm.value.applicantlist[i].applicantLocality;
            this.detailsForm.value.applicantlist[i].applicantComuCountry = this.detailsForm.value.applicantlist[i].applicantCounty;
            this.detailsForm.value.applicantlist[i].applicantComuCity = this.detailsForm.value.applicantlist[i].applicantCity;
            this.detailsForm.value.applicantlist[i].applicantComuPin = this.detailsForm.value.applicantlist[i].applicantPincode;
          }
          const suffics = (i == 0) ? '2nd' : '3rd';
          if (this.detailsForm.value.pan != null && this.detailsForm.value.pan != '' && (this.detailsForm.value.pan.toLocaleLowerCase() == this.detailsForm.value.applicantlist[i].appPan.toLocaleLowerCase())) {
            this.errrorPan = 'Primary Applicant & ' + suffics + ' Applicant Pan Number Is Same Please Change.';
          }
          if (this.detailsForm.value.passport != null && this.detailsForm.value.passport != '' && (this.detailsForm.value.passport.toLocaleLowerCase() == this.detailsForm.value.applicantlist[i].appPassport.toLocaleLowerCase())) {
            this.errorPassport = 'Primary Applicant & ' + suffics + ' Applicant Passport Number Is Same Please Change.';
          }
          if (i > 0) {
            if (this.detailsForm.value.applicantlist[0].appPan != '' &&
              this.detailsForm.value.applicantlist[i].appPan != '' &&
              (this.detailsForm.value.applicantlist[0].appPan.toLocaleLowerCase() == this.detailsForm.value.applicantlist[i].appPan.toLocaleLowerCase())) {
              this.errrorApplicantPan = 'Applicants Pan Number Can Not Be Same.';
            }
            if (this.detailsForm.value.applicantlist[0].appPassport != '' &&
              this.detailsForm.value.applicantlist[i].appPassport != '' &&
              this.detailsForm.value.applicantlist[0].appPassport.toLocaleLowerCase() == this.detailsForm.value.applicantlist[i].appPassport.toLocaleLowerCase()) {
              this.errorApplicantPassport = 'Applicants Passport Number Can Not Be Same.';
            }
          }
        }
      }
    });

    this.detailsForm.get('same').valueChanges.subscribe((v: any) => {
      this.readonly = false;
      if (v == true) {
        this.readonly = true;
        this.detailsForm.patchValue({
          communicationHouseNo: this.detailsForm.value.permanentAddress,
          communicationStreet: this.detailsForm.value.street,
          communicationLocality: this.detailsForm.value.locality,
          communicationCountry: this.detailsForm.value.country,
          communicationCity: this.detailsForm.value.city,
          communicationPincode: this.detailsForm.value.pincode
        });
      } else {
        if (this.isPrefilled) {
          this.detailsForm.patchValue({
            communicationHouseNo: '',
            communicationStreet: '',
            communicationLocality: '',
            communicationCity: '',
            communicationPincode: ''
          });
        }
      }
    });

    this.detailsForm.get('nationality').valueChanges.subscribe((v: any) => {

      this.detailsForm.value.country = this.countryInitials.name;
      this.detailsForm.patchValue({ country: this.countryInitials.name });

      this.detailsForm.value.communicationCountry = this.countryInitials.name;
      this.detailsForm.patchValue({
        communicationCountry: this.countryInitials.name,
      });

      if (v == 'Indian') {
        this.detailsForm.get("pan").clearValidators();
        const defaultPanNumberValidator = [Validators.required, Validators.pattern(this.panPattern)];
        this.detailsForm.controls["pan"].setValidators(defaultPanNumberValidator);
        this.detailsForm.get("pan").updateValueAndValidity({ emitEvent: false, onlySelf: true });

        this.detailsForm.get("pincode").clearValidators();
        const defaultPincodeValidator = [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(this.storageProvider.numberOnly())];
        this.detailsForm.controls["pincode"].setValidators(defaultPincodeValidator);
        this.detailsForm.get("pincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });

        this.detailsForm.get("communicationPincode").clearValidators();
        const defaultcommunicationPincodeValidator = [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(this.storageProvider.numberOnly())];
        this.detailsForm.controls["communicationPincode"].setValidators(defaultcommunicationPincodeValidator);
        this.detailsForm.get("communicationPincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });
      } else {
        this.detailsForm.get("passport").clearValidators();
        const defaultPassportNumberValidator = [Validators.required, Validators.pattern(this.passpostPattern)];
        this.detailsForm.controls["passport"].setValidators(defaultPassportNumberValidator);
        this.detailsForm.get("passport").updateValueAndValidity({ emitEvent: false, onlySelf: true });

        this.detailsForm.get("pincode").clearValidators();
        const defaultPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())]; //change
        this.detailsForm.controls["pincode"].setValidators(defaultPincodeValidator);
        this.detailsForm.get("pincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });

        this.detailsForm.get("communicationPincode").clearValidators();
        const defaultcommunicationPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())]; //change
        this.detailsForm.controls["communicationPincode"].setValidators(defaultcommunicationPincodeValidator);
        this.detailsForm.get("communicationPincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });
      }
    });

    this.detailsForm.patchValue({
      nationality: nation,
    });

    this.detailsForm.controls['nationality'].updateValueAndValidity();

    this.getOtherData();
  }

  getOtherData() {
    Observable.forkJoin([
      this.getProjectInfoDetails(),
      this.getTermsData()
    ]).subscribe((result: any) => {
      if (result[0].status == AppConst.HTTP_SUCESS_STATUS.OK) {
        this.projectInfoDetails = result[0].data;
        const newArr = [];
        result[0]['purpose'].map((item: any) => {
          newArr.push({
            id: item,
            name: item,
            selected: false
          })
        });
        this.purposeData = newArr;

        //Relationship
        const newArrRelationship = [];
        result[0]['relationship'].map((item: any) => {
          newArrRelationship.push({
            id: item,
            name: item,
            selected: false
          })
        });
        this.relationshipData = newArrRelationship;

        //Applicant Pre-Filled Data
        if (this.projectInfoDetails.hasOwnProperty('applicants')) {
          let isSameAddress = false;
          if (this.projectInfoDetails.applicants.hasOwnProperty("field_communication_city")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_communication_pincode")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_communication_country")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_communication_house_no_")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_communication_street")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_communication_locality")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_city")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_pincode")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_country")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_house_no_")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_street")
            && this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_locality")) {

            if ((this.projectInfoDetails.applicants.field_communication_city == this.projectInfoDetails.applicants.field_permanent_city)
              && (this.projectInfoDetails.applicants.field_communication_pincode == this.projectInfoDetails.applicants.field_permanent_pincode)
              && (this.projectInfoDetails.applicants.field_communication_country == this.projectInfoDetails.applicants.field_permanent_country)
              && (this.projectInfoDetails.applicants.field_communication_house_no_ == this.projectInfoDetails.applicants.field_permanent_house_no_)
              && (this.projectInfoDetails.applicants.field_communication_street == this.projectInfoDetails.applicants.field_permanent_street)
              && (this.projectInfoDetails.applicants.field_communication_locality == this.projectInfoDetails.applicants.field_permanent_locality)) {
              isSameAddress = true;
            } else {
              isSameAddress = false;
            }
          }

          let purposeOfPurchaseData = "";
          if (this.projectInfoDetails.applicants.hasOwnProperty("field_applicant_purpose")) {
            if (this.projectInfoDetails.applicants.field_applicant_purpose != "") {
              if (this.projectInfoDetails.applicants.field_applicant_purpose == "Self Use") {
                purposeOfPurchaseData = "Self-Use";
              } else {
                purposeOfPurchaseData = this.projectInfoDetails.applicants.field_applicant_purpose;
              }
            } else {
              purposeOfPurchaseData = "";
            }
          } else {
            purposeOfPurchaseData = "";
          }

          this.detailsForm.patchValue({
            permanentAddress: this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_house_no_") ? this.projectInfoDetails.applicants.field_permanent_house_no_ : '',
            street: this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_street") ? this.projectInfoDetails.applicants.field_permanent_street : '',
            locality: this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_locality") ? this.projectInfoDetails.applicants.field_permanent_locality : '',
            //country: this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_country") ? this.projectInfoDetails.applicants.field_permanent_country : '',
            city: this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_city") ? this.projectInfoDetails.applicants.field_permanent_city : '',
            pincode: this.projectInfoDetails.applicants.hasOwnProperty("field_permanent_pincode") ? this.projectInfoDetails.applicants.field_permanent_pincode : '',

            communicationHouseNo: this.projectInfoDetails.applicants.hasOwnProperty("field_communication_house_no_") ? this.projectInfoDetails.applicants.field_communication_house_no_ : '',
            communicationStreet: this.projectInfoDetails.applicants.hasOwnProperty("field_communication_street") ? this.projectInfoDetails.applicants.field_communication_street : '',
            communicationLocality: this.projectInfoDetails.applicants.hasOwnProperty("field_communication_locality") ? this.projectInfoDetails.applicants.field_communication_locality : '',
            //communicationCountry: this.projectInfoDetails.applicants.hasOwnProperty("field_communication_country") ? this.projectInfoDetails.applicants.field_communication_country : '',
            communicationCity: this.projectInfoDetails.applicants.hasOwnProperty("field_communication_city") ? this.projectInfoDetails.applicants.field_communication_city : '',
            communicationPincode: this.projectInfoDetails.applicants.hasOwnProperty("field_communication_pincode") ? this.projectInfoDetails.applicants.field_communication_pincode : '',

            pan: this.projectInfoDetails.applicants.hasOwnProperty("pan_no") ? this.projectInfoDetails.applicants.pan_no : '',
            passport: this.projectInfoDetails.applicants.hasOwnProperty("field_passport_no") ? this.projectInfoDetails.applicants.field_passport_no : '',

            same: isSameAddress,

            purposeOfPurchase: purposeOfPurchaseData
          });

          if (this.projectInfoDetails.applicants.hasOwnProperty("field_residance_type")) {
            if (this.projectInfoDetails.applicants.field_residance_type == 'Indian') {
              let fileIdProofMime = "";
              let fileaddressProofMime = "";
              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_id_name")
                && this.projectInfoDetails.applicants.fl_id_name != '') {
                const idType = this.projectInfoDetails.applicants.fl_id_name.split(".")[1].toString();
                if (idType.toLowerCase() == 'jpg' || idType.toLowerCase() == 'jpeg' || idType.toLowerCase() == 'png') {
                  fileIdProofMime = 'image';
                } else {
                  fileIdProofMime = idType;
                }
              }
              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_addr_name")
                && this.projectInfoDetails.applicants.fl_addr_name != '') {
                const addType = this.projectInfoDetails.applicants.fl_addr_name.split(".")[1].toString();
                if (addType.toLowerCase() == 'jpg' || addType.toLowerCase() == 'jpeg' || addType.toLowerCase() == 'png') {
                  fileaddressProofMime = 'image';
                } else {
                  fileaddressProofMime = addType;
                }
              }
              this.detailsForm.patchValue({
                idProofMime: fileIdProofMime.toLowerCase(),
                addressProofMime: fileaddressProofMime.toLowerCase(),
                idProofName: this.projectInfoDetails.applicants.hasOwnProperty("fl_id_name") ? this.projectInfoDetails.applicants.fl_id_name : '',
                addressProofName: this.projectInfoDetails.applicants.hasOwnProperty("fl_addr_name") ? this.projectInfoDetails.applicants.fl_addr_name : '',
              });

              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_id_url")) {
                if (this.projectInfoDetails.applicants.fl_id_url != "") {
                  /*  this.imageObj.primary.idProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                     this.projectInfoDetails.applicants.fl_id_url
                   ); */
                  this.imageObj.primary.idProof = this.projectInfoDetails.applicants.fl_id_url;
                } else {
                  this.imageObj.primary.idProof = "";
                }
              } else {
                this.imageObj.primary.idProof = "";
              }

              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_addr_url")) {
                if (this.projectInfoDetails.applicants.fl_addr_url != "") {
                  /* this.imageObj.primary.addressProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                    this.projectInfoDetails.applicants.fl_addr_url
                  ); */
                  this.imageObj.primary.addressProof = this.projectInfoDetails.applicants.fl_addr_url;
                } else {
                  this.imageObj.primary.addressProof = "";
                }
              } else {
                this.imageObj.primary.addressProof = "";
              }

              /*  this.imageObj.primary.idProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                 //"https://www.antennahouse.com/XSLsample/pdf/sample-link_1.pdf"
                 this.projectInfoDetails.applicants.hasOwnProperty("fl_id_url") ? this.projectInfoDetails.applicants.fl_id_url : ''
               );
               this.imageObj.primary.addressProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                 //"https://homepages.cae.wisc.edu/~ece533/images/airplane.png"
                 this.projectInfoDetails.applicants.hasOwnProperty("fl_addr_url") ? this.projectInfoDetails.applicants.fl_addr_url : ''
               ); */

              localStorage.setItem("indian_ID_F_ID_EOI", this.projectInfoDetails.applicants.hasOwnProperty("fl_id_fid") ? this.projectInfoDetails.applicants.fl_id_fid : '');
              localStorage.setItem("indian_ADD_F_ID_EOI", this.projectInfoDetails.applicants.hasOwnProperty("fl_addr_fid") ? this.projectInfoDetails.applicants.fl_addr_fid : '');

            } else {
              let fileIdProofMimeNRI = "";
              let fileaddressProofMimeNRI = "";
              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_front_name")
                && this.projectInfoDetails.applicants.fl_pass_front_name != '') {
                const fontType = this.projectInfoDetails.applicants.fl_pass_front_name.split(".")[1].toString();
                if (fontType.toLowerCase() == 'jpg' || fontType.toLowerCase() == 'jpeg' || fontType.toLowerCase() == 'png') {
                  fileIdProofMimeNRI = 'image';
                } else {
                  fileIdProofMimeNRI = fontType;
                }
              }
              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_back_name")
                && this.projectInfoDetails.applicants.fl_pass_back_name != '') {
                const backType = this.projectInfoDetails.applicants.fl_pass_back_name.split(".")[1].toString();
                if (backType.toLowerCase() == 'jpg' || backType.toLowerCase() == 'jpeg' || backType.toLowerCase() == 'png') {
                  fileaddressProofMimeNRI = 'image';
                } else {
                  fileaddressProofMimeNRI = backType;
                }
              }
              this.detailsForm.patchValue({
                idProofMime: fileIdProofMimeNRI,
                addressProofMime: fileaddressProofMimeNRI,
                idProofName: this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_front_name") ? this.projectInfoDetails.applicants.fl_pass_front_name : '',
                addressProofName: this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_back_name") ? this.projectInfoDetails.applicants.fl_pass_back_name : '',
              });

              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_front_url")) {
                if (this.projectInfoDetails.applicants.fl_pass_front_url != "") {
                  /*  this.imageObj.primary.idProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                     this.projectInfoDetails.applicants.fl_pass_front_url
                   ); */
                  this.imageObj.primary.idProof = this.projectInfoDetails.applicants.fl_pass_front_url;
                } else {
                  this.imageObj.primary.idProof = "";
                }
              } else {
                this.imageObj.primary.idProof = "";
              }

              if (this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_back_url")) {
                if (this.projectInfoDetails.applicants.fl_pass_back_url != "") {
                  /* this.imageObj.primary.addressProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                    this.projectInfoDetails.applicants.fl_pass_back_url
                  ); */
                  this.imageObj.primary.addressProof = this.projectInfoDetails.applicants.fl_pass_back_url;
                } else {
                  this.imageObj.primary.addressProof = "";
                }
              } else {
                this.imageObj.primary.addressProof = "";
              }

              /* this.imageObj.primary.idProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                //"https://www.antennahouse.com/XSLsample/pdf/sample-link_1.pdf"
                this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_front_url") ? this.projectInfoDetails.applicants.fl_pass_front_url : ''
              );
              this.imageObj.primary.addressProof = this.sanitizer.bypassSecurityTrustResourceUrl(
                //"https://homepages.cae.wisc.edu/~ece533/images/airplane.png"
                this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_back_url") ? this.projectInfoDetails.applicants.fl_pass_back_url : ''
              ); */

              localStorage.setItem("indian_ID_F_ID_EOI", this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_front_fid") ? this.projectInfoDetails.applicants.fl_pass_front_fid : '');
              localStorage.setItem("indian_ADD_F_ID_EOI", this.projectInfoDetails.applicants.hasOwnProperty("fl_pass_back_fid") ? this.projectInfoDetails.applicants.fl_pass_back_fid : '');
            }
          }
        }
        this.isPrefilled = true;
      }
      this.termsData = result[1][0].page_content;
      setTimeout(() => {
        this.loader.hide();
      }, 3000);
    }, (error) => {
      this.loader.hide();
      console.log('Error:::', error);
    });
  }

  getProjectInfoDetails() {
    const userData = JSON.parse(localStorage.getItem('userData'));
    const jSonData = {
      user_id: userData.data.userid,
      project_id: this.projectDetails.projectId,
      device_id: this.storageProvider.getDeviceInfo().uuid,
      typology_id: this.previousPageData.isSelectedId.typology,
      tower_id: this.previousPageData.isSelectedId.tower,
      floor_id: this.previousPageData.isSelectedId.floor,
    };
    return this.projectdetailsApi.getEOIPropertyInfo(jSonData);
  }

  getTermsData() {
    return this.projectdetailsApi.getEOITermsData();
  }

  checkForSameInApplicant(index) {

    if (this.detailsForm.value.applicantlist[index].applicantSameAddress) {

      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuHouseNo'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuStreet'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuLocality'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCountry'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCity'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].clearValidators();

      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuHouseNo'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuStreet'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuLocality'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCountry'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCity'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

    } else {

      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuHouseNo'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuStreet'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuLocality'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCountry'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCity'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].clearValidators();

      const appCommHouseNoValidation = [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuHouseNo'].setValidators(appCommHouseNoValidation);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuHouseNo'].updateValueAndValidity({ emitEvent: false, onlySelf: true });


      const appCommStreetValidation = [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuStreet'].setValidators(appCommStreetValidation);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuStreet'].updateValueAndValidity({ emitEvent: false, onlySelf: true });


      const appCommLocalityValidation = [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(32)
      ];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuLocality'].setValidators(appCommLocalityValidation);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuLocality'].updateValueAndValidity({ emitEvent: false, onlySelf: true });


      const appCommCountryValidation = [Validators.required];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCountry'].setValidators(appCommCountryValidation);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCountry'].updateValueAndValidity({ emitEvent: false, onlySelf: true });


      const appCommCityValidation = [
        Validators.required,
        Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
        Validators.maxLength(25)
      ];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCity'].setValidators(appCommCityValidation);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCity'].updateValueAndValidity({ emitEvent: false, onlySelf: true });


      let appCommPinValidation;
      /* if (this.detailsForm.value.applicantlist[index].applicantNationality == 'Indian') { */
      if (this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuCountry'].value.toString().toLowerCase() == "india") {
        appCommPinValidation = [
          Validators.required,
          Validators.pattern(this.storageProvider.numberOnly()),
          Validators.minLength(6),
          Validators.maxLength(6)
        ];
      } else {
        appCommPinValidation = [
          Validators.required,
          Validators.pattern(this.storageProvider.nriPinValidation())
        ];
      }
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].setValidators(appCommPinValidation);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

    }

    this.detailsForm.value.applicantlist[index].applicantComuCity = '';
    this.detailsForm.value.applicantlist[index].applicantComuHouseNo = '';
    this.detailsForm.value.applicantlist[index].applicantComuLocality = '';
    this.detailsForm.value.applicantlist[index].applicantComuPin = '';
    this.detailsForm.value.applicantlist[index].applicantComuStreet = '';
    this.detailsForm.controls.applicantlist['controls'][index].patchValue({
      applicantComuCity: '',
      applicantComuHouseNo: '',
      applicantComuLocality: '',
      applicantComuPin: '',
      applicantComuStreet: '',
    });

    if (this.detailsForm.value.applicantlist[index].applicantNationality == 'Indian') {
      this.detailsForm.value.applicantlist[index].applicantComuCountry = 'India';
      this.detailsForm.controls.applicantlist['controls'][index].patchValue({
        applicantComuCountry: 'India'
      });
    } else {
      this.detailsForm.value.applicantlist[index].applicantComuCountry = 'Afghanistan';
      this.detailsForm.controls.applicantlist['controls'][index].patchValue({
        applicantComuCountry: 'Afghanistan'
      });
    }
  }

  checkNationality(index) {
    if (this.detailsForm.value.applicantlist[index].applicantNationality == 'Indian') {
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPassport'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantMobile'].clearValidators();

      const defaultMobileNumberValidator = [Validators.required, Validators.pattern(this.storageProvider.onlyNumberPatternsignup()), Validators.minLength(10), Validators.maxLength(10)];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantMobile'].setValidators(defaultMobileNumberValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantMobile'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      const defaultPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.numberOnly()), Validators.minLength(6), Validators.maxLength(6)];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].setValidators(defaultPincodeValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      const defaultApplicantPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.numberOnly()), Validators.minLength(6), Validators.maxLength(6)];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].setValidators(defaultApplicantPincodeValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      const defaultApplicantPaneValidator = [Validators.required, Validators.pattern(this.panPattern)];
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPan'].setValidators(defaultApplicantPaneValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPan'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      this.detailsForm.value.applicantlist[index].showErrorMsg = false;
      this.detailsForm.value.applicantlist[index].flag = 'https://restcountries.eu/data/ind.svg';
      this.detailsForm.value.applicantlist[index].countryInitials = '+91';
      this.detailsForm.value.applicantlist[index].applicantCounty = 'India';
      this.detailsForm.value.applicantlist[index].applicantComuCountry = 'India';
      this.detailsForm.controls.applicantlist['controls'][index].patchValue({
        showErrorMsg: false,
        flag: 'https://restcountries.eu/data/ind.svg',
        countryInitials: '+91',
        applicantCounty: 'India',
        applicantComuCountry: 'India'
      });

    } else {
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPan'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPan'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].clearValidators();
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantMobile'].clearValidators();

      const defaultMobileNumberValidator = [Validators.required, Validators.pattern(this.storageProvider.numberOnly())];
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantMobile'].setValidators(defaultMobileNumberValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantMobile'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      const defaultPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())]; // Change
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].setValidators(defaultPincodeValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      const defaultApplicantPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())]; // Change
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].setValidators(defaultApplicantPincodeValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      const defaultApplicantPaneValidator = [Validators.required];
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPassport'].setValidators(defaultApplicantPaneValidator);
      this.detailsForm.controls.applicantlist['controls'][index].controls['appPassport'].updateValueAndValidity({ emitEvent: false, onlySelf: true });

      this.detailsForm.value.applicantlist[index].showErrorMsg = false;

      this.detailsForm.value.applicantlist[index].flag = 'https://restcountries.eu/data/afg.svg';
      this.detailsForm.value.applicantlist[index].countryInitials = '+93';

      this.detailsForm.value.applicantlist[index].applicantCounty = 'Afghanistan';
      this.detailsForm.value.applicantlist[index].applicantComuCountry = 'Afghanistan';
      this.detailsForm.controls.applicantlist['controls'][index].patchValue({
        showErrorMsg: false,
        flag: 'https://restcountries.eu/data/afg.svg',
        countryInitials: '+93',
        applicantCounty: 'Afghanistan',
        applicantComuCountry: 'Afghanistan'
      });
    }

    this.detailsForm.value.applicantlist[index].appIdproof = '';
    this.imageObj.secondary[index].idProof = '';
    this.detailsForm.value.applicantlist[index].appIdproofName = '';
    this.detailsForm.value.applicantlist[index].idProofErr = '';
    this.detailsForm.value.applicantlist[index].addressProofErr = '';
    this.detailsForm.value.applicantlist[index].appAddressproof = '';
    this.imageObj.secondary[index].addressProof = '';
    this.detailsForm.value.applicantlist[index].appAddressproofName = '';

    this.detailsForm.controls.applicantlist['controls'][index].patchValue({
      appIdproof: '',
      appIdproofName: '',
      idProofErr: '',
      appIdproofMime: '',
      appAddressproof: '',
      appAddressproofName: '',
      addressProofErr: '',
      appAddressproofMime: '',
      appPan: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].appPan : '',
      appPassport: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].appPassport : '',
      appRelationshipWithPriApplicant: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].appRelationshipWithPriApplicant : '',
      applicantCity: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantCity : '',
      applicantComuCity: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantComuCity : '',
      applicantComuCountry: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantComuCountry : '',
      applicantComuHouseNo: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantComuHouseNo : '',
      applicantComuLocality: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantComuLocality : '',
      applicantComuPin: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantComuPin : '',
      applicantComuStreet: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantComuStreet : '',
      applicantDob: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantDob : '',
      applicantEmail: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantEmail : '',
      applicantLocality: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantLocality : '',
      applicantMobile: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantMobile : '',
      applicantName: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantName : '',
      applicantNationality: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantNationality : '',
      applicantPAddress: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantPAddress : '',
      applicantPincode: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantPincode : '',
      applicantSameAddress: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantSameAddress : false,
      applicantStreet: (typeof (this.detailsForm.value.applicantlist[index]) != 'undefined') ? this.detailsForm.value.applicantlist[index].applicantStreet : '',
    });

  }

  onkeyDown(from, index) {
    if (from == 'pri') {
      const newPan = this.detailsForm.value.pan.toUpperCase();
      this.detailsForm.patchValue({
        pan: newPan
      });
    } else {
      const newPan = this.detailsForm.value.applicantlist[index].appPan.toUpperCase();
      this.detailsForm.controls.applicantlist['controls'][index].patchValue({
        appPan: newPan
      });
    }
  }

  flagChange(event: any) {
    this.flag = event.flag
  }

  toggleAccordion(value, index, from) {
    if (value == 'main') {
      if (from == 'html') {
        this.accordionExapanded = !this.accordionExapanded;
        this.icon = this.icon == "substract.svg" ? "add.svg" : "substract.svg";
        const findShowTrue = this.addPackage.value.findIndex(X => X.show == true);
        if (findShowTrue > -1) {
          this.addPackage.value[findShowTrue].show = false;
          this.addPackage.value[findShowTrue].icon = "add.svg";
        }
        if (this.addPackage.value.length > 0) {
          this.addPackage.value.map((items: any, indx: any) => {
            this.accordionExapandedApp.secondary[indx].isExpand = false;
          });
        }
      } else if (from == 'code') {
        if (this.accordionExapanded) {
          this.accordionExapanded = false;
          this.icon = this.icon == "substract.svg" ? "add.svg" : "substract.svg";
        }
      }
    } else if (value == 'secondary') {
      if (this.accordionExapanded) {
        this.accordionExapanded = false;
        this.icon = this.icon == "substract.svg" ? "add.svg" : "substract.svg";
      }
      this.accordionExapandedApp.secondary[index].isExpand = !this.accordionExapandedApp.secondary[index].isExpand;
      if (this.addPackage.value.length > 0) {
        this.addPackage.value.map((item: any, indxx: any) => {
          if (indxx != index) {
            this.accordionExapandedApp.secondary[indxx].isExpand = false;
          }
        });
      }
    }
  }

  get addPackage(): FormArray {
    return this.detailsForm.get('applicantlist') as FormArray;
  }

  initPackage() {
    return this.fb.group({
      applicantNationality: ['Indian'],
      applicantName: ['', [Validators.required, Validators.pattern(this.storageProvider.normalPatternForOnlyRequired())]],
      applicantMobile: ['', [Validators.required]],
      applicantEmail: ['', [Validators.required, Validators.pattern(this.storageProvider.emailPattern())]],
      applicantDob: ['', [Validators.required]],
      applicantPAddress: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
          Validators.maxLength(32)
        ]
      ],
      applicantStreet: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
          Validators.maxLength(32)
        ]
      ],
      applicantLocality: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
          Validators.maxLength(32)
        ]
      ],
      applicantCounty: ['', [Validators.required]],
      applicantCity: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.onlyAlphabetPattern()),
          Validators.maxLength(25)
        ]
      ],
      applicantPincode: ['', [Validators.required]],
      applicantSameAddress: [false],
      //applicantComuAddress:['',Validators.required],
      applicantComuHouseNo: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
          Validators.maxLength(32)
        ]
      ],
      applicantComuStreet: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
          Validators.maxLength(32)
        ]
      ],
      applicantComuLocality: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.normalPatternForOnlyRequired()),
          Validators.maxLength(32)
        ]
      ],
      applicantComuCountry: ['', [Validators.required]],
      applicantComuCity: ['',
        [
          Validators.required,
          Validators.pattern(this.storageProvider.onlyAlphabetPattern()),
          Validators.maxLength(25)
        ]
      ],
      applicantComuPin: ['', [Validators.required]],
      appPassport: [''],
      appPan: [''],
      appRelationshipWithPriApplicant: ['', [Validators.required]],
      appIdproof: [''],
      appAddressproof: [''],
      appIdproofName: [''],
      appAddressproofName: [''],
      appIdproofMime: [''],
      appAddressproofMime: [''],
      show: [true],
      icon: ['substract.svg'],
      flag: ['https://restcountries.eu/data/ind.svg'],
      countryInitials: ['+91'],
      errorMsg: ['Please Select A Valid Country Code.'],
      showErrorMsg: [false],
      idProofErr: [''],
      addressProofErr: ['']
    });
  }

  addPackageRow() {
    if (this.addApplicant) {
      this.addPackage.push(this.initPackage());
      this.accordionExapandedApp = {
        secondary: {
          0: {
            isExpand: false
          },
          1: {
            isExpand: false
          }
        }
      };
      this.toggleAccordion('secondary', this.addPackage.length - 1, 'code');

      this.detailsForm.value.applicantlist[this.addPackage.length - 1].applicantNationality = 'Indian';
      this.checkNationality(this.addPackage.length - 1);
      this.checkForSameInApplicant(this.addPackage.length - 1);

      const scrollDiv = document.getElementById('listApplic').offsetTop;
      this.rows.scrollTo(0, scrollDiv);
    } else {
      this.toggleAccordion('main', '', 'code');
      this.addApplicant = true;
      this.detailsForm.value.applicantlist[0].applicantNationality = 'Indian';
      this.checkNationality(0);
      this.checkForSameInApplicant(0);
    }
  }

  checkBoxSame(index) {
    console.log(index);
  }

  deletePackageRow(index) {
    if (this.detailsForm.value.applicantlist.length == 2 && index == 0) {
      this.deleteApplicant = true;
    }
    this.addPackage.removeAt(index);
  }

  get f() {
    return this.detailsForm.controls;
  }

  checkBox(index: any) {
    let findSelected = this.types.findIndex(X => X.selected === true);
    if (findSelected > -1) {
      this.types[findSelected].selected = false;
    }
    this.types[index].selected = true;
  }

  goToPay() {
    if (this.errorPassport == '' &&
      this.errrorPan == '' &&
      this.errrorApplicantPan == '' &&
      this.errorApplicantPassport == '' &&
      this.imageObj.primary.idProof != '' &&
      this.imageObj.primary.addressProof != '') {
      this.detailsForm.value.idProof = this.imageObj.primary.idProof;
      this.detailsForm.value.addressProof = this.imageObj.primary.addressProof;
      if (this.detailsForm.value.applicantlist.length > 0) {
        if (this.detailsForm.value.applicantlist.length == 1) {
          if (this.imageObj.secondary[0].idProof != '' && this.imageObj.secondary[0].addressProof != '') {
            this.detailsForm.value.applicantlist[0].appIdproof = this.imageObj.secondary[0].idProof;
            this.detailsForm.value.applicantlist[0].appAddressproof = this.imageObj.secondary[0].addressProof;
            if (this.deleteApplicant) {
              this.detailsForm.value.applicantlist[0].appIdproof = this.imageObj.secondary[1].idProof;
              this.detailsForm.value.applicantlist[0].appAddressproof = this.imageObj.secondary[1].addressProof;
            }
            this.imageError = false;
            this.termsError = false;
            if (this.temsCheck) {
              this.postBookingDetails();
            } else {
              this.termsError = true;
            }

          } else {
            if (this.imageObj.secondary[0].idProof == '') {
              this.detailsForm.value.applicantlist[0].idProofErr = 'This Is A Required Field';
              this.detailsForm.controls.applicantlist['controls'][0].patchValue({
                idProofErr: 'This Is A Required Field'
              });
            }
            if (this.imageObj.secondary[0].addressProof == '') {
              this.detailsForm.value.applicantlist[0].addressProofErr = 'This Is A Required Field';
              this.detailsForm.controls.applicantlist['controls'][0].patchValue({
                addressProofErr: 'This Is A Required Field'
              });
            }
          }
        } else if (this.detailsForm.value.applicantlist.length == 2) {
          if (this.imageObj.secondary[0].idProof != '' &&
            this.imageObj.secondary[1].idProof != '' &&
            this.imageObj.secondary[0].addressProof != '' &&
            this.imageObj.secondary[1].addressProof != ''
          ) {
            this.detailsForm.value.applicantlist[0].appIdproof = this.imageObj.secondary[0].idProof;
            this.detailsForm.value.applicantlist[0].appAddressproof = this.imageObj.secondary[0].addressProof;
            this.detailsForm.value.applicantlist[1].appIdproof = this.imageObj.secondary[1].idProof;
            this.detailsForm.value.applicantlist[1].appAddressproof = this.imageObj.secondary[1].addressProof;

            this.imageError = false;
            if (this.temsCheck) {
              this.postBookingDetails();
            } else {
              this.termsError = true;
            }
          } else {
            if (this.imageObj.secondary[0].idProof == '') {
              this.detailsForm.controls.applicantlist['controls'][0].patchValue({
                idProofErr: 'This Is A Required Field'
              });
              this.detailsForm.value.applicantlist[0].idProofErr = 'This Is A Required Field';
            }
            if (this.imageObj.secondary[0].addressProof == '') {
              this.detailsForm.value.applicantlist[0].addressProofErr = 'This Is A Required Field';
              this.detailsForm.controls.applicantlist['controls'][0].patchValue({
                addressProofErr: 'This Is A Required Field'
              });
            }

            if (this.imageObj.secondary[1].idProof == '') {
              this.detailsForm.controls.applicantlist['controls'][1].patchValue({
                idProofErr: 'This Is A Required Field'
              });
              this.detailsForm.value.applicantlist[1].idProofErr = 'This Is A Required Field';
            }
            if (this.imageObj.secondary[1].addressProof == '') {
              this.detailsForm.controls.applicantlist['controls'][1].patchValue({
                addressProofErr: 'This Is A Required Field'
              });
              this.detailsForm.value.applicantlist[1].addressProofErr = 'This Is A Required Field';
            }
          }
        }
      } else {
        this.imageError = false;
        if (this.temsCheck) {
          this.postBookingDetails();
        } else {
          this.termsError = true;
        }
      }
    } else {
      if (this.imageObj.primary.idProof == '') {
        this.primaryIdFrontErr = 'This Is A Required Field';
      }
      if (this.imageObj.primary.addressProof == '') {
        this.primaryIdBackErr = 'This Is A Required Field';
      }
    }
  }

  postBookingDetails() {

    if (this.detailsForm.value.applicantlist.length > 0) {
      for (let i = 0; i < this.detailsForm.value.applicantlist.length; i++) {
        this.detailsForm.value.applicantlist[i].applicantMobile = this.detailsForm.value.applicantlist[i].countryInitials + this.detailsForm.value.applicantlist[i].applicantMobile;
        if (this.detailsForm.value.applicantlist[i].countryInitials = '+91') {
          this.detailsForm.value.applicantlist[i].appPassport = '';
        } else {
          this.detailsForm.value.applicantlist[i].appPan = '';
        }
      }
    }

    const jsonData = {
      nationality: this.detailsForm.value.nationality,
      name: this.detailsForm.value.name,
      mobile: this.detailsForm.value.mobile,
      email: this.detailsForm.value.email,
      dob: this.detailsForm.value.dob,
      permanentAddress: this.detailsForm.value.permanentAddress,
      street: this.detailsForm.value.street,
      locality: this.detailsForm.value.locality,
      country: this.detailsForm.value.country,
      city: this.detailsForm.value.city,
      pincode: this.detailsForm.value.pincode,
      communicationHouseNo: this.detailsForm.value.communicationHouseNo,
      communicationStreet: this.detailsForm.value.communicationStreet,
      communicationLocality: this.detailsForm.value.communicationLocality,
      communicationCountry: this.detailsForm.value.communicationCountry,
      communicationCity: this.detailsForm.value.communicationCity,
      communicationPincode: this.detailsForm.value.communicationPincode,
      pan: this.detailsForm.value.pan,
      passport: this.detailsForm.value.passport,
      purposeOfPurchase: this.detailsForm.value.purposeOfPurchase,
      applicantlist: this.detailsForm.value.applicantlist,
      device_id: this.storageProvider.getDeviceInfo().uuid,
      project_id: this.projectDetails.projectId,
      user_id: localStorage.getItem('userId'),
      typology_id: this.previousPageData.isSelectedId.typology,
      tower_id: this.previousPageData.isSelectedId.tower,
      floor_id: this.previousPageData.isSelectedId.floor
    }

    this.loader.show();
    this.projectdetailsApi.postEoiBookingDetails(jsonData).subscribe((response: any) => {
      /* this.loader.hide(); */
      if (response.status == '200') {
        this.bookingId = response.booking_id;
        this.applicantsArr = response;

        const userData = JSON.parse(localStorage.getItem('userData'));
        if (!userData.data.hasOwnProperty('birth_dt')) {
          userData.data['birth_dt'] = this.detailsForm.value.dob;
        } else {
          userData.data.birth_dt = this.detailsForm.value.dob;
        }
        localStorage.setItem('userData', JSON.stringify(userData));

        this.openPaymentPage(response.booking_id, response.booking_no, response.amount);
        this.loader.hide();

      } else {
        this.loader.hide();
        this.toast.show(response.msg);
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  openPaymentPage(booking_id, booking_no, amount) {
    const sessonid = localStorage.getItem('Cookie').split('=')[1];
    const param = '?auth=' + sessonid + '&user_id=' + localStorage.getItem('userId') + '&booking_id=' + booking_id + '&booking_no=' + booking_no + '&amount=' + amount;
    const self = this;
    const url = AppConst.baseUrl + 'api_eoi_payment_redirect.json' + param;
    const option: InAppBrowserOptions = {
      location: 'no',
      hidden: 'no',
      toolbar: 'no',
    };
    const browser = this.iab.create(url, '_blank', option);
    browser.on('loadstop').subscribe(event => {
      if (event.url == AppConst.baseUrl + 'api_eoi_payment_success.json') {
        browser.close();
        this.loader.show();
        /* self.toast.show('Your booking has been successful');
         this.boookingconfrim(); */

        this.getBookingStatus(true, booking_id);
      } else if (event.url == AppConst.baseUrl + 'api_eoi_payment_failed.json') {
        browser.close();
        /* self.toast.show('Some thing went wrong in payment'); */

        this.loader.show();
        this.getBookingStatus(false, booking_id);
      }
    });
  }

  getBookingStatus(isSuccess: boolean, bookingId: any) {
    const data = {
      booking_id: this.bookingId
    }
    this.projectdetailsApi.getBookingStatus(data).subscribe((response: any) => {
      console.log("BOOKING_STATUS : " + response);
      if (response.status == '200' && response.booking_status == 'Completed') {
        if (isSuccess) {
          this.toast.show('Your Booking Has Been Successful.');
          this.boookingconfrim();
        } else {
          this.loader.hide();
          this.toast.show('Booking Failed. Please Try Again Later.');
        }
      } else {
        this.loader.hide();
        this.toast.show('Booking Failed. Please Try Again Later.');
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  boookingconfrim() {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: this.bookingId
    }
    /* this.loader.show('Please wait...'); */
    this.projectdetailsApi.postEoiBookingConfrimDetails(data).subscribe((response: any) => {
      /* this.loader.hide(); */
      console.log(response);
      if (response.status == '200') {
        this.uploadImage();
        this.detailsResponseData = response.data;
        /* this.navCtrl.push("PaymentPage", { details: '', bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' }); */
      } else {
        this.loader.hide();
      }
    }, (error: any) => {
      this.loader.hide();
    });

    /* this.navCtrl.push("PaymentPage", { details: '', bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' }); */
  }

  openCountry(applicant, from, index) {
    /* if (this.detailsForm.value.nationality != 'Indian') { */
    if (!this.isOpenModal) {
      this.isOpenModal = !this.isOpenModal;

      if (from == 'main' || (this.detailsForm.value.same == false && from == 'commu')) {
        /* this.loader.show('Please wait..'); */
        this.addModal = this.modalCtrl.create("CountryListPage");
        this.addModal.onDidDismiss((item: any) => {
          if (item) {
            const findSelected = item.findIndex(x => x.selected == true);
            if (applicant == 'pri') {
              if (from == 'main') {
                /* if (item[findSelected].callingCodes != '+91') { */
                this.detailsForm.patchValue({
                  country: item[findSelected].name
                });

                if (item[findSelected].name.toString().toLowerCase() == 'india') {
                  this.detailsForm.get("pincode").clearValidators();
                  const defaultPincodeValidator = [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(this.storageProvider.numberOnly())];
                  this.detailsForm.controls["pincode"].setValidators(defaultPincodeValidator);
                  this.detailsForm.get("pincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });
                } else {
                  this.detailsForm.get("pincode").clearValidators();
                  const defaultPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())]; //change
                  this.detailsForm.controls["pincode"].setValidators(defaultPincodeValidator);
                  this.detailsForm.get("pincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });
                }

                /*   this.countryError = false;
                } else {
                  this.countryError = true;
                } */
              } else if (from == 'commu') {
                this.detailsForm.patchValue({
                  communicationCountry: item[findSelected].name
                });

                if (item[findSelected].name.toString().toLowerCase() == 'india') {
                  this.detailsForm.get("communicationPincode").clearValidators();
                  const defaultcommunicationPincodeValidator = [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern(this.storageProvider.numberOnly())];
                  this.detailsForm.controls["communicationPincode"].setValidators(defaultcommunicationPincodeValidator);
                  this.detailsForm.get("communicationPincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });
                } else {
                  this.detailsForm.get("communicationPincode").clearValidators();
                  const defaultcommunicationPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())]; //change
                  this.detailsForm.controls["communicationPincode"].setValidators(defaultcommunicationPincodeValidator);
                  this.detailsForm.get("communicationPincode").updateValueAndValidity({ emitEvent: false, onlySelf: true });
                }
              }
            }
          }
          this.isOpenModal = !this.isOpenModal;
        });
        this.addModal.present();
      }
    }
    /*  } */
  }

  countryForApplicant(from, index) {
    if (from == 'mobile') {
      if (this.detailsForm.value.applicantlist[index].applicantNationality != 'Indian') {
        if (!this.isOpenModal) {
          this.isOpenModal = !this.isOpenModal;
          this.addModal = this.modalCtrl.create("CountryListPage", { showCodes: true, from: 'nri' });
          this.addModal.onDidDismiss((item: any) => {
            if (item) {
              let findSelected = item.findIndex(x => x.selected == true);

              this.detailsForm.value.applicantlist[index].flag = item[findSelected].flag;
              this.detailsForm.value.applicantlist[index].countryInitials = item[findSelected].callingCodes;

              this.detailsForm.controls.applicantlist['controls'][index].patchValue({
                flag: item[findSelected].flag,
                countryInitials: item[findSelected].callingCodes,
              });
            }
            this.isOpenModal = !this.isOpenModal;
          });
          this.addModal.present();
        }
      }
    } else {
      if (!this.isOpenModal) {
        this.isOpenModal = !this.isOpenModal;
        this.addModal = this.modalCtrl.create("CountryListPage");
        this.addModal.onDidDismiss((item: any) => {
          if (item) {
            let findSelected = item.findIndex(x => x.selected == true);
            if (from == 'country') {
              this.detailsForm.value.applicantlist[index].applicantCounty = item[findSelected].name;
              this.detailsForm.controls.applicantlist['controls'][index].patchValue({
                applicantCounty: item[findSelected].name
              });
              if (item[findSelected].name.toString().toLowerCase() == 'india') {
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].clearValidators();
                const defaultPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.numberOnly()), Validators.minLength(6), Validators.maxLength(6)];
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].setValidators(defaultPincodeValidator);
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
              } else {
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].clearValidators();
                const defaultPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())];
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].setValidators(defaultPincodeValidator);
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantPincode'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
              }
            } else if (from == 'communicationcountry') {
              this.detailsForm.value.applicantlist[index].applicantComuCountry = item[findSelected].name;
              this.detailsForm.controls.applicantlist['controls'][index].patchValue({
                applicantComuCountry: item[findSelected].name
              });

              if (item[findSelected].name.toString().toLowerCase() == 'india') {
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].clearValidators();
                const defaultApplicantPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.numberOnly()), Validators.minLength(6), Validators.maxLength(6)];
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].setValidators(defaultApplicantPincodeValidator);
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
              } else {
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].clearValidators();
                const defaultApplicantPincodeValidator = [Validators.required, Validators.pattern(this.storageProvider.nriPinValidation())];
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].setValidators(defaultApplicantPincodeValidator);
                this.detailsForm.controls.applicantlist['controls'][index].controls['applicantComuPin'].updateValueAndValidity({ emitEvent: false, onlySelf: true });
              }
            }
          }
          this.isOpenModal = !this.isOpenModal;
        });
        this.addModal.present();
      }
    }
  }

  presentActionSheet(applicant, value, index) {
    this.camera.cleanup().then((clean) => {
      console.log(clean);
    });
    let clickIndex = index;
    this.actionSheet = this.actionSheetCtrl.create({
      title: 'Select From',
      enableBackdropDismiss: true,
      cssClass: 'fileuploadpopup',
      buttons: [
        {
          icon: 'ios-camera-outline',
          cssClass: 'btnfileup',
          handler: () => {
            this.ngZone.run(() => {
              let options = this.storageProvider.getFromCamera('1');
              this.camera.getPicture(options).then((data: any) => {
                this.storageProvider.checkOnlyImageWith2Mb(data, 'cam').then((response: any) => {
                  this.imageSizeErr = '';
                  if (applicant == 'pri') {
                    if (value == 'idProof') {
                      localStorage.setItem("indian_ID_F_ID_EOI", '');
                      this.detailsForm.value.idProofMime = response.mime;
                      if (this.primaryIdFrontErr != '') {
                        this.primaryIdFrontErr = '';
                      }
                      this.imageObj.primary.idProof = response.localURL;
                      this.detailsForm.value.idProof = data;
                      this.detailsForm.value.idProofName = response.name;
                      this.detailsForm.patchValue({
                        idProofName: response.name,
                        idProofMime: response.mime
                      });
                    } else {
                      localStorage.setItem("indian_ADD_F_ID_EOI", '');
                      this.detailsForm.value.addressProofMime = response.mime;
                      if (this.primaryIdBackErr != '') {
                        this.primaryIdBackErr = '';
                      }
                      this.imageObj.primary.addressProof = response.localURL;
                      this.detailsForm.value.addressProof = data;
                      this.detailsForm.value.addressProofName = response.name;
                      this.detailsForm.patchValue({
                        addressProofName: response.name,
                        addressProofMime: response.mime
                      });
                    }
                  } else {
                    if (value == 'idProof') {
                      if (this.detailsForm.value.applicantlist[clickIndex].idProofErr != '') {
                        this.detailsForm.value.applicantlist[clickIndex].idProofErr = '';
                      }
                      this.detailsForm.value.applicantlist[clickIndex].appIdproof = data;
                      this.imageObj.secondary[clickIndex].idProof = response.localURL;
                      this.detailsForm.value.applicantlist[clickIndex].appIdproofName = response.name;
                      this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
                        appIdproof: data,
                        appIdproofName: response.name,
                        idProofErr: '',
                        appIdproofMime: response.mime
                      });
                    } else {
                      if (this.detailsForm.value.applicantlist[clickIndex].addressProofErr != '') {
                        this.detailsForm.value.applicantlist[clickIndex].addressProofErr = '';
                      }
                      this.detailsForm.value.applicantlist[clickIndex].appAddressproof = data;
                      this.imageObj.secondary[clickIndex].addressProof = response.localURL;
                      this.detailsForm.value.applicantlist[clickIndex].appAddressproofName = response.name;
                      this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
                        appAddressproof: data,
                        appAddressproofName: response.name,
                        addressProofErr: '',
                        appAddressproofMime: response.mime
                      });
                    }
                  }
                }).catch((error: any) => {
                  if (error == 20) {
                    error = '';
                  } else if (error == 'User canceled.') {
                    error = '';
                  } else if (error == 'No Image Selected') {
                    error = '';
                  }
                  if (applicant == 'pri') {
                    if (value == 'idProof') {
                      if (localStorage.getItem('indian_ID_F_ID_EOI') == '') {
                        localStorage.setItem("indian_ID_F_ID_EOI", '');
                        this.primaryIdFrontErr = error;

                        this.imageObj.primary.idProof = '';
                        this.detailsForm.value.idProof = '';
                        this.detailsForm.patchValue({
                          idProofName: '',
                          idProofMime: ''
                        });
                      }
                    } else {
                      if (localStorage.getItem('indian_ADD_F_ID_EOI') == '') {
                        localStorage.setItem("indian_ADD_F_ID_EOI", '');
                        this.primaryIdBackErr = error;

                        this.imageObj.primary.addressProof = '';
                        this.detailsForm.value.addressProof = '';
                        this.detailsForm.patchValue({
                          addressProofName: '',
                          addressProofMime: ''
                        });
                      }
                    }
                  } else {
                    if (value == 'idProof') {
                      this.detailsForm.value.applicantlist[clickIndex].idProofErr = error;
                    } else {
                      this.detailsForm.value.applicantlist[clickIndex].addressProofErr = error;
                    }
                  }
                });
              }).catch((error: any) => {
                if (error == 20) {
                  error = '';
                } else if (error == 'User canceled.') {
                  error = '';
                } else if (error == 'No Image Selected') {
                  error = '';
                }
                if (applicant == 'pri') {
                  if (value == 'idProof') {
                    if (localStorage.getItem('indian_ID_F_ID_EOI') == '') {
                      localStorage.setItem("indian_ID_F_ID_EOI", '');
                      this.primaryIdFrontErr = error;

                      this.imageObj.primary.idProof = '';
                      this.detailsForm.value.idProof = '';
                      this.detailsForm.patchValue({
                        idProofName: '',
                        idProofMime: ''
                      });
                    }
                  } else {
                    if (localStorage.getItem('indian_ADD_F_ID_EOI') == '') {
                      localStorage.setItem("indian_ADD_F_ID_EOI", '');
                      this.primaryIdBackErr = error;

                      this.imageObj.primary.addressProof = '';
                      this.detailsForm.value.addressProof = '';
                      this.detailsForm.patchValue({
                        addressProofName: '',
                        addressProofMime: ''
                      });
                    }
                  }
                } else {
                  if (value == 'idProof') {
                    this.detailsForm.value.applicantlist[clickIndex].idProofErr = error;
                  } else {
                    this.detailsForm.value.applicantlist[clickIndex].addressProofErr = error;
                  }
                }
              });
            });
          }
        },
        {
          icon: 'ios-folder-outline',
          cssClass: 'btnfileup',
          handler: () => {
            this.ngZone.run(() => {
              this.storageProvider.enquiryFileChoose().then((response: any) => {
                const splitTime = response.localURL;
                if (applicant == 'pri') {
                  if (value == 'idProof') {
                    localStorage.setItem("indian_ID_F_ID_EOI", '');
                    this.detailsForm.value.idProofMime = response.mime;
                    if (this.primaryIdFrontErr != '') {
                      this.primaryIdFrontErr = '';
                    }
                    this.imageObj.primary.idProof = splitTime;
                    this.detailsForm.value.idProof = splitTime;
                    this.detailsForm.value.idProofName = response.name;
                    this.detailsForm.patchValue({
                      idProofName: response.name,
                      idProofMime: response.mime
                    });
                  } else {
                    localStorage.setItem("indian_ADD_F_ID_EOI", '');
                    this.detailsForm.value.addressProofMime = response.mime;
                    if (this.primaryIdBackErr != '') {
                      this.primaryIdBackErr = '';
                    }
                    this.imageObj.primary.addressProof = splitTime;
                    this.detailsForm.value.addressProof = splitTime;
                    this.detailsForm.value.addressProofName = response.name;
                    this.detailsForm.patchValue({
                      addressProofName: response.name,
                      addressProofMime: response.mime
                    });
                  }
                } else {
                  if (value == 'idProof') {
                    if (this.detailsForm.value.applicantlist[clickIndex].idProofErr != '') {
                      this.detailsForm.value.applicantlist[clickIndex].idProofErr = '';
                    }
                    this.detailsForm.value.applicantlist[clickIndex].appIdproof = splitTime;
                    this.imageObj.secondary[clickIndex].idProof = splitTime;
                    this.detailsForm.value.applicantlist[clickIndex].appIdproofName = response.name;
                    this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
                      appIdproof: splitTime,
                      appIdproofName: response.name,
                      idProofErr: '',
                      appIdproofMime: response.mime
                    });
                  } else {
                    if (this.detailsForm.value.applicantlist[clickIndex].addressProofErr != '') {
                      this.detailsForm.value.applicantlist[clickIndex].addressProofErr = '';
                    }
                    this.detailsForm.value.applicantlist[clickIndex].appAddressproof = splitTime;
                    this.imageObj.secondary[clickIndex].addressProof = splitTime;
                    this.detailsForm.value.applicantlist[clickIndex].appAddressproofName = response.name;
                    this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
                      appAddressproof: splitTime,
                      appAddressproofName: response.name,
                      addressProofErr: '',
                      appAddressproofMime: response.mime
                    });
                  }
                }
              }).catch((error: any) => {
                if (error == 20) {
                  error = '';
                } else if (error == 'User canceled.') {
                  error = '';
                } else if (error == 'No Image Selected') {
                  error = '';
                }
                if (applicant == 'pri') {
                  if (value == 'idProof') {
                    if (localStorage.getItem('indian_ID_F_ID_EOI') == '') {
                      localStorage.setItem("indian_ID_F_ID_EOI", '');
                      this.primaryIdFrontErr = error;

                      this.imageObj.primary.idProof = '';
                      this.detailsForm.value.idProof = '';
                      this.detailsForm.patchValue({
                        idProofName: '',
                        idProofMime: ''
                      });
                    }
                  } else {
                    if (localStorage.getItem('indian_ADD_F_ID_EOI') == '') {
                      localStorage.setItem("indian_ADD_F_ID_EOI", '');
                      this.primaryIdBackErr = error;

                      this.imageObj.primary.addressProof = '';
                      this.detailsForm.value.addressProof = '';
                      this.detailsForm.patchValue({
                        addressProofName: '',
                        addressProofMime: ''
                      });
                    }
                  }
                } else {
                  if (value == 'idProof') {
                    this.detailsForm.value.applicantlist[clickIndex].idProofErr = error;
                    this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
                      appIdproof: '',
                      appIdproofName: '',
                      appIdproofMime: ''
                    });
                  } else {
                    this.detailsForm.value.applicantlist[clickIndex].addressProofErr = error;
                    this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
                      appAddressproof: '',
                      appAddressproofName: '',
                      appAddressproofMime: ''
                    });
                  }
                }
              });
            });
          }
        },
      ]
    });
    this.actionSheet.onDidDismiss((item: any) => {
      this.actionSheet = null;
    });
    this.actionSheet.present();
  }

  toggleFooter(from) {
    if (from == 'payment') {
      this.footerAccordian = {
        payment: (this.footerAccordian.payment) ? false : true,
        terms: false,
        propertyDetails: false
      }
    } else if (from == 'propertyDetails') {
      this.footerAccordian = {
        payment: false,
        terms: false,
        propertyDetails: (this.footerAccordian.propertyDetails) ? false : true
      }
    } else if (from == 'termsfooter') {
      this.footerAccordian = {
        payment: false,
        terms: true,
        propertyDetails: false
      }
    } else {
      this.footerAccordian = {
        payment: false,
        terms: (this.footerAccordian.terms) ? false : true,
        propertyDetails: false
      }
    }
  }

  checkboxTerms() {
    this.temsCheck = !this.temsCheck;
    (this.temsCheck) ? this.termsError = false : this.termsError = true;
  }

  uploadImage() {
    const enqueryFormData = this.detailsForm.value;
    const fileTransfer: FileTransferObject = this.transfer.create();
    let imageName, fileType, fileURL, applicantId, type;

    let fileUploaded = "YES";
    let fileID = "";

    if (this.count == 0) {
      fileType = 'addressProof';
      imageName = enqueryFormData.addressProofName;
      fileURL = enqueryFormData.addressProof;
      applicantId = this.applicantsArr.applications[0];
      type = (enqueryFormData.nationality == 'Indian') ? 'field_address_proof' : 'field_passport_back_page';

      if (localStorage.getItem('indian_ADD_F_ID_EOI') != null && localStorage.getItem('indian_ADD_F_ID_EOI') != '') {
        fileUploaded = "NO";
        fileID = localStorage.getItem('indian_ADD_F_ID_EOI');
      } else {
        fileUploaded = "YES";
        fileID = "";
      }
    } else if (this.count == 1) {
      fileType = 'idProof';
      imageName = enqueryFormData.idProofName;
      fileURL = enqueryFormData.idProof;
      applicantId = this.applicantsArr.applications[0];
      type = (enqueryFormData.nationality == 'Indian') ? 'field_id_proof' : 'field_passport_front_page';

      if (localStorage.getItem('indian_ID_F_ID_EOI') != null && localStorage.getItem('indian_ID_F_ID_EOI') != '') {
        fileUploaded = "NO";
        fileID = localStorage.getItem('indian_ID_F_ID_EOI');
      } else {
        fileUploaded = "YES";
        fileID = "";
      }
    } else {
      fileUploaded = "YES";
      fileID = "";
      if (enqueryFormData.applicantlist.length > 0) {
        if (enqueryFormData.applicantlist.length == 1) {
          if (this.count == 2) {
            fileType = 'appIdproof';
            imageName = enqueryFormData.applicantlist[0].appIdproofName;
            //enqueryFormData.applicantlist[0].appIdproof.split('/')[1];
            fileURL = enqueryFormData.applicantlist[0].appIdproof;
            applicantId = this.applicantsArr.applications[1];
            type = (enqueryFormData.applicantlist[0].applicantNationality == 'Indian') ? 'field_id_proof' : 'field_passport_front_page';
          } else if (this.count == 3) {
            fileType = 'appAddressproof';
            imageName = enqueryFormData.applicantlist[0].appAddressproofName;
            //enqueryFormData.applicantlist[0].appAddressproof.split('/')[1];
            fileURL = enqueryFormData.applicantlist[0].appAddressproof;
            applicantId = this.applicantsArr.applications[1];
            type = (enqueryFormData.applicantlist[0].applicantNationality == 'Indian') ? 'field_address_proof' : 'field_passport_back_page';
          } else {
            this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' });
            /* this.loader.hide(); */
            return false;
          }
        } else {
          if (this.count == 2) {
            fileType = 'appIdproof';
            imageName = enqueryFormData.applicantlist[0].appIdproofName;
            //enqueryFormData.applicantlist[0].appIdproof.split('/')[1];
            fileURL = enqueryFormData.applicantlist[0].appIdproof;
            applicantId = this.applicantsArr.applications[1];
            type = (enqueryFormData.applicantlist[0].applicantNationality == 'Indian') ? 'field_id_proof' : 'field_passport_front_page';
          } else if (this.count == 3) {
            fileType = 'appAddressproof';
            imageName = enqueryFormData.applicantlist[0].appAddressproofName;
            //enqueryFormData.applicantlist[0].appAddressproof.split('/')[1];
            fileURL = enqueryFormData.applicantlist[0].appAddressproof;
            applicantId = this.applicantsArr.applications[1];
            type = (enqueryFormData.applicantlist[0].applicantNationality == 'Indian') ? 'field_address_proof' : 'field_passport_back_page';
          } else if (this.count == 4) {
            fileType = 'appIdproof';
            imageName = enqueryFormData.applicantlist[1].appIdproofName;
            //enqueryFormData.applicantlist[1].appIdproof.split('/')[1];
            fileURL = enqueryFormData.applicantlist[1].appIdproof;
            applicantId = this.applicantsArr.applications[2];
            type = (enqueryFormData.applicantlist[1].applicantNationality == 'Indian') ? 'field_id_proof' : 'field_passport_front_page';
          } else if (this.count == 5) {
            fileType = 'appAddressproof';
            imageName = enqueryFormData.applicantlist[1].appAddressproofName;
            //enqueryFormData.applicantlist[1].appAddressproof.split('/')[1];
            fileURL = enqueryFormData.applicantlist[1].appAddressproof;
            applicantId = this.applicantsArr.applications[2];
            type = (enqueryFormData.applicantlist[1].applicantNationality == 'Indian') ? 'field_address_proof' : 'field_passport_back_page';
          } else {
            this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' });
            /* this.loader.hide(); */
            return false;
          }
        }
      } else {
        this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' });
        /* this.loader.hide(); */
        return false;
      }
    }
    const fileName = localStorage.getItem('userId') + '_' + fileType + '_' + imageName;
    const url = AppConst.baseUrl + 'upload_eoi_applicant_doc.json';
    const jsonData = {
      user_id: localStorage.getItem('userId'),
      applicant_id: applicantId,
      type: type,
      file_uploaded: fileUploaded.toLowerCase(),
      fid: fileID
    };
    let options: FileUploadOptions = {
      fileKey: 'myDoc',
      fileName: fileName,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      headers: {
        'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
        'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
      },
      params: jsonData
    };
    /* this.loader.show(); */

    if (fileUploaded == "YES") {
      fileTransfer.upload(fileURL.changingThisBreaksApplicationSecurity, url, options).then((data: any) => {
        const responseData = JSON.parse(data.response);
        /* this.toast.show(responseData.msg); */
        if (responseData.status == '200') {
          if (this.count == 0) {
            enqueryFormData.addressProof = '';
            this.count++;
            this.uploadImage();
          } else if (this.count == 1) {
            enqueryFormData.idProof = '';

            this.count++;
            this.uploadImage();
          } else {
            if (enqueryFormData.applicantlist.length > 0) {
              if (enqueryFormData.applicantlist.length == 1) {
                if (this.count == 2) {
                  enqueryFormData.applicantlist[0].appIdproof = '';
                  this.count++;
                  this.uploadImage();
                } else {
                  enqueryFormData.applicantlist[0].appAddressproof = '';
                  this.count++;
                  this.uploadImage();
                }
              } else {
                if (this.count == 2) {
                  enqueryFormData.applicantlist[0].appIdproof = '';
                  this.count++;
                  this.uploadImage();
                } else if (this.count == 3) {
                  enqueryFormData.applicantlist[0].appAddressproof = '';
                  this.count++;
                  this.uploadImage();
                } else if (this.count == 4) {
                  enqueryFormData.applicantlist[1].appIdproof = '';
                  this.count++;
                  this.uploadImage();
                } else if (this.count == 5) {
                  enqueryFormData.applicantlist[1].appAddressproof = '';
                  this.count++;
                  this.uploadImage();
                }
              }
            }
          }
        } else {
          this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' });
        }
      }).catch((err) => {
        console.log(err);

        this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'EOIEnquiryFormPage' });
        this.loader.hide();
      });
    } else {
      this.projectdetailsApi.applicantExistingImgUpload(url, options).subscribe((responseData: any) => {
        console.log(JSON.stringify(responseData));
        if (responseData.status == '200') {
          if (this.count == 0) {
            enqueryFormData.addressProof = '';
            this.count++;
            this.uploadImage();
          } else if (this.count == 1) {
            enqueryFormData.idProof = '';
            this.count++;
            this.uploadImage();
          }
        } else {
          this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'ApplicationFormPage' });
        }
      }, (error: any) => {
        console.log(error);
        this.navCtrl.push("PaymentPage", { details: this.detailsResponseData, bookingid: this.bookingId, fromPage: 'ApplicationFormPage' });
      });
    }
  }

  updateMyDate(event, from: any, index) {
    /* if(event == ''){
      
    } else {
      const newDate=new Date();
      this.maxDateDob = (newDate.getFullYear() - 18)+"-"+ (newDate.getMonth() + 1)+"-"+newDate.getDate(); //"1999-12-31";
    } */
  }

  onSelectChange(projectId: any) {
    this.detailsForm.patchValue({
      purposeOfPurchase: projectId
    });
  }

  /*  onSelectChangeRelationship(relationID: any, clickIndex: any) {
     this.detailsForm.controls.applicantlist['controls'][clickIndex].patchValue({
       appRelationshipWithPriApplicant: relationID
     });
   } */


  downloadApplicantDoc(type, fileSrc, fileName) {
    if (type == 'ID') {
      if (localStorage.getItem('indian_ID_F_ID_EOI') != null && localStorage.getItem('indian_ID_F_ID_EOI') != '') {
        this.downloadApplicantDocImg(fileSrc, fileName);
      }
    } else if (type == 'ADD') {
      if (localStorage.getItem('indian_ADD_F_ID_EOI') != null && localStorage.getItem('indian_ADD_F_ID_EOI') != '') {
        this.downloadApplicantDocImg(fileSrc, fileName);
      }
    }
  }

  downloadApplicantDocImg(fileSrc, fileNameData) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    this.storageProvider.getpermitionAndroid().then((entry) => {
      console.log("Permition Android: " + entry);
      this.loader.show();
      const downloadFileURL = encodeURI(fileSrc);
      const fileName = this.file.externalRootDirectory + "GPL/" + fileNameData;
      fileTransfer.download(downloadFileURL, fileName).then((entry) => {
        setTimeout(() => {
          // this.toast.show("File Downloaded in " + entry.filesystem.name + entry.fullPath);
          //this.openUserOption(entry.filesystem.name + entry.fullPath);
          this.fileOpener.open(entry.filesystem.name + entry.fullPath, this.storageProvider.getMIMEtype(fileNameData.split('.')[1]))
            .then(() => {
              console.log('File is opened');
              this.loader.hide();
            })
            .catch(e => {
              this.loader.hide();
              console.log('Error opening file', e)
            });
        }, 850);
      }, (error: any) => {
        /*  if (this.count == 1) {
           this.count = 0;
           this.downloadCostSheet();
         } else { */
        setTimeout(() => {
          this.loader.hide();
          console.log("Cost Sheet Download Error : " + JSON.stringify(error));
          this.toast.show("Failed To Download File.");
        }, 200);
        /* } */
      });
    }, (error: any) => {
      //this.toast.show('Storage Permission is needed to download the file');
    });
  }

  ionViewWillLeave() {
    try {
      if (typeof this.select != undefined) {
        this.select.close();
      }
    } catch (e) {
      console.log(e);
    }

    try {
      if (typeof this.actionSheet != undefined && this.actionSheet != null) {
        this.actionSheet.dismiss();
      }
    } catch (err) {
      console.log(err);
    }
  }
}