import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EoiEnquiryFormPage } from './eoi-enquiry-form';
import { ComponentsModule } from '../../components/components.module';
import { ProjectDetailsApiProvider } from '../../providers/apis/projectdetails';

@NgModule({
  declarations: [
    EoiEnquiryFormPage,
  ],
  imports: [
    IonicPageModule.forChild(EoiEnquiryFormPage),
    ComponentsModule
  ],
  providers: [ProjectDetailsApiProvider]
})
export class EoiEnquiryFormPageModule {}
