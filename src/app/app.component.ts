import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, ModalController, Events, Config, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DbProvider } from '../providers/db/db';
import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Vibration } from '@ionic-native/vibration';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AuthProvider } from '../providers/apis/auth';
import { LoadingProvider } from '../providers/utils/loader';
import { ToasterProvider } from '../providers/utils/toast';
import { DashordApiProvider } from '../providers/apis/dashbord';
import { NotificationApiProvider } from '../providers/apis/notification';
import { ProjectDetailsApiProvider } from '../providers/apis/projectdetails';
import { DomSanitizer } from '@angular/platform-browser';
import { Deeplinks } from '@ionic-native/deeplinks';
import { ProjectDetailsPage } from '../pages/project-details/project-details';
import { AppConst } from './app.constants';
import { AppAvailability } from '@ionic-native/app-availability';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public rootPage: any;

  pages: any[] = [
    { title: 'Profile', component: 'UserProfilePage' }
  ];
  pages2: any;
  public showSubmenu: boolean = false;
  errorMessages: any = {
    offline: "Sorry! You Are Offline.",
    slowNetwork: "Apologies! You Are On A Slow Network. Please Connect To A High Speed Internet Conection.",
    backButton: "Press Again To Exit."
  };
  public noNetModal: any;
  public count: any;
  public interval: any;
  public profileDetails: any = {};
  public userPic: any;
  public nameInitials: any;
  public date: any;
  public time: any;
  public year: any;
  public month: any;

  public isGuest: any;
  public isCustomer: any;

  public propertySubmenu = [];
  public P5AppDeviceInfo: any;
  @ViewChild(Nav) navChild: Nav;

  public buildType: string = "";
  public appVersionNumber: any = "";

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public app: App,
    public storagePrivider: DbProvider,
    private network: Network,
    public modalCtrl: ModalController,
    public events: Events,
    private appVersion: AppVersion,
    private push: Push,
    private vibration: Vibration,
    private localNotifications: LocalNotifications,
    private Authapi: AuthProvider,
    private loader: LoadingProvider,
    private toast: ToasterProvider,
    private config: Config,
    public DashbordApi: DashordApiProvider,
    public alertCtrl: AlertController,
    public NotificationApi: NotificationApiProvider,
    public projectdetailsApi: ProjectDetailsApiProvider,
    private sanitizer: DomSanitizer,
    private deeplinks: Deeplinks,
    public storageProvider: DbProvider,
    private appAvailability: AppAvailability,
  ) {

    /* this.appVersionNumber = storageProvider.getAppVersionNumber().then((res: any) => {
      this.appVersionNumber = res;
    }).catch((err) => {
      this.appVersionNumber = err;
    }); */
    if (storageProvider.getAppBuildType() == "") {
      this.appVersionNumber = AppConst.appVersionProd;
    } else {
      this.appVersionNumber = AppConst.appVersion;
    }
    this.buildType = storageProvider.getAppBuildType();

    localStorage.setItem('isGuest', '0');
    this.propertySubmenu = [];
    this.initializeApp();
    this.initializesidepanel();
    this.events.subscribe('setUserDetails', () => {
      this.getUserDetails();
    });

    this.events.subscribe('openSearchListPage', (pageName: any) => {
      this.setRootPage(pageName);
    });

    this.events.subscribe('afterLoginRedirect', (pageName: any) => {
      this.initializesidepanel();
      this.openPage(pageName);
    });

    this.events.subscribe('guestToUser', () => {
      this.initializesidepanel();
    });

    this.events.subscribe('getDeviceToken', () => {
      this.pushNotification();
    });

    this.events.subscribe('getSubmenu', () => {
      /* this.getPropertySubmenu(); */ /* Phase 2 */
    });

    this.events.subscribe('refreshSidepanelAfterPayment', () => {
      this.initializesidepanel();
    });
    this.events.subscribe('updateLastLoginDate', () => {
      this.updateLastLoginDate();
    });
    this.events.subscribe('redirectFromFooter', (componentsName) => {
      /* console.log("componentsName : " + componentsName); */
      const findComponentName = this.pages2.findIndex(x => x.component == componentsName);
      if (findComponentName > -1) {
        const findTrue = this.pages2.findIndex(x => x.click == true);
        const currPageName = this.nav.getActive().name;
        if (findTrue > -1 && (currPageName != this.pages2[findTrue].component)) {
          this.pages2[findTrue].click = false;
        }
        this.openPage(this.pages2[findComponentName]);
      }
    });

    this.events.subscribe('startOTPAutoRead', () => {
      this.startOTPWatching();
    });

  }

  initializesidepanel() {
    this.isGuest = this.storagePrivider.isGuest();
    this.isCustomer = localStorage.getItem('isCustomer');

    if (this.isGuest && this.isGuest == '1') {
      this.pages2 = [
        { title: 'Godrej Home', component: 'GodrejHomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/home-icon.png" },
        { title: 'Book a Visit', component: 'BookAVisitPage', submenu: [], click: false, icon: "assets/imgs/menuicon/schedule-a-visit.png" },
        { title: 'Login', component: 'HomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/logout.png" }
      ];
    }
    else if (this.isCustomer && this.isCustomer == '0') {
      this.pages2 = [
        { title: 'Godrej Home', component: 'GodrejHomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/home-icon.png" },
        { title: 'My Wishlist', component: 'WishlistPage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-wishlist.png" },
        { title: 'My Profile', component: 'UserProfilePage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-profile.png" },
        { title: 'Book a Visit', component: 'BookAVisitPage', submenu: [], click: false, icon: "assets/imgs/menuicon/schedule-a-visit.png" },
        { title: 'Logout', component: 'HomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/logout.png" }
      ];
    }
    else {
      this.pages2 = [
        { title: 'Godrej Home', component: 'GodrejHomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/home-icon.png" },

        /*{ title: 'My Dashboard', component: 'DashboardPage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-dashboard.png" },
     { title: 'My Properties', component: '', submenu: this.propertySubmenu, icon: "assets/imgs/menuicon/my-properties.png" },
      { title: 'My Tasks', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-tasks.png" },
      { title: 'My Documents', component: 'MyDocumentsListPage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-documents.png" }, */

        { title: 'My Wishlist', component: 'WishlistPage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-wishlist.png" },
        { title: 'My Profile', component: 'UserProfilePage', submenu: [], click: false, icon: "assets/imgs/menuicon/my-profile.png" },

        /* { title: 'My Rewards', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-reward.png" },
        { title: 'My Service Request', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-service-requests.png" },
        { title: 'My Account Statement', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/my-account-statement.png" },
        { title: 'Home Loan Support', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/home-loan-support.png" }, */

        { title: 'Book a Visit', component: 'BookAVisitPage', submenu: [], click: false, icon: "assets/imgs/menuicon/schedule-a-visit.png" },

        /* { title: 'Offers', component: '', submenu: [], click: false, icon: "assets/imgs/menuicon/offers.png" }, */

        { title: 'Logout', component: 'HomePage', submenu: [], click: false, icon: "assets/imgs/menuicon/logout.png" }
      ];
    }



  }

  initializeApp() {

    this.platform.ready().then(() => {
      this.count = 0;
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //statusBar.styleDefault();
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString('#37b0de');
      this.splashScreen.hide();
      // to initialize push notifications
      // this.pushNotification();
      // to initialize push notifications

      /* Version Code & Version Number */
      this.appVersion.getVersionCode().then((code: any) => {
        localStorage.setItem('versionCode', code);
      }).catch((err) => {
        console.log(err);
      });
      this.config.set('backButtonIcon', 'ios-arrow-back-outline');
      this.appVersion.getVersionNumber().then((vNum) => {
        localStorage.setItem('versionNumber', vNum);
      }).catch((err) => {
        console.log(err);
      });
      /* Version Code & Version Number */



      /* Set Root Page */

      if (localStorage.getItem('isLoggedIn') == '1') {
        this.rootPage = 'GodrejHomePage';
        this.pages2[0].click = true;
        this.getUserDetails();
        // this.getPropertySubmenu(); /* Phae 2 */
      } else {
        this.pages2[0].click = false;
        this.nav.setRoot('HomePage', {}, {
          animate: true,
          direction: 'forward'
        });
        //this.rootPage = 'HomePage';
      }
      this.pushNotification();
      /* Set Root Page */

      /* Back Button Exit / Pop Page */
      this.platform.registerBackButtonAction((event: any) => {
        let pageName = this.nav.getActive().name;
        /* Modal Or Popup Checks */
        const overlayView = this.app.getActiveNavs();

        const overlayViewPop = this.app._appRoot._overlayPortal._views[0];
        if (overlayViewPop && overlayViewPop.dismiss) {
          overlayViewPop.dismiss();

        } else {

          if (overlayView[0]._views[0].data.hasOwnProperty("component")
            && overlayView[0]._views[0].data.component.name != "NoInternetPage"
            && overlayView[0]._views[0].data.component.name != "") {

            if (overlayView[0]._views[0].data.component.name == "SearchPage") {
              this.events.publish('closeSearchModal');
            } else if (overlayView[0]._views[0].data.component.name == "InventoryFilterPage") {
              this.events.publish('closeInventoryModal');
            } else if (overlayView[0]._views[0].data.component.name == "CountryListPage") {
              this.events.publish('closeCountryModal');
            } else if (overlayView[0]._views[0].data.component.name == "TermsConditionPage") {
              this.events.publish('closeTermsConditionPage');
            } else if (overlayView[0]._views[0].data.component.name == "ProfileVerificationPage") {
              this.events.publish('closeProfileVerificationPage');
            } else if (overlayView[0]._views[0].data.component.name == "HomePage") {
              this.events.publish('closeLoginPageModal');
            }

          } else {
            if (pageName == "HomePage"
              || pageName == "DashboardPage"
              || pageName == "MyDocumentsListPage"
              || pageName == "GodrejHomePage"
              || pageName == "UserProfilePage"
              || pageName == "SearchListPage") {

              if (this.count < 1) {
                this.count++;
                if (this.count == 1) {
                  this.toast.show(this.errorMessages.backButton);
                }
                setTimeout(() => {
                  this.count = 0
                }, 3000);
              } else {
                this.platform.exitApp();
              }

            } else if (pageName == "PaymentPage") {

              if (this.nav.getPrevious().component.name == "NotificationlistPage") {
                this.nav.pop();
              } else {
                const pageDetails = 'GodrejHomePage';
                this.events.publish('openSearchListPage', pageDetails);
              }

            } else {
              this.nav.pop();
            }
          }

        }

      }, 0);

      this.localNotifications.on('click').subscribe((data: any) => {
        if (localStorage.getItem('isLoggedIn') == '1') {
          this.opennotification(data.data.secret);
        }
        console.log(data);
      });

      if (localStorage.getItem('plumb5') == null) {
        const jsonData = {
          /* "AppKey": "p5m1a2i3sdk400", */ // Dev p5m1a2i3sdk1
          "AppKey": "p5m1a2i3sdk500", // Prod
          "GcmRegId": JSON.parse(localStorage.getItem('deviceToken')),
          "DeviceInfo": {
            "Manufacturer": this.storagePrivider.getDeviceInfo().manufacturer,
            "Name": this.storagePrivider.getDeviceInfo().model,
            "Os": this.storagePrivider.getDeviceInfo().platform,
            "Id": this.storagePrivider.getDeviceInfo().uuid,
            "OsVersion": this.storagePrivider.getDeviceInfo().version,
            "CarrierName": "",
            "Resolution": "",
            "AppVersion": localStorage.getItem('versionNumber')
          }
        };
        this.Authapi.plumb5Api(jsonData).subscribe((response: any) => {
          console.log("plumb", response);
          if (response[0].Message == "sucess") {
            localStorage.setItem('plumb5', '1');
          }
        })
      }

      this.nav.viewDidLeave.subscribe((view) => {
        this.storagePrivider.getLocation().then((data: any) => {
          this.storagePrivider.getReverseGeoCode(data.coords.latitude, data.coords.longitude).then((response: any) => {
            this.P5AppDeviceInfo = {
              P5AppDeviceInfo: this.storagePrivider.getDeviceInfo().model,
              DeviceId: this.storagePrivider.getDeviceInfo().uuid,
              Locality: response.locality,
              City: response.subLocality,
              State: response.administrativeArea,
              Country: response.countryName,
              CountryCode: response.countryCode,
              Latitude: data.coords.latitude,
              Longitude: data.coords.longitude,
              IsObjectReady: true
            };
          }).catch((error: any) => {
            this.P5AppDeviceInfo = {
              P5AppDeviceInfo: this.storagePrivider.getDeviceInfo().model,
              DeviceId: this.storagePrivider.getDeviceInfo().uuid,
              Locality: "",
              City: "",
              State: "",
              Country: "",
              CountryCode: "",
              Latitude: data.coords.latitude,
              Longitude: data.coords.longitude,
              IsObjectReady: true
            };
          });

        }).catch((erro: any) => {
          this.P5AppDeviceInfo = {
            P5AppDeviceInfo: this.storagePrivider.getDeviceInfo().model,
            DeviceId: this.storagePrivider.getDeviceInfo().uuid,
            Locality: "",
            City: "",
            State: "",
            Country: "",
            CountryCode: "",
            Latitude: "",
            Longitude: "",
            IsObjectReady: true
          };
        });

      });

      this.nav.viewDidEnter.subscribe((view: any) => {
        if (view.id == "GodrejHomePage") {
          this.events.publish('fontBold');
        }
      });


      this.deeplinks.routeWithNavController(this.navChild, { //Deeplink
        '/ProjectDetailsPage': ProjectDetailsPage,
      }).subscribe((match) => {

        console.log("Match Data: " + JSON.stringify(match));
        if (match.$link.hasOwnProperty('queryString') && match.$link.queryString == 'ref=logo') {
          if (localStorage.getItem('isLoggedIn') == '1') {
            this.rootPage = 'GodrejHomePage';
            this.pages2[0].click = true;
            this.getUserDetails();
          } else {
            this.pages2[0].click = false;
            this.nav.setRoot('HomePage', {}, {
              animate: true,
              direction: 'forward'
            });
          }
        } else {
          if (localStorage.getItem('isLoggedIn') != '1') {
            this.loader.show();
            let data = { title: 'Godrej Home', component: 'GodrejHomePage' };
            localStorage.setItem('isGuest', '1');
            this.events.publish('afterLoginRedirect', data);
            this.events.publish('updateLastLoginDate');

            if (match.$link.hasOwnProperty('queryString')) {

              const getId = match.$link.queryString.split("=")[1];
              setTimeout(() => {
                this.loader.hide();
                this.events.publish('deepLinkDirect', getId);
              }, 2000);
            } else {
              this.loader.hide();
            }
          } else {
            if (match.$link.hasOwnProperty('queryString')) {
              const getId = match.$link.queryString.split("=")[1];
              this.navChild.push('ProjectDetailsPage', { fullData: { proj_id: getId } });
            } else {
              let data = { title: 'Godrej Home', component: 'GodrejHomePage' };
              localStorage.setItem('isGuest', '0');
              this.events.publish('afterLoginRedirect', data);
              this.events.publish('updateLastLoginDate');
            }
          }
        }
      }, (nomatch) => {

        console.warn('Unmatched Route', nomatch);

        if (localStorage.getItem('isLoggedIn') == '1') {
          this.rootPage = 'GodrejHomePage';
          this.getUserDetails();
        } else {
          this.nav.setRoot('HomePage', {}, {
            animate: true,
            direction: 'forward'
          });
        }
      });

    });
  }

  startOTPWatching() {
    console.log("Start Watching -- Appcomponent");
    window['cordova']['plugins']['smsRetriever']['startWatching']((result) => {
      try {
        this.events.publish('getOTPAutoReadValue', result);
      } catch (error) {
        console.log(error);
      }
    }, (error) => {
      console.log("OTP Auto Read Error : " + error);
    });
  }


  openPage(page: any) {

    this.isCustomer = localStorage.getItem('isCustomer');
    if (page.component == '') {
      if (this.isCustomer && this.isCustomer == '0') {
        if (page.title == "My Rewards" ||
          page.title == "My Properties" ||
          page.title == "My Service Request" ||
          page.title == "My Tasks" ||
          page.title == "Home Loan Support") {
          this.showInvalidClickAlert();
        }
      } else {
        if (page.title != 'My Properties') {
          alert('Page is under construction');
        } else {
          const clickIndex = this.pages2.findIndex(X => X.title == page.title);
          let previousClick = this.pages2.findIndex(X => X.click == true);

          if (previousClick > -1) {
            this.pages2[previousClick].click = false;
          }
          // this.pages2[clickIndex].click = true;
          this.showSubmenu = !this.showSubmenu;
        }
      }
    } else {
      this.showSubmenu = false;
      if (page.title == "Logout" || page.title == "Login") {
        // localStorage.removeItem('token');
        // localStorage.removeItem('Cookie');

        this.propertySubmenu = [];
        let previousClick = this.pages2.findIndex(X => X.click == true);
        if (previousClick > -1) {
          this.pages2[previousClick].click = false;
        }
        if (localStorage.getItem('userData') != null) {
          let userParseData = JSON.parse(localStorage.getItem('userData'));
          let data = {
            user_id: userParseData.data.userid
          };
          this.storagePrivider.clear();
          //localStorage.setItem("isTutorial", "1");
          this.Authapi.postLogoutApi(data).subscribe((response: any) => {
            if (response.status == "200") {
              // this.toast.show(response.msg);
            }
          }, (error: any) => {
            console.log(error);
          })
        }

        localStorage.clear();
        this.nav.setRoot(page.component);
      } else {
        /* this.pages2[0].click = true;
        console.log(this.pages2); */
        let clickIndex = this.pages2.findIndex(X => X.title == page.title);
        let previousClick = this.pages2.findIndex(X => X.click == true);
        //console.log(clickIndex, previousClick);
        if (clickIndex != previousClick) {


          if (page.component != 'PropertyPaymentPage' &&
            page.component != 'BookAVisitPage' &&
            page.component != 'WishlistPage') {
            this.pages2[clickIndex].click = true
            if (previousClick > -1) {
              this.pages2[previousClick].click = false;
            }
          }

          if (page.title == "Godrej Home" || page.title == "My Profile") {// || page.title == "Book a Visit"  page.title == "My Wishlist
            this.nav.setRoot(page.component, {}, {
              animate: true,
              direction: 'forward'
            });
          } else if (page.title == "My Dashboard" || page.title == "My Documents") {
            if (this.isCustomer && this.isCustomer == '0') {
              this.showInvalidClickAlert();
              if (previousClick > -1) {
                this.pages2[previousClick].click = true;
              }
              this.pages2[clickIndex].click = false;
            } else {
              this.nav.setRoot(page.component, {}, {
                animate: true,
                direction: 'forward'
              });
            }
          } else {
            if (page.component == 'PropertyPaymentPage') {
              const subClick = this.propertySubmenu.findIndex(X => X.click == true);
              if (subClick > -1) {
                this.propertySubmenu[subClick].click = false;
              }
              // this.pages2[2].click = true;
              page.click = true;
              this.showSubmenu = true;
              if (this.isCustomer && this.isCustomer == '0') {
                this.showInvalidClickAlert();
              } else {
                this.nav.push(page.component, {
                  bookingId: page.bookingId,
                  subHeadingText: page.title
                });
              }
            } else {
              if (page.component == 'BookAVisitPage') {
                this.nav.push(page.component, {
                  openView: 'BookAVisitPage'
                });
              } else {
                this.nav.push(page.component);
              }
            }
          }
        } else {
          if (page.component == 'PropertyPaymentPage') {
            const subClick = this.propertySubmenu.findIndex(X => X.click == true);
            if (subClick > -1) {
              this.propertySubmenu[subClick].click = false;
            }
            // this.pages2[2].click = true;
            page.click = true;
            this.showSubmenu = true;
            if (this.isCustomer && this.isCustomer == '0') {
              this.showInvalidClickAlert();
            } else {
              this.nav.push(page.component, {
                bookingId: page.bookingId,
                subHeadingText: page.title
              });
            }
          }
        }
      }
    }

    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //this.nav.setRoot(page.component);
  }

  showSubMenu() {
    this.showSubmenu = !this.showSubmenu;
  }

  getUserDetails() {
    //this.loader.show('Please wait...');
    /* this.storagePrivider.getVal('userData').then((data: any) => {
      this.loader.hide();
      if (data != "error") {
        this.profileDetails = data.userDetails;
        this.nameInitials = data.initials;
        this.userPic = data.image;
      } else {
        
      }

    }); */
    const userDetails = JSON.parse(localStorage.getItem('userData'));
    let fInitials = userDetails.data.first_name.substring(0, 1).toUpperCase();
    let lInitials = userDetails.data.last_name.substring(0, 1).toUpperCase();
    let fullInitial = fInitials + lInitials;
    this.profileDetails = userDetails;
    this.nameInitials = fullInitial;

    // this.userPic = (typeof (userDetails.data.user_image) != 'undefined') ? userDetails.data.user_image : '';
    //this.userPic = (typeof (userDetails.data.profile_pic) != 'undefined') ? this.sanitizer.bypassSecurityTrustResourceUrl(userDetails.data.profile_pic) : '';
    this.userPic = (typeof (userDetails.data.user_image) != 'undefined') ? userDetails.data.user_image : '';
    let newDate = new Date();
    this.date = newDate.getDate();
    this.time = newDate.getTime();
    this.year = newDate.getFullYear();
    let monthIndex = newDate.getMonth();
    this.month = this.storagePrivider.getMonth()[monthIndex];
  }

  setRootPage(pageName: any) {
    let previousPage = this.pages2.findIndex(X => X.click == true);
    if (previousPage > -1) {
      this.pages2[previousPage].click = false;
    }
    this.nav.setRoot(pageName, {}, {
      animate: true,
      direction: 'forward'
    });
  }

  pushNotification() {
    const options: PushOptions = {
      android: {
        sound: 'true',
        clearNotifications: "false"
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      }
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      this.storagePrivider.localNotificationWhenAppIsInForeGround(notification);
      this.events.publish('showNotiDot');
    });

    pushObject.on('registration').subscribe((registration: any) => {
      if (localStorage.getItem('deviceToken') == "" || typeof (localStorage.getItem('deviceToken')) == "undefined" ||
        localStorage.getItem('deviceToken') == null
      ) {
        localStorage.setItem('deviceToken', JSON.stringify(registration.registrationId));
        if (localStorage.getItem('userId') != null && localStorage.getItem('userId') != '') {
          this.events.publish('updateDeviceToken');
        }
      }
      localStorage.setItem('deviceToken', JSON.stringify(registration.registrationId));
      console.log('Device localstorage', JSON.stringify(registration));
    });
    pushObject.on('error').subscribe((error) => {
      localStorage.setItem('deviceToken', '');
    });
  }

  opennotification(notification) {
    console.log(JSON.stringify(notification));
    this.goReadNotification(notification.additionalData.moredata.notification_id);
    if (notification.additionalData.moredata && notification.additionalData.moredata.hasOwnProperty('target_link')) {
      const PageApi = notification.additionalData.moredata.target_link.split('/')[1];
      console.log(PageApi);
      if (PageApi == 'get_user_data_api.json') {
        this.nav.push('UserProfilePage');
      } else if (PageApi == 'list_document_api.json') {
        this.nav.push('MyDocumentsListPage');
      } else if (PageApi == 'booking_confirmation_data.json') {
        this.boookingconfrim(notification.field_mobile_app_data.data.booking_id);
      } else if (PageApi == 'eoi_booking_confirmation_data.json') {
        this.eoiBoookingconfrim(notification.field_mobile_app_data.data.booking_id);
      } else {
        this.nav.push('NotificationlistPage');
      }
    } else {
      this.nav.push('NotificationlistPage');
    }
  }

  goReadNotification(notification_id) {
    const param = {
      user_id: localStorage.getItem('userId'),
      notification_id: notification_id
    };
    this.NotificationApi.readnotification(param).subscribe((response: any) => {
      this.events.publish('showNotiDot');
    }, (error: any) => {
      console.log(error);
    });
  }

  boookingconfrim(booking_id) {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: booking_id
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.postBookingConfrimDetails(data).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == '200') {
        /* this.nav.push("PaymentPage", { details: response.data, bookingid: booking_id }); */
        this.nav.push("PaymentPage", { details: response.data, bookingid: booking_id, fromPage: 'ApplicationFormPage' });
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  eoiBoookingconfrim(booking_id) {
    const data = {
      user_id: localStorage.getItem('userId'),
      booking_id: booking_id
    }
    this.loader.show('Please wait...');
    this.projectdetailsApi.postEoiBookingConfrimDetails(data).subscribe((response: any) => {
      this.loader.hide();
      console.log(response);
      if (response.status == '200') {
        /* this.nav.push("PaymentPage", { details: response.data, bookingid: booking_id }); */
        this.nav.push("PaymentPage", { details: response.data, bookingid: booking_id, fromPage: 'EOIEnquiryFormPage' });
      }
    }, (error: any) => {
      this.loader.hide();
    });
  }

  getAccountSummary() {
    const jsonData = {
      user_id: localStorage.getItem('userId')
    };
    return this.DashbordApi.getAccountSummary(jsonData);
  }

  getPropertySubmenu() {
    this.isCustomer = localStorage.getItem('isCustomer');
    if (this.isCustomer == '1') {
      // this.propertySubmenu = [];
      this.getAccountSummary().subscribe((result: any) => {
        if (result.status == '200') {
          if (result.data.booking_items.length > 0) {
            result.data.booking_items.map((item: any) => {
              this.propertySubmenu.push({
                title: item.project_code + ' / ' + item.inventory_code,
                bookingId: item.booking_id,
                component: 'PropertyPaymentPage',
                click: false
              });
            });
          }
        }
      }, (error) => {
        console.log('Error:::', error);
      });
    }
  }

  redirectPageFromPushNotification(data: any) {

  }

  showInvalidClickAlert() {
    const alert = this.alertCtrl.create({
      title: 'Unauthorized Access',
      subTitle: 'Please buy a property to enable this option.',
      buttons: ['OK']
    });
    alert.present();
  }

  updateLastLoginDate() {
    let newDate = new Date();
    this.date = newDate.getDate();
    this.time = newDate.getTime();
    this.year = newDate.getFullYear();
    let monthIndex = newDate.getMonth();
    this.month = this.storagePrivider.getMonth()[monthIndex];
  }

  openSocialLink(socialLink) {
    window.open(socialLink, '_system');
  }

  openSocialLinkFB() {
    /*  window.open('fb://facebook.com/godrejproperties', '_system', 'location=no'); */
    /* window.open("fb://page/251514708209239", '_system', 'location=no');  */
    /* window.open('https://www.facebook.com/251514708209239', '_system'); */

    let app;

    if (this.platform.is('ios')) {
      app = 'fb://';
    } else if (this.platform.is('android')) {
      app = 'com.facebook.katana';
    }

    this.appAvailability.check(app).then(
      (yes: boolean) => {
        console.log("FB App Is Installed..");
        window.open("fb://page/251514708209239", '_system', 'location=no')
      }, (no: boolean) => {
        console.log("FB App Is Not Installed..");
        window.open('https://www.facebook.com/godrejproperties/', '_system')
      }
    );
  }
}

