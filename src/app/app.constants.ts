import { HttpErrorStatus, HttpSuccessStatus } from '../models/http';


/**
 * All application constant should define here.
 *
 * @export
 * @class AppConst
 * @author TATHAGATA SUR
 */
export class AppConst {


	public static readonly appVersion = "0.0.16";
	public static readonly appVersionProd = "1.0.6";

	//public static readonly baseUrl = "http://10.0.4.77/gpl-project/gpl-api/"; //Subhojit
	// public static readonly baseUrl = "http://10.0.4.182/gpl-sproject/gpl-api/"; //GARGI Di
	// public static readonly baseUrl = 'http://10.0.4.75/gpl-project/api/';	
	//public static readonly baseUrl = 'http://43.242.212.209/gpl-project-api/gpl-api/';//QA SECOND INSTANCE
	//public static readonly baseUrl ="http://10.0.4.75/gpl-project/";


	// public static readonly baseUrl = 'http://43.242.212.209/gpl-project/gpl-api/'; //QA
	// public static readonly baseUrl = 'http://43.242.212.166/gpl-project/gpl-api/'; // UAT 
	// public static readonly baseUrl = "http://43.242.212.209/beta/gpl-project/gpl-api/"; //QA Beta
	public static readonly baseUrl = 'https://customercare.godrejproperties.com/gpl-api/'; // Production

	// public static readonly BUILD_TYPE = 'QA'; // QA
	// public static readonly BUILD_TYPE = 'UAT'; // UAT
	// public static readonly BUILD_TYPE = 'QA Beta'; // QA Beta
	public static readonly BUILD_TYPE = ''; // PROD

	/**  server HTTP header status code */
	public static readonly HTTP_ERROR_STATUS: HttpErrorStatus = {
		BAD_REQUEST: 400,
		UN_AUTHORIZED: 401,
		NOT_FOUND: 404,
		SERVER_INTERNAL_ERROR: 500
	};
	// public static readonly Authorization = 'Basic Z2FyZ2kuZGFzQGluZHVzbmV0LmNvLmluOjEyMzQ1Ng==';

	/** application's bussiness related api status code */
	public static readonly HTTP_SUCESS_STATUS: HttpSuccessStatus = {
		OK: 200,
		CREATED: 201,
		ACCEPTED: 202,
		NO_RECORD: 204
	};

	/** application's bussiness related api status code */
	public static readonly API_STATUS = {
		OK: 200,
		NO_RECORD: 204
	};
}
