import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common'
import { MyApp } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { SharedModule } from './app.shared.module';
import { MyHttpInterceptor } from '../providers/interceptors/my-http-interceptor';
import { DashordApiProvider } from '../providers/apis/dashbord';
import { MypropertypaymentProvider } from '../providers/apis/mypropertypayment';
import { IonicSimpleProgressBarModule, SimpleProgressBarProvider } from 'ionic-progress-bar';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      scrollAssist: true,
      autoFocusAssist: true
    }),
    SharedModule,
    CommonModule,
    IonicSimpleProgressBarModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true
    },
    DashordApiProvider,
    MypropertypaymentProvider,
    SimpleProgressBarProvider
  ]
})
export class AppModule { }
