import { NgModule } from '@angular/core';
import { AuthProvider } from '../providers/apis/auth';
import { LoadingProvider } from '../providers/utils/loader';
import { ToasterProvider } from '../providers/utils/toast';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DbProvider } from '../providers/db/db';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { ToastController, NavController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { AppVersion } from '@ionic-native/app-version';
import { Push } from '@ionic-native/push';
import { Vibration } from '@ionic-native/vibration';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Calendar } from '@ionic-native/calendar';
import { Device } from '@ionic-native/device';
import { HttpClientModule } from '@angular/common/http';
import { NetworkProvider } from '../providers/utils/network';
import { SearchApiProvider } from '../providers/apis/search';
import { AgmCoreModule } from '@agm/core';
import { WishListApiProvider } from '../providers/apis/wishlistapi';
import { FileChooser } from '@ionic-native/file-chooser';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { NotificationApiProvider } from '../providers/apis/notification';
import { ProjectDetailsApiProvider } from '../providers/apis/projectdetails';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DatePicker } from '@ionic-native/date-picker';
import { Deeplinks } from '@ionic-native/deeplinks';
import { DatePipe } from '@angular/common';
import { FileOpener } from '@ionic-native/file-opener';
import { AppAvailability } from '@ionic-native/app-availability';
/**
 * Add all dependency module here, don't forgot export them otherwise it will not accessable to other modules.
 * All Third party module should import here.
 * All Ionic native provider should declear here.
 *
 * @export
 * @class SharedModule
 */

/**
* We Used -	AIzaSyAOc6UFXw1mfs4xCODUJJcrW-H1XAHlVTc 	
* TCS 16-04-2019 - AIzaSyBg2UIaZE_0hhQAM6cib4XabNi73Y3ReRk 
* TCS 09-05-2019 - AIzaSyBOHs8dInS4RMJxKWp75zOWp-D8Ehn0Kd8 
*/


@NgModule({
	imports: [
		HttpClientModule,
		AgmCoreModule,
		AgmCoreModule.forRoot({
			apiKey : 'AIzaSyBOHs8dInS4RMJxKWp75zOWp-D8Ehn0Kd8'
		}),
	],
	exports: [
		HttpClientModule,
		AgmCoreModule
	],
	providers: [
		AuthProvider,
		LoadingProvider,
		ToasterProvider,
		StatusBar,
		SplashScreen,
		DbProvider,
		SocialSharing,
		NativeStorage,
		Network,
		ToastController,
		Camera,
		File,
		FilePath,
		FileTransfer,
		AppVersion,
		Push,
		Vibration,
		LocalNotifications,
		Geolocation,
		Diagnostic,
		NativeGeocoder,
		InAppBrowser,
		Calendar,
		Device,
		NetworkProvider,
		SearchApiProvider,
		WishListApiProvider,
		FileChooser,
		IOSFilePicker,
		AndroidPermissions,
		NotificationApiProvider,
		ProjectDetailsApiProvider,
		ScreenOrientation,
		DatePicker,
		Deeplinks,
		DatePipe,
		FileOpener,
		AppAvailability
	]
})
export class SharedModule {
}
