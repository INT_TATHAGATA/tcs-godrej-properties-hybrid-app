import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
/**
 * This provider is responsible for calling all dashbord api.
 *
 * @export
 * @class DashordApiProvider
 * @author 
 */
@Injectable()
export class MyProfileApiProvider {
    constructor(private http: HttpClient) {
    }

    getProfileList(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'get_user_data_api.json';
        return this.http.post<any>(url, payload);
    }

    updateUserProfile(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'update_user_api.json';
        return this.http.post<any>(url, payload);
    }

    updateEmailMobile(payload: any, type: any): Observable<any> {
        let url;
        if (type == "Email") {
            url = 'update_email_api.json';
        } else if (type == "Mobile") {
            url = 'update_mobile_api.json';
        } else if (type == "E" || type == "EM" || type == "M" || type == "MM") {
            url = 'verify_userdata_api.json';
        }
        const fullUrl = AppConst.baseUrl + url;
        return this.http.post<any>(fullUrl, payload);
    }

    changePassword(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'update_userpass_api.json';
        return this.http.post<any>(url, payload);
    }

    verifyEmailOtp(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'verify_userdata_api.json';
        return this.http.post<any>(url, payload);
    }


}
