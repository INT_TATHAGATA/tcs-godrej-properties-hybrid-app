import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
/**
 * This provider is responsible for calling all dashbord api.
 *
 * @export
 * @class DashordApiProvider
 * @author 
 */
@Injectable()
export class DocumentsApiProvider {
    constructor(private http: HttpClient) {
    }

    getDocumentList(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'list_document_api.json';
        return this.http.post<any>(url, payload);
    }

    getDocumentData(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'get_document_data.json';
        return this.http.post<any>(url, payload);
    }
    createDocApi(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'create_document_api.json';
        return this.http.post<any>(url, payload);
    }

    getPropertyDocumentList(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'api_property_document_list.json';
        return this.http.post<any>(url, payload);
    }
}
