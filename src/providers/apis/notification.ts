import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

/**
 * This provider is responsible for calling alll Notification
 *
 * @export
 * @class NotificationApiProvider
 * @author tathagata sur
 */
@Injectable()


export class NotificationApiProvider {
    private NOTIFICATION_LIST_URL: string;
    private READ_NOTIFICATION_URL: string;

    constructor(private http: HttpClient) {
        this.NOTIFICATION_LIST_URL = 'get_user_notifications_api.json';
        this.READ_NOTIFICATION_URL = 'change_read_status_notification_api.json';

    }
    getNotificationlisting(param): Observable<any> {
        const url = AppConst.baseUrl + this.NOTIFICATION_LIST_URL;
        return this.http.post<any>(url, param);
    }
    readnotification(param): Observable<any> {
        const url = AppConst.baseUrl + this.READ_NOTIFICATION_URL;
        return this.http.post<any>(url, param);
    }
}