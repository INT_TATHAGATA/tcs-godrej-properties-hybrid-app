import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Events } from 'ionic-angular';

/**
 * This provider is responsible for calling alll wish list Api  wish listpage
 *
 * @export
 * @class WishListApiProvider
 * @author tathagata sur
 */
@Injectable()


export class WishListApiProvider {
    private WISH_LIST_URL: string;
    private MODIFY_WISH_LIST: string;
    private COUNT_WISH_LIST: string;
    constructor(private http: HttpClient,
        public events: Events, ) {
        this.WISH_LIST_URL = 'res_wishlist_items.json';
        this.MODIFY_WISH_LIST = 'res_wishlist.json';
        this.COUNT_WISH_LIST = 'res_wishlist_count.json';
    }
    getwishlisting(param): Observable<any> {
        const url = AppConst.baseUrl + this.WISH_LIST_URL;
        return this.http.post<any>(url, param);
    }
    modifyWishlist(param): Observable<any> {
        const url = AppConst.baseUrl + this.MODIFY_WISH_LIST;
        return this.http.post<any>(url, param);
    }
    countWishlist(param): Observable<any> {
        const url = AppConst.baseUrl + this.COUNT_WISH_LIST;
        return this.http.post<any>(url, param);
    }


}