import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
import { PropertySearchApiresponse } from '../../models/search';
/**
 * This provider is responsible for calling property search api.
 *
 * @export
 * @class SearchApiProvider
 * @author tathagata sur
 */
@Injectable()


export class SearchApiProvider {
    private PROPERTY_FILTER_INFO: string;
    private PROPERTY_LISTING_URL: string;
    private PROJECT_SEARCH: string;
    private PROJECT_PROPERTY_SUB_LOCATION: string;

    constructor(private http: HttpClient) {
        this.PROPERTY_FILTER_INFO = 'property_filter_info.json';
        this.PROPERTY_LISTING_URL = 'property-listing';
        this.PROJECT_SEARCH = 'property_api_search.json';
        this.PROJECT_PROPERTY_SUB_LOCATION = 'property_sublocation_list.json';
    }

    propertySearchInfo(searchParam): Observable<any> {
        const url = AppConst.baseUrl + this.PROPERTY_FILTER_INFO;
        return this.http.post<any>(url, searchParam);
    }

    propertysearch(): Observable<PropertySearchApiresponse> {
        const url = AppConst.baseUrl + this.PROPERTY_LISTING_URL;
        return this.http.get<PropertySearchApiresponse>(url);
    }

    projectSearch(searchdata): Observable<any> {
        const url = AppConst.baseUrl + this.PROJECT_SEARCH;
        return this.http.post<any>(url, searchdata);
    }

    getSubLocation(params): Observable<any> {
        const url = AppConst.baseUrl + this.PROJECT_PROPERTY_SUB_LOCATION;
        return this.http.post<any>(url, params);
    }
}