import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
import { GodrejHomeRecomProjectApiresponse } from '../../models/godrejhome';

/**
 * This provider is responsible for calling Godrej Home Apis
 *
 * @export
 * @class GodrejHomeApiProvider
 * @author tathagata sur
 */
@Injectable()


export class GodrejHomeApiProvider {
    private RECOMMENDED_PROJECT: string;
    private RECENT_SEARCHED_PROJECT: string;

    private BUY_NOW_PROJECT_LIST_URL: string;

    constructor(private http: HttpClient) {
        this.RECOMMENDED_PROJECT = 'get_recommended_project.json';
        this.RECENT_SEARCHED_PROJECT = 'get_recent_searched_project.json';
        this.BUY_NOW_PROJECT_LIST_URL = 'get_foyr_project_list.json';
    }
    RecommededProject(UID): Observable<GodrejHomeRecomProjectApiresponse> {
        const url = AppConst.baseUrl + this.RECOMMENDED_PROJECT;
        return this.http.post<GodrejHomeRecomProjectApiresponse>(url, UID);
    }
    recentSearchProject(UID): Observable<any> {
        const url = AppConst.baseUrl + this.RECENT_SEARCHED_PROJECT;
        return this.http.post<any>(url, UID);
    }
    getBuyNowProjectList(): Observable<any> {
        const url = AppConst.baseUrl + this.BUY_NOW_PROJECT_LIST_URL;
        return this.http.get<any>(url);
    }

}