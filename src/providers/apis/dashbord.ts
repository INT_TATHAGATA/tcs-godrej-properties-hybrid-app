import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
/**
 * This provider is responsible for calling all dashbord api.
 *
 * @export
 * @class DashordApiProvider
 * @author tathagata sur
 */
@Injectable()


export class DashordApiProvider {
    private ACCOUNT_SUMMARY: string;
    private SINGLE_ACCOUNT_SUMMARY: string;
    constructor(private http: HttpClient) {
        this.ACCOUNT_SUMMARY = 'account_summary_api.json';
        this.SINGLE_ACCOUNT_SUMMARY = 'single_account_summary_api.json'
    }

    getRmContacts(payload: any): Observable<any> {
        const url = AppConst.baseUrl + 'get_rm_info.json';
        return this.http.post<any>(url, payload);
    }
    getAccountSummary(payload: any): Observable<any> {
        const url = AppConst.baseUrl + this.ACCOUNT_SUMMARY;
        return this.http.post<any>(url, payload);
    }
    getSingleAccountSummary(payload: any): Observable<any> {
        const url = AppConst.baseUrl + this.SINGLE_ACCOUNT_SUMMARY;
        return this.http.post<any>(url, payload);
    }

}