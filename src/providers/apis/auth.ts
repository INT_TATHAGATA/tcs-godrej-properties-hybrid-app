
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';
import { LoginApiresponse, OtpApiresponse } from '../../models/auth';
/**
 * This provider is responsible for calling all auth api.
 *
 * @export
 * @class AuthProvider
 * @author tathagata sur
 */
@Injectable()

export class AuthProvider {
	private REGISTRATION_URL: string;
	private REGISTRATION_OTP_URL: string;
	private LOGIN_URL: string;
	private TRAMSCONDITION_URL: string;
	private FORGOTPASS_URL: String;
	private RESET_PASS_URL: String;
	private LOGOUT_URL: String;
	private VERIFY_OTP_URL: String;
	private UPDATE_DEVICE_TOKEN_URL: String;
	private BADGE_COUT_URL: String;
	private PLUMB_API: string;
	constructor(private http: HttpClient) {
		this.REGISTRATION_URL = 'user_register.json';
		this.REGISTRATION_OTP_URL = 'user_register_otp.json';
		this.LOGIN_URL = 'user_login.json';
		this.TRAMSCONDITION_URL = 'term_of_use';
		this.FORGOTPASS_URL = 'user_forgot_pass.json';
		this.RESET_PASS_URL = 'user_new_pass.json';
		this.LOGOUT_URL = 'user_custom_logout.json';
		this.VERIFY_OTP_URL = 'otp_verify.json';
		this.UPDATE_DEVICE_TOKEN_URL = 'update_device_token.json';
		this.BADGE_COUT_URL = 'get_user_badge_count_api.json';
		/* this.PLUMB_API="https://p5mobtrk.godrejproperties.com/mTracker.svc/RegisterDevice"; */ // Dev
		this.PLUMB_API = "https://p5mobtrkprod.godrejproperties.com/mTracker.svc/RegisterDevice"; // Prod
	}

	postRegistrationapi(postData): Observable<any> {
		const url = AppConst.baseUrl + this.REGISTRATION_URL;
		return this.http.post<any>(url, postData);
	};
	postRegisterOtpApi(data): Observable<OtpApiresponse> {
		const url = AppConst.baseUrl + this.REGISTRATION_OTP_URL;
		return this.http.post<OtpApiresponse>(url, data);
	};
	postLoginapi(data): Observable<LoginApiresponse> {
		const url = AppConst.baseUrl + this.LOGIN_URL;
		return this.http.post<LoginApiresponse>(url, data);
	};
	getTermsConditionApi(): Observable<any> {
		const url = AppConst.baseUrl + this.TRAMSCONDITION_URL;
		return this.http.get<any>(url);
	};
	postForgotPasswordApi(postData): Observable<any> {
		const url = AppConst.baseUrl + this.FORGOTPASS_URL;
		return this.http.post<any>(url, postData);
	};
	postResetPasswordApi(postData): Observable<any> {
		const url = AppConst.baseUrl + this.RESET_PASS_URL;
		return this.http.post<any>(url, postData);
	};
	postLogoutApi(postData): Observable<any> {
		const url = AppConst.baseUrl + this.LOGOUT_URL;
		return this.http.post<any>(url, postData);
	};

	postVerifyOtp(postData): Observable<any> {
		const url = AppConst.baseUrl + this.VERIFY_OTP_URL;
		return this.http.post<any>(url, postData);
	};

	updateDeviceToken(postData): Observable<any> {
		const url = AppConst.baseUrl + this.UPDATE_DEVICE_TOKEN_URL;
		return this.http.post<any>(url, postData);
	};
	BadgeCount(postData): Observable<any> {
		const url = AppConst.baseUrl + this.BADGE_COUT_URL;
		return this.http.post<any>(url, postData);
	};

	plumb5Api(postData): Observable<any> {
		return this.http.post<any>(this.PLUMB_API, postData);
	};

}
