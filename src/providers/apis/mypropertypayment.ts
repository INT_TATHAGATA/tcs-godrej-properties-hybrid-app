import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';

/*
 Generated class for the MypropertypaymentProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
*/
@Injectable()
export class MypropertypaymentProvider {

    private PROPERTY_OVERVIEW: string;
    private MY_ACCOUNT: string;
    constructor(private http: HttpClient) {
        this.PROPERTY_OVERVIEW = 'api_property_overview.json';
        this.MY_ACCOUNT = 'api_property_myaccount.json';
    }

    getPropertyOverview(payload: any): Observable<any> {
        const url = AppConst.baseUrl + this.PROPERTY_OVERVIEW;
        return this.http.post<any>(url, payload);
    }

    getMyAccount(payload: any): Observable<any> {
        const url = AppConst.baseUrl + this.MY_ACCOUNT;
        return this.http.post<any>(url, payload);
    }

}