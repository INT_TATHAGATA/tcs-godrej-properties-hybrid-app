import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app/app.constants';

/**
 * This provider is responsible for calling Project Details Api Project Details page
 *
 * @export
 * @class ProjectDetailsApiProvider
 * @author tathagata sur
 */
@Injectable()


export class ProjectDetailsApiProvider {
    private LOCATION_URL: String;
    private UPDATEPROJECT_URL: String;
    private INVENTORY_LIST: String;
    private VISIT_BOOK: String;
    private REQUEST_CALL: String;
    private INVENTORY_FILTER_DATA: String;
    private INVENTORY_SEARCH: String;
    private COSTSHEETBRK_UP: string;
    private BOOKING_ADD: String;
    private BOOKING_CONFRIM: String;
    private EMAIL_INVOICE: string;
    private CHECK_INVENTORY_AVAILABLE: string;
    private GET_PROJECT_LIST: string;
    private GET_PAYMENT_PLAN: string;
    private GET_PROPERTY_INFO: string;
    private GET_TERMS_DATA: string;
    private GET_INV_DATA: string;
    private GET_EOI: string;
    private GET_EOI_STOCK: string;
    private GET_EOI_PROPERTY_INFO: string;
    private EOI_BOOKING_ADD: string;
    private EOI_BOOKING_CONFRIM: string;
    private EOI_EMAIL_INVOICE: string;
    private GET_EOI_TERMS_DATA: string;

    private BOOKING_STATUS: string;


    private GET_COST_SHEET_TERMS_DATA: string;
    private GET_BOOKING_SUMMERY_TERMS_DATA: string;

    constructor(private http: HttpClient) {
        this.LOCATION_URL = 'get_property_location_rest.json';
        this.UPDATEPROJECT_URL = 'update_history.json';
        this.INVENTORY_LIST = 'res_inventory_list.json';
        this.VISIT_BOOK = 'book_visit.json';
        this.REQUEST_CALL = 'request_callback.json';
        this.INVENTORY_FILTER_DATA = 'res_inventory_filter.json';
        this.INVENTORY_SEARCH = 'res_inventory_search.json';
        this.COSTSHEETBRK_UP = 'res_booking_cross_breakup.json';
        this.BOOKING_ADD = 'res_booking_chk_add.json';
        this.BOOKING_CONFRIM = 'booking_confirmation_data.json';
        this.EMAIL_INVOICE = 'api_send_payment_mail';
        this.CHECK_INVENTORY_AVAILABLE = 'check_inventory_status.json';
        this.GET_PROJECT_LIST = 'get_project_list.json';
        this.GET_PAYMENT_PLAN = 'get_payment_plan.json';
        this.GET_PROPERTY_INFO = 'get_booking_property_info.json';
        this.GET_TERMS_DATA = 'term_of_use_payment';
        this.GET_INV_DATA = 'get_inv_info.json';
        this.GET_EOI = 'get_eoi_filters.json';
        this.GET_EOI_STOCK = 'get_eoi_stock_status.json';
        this.GET_EOI_PROPERTY_INFO = 'get_eoi_booking_property_info.json';
        this.EOI_BOOKING_ADD = 'res_eoi_booking_chk_add.json';
        this.EOI_BOOKING_CONFRIM = 'eoi_booking_confirmation_data.json';
        this.EOI_EMAIL_INVOICE = 'api_eoi_send_payment_mail.json';
        this.GET_EOI_TERMS_DATA = 'term_of_use_eoi_payment';

        this.BOOKING_STATUS = 'get_booking_confirmation_status.json';

        this.GET_COST_SHEET_TERMS_DATA = 'term_of_use_costsheet';
        this.GET_BOOKING_SUMMERY_TERMS_DATA = 'term_of_use_booking_summary';
    }
    goprojectdetails(PROJECTID): Observable<any> {
        const url = AppConst.baseUrl + 'property_details_rest/' + PROJECTID + '/?_format=json';
        return this.http.get<any>(url, PROJECTID);
    }
    getprojectbanar(PROJECTID): Observable<any> {
        const url = AppConst.baseUrl + 'property_banner/' + PROJECTID + '/?_format=json';
        return this.http.get<any>(url);
    }

    getprojectbanarImage(PROJECTID): Observable<any> {
        const url = AppConst.baseUrl + 'property_banner_single/' + PROJECTID + '/?_format=json';
        return this.http.get<any>(url);
    }

    getprojectaminities(PROJECTID): Observable<any> {
        const url = AppConst.baseUrl + 'property_aminities_rest/' + PROJECTID + '/?_format=json';
        return this.http.get<any>(url);
    }
    getprojectgalleries(PROJECTID): Observable<any> {
        const url = AppConst.baseUrl + 'property_galleries_rest/' + PROJECTID + '/?_format=json';
        return this.http.get<any>(url);
    }
    getprojectlocation(PROJECTID): Observable<any> {
        const url = AppConst.baseUrl + this.LOCATION_URL;
        return this.http.post<any>(url, PROJECTID);
    }
    updateProjectDetails(data): Observable<any> {
        const url = AppConst.baseUrl + this.UPDATEPROJECT_URL;
        return this.http.post<any>(url, data);
    }
    getinventorylist(data): Observable<any> {
        const url = AppConst.baseUrl + this.INVENTORY_LIST;
        return this.http.post<any>(url, data);
    }
    postVisitBook(data): Observable<any> {
        const url = AppConst.baseUrl + this.VISIT_BOOK;
        return this.http.post<any>(url, data);
    }
    postRequestCall(data): Observable<any> {
        const url = AppConst.baseUrl + this.REQUEST_CALL;
        return this.http.post<any>(url, data);
    }
    getinventoryfiterdata(data): Observable<any> {
        const url = AppConst.baseUrl + this.INVENTORY_FILTER_DATA;
        return this.http.post<any>(url, data);
    }
    getinventorysearch(data): Observable<any> {
        const url = AppConst.baseUrl + this.INVENTORY_SEARCH;
        return this.http.post<any>(url, data);
    }
    getcostsheetbrkup(data): Observable<any> {
        const url = AppConst.baseUrl + this.COSTSHEETBRK_UP;
        return this.http.post<any>(url, data);
    }
    postBookingDetails(data): Observable<any> {
        const url = AppConst.baseUrl + this.BOOKING_ADD;
        return this.http.post<any>(url, data);
    }
    postBookingConfrimDetails(data): Observable<any> {
        const url = AppConst.baseUrl + this.BOOKING_CONFRIM;
        return this.http.post<any>(url, data);
    }
    sendInvoice(data, from): Observable<any> {
        let url;
        if (from == 'EOIEnquiryFormPage' || from == 'EOIEnquiryFormPageN') {
            url = AppConst.baseUrl + this.EOI_EMAIL_INVOICE;
        } else {
            url = AppConst.baseUrl + this.EMAIL_INVOICE;
        }
        return this.http.post<any>(url, data);
    }
    getInventoryAvailable(data): Observable<any> {
        const url = AppConst.baseUrl + this.CHECK_INVENTORY_AVAILABLE;
        return this.http.post<any>(url, data);
    }
    getAllProjectList(): Observable<any> {
        const url = AppConst.baseUrl + this.GET_PROJECT_LIST;
        return this.http.get<any>(url);
    }
    getPaymentPlan(data): Observable<any> {
        const url = AppConst.baseUrl + this.GET_PAYMENT_PLAN;
        return this.http.post<any>(url, data);
    }
    getProperyInfo(data): Observable<any> {
        const url = AppConst.baseUrl + this.GET_PROPERTY_INFO;
        return this.http.post<any>(url, data);
    }
    getTermsData(): Observable<any> {
        const url = AppConst.baseUrl + this.GET_TERMS_DATA;
        return this.http.get<any>(url);
    }
    getInvData(data): Observable<any> {
        const url = AppConst.baseUrl + this.GET_INV_DATA;
        return this.http.post<any>(url, data);
    }
    getEoiList(data): Observable<any> {
        const url = AppConst.baseUrl + this.GET_EOI;
        return this.http.post<any>(url, data);
    }
    getEoiStock(data): Observable<any> {
        const url = AppConst.baseUrl + this.GET_EOI_STOCK;
        return this.http.post<any>(url, data);
    }
    getEOIPropertyInfo(data): Observable<any> {
        const url = AppConst.baseUrl + this.GET_EOI_PROPERTY_INFO;
        return this.http.post<any>(url, data);
    }
    postEoiBookingDetails(data): Observable<any> {
        const url = AppConst.baseUrl + this.EOI_BOOKING_ADD;
        return this.http.post<any>(url, data);
    }
    postEoiBookingConfrimDetails(data): Observable<any> {
        const url = AppConst.baseUrl + this.EOI_BOOKING_CONFRIM;
        return this.http.post<any>(url, data);
    }
    getEOITermsData(): Observable<any> {
        const url = AppConst.baseUrl + this.GET_EOI_TERMS_DATA;
        return this.http.get<any>(url);
    }

    getBookingStatus(data): Observable<any> {
        const url = AppConst.baseUrl + this.BOOKING_STATUS;
        return this.http.post<any>(url, data);
    }


    getCostSheetTermsData(): Observable<any> {
        const url = AppConst.baseUrl + this.GET_COST_SHEET_TERMS_DATA;
        return this.http.get<any>(url);
    }

    getBookingSummeryTermsData(): Observable<any> {
        const url = AppConst.baseUrl + this.GET_BOOKING_SUMMERY_TERMS_DATA;
        return this.http.get<any>(url);
    }



    applicantExistingImgUpload(url, data): Observable<any> {
        return this.http.post<any>(url, data);
    }
}