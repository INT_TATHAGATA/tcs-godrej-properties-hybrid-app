import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToasterProvider } from '../utils/toast';
import { NetworkProvider } from '../utils/network';
import { AppConst } from '../../app/app.constants';
import { LoadingProvider } from '../utils/loader';
import { Events, AlertController } from 'ionic-angular';
import { DbProvider } from '../db/db';


const defaultTimeout = 30000;
@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  token: string;
  Cookie: string;
  islogout: boolean;

  constructor(private toast: ToasterProvider, private network: NetworkProvider,
    public events: Events,
    private loader: LoadingProvider,
    public storagePrivider: DbProvider,
    private alertCtrl: AlertController
  ) {
    this.islogout = false;
    console.log('MyHttpInterceptor');
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const newRequest = req.clone({
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': localStorage.getItem('token') ? localStorage.getItem('token') : '',
        //'Authorization': localStorage.getItem('Authorization') ? localStorage.getItem('Authorization') : '',
        'x-requested-with': localStorage.getItem('Cookie') ? localStorage.getItem('Cookie') : ''
      })

    });


    /** send cloned request with header to the next handler. */
    if (this.network.isOnline()) {
      // console.log(newRequest);
      return next.handle(newRequest).timeout(defaultTimeout).do((event: any) => {
        // do nothing
        if (event instanceof HttpResponse) {
          //this.loader.hide();
          if ((<any>Object).values(AppConst.HTTP_SUCESS_STATUS).indexOf(event.status) > -1) {
            const data = event.url.split('/');
            if (data.length > 0 && data[data.length - 1] == 'user_login.json') {
              this.islogout = false;
            } else {
              if (event.body.status == AppConst.HTTP_ERROR_STATUS.SERVER_INTERNAL_ERROR
                && (event.body.msg.toLowerCase() == 'unauthorised access' || event.body.msg.toLowerCase() == 'unauthorized access')
                && !this.islogout) {
                this.islogout = true;
                this.loader.hide();
                /* this.toast.show(event.body.msg); 
                this.toast.show('It seems you have logged in from another device');
                this.logout();*/
                this.logoutWithAlert();
              }
            }
          } else if ((<any>Object).values(AppConst.HTTP_SUCESS_STATUS).indexOf(event.status) == -1) {
            this.loader.hide();
            return Observable.throw('');
          }
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          this.loader.hide();
          // Check for common HTTP Error Response
          if ((<any>Object).values(AppConst.HTTP_ERROR_STATUS).indexOf(err.status) > -1) {

            console.log('HTTP Error:::', err);
            // this.toast.show(err.error.message);
            return Observable.throw(err);
          } else {
            console.log('Unknown HTTP Error:::', err);
            // this.toast.show(err.error.message);
            return Observable.throw(err);
          }
        } else {
          this.loader.hide();
          console.log('Invalid Server Response:::', err);
          /*  this.toast.show('Invalid Server Response'); */
          return Observable.throw(err);
        }
      });
    } else {
      this.loader.hide();
      this.toast.show('No Network Connection Found.');
      return Observable.empty<HttpEvent<any>>();
    }
  }

  logoutWithAlert() {

    let alert = this.alertCtrl.create({
      title: 'Oops!',
      message: 'It seems you have logged in from another device.',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.logout();
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();

    /* this.toast.show('It seems you have logged in from another device');
    this.logout(); */
  }

  logout() {
    this.storagePrivider.clear();
    localStorage.clear();

    const data = { title: 'Logout', component: 'HomePage' };
    this.events.publish('afterLoginRedirect', data);
  }
}
