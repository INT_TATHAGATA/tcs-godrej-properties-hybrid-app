import { Injectable } from '@angular/core';
import { CartModel } from "../../models/cartmodel";
import 'rxjs/add/operator/map';
import { Events, LoadingController, Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Calendar } from '@ionic-native/calendar';
import { Device } from '@ionic-native/device';
import { ToasterProvider } from '../utils/toast';
import { LoadingProvider } from '../utils/loader';
import { FormControl, FormGroup } from '@angular/forms';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { File, FileEntry } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { DomSanitizer } from '@angular/platform-browser';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AppConst } from '../../app/app.constants';
import { AppVersion } from '@ionic-native/app-version';
/*
  Generated class for the DbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DbProvider {
    public loading;
    public cartlist: CartModel[];
    private _setValue: any = [];
    private _cityList: any = [];
    private _possessionList: any = [];
    private _typologyList: any = [];
    private _price: any = {};
    fileTransfer: FileTransferObject;
    public checkfileSize = '10000000';//10MB
    public checkfileSizeWith2MbFile = '2000000';//2MB
    public checkfileSizeWith2MbCam = '2000000';//2MB
    options: InAppBrowserOptions = {
        location: 'yes',//Or 'no'
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        zoom: 'yes',//Android only ,shows browser zoom controls
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only
        closebuttoncaption: 'Close', //iOS only
        disallowoverscroll: 'no', //iOS only
        toolbar: 'yes', //iOS only
        enableViewportScale: 'no', //iOS only
        allowInlineMediaPlayback: 'no',//iOS only
        presentationstyle: 'pagesheet',//iOS only
        fullscreen: 'yes',//Windows only
    };
    constructor(
        //private storage: Storage,
        public events: Events,
        private nativeStorage: NativeStorage,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public camera: Camera,
        private localNotifications: LocalNotifications,
        private geolocation: Geolocation,
        private diagnostic: Diagnostic,
        private nativeGeocoder: NativeGeocoder,
        private socialSharing: SocialSharing,
        private iab: InAppBrowser,
        private calendar: Calendar,
        private device: Device,
        private toast: ToasterProvider,
        private loader: LoadingProvider,
        private fileChooser: FileChooser,
        private iosFilePicker: IOSFilePicker,
        private file: File,
        private transfer: FileTransfer,
        private sanitizer: DomSanitizer,
        private androidPermissions: AndroidPermissions,
        private appVersion: AppVersion,
        private appPlatform: Platform
    ) {
        console.log('Hello DbProvider Provider');
        this.loading = null;


    }

    emailPattern() {
        //return "[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}";
        //return "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9]+\\.([a-zA-Z]{3,5}|[a-zA-z]{2,5}\\.[a-zA-Z]{2,5})"; 
        // return "^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@((\\w+\\-+)|(\\w+\\.))*\\w{1,63}\\.[a-zA-Z]{2,6}$"; 

        // return "^[a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]+[.]){1,2}[a-zA-Z]{2,10}$";

        return "^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@([a-zA-Z0-9-]+[.]){1,2}[a-zA-Z]{2,10}$";

    }

    onlyNumberPattern() {
        //return "^\+(?:[0-9] ?){6,14}[0-9]$";
        return "^(?=.*[0-9])[- +()0-9]+$";
        // return "^(?=.*[0-9])[0-9]+$";
    }
    onlyNumberPatternsignup() {
        //return "^\+(?:[0-9] ?){6,14}[0-9]$";
        return "^(?=.*[0-9])[0-9]+$";
        // return "^(?=.*[0-9])[0-9]+$";
    }
    numberOnly() {
        return "^[0-9]*$";
    }
    maxlength(control: FormControl) {
        console.log(control.value.toString().length)
        if (control.value.toString().length == 0) {
            return control.value
        } else {
            // return (null)
        }


    }

    passwordPattern() {
        // return "^((?=.*\\d)(?=.*[a-zA-Z]).{6,20})";
        // return "^[a-zA-Z0-9]{6,20}$"; 

        // Minimum 6 characters, at least one letter and one number
        /* return "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"; */
        /* return "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^&*?%{}/'.<>]{6,}$"; */
        return "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^*?%/'._\\-,\\\\`~]{6,}$";
    }

    normalPatternForOnlyRequired() {
        return "[a-zA-Z0-9.%$#!@,/'.:\\-_ ]+";
    }

    panPattern() {
        return "^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$";
    }

    onlyAlphabetPattern() {
        // return "^[a-zA-Z]+$";
        return /^[ A-Za-z .']*$/
    }
    alphabetPattren() {
        return /^[ A-Za-z']*$/
    }

    nriPinValidation() {
        return /^[ A-Za-z0-9']*$/
    }

    passportPattern() {
        return '^(?!^0+$)[a-zA-Z0-9]{3,20}$';
    }

    set(key: any, value: any) {
        return new Promise((resolve, reject) => {
            return this.nativeStorage.setItem(key, value)
                .then(
                    (suce) => {
                        console.log('Stored item!' + suce);
                        resolve();
                    },
                    (error) => {
                        console.log('Error storing item' + error);
                        reject();
                    }
                );
        });

    }

    getVal(key: any) {
        return new Promise((resolve, reject) => {
            return this.nativeStorage.getItem(key)
                .then(
                    (data) => {
                        resolve(data);
                        //console.log(data)
                    },
                    (error) => {
                        reject(error)
                        //console.error(error)
                    });
        });
    }
    getpermitionAndroid() {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE]);
        return new Promise((resolve, reject) => {
            return this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
                (data) => {
                    if (data.hasPermission) {
                        resolve(data);
                    } else {
                        reject(data);
                    }

                    //console.log(data)
                },
                (error) => {
                    // this.getpermitionAndroid();
                    reject(error);
                });
        });

    }

    clear() {
        this.nativeStorage.clear();
    }

    getValue() {
        return this._setValue;
    }
    setValue(value: any) {
        this._setValue = value;
    }

    setcityList(list) {
        this._cityList = list;
    }

    getCityList() {
        let sucess = localStorage.getItem("fromDashboard");
        localStorage.setItem("city", JSON.stringify(this._cityList));
        if (sucess == "Y") {
            let newCityList = JSON.parse(localStorage.getItem("city"));
            let selectedCity = newCityList.findIndex(X => X.selected == true);
            if (selectedCity > -1) {
                newCityList[selectedCity].selected = false;
            }
            return newCityList;
        } else {
            return this._cityList;
        }

    }

    setPossessionList(value) {
        this._possessionList = value;
    }
    getPossessionList() {
        let sucess = localStorage.getItem("fromDashboard");
        localStorage.setItem("possession", JSON.stringify(this._possessionList));
        if (sucess == "Y") {
            let newPosseList = JSON.parse(localStorage.getItem("possession"));
            let selectedPosse = newPosseList.findIndex(X => X.selected == true);
            if (selectedPosse > -1) {
                newPosseList[selectedPosse].selected = false;
            }
            return newPosseList;
        } else {
            return this._possessionList;
        }
    }
    setTypologyList(value) {
        this._typologyList = value;
    }
    getTypologyList() {
        localStorage.setItem("typo", JSON.stringify(this._typologyList));
        let sucess = localStorage.getItem("fromDashboard");
        if (sucess == "Y") {
            let newTypoList = JSON.parse(localStorage.getItem("typo"));
            let selectedTypo = newTypoList.findIndex(X => X.selected == true);
            if (selectedTypo > -1) {
                newTypoList[selectedTypo].selected = false;
            }
            return newTypoList;
        } else {
            return this._typologyList;
        }
    }
    setPrice(value) {
        this._price = value;
    }
    getPrice() {
        return this._price;
    }
    getFromCamera(source: any) {

        const options: CameraOptions = {
            quality: 90,
            /*
              DATA_URL (0) : Return image as base64-encoded string
              FILE_URI (1) : Return image file URI
              NATIVE_URI(2):Return image native URI
            */
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: source,/* PHOTOLIBRARY : 0, CAMERA : 1, SAVEDPHOTOALBUM : 2 */
            allowEdit: true,
            encodingType: this.camera.EncodingType.JPEG,/* JPEG : 0 Return JPEG encoded image PNG : 1 Return PNG encoded image */
            targetWidth: 450,
            targetHeight: 450,
            mediaType: 0,
            /*
              Only works when PictureSourceType is PHOTOLIBRARY or SAVEDPHOTOALBUM.
              Defined in Camera.MediaType PICTURE: 0 allow selection of still pictures only.
              DEFAULT. Will return format specified via DestinationType VIDEO: 1 allow selection of video only,
              WILL ALWAYS RETURN FILE_URI ALLMEDIA : 2 allow selection from all media types
            */
            correctOrientation: true,
            saveToPhotoAlbum: false,
            cameraDirection: 0,/* BACK: 0 FRONT: 1 */

        }

        return options;
    }

    localNotificationNoNetwork(value: any) {
        if (value == "offline") {
            this.localNotifications.schedule({
                id: 0,
                title: 'No Network',
                text: 'Searching For Network',
                sticky: true,
                vibrate: false,
                progressBar: { value: 100 }
            });
        } else {

            this.localNotifications.clear(0).then((data: any) => {
                this.localNotifications.getIds().then((notiArr: any) => {
                    console.log(notiArr);
                }, (err: any) => {
                    console.log(err);
                }).catch((error: any) => {
                    console.log(error);
                });
            }, (err: any) => {
                console.log(err);
            }).catch((error: any) => {
                console.log(error);
            });
        }
    }

    updateLocalNotification(value: any) {
        this.localNotifications.get(value).then(
            (data: any) => {
                let options = {
                    id: 0,
                    vibrate: false,
                    progressBar: { value: (data.progressBar.value < 100) ? data.progressBar.value + 1 : 1 }
                }
                this.localNotifications.setDefaults({ vibrate: false });
                this.localNotifications.update(options);
                console.log(data);
            }, (err: any) => {
                console.log(err);
            }
        ).catch(
            (error: any) => {
                console.log(error);
            }
        );
    }

    localNotificationWhenAppIsInForeGround(data: any) {
        console.log("localNotificationWhenAppIsInForeGround : " + JSON.stringify(data));
        this.localNotifications.schedule({
            id: 1,
            text: data.message,
            //sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
            data: { secret: data },
            foreground: true
            //attachments: ['https://internal.talash.net/inthub/public/images/logo.jpg'],
            //icon:'https://internal.talash.net/inthub/public/images/madAward.png',
        });
    }

    getLocation() {
        return new Promise((resolve, reject) => {
            /* const isAndroid = (this.device.platform.toLowerCase() === 'android') ? true : false; */
            this.geolocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 5000 }).then((resp) => {
                resolve(resp);
                this.events.publish('locationPermissionResolve', resp);
            }).catch((error) => {
                reject("location error");
                this.events.publish('locationPermissionReject');
            });
            /* if (isAndroid) {
                this.diagnostic.isLocationEnabled().then((callback) => {
                    if (callback == false) {
                        // this.toast.show('Please enable your lcation');
                        reject("error");
                    } else {
                        this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
                            resolve(resp);
                        }).catch((error) => {
                            reject("error");
                        });
                    }
                }, (err) => {
                    reject("error");
                }).catch((error) => {
                    reject("error");
                });
            } else {
                this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
                    resolve(resp);
                }).catch((error) => {
                    reject("error");
                });
            } */

        });
    }

    getReverseGeoCode(lat: any, long: any) {

        return new Promise((resolve, reject) => {
            let options: NativeGeocoderOptions = {
                useLocale: true,
                maxResults: 10
            };

            this.nativeGeocoder.reverseGeocode(lat, long, options)
                .then((result: NativeGeocoderReverseResult[]) => {
                    resolve(result[0]);
                }, (err: any) => {
                    reject("error");
                })
                .catch((error: any) => {
                    reject("error");
                });

        })

    }

    setEvent(eventName: any, date: any) {

        var dOb = date.split('-');
        this.localNotifications.schedule({
            id: 110,
            title: eventName + '!!!',
            trigger: { every: { month: Number(dOb[1]), day: Number(dOb[2]), hour: 10, minute: 50 } },
            foreground: false
        });
    }

    getInitials(fName: any, lName: any) {
        return new Promise((resolve, reject) => {
            if (fName != "" && lName != "") {
                let fInitials = fName.substring(0, 1).toUpperCase();
                let lInitials = lName.substring(0, 1).toUpperCase();
                let fullInitial = fInitials + lInitials;
                resolve(fullInitial)
            } else {
                resolve("GPL");
            }
        });
    }

    getUserDetails() {
        return new Promise((resolve, reject) => {
            return this.getVal("userDetails").then((data: any) => {
                let userDetails = JSON.parse(data);
                let fInitials = userDetails.first_name.substring(0, 1).toUpperCase();
                let lInitials = userDetails.last_name.substring(0, 1).toUpperCase();
                let fullInitial = fInitials + lInitials;
                resolve({ userDetails: userDetails, image: "", initials: fullInitial });
            }, (err) => {
                resolve("error");
            }).catch((error) => {
                resolve("error");
            });
        });

    }

    setMinMaxDate() {
        return new Promise((resolve, reject) => {
            let day = new Date();
            let nextDay = new Date(day);
            nextDay.setDate(day.getDate() + 1);
            let minDate = nextDay.toISOString();
            let nextYear = nextDay.setFullYear(nextDay.getFullYear() + 1);
            let maxDate = new Date(nextYear).toISOString();
            resolve(minDate + "~" + maxDate);
        });

    }

    socialShare(message: any, subject: any, file: any, url: any) {
        console.log("message : " + message + " \nsubject : " + subject + " \nfile : " + file + " \nurl : " + url);
        this.loader.show('Please wait...');
        this.socialSharing.share(message, subject, file, url).then(() => {
            this.loader.hide();
        }).catch(() => {
            this.loader.hide();
            // Error!
        });

    }

    openInAppBrowser(url: any) {
        // const browser = this.iab.create('https://ionicframework.com/');
        this.iab.create(url, "_self", this.options)
    }

    calendarEvent(title: any, location: any, notes: any, startDate: any, endDate: any) {

        this.calendar.createEventInteractively(title, location, notes, startDate, endDate).then((data: any) => {
            console.log(data);
        }).catch((err: any) => {
            console.log(err)
        });
    }


    getCountryCodes() {
        let countryCodes = [
            {
                "name": "India",
                "callingCodes": "+91",
                "alpha3Code": "IND",
                "flag": "https://restcountries.eu/data/ind.svg"
            },
            {
                "name": "Afghanistan",
                "callingCodes": "+93",
                "alpha3Code": "AFG",
                "flag": "https://restcountries.eu/data/afg.svg"
            },
            {
                "name": "Åland Islands",
                "callingCodes": "+358",
                "alpha3Code": "ALA",
                "flag": "https://restcountries.eu/data/ala.svg"

            },
            {
                "name": "Albania",
                "callingCodes": "+355",
                "alpha3Code": "ALB",
                "flag": "https://restcountries.eu/data/alb.svg"

            },
            {
                "name": "Algeria",
                "callingCodes": "+213",
                "alpha3Code": "DZA",
                "flag": "https://restcountries.eu/data/dza.svg"
            },
            {
                "name": "American Samoa",
                "callingCodes": "+1684",
                "alpha3Code": "ASM",
                "flag": "https://restcountries.eu/data/asm.svg"
            },
            {
                "name": "Andorra",
                "callingCodes": "+376",
                "alpha3Code": "AND",
                "flag": "https://restcountries.eu/data/and.svg"
            },
            {
                "name": "Angola",
                "callingCodes": "+244",
                "alpha3Code": "AGO",
                "flag": "https://restcountries.eu/data/ago.svg"

            },
            {
                "name": "Anguilla",
                "callingCodes": "+1264",
                "alpha3Code": "AIA",
                "flag": "https://restcountries.eu/data/aia.svg"

            },
            {
                "name": "Antarctica",
                "callingCodes": "+672",
                "alpha3Code": "ATA",
                "flag": "https://restcountries.eu/data/ata.svg"

            },
            {
                "name": "Antigua and Barbuda",
                "callingCodes": "+1268",
                "alpha3Code": "ATG",
                "flag": "https://restcountries.eu/data/atg.svg"

            },

            {
                "name": "Argentina",
                "callingCodes": "+54",
                "alpha3Code": "ARG",
                "flag": "https://restcountries.eu/data/arg.svg"

            },
            {
                "name": "Armenia",
                "callingCodes": "+374",
                "alpha3Code": "ARM",
                "flag": "https://restcountries.eu/data/arm.svg"

            },
            {
                "name": "Aruba",
                "callingCodes": "+297",
                "alpha3Code": "ABW",
                "flag": "https://restcountries.eu/data/abw.svg"
            },
            {
                "name": "Australia",
                "callingCodes": "+61",
                "alpha3Code": "AUS",
                "flag": "https://restcountries.eu/data/aus.svg"

            },
            {
                "name": "Austria",
                "callingCodes": "+43",
                "alpha3Code": "AUT",
                "flag": "https://restcountries.eu/data/aut.svg"

            },
            {
                "name": "Azerbaijan",
                "callingCodes": "+994",
                "alpha3Code": "AZE",
                "flag": "https://restcountries.eu/data/aze.svg"

            },
            {
                "name": "Bahamas",
                "callingCodes": "+1242",
                "alpha3Code": "BHS",
                "flag": "https://restcountries.eu/data/bhs.svg"

            },
            {
                "name": "Bahrain",
                "callingCodes": "+973",
                "alpha3Code": "BHR",
                "flag": "https://restcountries.eu/data/bhr.svg"

            },
            {
                "name": "Bangladesh",
                "callingCodes": "+880",
                "alpha3Code": "BGD",
                "flag": "https://restcountries.eu/data/bgd.svg"

            },
            {
                "name": "Barbados",
                "callingCodes": "+1246",
                "alpha3Code": "BRB",
                "flag": "https://restcountries.eu/data/brb.svg"

            },
            {
                "name": "Belarus",
                "callingCodes": "+375",
                "alpha3Code": "BLR",
                "flag": "https://restcountries.eu/data/blr.svg"

            },
            {
                "name": "Belgium",
                "callingCodes": "+32",
                "alpha3Code": "BEL",
                "flag": "https://restcountries.eu/data/bel.svg"

            },
            {
                "name": "Belize",
                "callingCodes": "+501",
                "alpha3Code": "BLZ",
                "flag": "https://restcountries.eu/data/blz.svg"

            },
            {
                "name": "Benin",
                "callingCodes": "+229",
                "alpha3Code": "BEN",
                "flag": "https://restcountries.eu/data/ben.svg"

            },
            {
                "name": "Bermuda",
                "callingCodes": "+1441",
                "alpha3Code": "BMU",
                "flag": "https://restcountries.eu/data/bmu.svg"

            },
            {
                "name": "Bhutan",
                "callingCodes": "+975",
                "alpha3Code": "BTN",
                "flag": "https://restcountries.eu/data/btn.svg"

            },
            {
                "name": "Bolivia (Plurinational State of)",
                "callingCodes": "+591",
                "alpha3Code": "BOL",
                "flag": "https://restcountries.eu/data/bol.svg"

            },
            {
                "name": "Bonaire, Sint Eustatius and Saba",
                "callingCodes": "+5997",
                "alpha3Code": "BES",
                "flag": "https://restcountries.eu/data/bes.svg"

            },
            {
                "name": "Bosnia and Herzegovina",
                "callingCodes": "+387",
                "alpha3Code": "BIH",
                "flag": "https://restcountries.eu/data/bih.svg"

            },
            {
                "name": "Botswana",
                "callingCodes": "+267",
                "alpha3Code": "BWA",
                "flag": "https://restcountries.eu/data/bwa.svg"

            },
            {
                "name": "Brazil",
                "callingCodes": "+55",
                "alpha3Code": "BRA",
                "flag": "https://restcountries.eu/data/bra.svg"

            },
            {
                "name": "British Indian Ocean Territory",
                "callingCodes": "+246",
                "alpha3Code": "IOT",
                "flag": "https://restcountries.eu/data/iot.svg"

            },
            {
                "name": "Virgin Islands (British)",
                "callingCodes": "+1284",
                "alpha3Code": "VGB",
                "flag": "https://restcountries.eu/data/vgb.svg"

            },
            {
                "name": "Virgin Islands (U.S.)",
                "callingCodes": "+1 340",
                "alpha3Code": "VIR",
                "flag": "https://restcountries.eu/data/vir.svg"

            },
            {
                "name": "Brunei Darussalam",
                "callingCodes": "+673",
                "alpha3Code": "BRN",
                "flag": "https://restcountries.eu/data/brn.svg"

            },
            {
                "name": "Bulgaria",
                "callingCodes": "+359",
                "alpha3Code": "BGR",
                "flag": "https://restcountries.eu/data/bgr.svg"

            },
            {
                "name": "Burkina Faso",
                "callingCodes": "+226",
                "alpha3Code": "BFA",
                "flag": "https://restcountries.eu/data/bfa.svg"

            },
            {
                "name": "Burundi",
                "callingCodes": "+257",
                "alpha3Code": "BDI",
                "flag": "https://restcountries.eu/data/bdi.svg"

            },
            {
                "name": "Cambodia",
                "callingCodes": "+855",
                "alpha3Code": "KHM",
                "flag": "https://restcountries.eu/data/khm.svg"

            },
            {
                "name": "Cameroon",
                "callingCodes": "+237",
                "alpha3Code": "CMR",
                "flag": "https://restcountries.eu/data/cmr.svg"

            },
            {
                "name": "Canada",
                "callingCodes": "+1",
                "alpha3Code": "CAN",
                "flag": "https://restcountries.eu/data/can.svg"

            },
            {
                "name": "Cabo Verde",
                "callingCodes": "238",
                "alpha3Code": "CPV",
                "flag": "https://restcountries.eu/data/cpv.svg"

            },
            {
                "name": "Cayman Islands",
                "callingCodes": "+1345",
                "alpha3Code": "CYM",
                "flag": "https://restcountries.eu/data/cym.svg"

            },
            {
                "name": "Central African Republic",
                "callingCodes": "+236",
                "alpha3Code": "CAF",
                "flag": "https://restcountries.eu/data/caf.svg"

            },
            {
                "name": "Chad",
                "callingCodes": "+235",
                "alpha3Code": "TCD",
                "flag": "https://restcountries.eu/data/tcd.svg"

            },
            {
                "name": "Chile",
                "callingCodes": "+56",
                "alpha3Code": "CHL",
                "flag": "https://restcountries.eu/data/chl.svg"

            },
            {
                "name": "China",
                "callingCodes": "+86",
                "alpha3Code": "CHN",
                "flag": "https://restcountries.eu/data/chn.svg"

            },
            {
                "name": "Christmas Island",
                "callingCodes": "+61",
                "alpha3Code": "CXR",
                "flag": "https://restcountries.eu/data/cxr.svg"

            },
            {
                "name": "Colombia",
                "callingCodes": "+57",
                "alpha3Code": "COL",
                "flag": "https://restcountries.eu/data/col.svg"

            },
            {
                "name": "Comoros",
                "callingCodes": "+269",
                "alpha3Code": "COM",
                "flag": "https://restcountries.eu/data/com.svg"

            },
            {
                "name": "Congo",
                "callingCodes": "+242",
                "alpha3Code": "COG",
                "flag": "https://restcountries.eu/data/cog.svg"

            },
            {
                "name": "Congo (Democratic Republic of the)",
                "callingCodes": "+243",
                "alpha3Code": "COD",
                "flag": "https://restcountries.eu/data/cod.svg"

            },
            {
                "name": "Cook Islands",
                "callingCodes": "+682",
                "alpha3Code": "COK",
                "flag": "https://restcountries.eu/data/cok.svg"

            },
            {
                "name": "Costa Rica",
                "callingCodes": "+506",
                "alpha3Code": "CRI",
                "flag": "https://restcountries.eu/data/cri.svg"

            },
            {
                "name": "Croatia",
                "callingCodes": "+385",
                "alpha3Code": "HRV",
                "flag": "https://restcountries.eu/data/hrv.svg"

            },
            {
                "name": "Cuba",
                "callingCodes": "+53",
                "alpha3Code": "CUB",
                "flag": "https://restcountries.eu/data/cub.svg"

            },
            {
                "name": "Curaçao",
                "callingCodes": "+599",
                "alpha3Code": "CUW",
                "flag": "https://restcountries.eu/data/cuw.svg"

            },
            {
                "name": "Cyprus",
                "callingCodes": "+357",
                "alpha3Code": "CYP",
                "flag": "https://restcountries.eu/data/cyp.svg"

            },
            {
                "name": "Czech Republic",
                "callingCodes": "+420",
                "alpha3Code": "CZE",
                "flag": "https://restcountries.eu/data/cze.svg"

            },
            {
                "name": "Denmark",
                "callingCodes": "+45",
                "alpha3Code": "DNK",
                "flag": "https://restcountries.eu/data/dnk.svg"

            },
            {
                "name": "Djibouti",
                "callingCodes": "+253",
                "alpha3Code": "DJI",
                "flag": "https://restcountries.eu/data/dji.svg"

            },
            {
                "name": "Dominica",
                "callingCodes": "+1767",
                "alpha3Code": "DMA",
                "flag": "https://restcountries.eu/data/dma.svg"

            },
            {
                "name": "Dominican Republic",
                "callingCodes": "+1809",
                "alpha3Code": "DOM",
                "flag": "https://restcountries.eu/data/dom.svg"

            },
            {
                "name": "Ecuador",
                "callingCodes": "+593",
                "alpha3Code": "ECU",
                "flag": "https://restcountries.eu/data/ecu.svg"
            },
            {
                "name": "Egypt",
                "callingCodes": "+20",
                "alpha3Code": "EGY",
                "flag": "https://restcountries.eu/data/egy.svg"

            },
            {
                "name": "El Salvador",
                "callingCodes": "+503",
                "alpha3Code": "SLV",
                "flag": "https://restcountries.eu/data/slv.svg"

            },
            {
                "name": "Equatorial Guinea",
                "callingCodes": "+240",
                "alpha3Code": "GNQ",
                "flag": "https://restcountries.eu/data/gnq.svg"

            },
            {
                "name": "Eritrea",
                "callingCodes": "+291",
                "alpha3Code": "ERI",
                "flag": "https://restcountries.eu/data/eri.svg"

            },
            {
                "name": "Estonia",
                "callingCodes": "+372",
                "alpha3Code": "EST",
                "flag": "https://restcountries.eu/data/est.svg"

            },
            {
                "name": "Ethiopia",
                "callingCodes": "+251",
                "alpha3Code": "ETH",
                "flag": "https://restcountries.eu/data/eth.svg"

            },
            {
                "name": "Falkland Islands (Malvinas)",
                "callingCodes": "+500",
                "alpha3Code": "FLK",
                "flag": "https://restcountries.eu/data/flk.svg"

            },
            {
                "name": "Faroe Islands",
                "callingCodes": "+298",
                "alpha3Code": "FRO",
                "flag": "https://restcountries.eu/data/fro.svg"

            },
            {
                "name": "Fiji",
                "callingCodes": "+679",
                "alpha3Code": "FJI",
                "flag": "https://restcountries.eu/data/fji.svg"

            },
            {
                "name": "Finland",
                "callingCodes": "+358",
                "alpha3Code": "FIN",
                "flag": "https://restcountries.eu/data/fin.svg"

            },
            {
                "name": "France",
                "callingCodes": "+33",
                "alpha3Code": "FRA",
                "flag": "https://restcountries.eu/data/fra.svg"

            },
            {
                "name": "French Guiana",
                "callingCodes": "+594",
                "alpha3Code": "GUF",
                "flag": "https://restcountries.eu/data/guf.svg"


            },
            {
                "name": "French Polynesia",
                "callingCodes": "+689",
                "alpha3Code": "PYF",
                "flag": "https://restcountries.eu/data/pyf.svg"

            },

            {
                "name": "Gabon",
                "callingCodes": "241",
                "alpha3Code": "GAB",
                "flag": "https://restcountries.eu/data/gab.svg"

            },
            {
                "name": "Gambia",
                "callingCodes": "+220",
                "alpha3Code": "GMB",
                "flag": "https://restcountries.eu/data/gmb.svg"

            },
            {
                "name": "Georgia",
                "callingCodes": "+995",
                "alpha3Code": "GEO",
                "flag": "https://restcountries.eu/data/geo.svg"

            },
            {
                "name": "Germany",
                "callingCodes": "+49",
                "alpha3Code": "DEU",
                "flag": "https://restcountries.eu/data/deu.svg"

            },
            {
                "name": "Ghana",
                "callingCodes": "+233",
                "alpha3Code": "GHA",
                "flag": "https://restcountries.eu/data/gha.svg"

            },
            {
                "name": "Gibraltar",
                "callingCodes": "+350",
                "alpha3Code": "GIB",
                "flag": "https://restcountries.eu/data/gib.svg"

            },
            {
                "name": "Greece",
                "callingCodes": "+30",
                "alpha3Code": "GRC",
                "flag": "https://restcountries.eu/data/grc.svg"

            },
            {
                "name": "Greenland",
                "callingCodes": "+299",
                "alpha3Code": "GRL",
                "flag": "https://restcountries.eu/data/grl.svg"

            },
            {
                "name": "Grenada",
                "callingCodes": "+1473",
                "alpha3Code": "GRD",
                "flag": "https://restcountries.eu/data/grd.svg"

            },
            {
                "name": "Guadeloupe",
                "callingCodes": "590",
                "alpha3Code": "GLP",
                "flag": "https://restcountries.eu/data/glp.svg"

            },
            {
                "name": "Guam",
                "callingCodes": "+1671",
                "alpha3Code": "GUM",
                "flag": "https://restcountries.eu/data/gum.svg"

            },
            {
                "name": "Guatemala",
                "callingCodes": "+502",
                "alpha3Code": "GTM",
                "flag": "https://restcountries.eu/data/gtm.svg"

            },
            {
                "name": "Guernsey",
                "callingCodes": "+44",
                "alpha3Code": "GGY",
                "flag": "https://restcountries.eu/data/ggy.svg"

            },
            {
                "name": "Guinea",
                "callingCodes": "+224",
                "alpha3Code": "GIN",
                "flag": "https://restcountries.eu/data/gin.svg"

            },
            {
                "name": "Guinea-Bissau",
                "callingCodes": "+245",
                "alpha3Code": "GNB",
                "flag": "https://restcountries.eu/data/gnb.svg"

            },
            {
                "name": "Guyana",
                "callingCodes": "+592",
                "alpha3Code": "GUY",
                "flag": "https://restcountries.eu/data/guy.svg"

            },
            {
                "name": "Haiti",
                "callingCodes": "+509",
                "alpha3Code": "HTI",
                "flag": "https://restcountries.eu/data/hti.svg"

            },

            {
                "name": "Holy See",
                "callingCodes": "+379",
                "alpha3Code": "VAT",
                "flag": "https://restcountries.eu/data/vat.svg"

            },
            {
                "name": "Honduras",
                "callingCodes": "+504",
                "alpha3Code": "HND",
                "flag": "https://restcountries.eu/data/hnd.svg"

            },
            {
                "name": "Hong Kong",
                "callingCodes": "+852",
                "alpha3Code": "HKG",
                "flag": "https://restcountries.eu/data/hkg.svg"

            },
            {
                "name": "Hungary",
                "callingCodes": "+36",
                "alpha3Code": "HUN",
                "flag": "https://restcountries.eu/data/hun.svg"

            },
            {
                "name": "Iceland",
                "callingCodes": "+354",
                "alpha3Code": "ISL",
                "flag": "https://restcountries.eu/data/isl.svg"

            },

            {
                "name": "Indonesia",
                "callingCodes": "+62",
                "alpha3Code": "IDN",
                "flag": "https://restcountries.eu/data/idn.svg"

            },
            {
                "name": "Côte d'Ivoire",
                "callingCodes": "+225",
                "alpha3Code": "CIV",
                "flag": "https://restcountries.eu/data/civ.svg"

            },
            {
                "name": "Iran (Islamic Republic of)",
                "callingCodes": "+98",
                "alpha3Code": "IRN",
                "flag": "https://restcountries.eu/data/irn.svg"

            },
            {
                "name": "Iraq",
                "callingCodes": "+964",
                "alpha3Code": "IRQ",
                "flag": "https://restcountries.eu/data/irq.svg"

            },
            {
                "name": "Ireland",
                "callingCodes": "+353",
                "alpha3Code": "IRL",
                "flag": "https://restcountries.eu/data/irl.svg"

            },
            {
                "name": "Isle of Man",
                "callingCodes": "+44",
                "alpha3Code": "IMN",
                "flag": "https://restcountries.eu/data/imn.svg"

            },
            {
                "name": "Israel",
                "callingCodes": "+972",
                "alpha3Code": "ISR",
                "flag": "https://restcountries.eu/data/isr.svg"

            },
            {
                "name": "Italy",
                "callingCodes": "+39",
                "alpha3Code": "ITA",
                "flag": "https://restcountries.eu/data/ita.svg"

            },
            {
                "name": "Jamaica",
                "callingCodes": "+1876",
                "alpha3Code": "JAM",
                "flag": "https://restcountries.eu/data/jam.svg"

            },
            {
                "name": "Japan",
                "callingCodes": "+81",
                "alpha3Code": "JPN",
                "flag": "https://restcountries.eu/data/jpn.svg"

            },
            {
                "name": "Jersey",
                "callingCodes": "+44",
                "alpha3Code": "JEY",
                "flag": "https://restcountries.eu/data/jey.svg"

            },
            {
                "name": "Jordan",
                "callingCodes": "+962",
                "alpha3Code": "JOR",
                "flag": "https://restcountries.eu/data/jor.svg"

            },
            {
                "name": "Kazakhstan",
                "callingCodes": "+76",
                "alpha3Code": "KAZ",
                "flag": "https://restcountries.eu/data/kaz.svg"

            },
            {
                "name": "Kenya",
                "callingCodes": "+254",
                "alpha3Code": "KEN",
                "flag": "https://restcountries.eu/data/ken.svg"

            },
            {
                "name": "Kiribati",
                "callingCodes": "+686",
                "alpha3Code": "KIR",
                "flag": "https://restcountries.eu/data/kir.svg"

            },
            {
                "name": "Kuwait",
                "callingCodes": "+965",
                "alpha3Code": "KWT",
                "flag": "https://restcountries.eu/data/kwt.svg"

            },
            {
                "name": "Kyrgyzstan",
                "callingCodes": "+996",
                "alpha3Code": "KGZ",
                "flag": "https://restcountries.eu/data/kgz.svg"

            },
            {
                "name": "Lao People's Democratic Republic",
                "callingCodes": "+856",
                "alpha3Code": "LAO",
                "flag": "https://restcountries.eu/data/lao.svg"

            },
            {
                "name": "Latvia",
                "callingCodes": "+371",
                "alpha3Code": "LVA",
                "flag": "https://restcountries.eu/data/lva.svg"

            },
            {
                "name": "Lebanon",
                "callingCodes": "+961",
                "alpha3Code": "LBN",
                "flag": "https://restcountries.eu/data/lbn.svg"

            },
            {
                "name": "Lesotho",
                "callingCodes": "+266",
                "alpha3Code": "LSO",
                "flag": "https://restcountries.eu/data/lso.svg"
            },
            {
                "name": "Liberia",
                "callingCodes": "+231",
                "alpha3Code": "LBR",
                "flag": "https://restcountries.eu/data/lbr.svg"
            },
            {
                "name": "Libya",
                "callingCodes": "+218",
                "alpha3Code": "LBY",
                "flag": "https://restcountries.eu/data/lby.svg"
            },
            {
                "name": "Liechtenstein",
                "callingCodes": "+423",
                "alpha3Code": "LIE",
                "flag": "https://restcountries.eu/data/lie.svg"
            },
            {
                "name": "Lithuania",
                "callingCodes": "+370",
                "alpha3Code": "LTU",
                "flag": "https://restcountries.eu/data/ltu.svg"
            },
            {
                "name": "Luxembourg",
                "callingCodes": "+352",
                "alpha3Code": "LUX",
                "flag": "https://restcountries.eu/data/lux.svg"
            },
            {
                "name": "Macao",
                "callingCodes": "+853",
                "alpha3Code": "MAC",
                "flag": "https://restcountries.eu/data/mac.svg"
            },
            {
                "name": "Macedonia (the former Yugoslav Republic of)",
                "callingCodes": "+389",
                "alpha3Code": "MKD",
                "flag": "https://restcountries.eu/data/mkd.svg"
            },
            {
                "name": "Madagascar",
                "callingCodes": "+261",
                "alpha3Code": "MDG",
                "flag": "https://restcountries.eu/data/mdg.svg"
            },
            {
                "name": "Malawi",
                "callingCodes": "+265",
                "alpha3Code": "MWI",
                "flag": "https://restcountries.eu/data/mwi.svg"
            },
            {
                "name": "Malaysia",
                "callingCodes": "+60",
                "alpha3Code": "MYS",
                "flag": "https://restcountries.eu/data/mys.svg"
            },
            {
                "name": "Maldives",
                "callingCodes": "+960",
                "alpha3Code": "MDV",
                "flag": "https://restcountries.eu/data/mdv.svg"
            },
            {
                "name": "Mali",
                "callingCodes": "+223",
                "alpha3Code": "MLI",
                "flag": "https://restcountries.eu/data/mli.svg"
            },
            {
                "name": "Malta",
                "callingCodes": "+356",
                "alpha3Code": "MLT",
                "flag": "https://restcountries.eu/data/mlt.svg"
            },
            {
                "name": "Marshall Islands",
                "callingCodes": "+692",
                "alpha3Code": "MHL",
                "flag": "https://restcountries.eu/data/mhl.svg"
            },
            {
                "name": "Martinique",
                "callingCodes": "+596",
                "alpha3Code": "MTQ",
                "flag": "https://restcountries.eu/data/mtq.svg"
            },
            {
                "name": "Mauritania",
                "callingCodes": "+222",
                "alpha3Code": "MRT",
                "flag": "https://restcountries.eu/data/mrt.svg"
            },
            {
                "name": "Mauritius",
                "callingCodes": "+230",
                "alpha3Code": "MUS",
                "flag": "https://restcountries.eu/data/mus.svg"
            },
            {
                "name": "Mayotte",
                "callingCodes": "+262",
                "alpha3Code": "MYT",
                "flag": "https://restcountries.eu/data/myt.svg"
            },
            {
                "name": "Mexico",
                "callingCodes": "+52",
                "alpha3Code": "MEX",
                "flag": "https://restcountries.eu/data/mex.svg"
            },
            {
                "name": "Micronesia (Federated States of)",
                "callingCodes": "+691",
                "alpha3Code": "FSM",
                "flag": "https://restcountries.eu/data/fsm.svg"
            },
            {
                "name": "Moldova (Republic of)",
                "callingCodes": "+373",
                "alpha3Code": "MDA",
                "flag": "https://restcountries.eu/data/mda.svg"
            },
            {
                "name": "Monaco",
                "callingCodes": "+377",
                "alpha3Code": "MCO",
                "flag": "https://restcountries.eu/data/mco.svg"
            },
            {
                "name": "Mongolia",
                "callingCodes": "+976",
                "alpha3Code": "MNG",
                "flag": "https://restcountries.eu/data/mng.svg"
            },
            {
                "name": "Montenegro",
                "callingCodes": "+382",
                "alpha3Code": "MNE",
                "flag": "https://restcountries.eu/data/mne.svg"
            },
            {
                "name": "Montserrat",
                "callingCodes": "+1664",
                "alpha3Code": "MSR",
                "flag": "https://restcountries.eu/data/msr.svg"
            },
            {
                "name": "Morocco",
                "callingCodes": "+212",
                "alpha3Code": "MAR",
                "flag": "https://restcountries.eu/data/mar.svg"
            },
            {
                "name": "Mozambique",
                "callingCodes": "+258",
                "alpha3Code": "MOZ",
                "flag": "https://restcountries.eu/data/moz.svg"
            },
            {
                "name": "Myanmar",
                "callingCodes": "+95",
                "alpha3Code": "MMR",
                "flag": "https://restcountries.eu/data/mmr.svg"
            },
            {
                "name": "Namibia",
                "callingCodes": "+264",
                "alpha3Code": "NAM",
                "flag": "https://restcountries.eu/data/nam.svg"
            },
            {
                "name": "Nauru",
                "callingCodes": "+674",
                "alpha3Code": "NRU",
                "flag": "https://restcountries.eu/data/nru.svg"
            },
            {
                "name": "Nepal",
                "callingCodes": "+977",
                "alpha3Code": "NPL",
                "flag": "https://restcountries.eu/data/npl.svg"
            },
            {
                "name": "Netherlands",
                "callingCodes": "+31",
                "alpha3Code": "NLD",
                "flag": "https://restcountries.eu/data/nld.svg"
            },
            {
                "name": "New Caledonia",
                "callingCodes": "+687",
                "alpha3Code": "NCL",
                "flag": "https://restcountries.eu/data/ncl.svg"
            },
            {
                "name": "New Zealand",
                "callingCodes": "+64",
                "alpha3Code": "NZL",
                "flag": "https://restcountries.eu/data/nzl.svg"
            },
            {
                "name": "Nicaragua",
                "callingCodes": "+505",
                "alpha3Code": "NIC",
                "flag": "https://restcountries.eu/data/nic.svg"
            },
            {
                "name": "Niger",
                "callingCodes": "+227",
                "alpha3Code": "NER",
                "flag": "https://restcountries.eu/data/ner.svg"
            },
            {
                "name": "Nigeria",
                "callingCodes": "+234",
                "alpha3Code": "NGA",
                "flag": "https://restcountries.eu/data/nga.svg"
            },
            {
                "name": "Niue",
                "callingCodes": "+683",
                "alpha3Code": "NIU",
                "flag": "https://restcountries.eu/data/niu.svg"
            },
            {
                "name": "Norfolk Island",
                "callingCodes": "+672",
                "alpha3Code": "NFK",
                "flag": "https://restcountries.eu/data/nfk.svg"
            },
            {
                "name": "Korea (Democratic People's Republic of)",
                "callingCodes": "+850",
                "alpha3Code": "PRK",
                "flag": "https://restcountries.eu/data/prk.svg"
            },
            {
                "name": "Northern Mariana Islands",
                "callingCodes": "+1670",
                "alpha3Code": "MNP",
                "flag": "https://restcountries.eu/data/mnp.svg"
            },
            {
                "name": "Norway",
                "callingCodes": "+47",
                "alpha3Code": "NOR",
                "flag": "https://restcountries.eu/data/nor.svg"
            },
            {
                "name": "Oman",
                "callingCodes": "+968",
                "alpha3Code": "OMN",
                "flag": "https://restcountries.eu/data/omn.svg"
            },
            {
                "name": "Pakistan",
                "callingCodes": "+92",
                "alpha3Code": "PAK",
                "flag": "https://restcountries.eu/data/pak.svg"
            },
            {
                "name": "Palau",
                "callingCodes": "+680",
                "alpha3Code": "PLW",
                "flag": "https://restcountries.eu/data/plw.svg"
            },
            {
                "name": "Palestine, State of",
                "callingCodes": "+970",
                "alpha3Code": "PSE",
                "flag": "https://restcountries.eu/data/pse.svg"
            },
            {
                "name": "Panama",
                "callingCodes": "+507",
                "alpha3Code": "PAN",
                "flag": "https://restcountries.eu/data/pan.svg"
            },
            {
                "name": "Papua New Guinea",
                "callingCodes": "+675",
                "alpha3Code": "PNG",
                "flag": "https://restcountries.eu/data/png.svg"
            },
            {
                "name": "Paraguay",
                "callingCodes": "+595",
                "alpha3Code": "PRY",
                "flag": "https://restcountries.eu/data/pry.svg"
            },
            {
                "name": "Peru",
                "callingCodes": "+51",
                "alpha3Code": "PER",
                "flag": "https://restcountries.eu/data/per.svg"
            },
            {
                "name": "Philippines",
                "callingCodes": "+63",
                "alpha3Code": "PHL",
                "flag": "https://restcountries.eu/data/phl.svg"
            },
            {
                "name": "Pitcairn",
                "callingCodes": "+64",
                "alpha3Code": "PCN",
                "flag": "https://restcountries.eu/data/pcn.svg"
            },
            {
                "name": "Poland",
                "callingCodes": "+48",
                "alpha3Code": "POL",
                "flag": "https://restcountries.eu/data/pol.svg"
            },
            {
                "name": "Portugal",
                "callingCodes": "+351",
                "alpha3Code": "PRT",
                "flag": "https://restcountries.eu/data/prt.svg"
            },
            {
                "name": "Puerto Rico",
                "callingCodes": "+1787",
                "alpha3Code": "PRI",
                "flag": "https://restcountries.eu/data/pri.svg"
            },
            {
                "name": "Qatar",
                "callingCodes": "+974",
                "alpha3Code": "QAT",
                "flag": "https://restcountries.eu/data/qat.svg"
            },
            {
                "name": "Republic of Kosovo",
                "callingCodes": "+383",
                "alpha3Code": "KOS",
                "flag": "https://restcountries.eu/data/kos.svg"
            },
            {
                "name": "Réunion",
                "callingCodes": "+262",
                "alpha3Code": "REU",
                "flag": "https://restcountries.eu/data/reu.svg"
            },
            {
                "name": "Romania",
                "callingCodes": "+40",
                "alpha3Code": "ROU",
                "flag": "https://restcountries.eu/data/rou.svg"
            },
            {
                "name": "Russian Federation",
                "callingCodes": "+7",
                "alpha3Code": "RUS",
                "flag": "https://restcountries.eu/data/rus.svg"
            },
            {
                "name": "Rwanda",
                "callingCodes": "+250",
                "alpha3Code": "RWA",
                "flag": "https://restcountries.eu/data/rwa.svg"
            },
            {
                "name": "Saint Barthélemy",
                "callingCodes": "+590",
                "alpha3Code": "BLM",
                "flag": "https://restcountries.eu/data/blm.svg"
            },
            {
                "name": "Saint Helena, Ascension and Tristan da Cunha",
                "callingCodes": "+290",
                "alpha3Code": "SHN",
                "flag": "https://restcountries.eu/data/shn.svg"
            },
            {
                "name": "Saint Kitts and Nevis",
                "callingCodes": "+1869",
                "alpha3Code": "KNA",
                "flag": "https://restcountries.eu/data/kna.svg"
            },
            {
                "name": "Saint Lucia",
                "callingCodes": "+1758",
                "alpha3Code": "LCA",
                "flag": "https://restcountries.eu/data/lca.svg"
            },
            {
                "name": "Saint Martin (French part)",
                "callingCodes": "+590",
                "alpha3Code": "MAF",
                "flag": "https://restcountries.eu/data/maf.svg"
            },
            {
                "name": "Saint Pierre and Miquelon",
                "callingCodes": "+508",
                "alpha3Code": "SPM",
                "flag": "https://restcountries.eu/data/spm.svg"
            },
            {
                "name": "Saint Vincent and the Grenadines",
                "callingCodes": "+1784",
                "alpha3Code": "VCT",
                "flag": "https://restcountries.eu/data/vct.svg"
            },
            {
                "name": "Samoa",
                "callingCodes": "+685",
                "alpha3Code": "WSM",
                "flag": "https://restcountries.eu/data/wsm.svg"
            },
            {
                "name": "San Marino",
                "callingCodes": "+378",
                "alpha3Code": "SMR",
                "flag": "https://restcountries.eu/data/smr.svg"
            },
            {
                "name": "Sao Tome and Principe",
                "callingCodes": "+239",
                "alpha3Code": "STP",
                "flag": "https://restcountries.eu/data/stp.svg"
            },
            {
                "name": "Saudi Arabia",
                "callingCodes": "+966",
                "alpha3Code": "SAU",
                "flag": "https://restcountries.eu/data/sau.svg"
            },
            {
                "name": "Senegal",
                "callingCodes": "+221",
                "alpha3Code": "SEN",
                "flag": "https://restcountries.eu/data/sen.svg"
            },
            {
                "name": "Serbia",
                "callingCodes": "+381",
                "alpha3Code": "SRB",
                "flag": "https://restcountries.eu/data/srb.svg"
            },
            {
                "name": "Seychelles",
                "callingCodes": "+248",
                "alpha3Code": "SYC",
                "flag": "https://restcountries.eu/data/syc.svg"
            },
            {
                "name": "Sierra Leone",
                "callingCodes": "+232",
                "alpha3Code": "SLE",
                "flag": "https://restcountries.eu/data/sle.svg"
            },
            {
                "name": "Singapore",
                "callingCodes": "+65",
                "alpha3Code": "SGP",
                "flag": "https://restcountries.eu/data/sgp.svg"
            },
            {
                "name": "Sint Maarten (Dutch part)",
                "callingCodes": "+1721",
                "alpha3Code": "SXM",
                "flag": "https://restcountries.eu/data/sxm.svg"
            },
            {
                "name": "Slovakia",
                "callingCodes": "+421",
                "alpha3Code": "SVK",
                "flag": "https://restcountries.eu/data/svk.svg"
            },
            {
                "name": "Slovenia",
                "callingCodes": "+386",
                "alpha3Code": "SVN",
                "flag": "https://restcountries.eu/data/svn.svg"
            },
            {
                "name": "Solomon Islands",
                "callingCodes": "+677",
                "alpha3Code": "SLB",
                "flag": "https://restcountries.eu/data/slb.svg"
            },
            {
                "name": "Somalia",
                "callingCodes": "+252",
                "alpha3Code": "SOM",
                "flag": "https://restcountries.eu/data/som.svg"
            },
            {
                "name": "South Africa",
                "callingCodes": "+27",
                "alpha3Code": "ZAF",
                "flag": "https://restcountries.eu/data/zaf.svg"
            },
            {
                "name": "South Georgia and the South Sandwich Islands",
                "callingCodes": "+500",
                "alpha3Code": "SGS",
                "flag": "https://restcountries.eu/data/sgs.svg"
            },
            {
                "name": "Korea (Republic of)",
                "callingCodes": "+82",
                "alpha3Code": "KOR",
                "flag": "https://restcountries.eu/data/kor.svg"
            },
            {
                "name": "South Sudan",
                "callingCodes": "+211",
                "alpha3Code": "SSD",
                "flag": "https://restcountries.eu/data/ssd.svg"
            },
            {
                "name": "Spain",
                "callingCodes": "+34",
                "alpha3Code": "ESP",
                "flag": "https://restcountries.eu/data/esp.svg"
            },
            {
                "name": "Sri Lanka",
                "callingCodes": "+94",
                "alpha3Code": "LKA",
                "flag": "https://restcountries.eu/data/lka.svg"
            },
            {
                "name": "Sudan",
                "callingCodes": "+249",
                "alpha3Code": "SDN",
                "flag": "https://restcountries.eu/data/sdn.svg"
            },
            {
                "name": "Suriname",
                "callingCodes": "+597",
                "alpha3Code": "SUR",
                "flag": "https://restcountries.eu/data/sur.svg"
            },
            {
                "name": "Svalbard and Jan Mayen",
                "callingCodes": "+4779",
                "alpha3Code": "SJM",
                "flag": "https://restcountries.eu/data/sjm.svg"
            },
            {
                "name": "Swaziland",
                "callingCodes": "+268",
                "alpha3Code": "SWZ",
                "flag": "https://restcountries.eu/data/swz.svg"
            },
            {
                "name": "Sweden",
                "callingCodes": "+46",
                "alpha3Code": "SWE",
                "flag": "https://restcountries.eu/data/swe.svg"
            },
            {
                "name": "Switzerland",
                "callingCodes": "+41",
                "alpha3Code": "CHE",
                "flag": "https://restcountries.eu/data/che.svg"
            },
            {
                "name": "Syrian Arab Republic",
                "callingCodes": "+963",
                "alpha3Code": "SYR",
                "flag": "https://restcountries.eu/data/syr.svg"
            },
            {
                "name": "Taiwan",
                "callingCodes": "+886",
                "alpha3Code": "TWN",
                "flag": "https://restcountries.eu/data/twn.svg"
            },
            {
                "name": "Tajikistan",
                "callingCodes": "+992",
                "alpha3Code": "TJK",
                "flag": "https://restcountries.eu/data/tjk.svg"
            },
            {
                "name": "Tanzania, United Republic of",
                "callingCodes": "+255",
                "alpha3Code": "TZA",
                "flag": "https://restcountries.eu/data/tza.svg"
            },
            {
                "name": "Thailand",
                "callingCodes": "+66",
                "alpha3Code": "THA",
                "flag": "https://restcountries.eu/data/tha.svg"
            },
            {
                "name": "Timor-Leste",
                "callingCodes": "+670",
                "alpha3Code": "TLS",
                "flag": "https://restcountries.eu/data/tls.svg"
            },
            {
                "name": "Togo",
                "callingCodes": "+228",
                "alpha3Code": "TGO",
                "flag": "https://restcountries.eu/data/tgo.svg"
            },
            {
                "name": "Tokelau",
                "callingCodes": "+690",
                "alpha3Code": "TKL",
                "flag": "https://restcountries.eu/data/tkl.svg"
            },
            {
                "name": "Tonga",
                "callingCodes": "+676",
                "alpha3Code": "TON",
                "flag": "https://restcountries.eu/data/ton.svg"
            },
            {
                "name": "Trinidad and Tobago",
                "callingCodes": "+1868",
                "alpha3Code": "TTO",
                "flag": "https://restcountries.eu/data/tto.svg"
            },
            {
                "name": "Tunisia",
                "callingCodes": "+216",
                "alpha3Code": "TUN",
                "flag": "https://restcountries.eu/data/tun.svg"
            },
            {
                "name": "Turkey",
                "callingCodes": "+90",
                "alpha3Code": "TUR",
                "flag": "https://restcountries.eu/data/tur.svg"
            },
            {
                "name": "Turkmenistan",
                "callingCodes": "+993",
                "alpha3Code": "TKM",
                "flag": "https://restcountries.eu/data/tkm.svg"
            },
            {
                "name": "Turks and Caicos Islands",
                "callingCodes": "+1649",
                "alpha3Code": "TCA",
                "flag": "https://restcountries.eu/data/tca.svg"
            },
            {
                "name": "Tuvalu",
                "callingCodes": "+688",
                "alpha3Code": "TUV",
                "flag": "https://restcountries.eu/data/tuv.svg"
            },
            {
                "name": "Uganda",
                "callingCodes": "+256",
                "alpha3Code": "UGA",
                "flag": "https://restcountries.eu/data/uga.svg"
            },
            {
                "name": "Ukraine",
                "callingCodes": "+380",
                "alpha3Code": "UKR",
                "flag": "https://restcountries.eu/data/ukr.svg"
            },
            {
                "name": "United Arab Emirates",
                "callingCodes": "+971",
                "alpha3Code": "ARE",
                "flag": "https://restcountries.eu/data/are.svg"
            },
            {
                "name": "United Kingdom of Great Britain and Northern Ireland",
                "callingCodes": "+44",
                "alpha3Code": "GBR",
                "flag": "https://restcountries.eu/data/gbr.svg"
            },
            {
                "name": "United States of America",
                "callingCodes": "+1",
                "alpha3Code": "USA",
                "flag": "https://restcountries.eu/data/usa.svg"
            },
            {
                "name": "Uruguay",
                "callingCodes": "+598",
                "alpha3Code": "URY",
                "flag": "https://restcountries.eu/data/ury.svg"
            },
            {
                "name": "Uzbekistan",
                "callingCodes": "+998",
                "alpha3Code": "UZB",
                "flag": "https://restcountries.eu/data/uzb.svg"
            },
            {
                "name": "Vanuatu",
                "callingCodes": "+678",
                "alpha3Code": "VUT",
                "flag": "https://restcountries.eu/data/vut.svg"
            },
            {
                "name": "Venezuela (Bolivarian Republic of)",
                "callingCodes": "+58",
                "alpha3Code": "VEN",
                "flag": "https://restcountries.eu/data/ven.svg"
            },
            {
                "name": "Viet Nam",
                "callingCodes": "+84",
                "alpha3Code": "VNM",
                "flag": "https://restcountries.eu/data/vnm.svg"
            },
            {
                "name": "Wallis and Futuna",
                "callingCodes": "+681",
                "alpha3Code": "WLF",
                "flag": "https://restcountries.eu/data/wlf.svg"
            },
            {
                "name": "Western Sahara",
                "callingCodes": "+212",
                "alpha3Code": "ESH",
                "flag": "https://restcountries.eu/data/esh.svg"

            },
            {
                "name": "Yemen",
                "callingCodes": "+967",
                "alpha3Code": "YEM",
                "flag": "https://restcountries.eu/data/yem.svg"

            },
            {
                "name": "Zambia",
                "callingCodes": "+260",
                "alpha3Code": "ZMB",
                "flag": "https://restcountries.eu/data/zmb.svg"

            },
            {
                "name": "Zimbabwe",
                "callingCodes": "+263",
                "alpha3Code": "ZWE",
                "flag": "https://restcountries.eu/data/zwe.svg"

            }
        ];

        return countryCodes;
    }

    getDeviceInfo() {
        return this.device;
        /* const data = { uuid: "1223645",manufacturer:'',model:'',platform:'',version:'' };
        return data; */
    }

    getMonth() {
        let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return month;
    }
    isGuest() {
        return localStorage.getItem('isGuest');
    }
    chooseFile() {
        return new Promise((resolve, reject) => {
            const isAndroid = (this.device.platform.toLowerCase() === 'android') ? true : false;
            if (isAndroid) {
                this.fileChooser.open().then((uri: any) => {
                    this.file.resolveLocalFilesystemUrl(uri).then((fileEntry: FileEntry) => {
                        if (fileEntry) {
                            fileEntry.file((success: any) => {
                                if (success.size <= parseInt(this.checkfileSize)) {
                                    const mimeType = success.type.split('/');

                                    if (success.type == 'application/pdf' ||
                                        mimeType[0] == 'image' ||
                                        success.type == 'application/msword' ||
                                        mimeType[1] == 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
                                        if (success.type == 'application/pdf') {
                                            success['mime'] = 'pdf';
                                            success['extention'] = 'pdf';
                                        } else if (mimeType[0] == 'image') {
                                            success['mime'] = 'image';
                                            success['extention'] = mimeType[1];
                                        } else {
                                            success['mime'] = 'word';
                                            success['extention'] = 'doc';
                                        }
                                        const imageName = success.localURL.split('/');
                                        const getName = decodeURIComponent(imageName[imageName.length - 1]);
                                        success['uploadFilePath'] = uri;
                                        success['localURL'] = this.sanitizer.bypassSecurityTrustResourceUrl(uri);
                                        success['getName'] = getName;

                                        resolve(success);
                                    } else {
                                        reject('Upload type .pdf,.jpg,.jpeg,.png,word document');
                                    }

                                } else {
                                    reject('File size max 10 MB');
                                }

                            }, error => {
                                reject('Invalid File Format');
                            });
                        }
                    }, error => {
                        reject(error);
                    })
                }).catch((e: any) => {
                    console.log(e);
                    reject();
                });
            } else {
                this.iosFilePicker.pickFile()
                    .then(uri => {
                        resolve(uri);
                    })
                    .catch(err => {
                        console.log(err);
                        reject();
                    });
            }
        });
    }

    checkOnlyImage(uri) {
        return new Promise((resolve, reject) => {
            this.file.resolveLocalFilesystemUrl(uri).then((fileEntry: FileEntry) => {
                if (fileEntry) {
                    fileEntry.file((success: any) => {
                        if (success.size <= parseInt(this.checkfileSize)) {
                            const mimeType = success.type.split('/');
                            if (mimeType[0] == 'image') {
                                success['mime'] = 'image';
                                success['extention'] = mimeType[1];
                                const imageName = success.localURL.split('/');
                                const getName = decodeURIComponent(imageName[imageName.length - 1]);
                                success['uploadFilePath'] = uri;
                                success['localURL'] = this.sanitizer.bypassSecurityTrustResourceUrl(uri);
                                success['getName'] = getName;
                                resolve(success);
                            } else {
                                reject('Upload type .jpg,.jpeg,.png');
                            }
                        } else {
                            reject('File size max 10 MB');
                        }
                    }, error => {
                        reject('Invalid File Format');
                    });
                }
            }).catch((e: any) => {
                console.log(e);
                reject();
            });
        });
    }

    getCurrency(value) {
        const divisibleBy = 10000000;
        let originalValue: any;
        originalValue = Math.abs(value);
        if (originalValue >= divisibleBy) {
            originalValue = (originalValue / divisibleBy).toFixed(2) + ' CR onwards';
        } else if (originalValue <= divisibleBy) {
            originalValue = ((originalValue / divisibleBy) * 100).toFixed(2) + ' Lakh onwards';
        }
        return originalValue;
    }

    getSearchCurrency(value, type?) {
        const divisibleBy = 10000000;
        let originalValue: any;
        if (value == "" || value == null || value == 0) {
            return originalValue = "0";
        } else {
            originalValue = Math.abs(value);
            if (originalValue >= divisibleBy) {
                if (type == "inventoryBooking") {
                    originalValue = (originalValue / divisibleBy).toFixed(2) + ' CR';
                } else {
                    originalValue = (originalValue / divisibleBy).toFixed(2) + ' CR';
                }
            } else if (originalValue <= divisibleBy) {
                if (type == "inventoryBooking") {
                    originalValue = ((originalValue / divisibleBy) * 100).toFixed(2) + ' Lakhs';
                } else {
                    originalValue = ((originalValue / divisibleBy) * 100).toFixed(2) + ' Lakh';
                }
            }
            return originalValue;
        }
    }

    checkOnlyImageWith2Mb(uri, from) {
        return new Promise((resolve, reject) => {
            this.file.resolveLocalFilesystemUrl(uri).then((fileEntry: FileEntry) => {
                if (fileEntry) {
                    fileEntry.getMetadata((metadata) => {
                        console.log("image size : " + metadata.size);
                        console.log("image date : " + metadata.modificationTime);
                    });
                    fileEntry.file((success: any) => {
                        let filesizeCheck;
                        if (from == 'cam') {
                            filesizeCheck = parseInt(this.checkfileSizeWith2MbCam);
                        } else {
                            filesizeCheck = parseInt(this.checkfileSizeWith2MbFile);
                        }
                        if (success.size <= filesizeCheck) {
                            const mimeType = success.type.split('/');
                            if (mimeType[0] == 'image') {
                                success['mime'] = 'image';
                                success['extention'] = mimeType[1];
                                success['localURL'] = this.sanitizer.bypassSecurityTrustResourceUrl(uri);
                                resolve(success);
                            } else {
                                reject('Upload type .jpg,.jpeg,.png');
                            }
                        } else {
                            reject('File size max 2 MB');
                        }
                    }, error => {
                        reject('Invalid File Format');
                    });
                }
            }).catch((e: any) => {
                console.log(e);
                reject();
            });
        });
    }

    enquiryFileChoose() {
        return new Promise((resolve, reject) => {
            this.fileChooser.open().then((uri: any) => {
                this.file.resolveLocalFilesystemUrl(uri).then((fileEntry: FileEntry) => {
                    if (fileEntry) {
                        fileEntry.file((success: any) => {
                            if (success.size <= parseInt(this.checkfileSizeWith2MbFile)) {
                                const mimeType = success.type.split('/');
                                if (success.type == 'application/pdf') {
                                    success['mime'] = 'pdf';
                                    success['extention'] = 'pdf';
                                } else if (mimeType[0] == 'image') {
                                    success['mime'] = 'image';
                                    success['extention'] = mimeType[1];
                                }
                                if (success.type == 'application/pdf' || mimeType[0] == 'image') {
                                    success['uploadFilePath'] = uri;
                                    success['localURL'] = this.sanitizer.bypassSecurityTrustResourceUrl(uri);
                                    success['name'] = new Date().getTime() + '_' + success.mime + '.' + success.extention
                                    resolve(success);
                                } else {
                                    reject('Upload type .pdf,.jpg,.jpeg,.png');
                                }

                            } else {
                                reject('File size max 2 MB');
                            }
                        }, error => {
                            reject('Invalid File Format');
                        });
                    }
                }, error => {
                    reject(error);
                }).catch((error: any) => {
                    reject(error);
                });
            }).catch((err: any) => {
                reject(err);
            })
        });
    }

    getSearchListIntCurrency(value, type?) {
        const divisibleBy = 10000000;
        let originalValue: any;
        if (value == "" || value == null || value == 0) {
            return originalValue = "0";
        } else {
            originalValue = Math.abs(value);
            if (originalValue >= divisibleBy) {
                if (type == "inventoryBooking") {
                    originalValue = (originalValue / divisibleBy).toFixed(0) + ' CR';
                } else {
                    originalValue = (originalValue / divisibleBy).toFixed(0) + ' CR';
                }
            } else if (originalValue <= divisibleBy) {
                if (type == "inventoryBooking") {
                    originalValue = ((originalValue / divisibleBy) * 100).toFixed(0) + ' Lakhs';
                } else {
                    originalValue = ((originalValue / divisibleBy) * 100).toFixed(0) + ' Lakh';
                }
            }
            return originalValue;
        }
    }

    public getAppBuildType() {
        return AppConst.BUILD_TYPE;
    }

    public getAppVersionNumber() {
        return new Promise((resolve, reject) => {
            this.appVersion.getVersionNumber().then((version) => {
                resolve(version);
            }).catch((error) => {
                reject(0);
            });
        });
    }

    public getIndianCurrencyFormat(price: any) {
        if (typeof price == undefined || price == '' || price == null) {
            return '0.00';
        } else {
            const indianPrice = price.toString();
            if (typeof indianPrice == undefined || indianPrice == '') {
                return '0.00';
            } else {
                try {
                    let preVal = '';
                    let deciVal = '';

                    if (indianPrice.includes('.')) {
                        preVal = indianPrice.split('.')[0];
                        deciVal = indianPrice.split('.')[1].substring(0, 2);
                        if (deciVal.length == 1) {
                            deciVal = deciVal + "0";
                        }
                    } else {
                        preVal = indianPrice;
                        deciVal = "00";
                    }

                    let lastThree = preVal.substring(preVal.length - 3);
                    let otherNumbers = preVal.substring(0, preVal.length - 3);
                    if (otherNumbers != '')
                        lastThree = ',' + lastThree;
                    let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                    if (typeof res == undefined) {
                        return indianPrice;
                    } else {
                        return res + '.' + deciVal
                    }
                } catch (error) {
                    return '0.00';
                }
            }
        }
    }

    getMIMEtype(extn) {
        let ext = extn.toLowerCase();
        let MIMETypes = {
            'txt': 'text/plain',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'pdf': 'application/pdf',
            'jpg': 'image/jpeg',
            'jpeg': 'image/jpeg',
            'bmp': 'image/bmp',
            'png': 'image/png',
            'xls': 'application/vnd.ms-excel',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'rtf': 'application/rtf',
            'ppt': 'application/vnd.ms-powerpoint',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        }
        return MIMETypes[ext];
    }

}
