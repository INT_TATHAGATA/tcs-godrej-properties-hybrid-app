import { Injectable } from '@angular/core';
import { ToastController, Toast } from 'ionic-angular';
/**
 * This provider is responsible for managing loader.
 *
 * @export
 * @class LoaderProvider
 * @author tathagata sur
 */
@Injectable()
export class ToasterProvider {

	toast: Toast = null;

	constructor(private toastCtrl: ToastController) { }
	/**
	 * Show toast message with 3 sec duration
	 *
	 * @param {string} message the toast message
	 * @memberof ToasterProvider
	 */
	show(message: string) {
		/* const toast = this.toastCtrl.create({
			message: message,
			duration: 5000,
			position: "bottom",
			cssClass: 'toast-css-class',
			dismissOnPageChange: true
		});
		toast.present(); */

		let toastData = {
			message: message,
			duration: 5000,
			position: "bottom",
			cssClass: 'toast-css-class',
			dismissOnPageChange: true
		}

		this.toast ? this.toast.dismiss() : false;
		this.toast = this.toastCtrl.create(toastData);
		this.toast.present();

	}

	private showToast(data: any): void {
		this.toast ? this.toast.dismiss() : false;
		this.toast = this.toastCtrl.create(data);
		this.toast.present();
	}

	/**
	 * Toast with message and user defined duration
	 *
	 * @param {string} message the toast message
	 * @param {number} duration the duration in milisecond
	 * @memberof ToasterProvider
	 */
	showWithDuration(message: string, duration: number) {
		const toast = this.toastCtrl.create({
			message: message,
			duration: duration,
			position: "top"
		});
		toast.present();
	}
}
