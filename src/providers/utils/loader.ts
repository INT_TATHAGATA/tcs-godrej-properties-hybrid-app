import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
/**
 * This provider is responsible for managing loader.
 *
 * @export
 * @class LoaderProvider
 * @author tathagata sur
 */
@Injectable()
export class LoadingProvider {
	private loader;

	constructor(private loaderCtrl: LoadingController) {
		this.loader = null;
	}
	/**
	 * This method is responsible for show loader with message.
	 *
	 * @param {string} [message] pass message if you want to show custom message, otherwise it will show 'Please wait...'
	 * @memberof LoadingProvider
	 */
	show(message?: string) {
		/* if (this.loader !== null) {
			this.loader.dismiss();
			this.loader = null;
		} */
		if (this.loader == null) {
			this.loader = this.loaderCtrl.create({
				spinner: 'hide',
				content: `<div class="body_ajax_overlay" style="display: block;"><div class="loader"></div></div>`,
				//duration: 1000 * 60 * 2
			});
			this.loader.present();
		} else {
			this.loader.present();
		}
	}
	/**
	 * This method is responsible to hide loader.
	 *
	 * @memberof LoadingProvider
	 */
	hide() {
		setTimeout(() => {
			if (this.loader !== null) {
				this.loader.dismiss();
				this.loader = null;
			}
		}, 800);
	}

	hideNoTimeout() {
		if (this.loader !== null) {
			this.loader.dismiss();
			this.loader = null;
		}
	}
}
