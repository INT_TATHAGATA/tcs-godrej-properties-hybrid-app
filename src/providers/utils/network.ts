import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';
/**
 * This provider is responsible for checking network state.
 *
 * @export
 * @class NetworkProvider
 * @author Mithun Sen
 */
@Injectable()
export class NetworkProvider {
	connectionObj: any;
	disConnectionObj: any;

	constructor(private network: Network, private platform: Platform) {
		this.platform.ready().then(() => {
			this.isOnline();
		});
	}

	/**
	 * Subscribe to the disconnect event to know when the network is not available
	 */
	public disconnectSub() {
		return new Promise((resolve, reject) => {
			this.disConnectionObj = this.network.onDisconnect().subscribe(() => {
				console.log('No Network Connection Found.');
				resolve('No Network Connection Found.');
			});
		});
	}

	/**
	 * Unsubscribe to the disconnect event to know when the network is not available
	 */
	public disconnectUnsub() {
		this.disConnectionObj.unsubscribe();
	}

	/**
	 * Subscribe to the disconnect event to know when the network is available
	 */
	public connectSub() {
		return new Promise((resolve, reject) => {
			this.connectionObj = this.network.onConnect().subscribe(() => {
				console.log("network connected!");
				resolve("network connected!");
			});
		})
	}

	/**
	 * Unsubscribe to the disconnect event to know when the network is not available
	 */
	public connectUnsub() {
		this.connectionObj.unsubscribe();
	}

	/**
	 * Know the current network state
	 */
	public isOnline() {
		if (navigator && navigator["connection"] && navigator["connection"]["type"] === 'none') {
			return false;
		} else {
			return true;
		}
	}
}
